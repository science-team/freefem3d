//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DoubleHashedMatrix.hpp,v 1.7 2007/06/09 10:37:09 delpinux Exp $


// This class defines a template SparseMatrix class.

#ifndef DOUBLE_HASHED_MATRIX_HPP
#define DOUBLE_HASHED_MATRIX_HPP

#include <BaseMatrix.hpp>
#include <Vector.hpp>

#include <Types.hpp>
#include <ErrorHandler.hpp>

#include <map>
#include <vector>

/*!
  \class DoubleHashedMatrix
  This class defines a temporary matrix 

  \author St�phane Del Pino.
*/

class DoubleHashedMatrix
  : public BaseMatrix
{
  // member classes
private:
  /*!
    \class DoubleEntry

    This class describes two entries indices.

    \author St�phane Del Pino.
   */

  class DoubleEntry
  {
  private:
    size_t __i;
    size_t __j;
  public:

    bool operator<(const DoubleEntry& d) const
    {
      return ((__i < d.__i)
	      ||((__i == d.__i)
		 &&(__j < d.__j)));
    }

    DoubleEntry(const DoubleEntry& d)
      : __i(d.__i),
	__j(d.__j)
    {
      ;
    }

    DoubleEntry(size_t i, size_t j)
      : __i(i),
	__j(j)
    {
      ;
    }

    ~DoubleEntry()
    {
      ;
    }
  };
public:
  /*!
    \class iterator

    This class provides a smart way to iterate on lines or columns of the
    matrix.

    \author St�phane Del Pino.
  */
  class iterator
  {
  private:
    typedef std::map<size_t, real_t*>::iterator Iterator;
    Iterator __i;
  public:
    const iterator& operator=(const iterator::Iterator& i)
    {
      __i=i;
      return (*this);
    }

    inline bool operator != (const iterator::Iterator& i) const
    {
      return (i!=__i);
    }

    const iterator operator++()
    {
      __i++;
      return (*this);
    }

    const iterator& operator++(int)
    {
      ++__i;
      return (*this);
    }

    const iterator& operator--()
    {
      __i++;
      return (*this);
    }

    size_t first()
    {
      return ((*__i).first);
    }

    real_t& second()
    {
      return (*(*__i).second);
    }

    iterator(std::map<size_t, real_t*>::iterator i)
      : __i(i)
    {
      ;
    }
  };


  /*!
    \class const_iterator

    This class provides a smart way to iterate on lines or columns of the
    matrix.

    \author St�phane Del Pino.
  */
  class const_iterator
  {
  private:
    typedef std::map<size_t, real_t*>::const_iterator ConstIterator;
    ConstIterator __i;
  public:
    const const_iterator& operator=(const ConstIterator& i)
    {
      __i=i;
      return (*this);
    }

    inline bool operator != (const ConstIterator& i) const
    {
      return (i!=__i);
    }

    const_iterator operator++()
    {
      __i++;
      return (*this);
    }

    const_iterator operator++(int)
    {
      ++__i;
      return (*this);
    }

    const_iterator& operator--()
    {
      __i++;
      return (*this);
    }

    const size_t& first() const
    {
      return ((*__i).first);
    }

    const real_t& second() const
    {
      return (*(*__i).second);
    }

    const_iterator(std::map<size_t, real_t*>::const_iterator i)
      : __i(i)
    {
      ;
    }
  };

private:
  //! allows to browse lines
  std::vector<std::map<size_t,real_t*> > __lines;

  //! allows to brows columns
  std::vector<std::map<size_t,real_t*> > __columns;

  //! stored the elements
  std::map<DoubleEntry, real_t> __elements;
public:
  size_t numberOfColumns() const
  {
    return __columns.size();
  }

  std::map<size_t,real_t*>::iterator beginOfColumn(const size_t& i)
  {
    ASSERT (i<__columns.size());
    return __columns[i].begin();
  }

  std::map<size_t,real_t*>::iterator endOfColumn(const size_t& i)
  {
    ASSERT (i<__columns.size());
    return __columns[i].end();
  }

  size_t numberOfLines() const
  {
    return __lines.size();
  }

  std::map<size_t,real_t*>::iterator beginOfLine(const size_t& i)
  {
    ASSERT (i<__lines.size());
    return __lines[i].begin();
  }

  std::map<size_t,real_t*>::iterator endOfLine(const size_t& i)
  {
    ASSERT (i<__lines.size());
    return __lines[i].end();
  }

  std::map<size_t,real_t*>::const_iterator beginOfLine(const size_t& i) const
  {
    ASSERT (i<__lines.size());
    return __lines[i].begin();
  }

  std::map<size_t,real_t*>::const_iterator endOfLine(const size_t& i) const
  {
    ASSERT (i<__lines.size());
    return __lines[i].end();
  }

  size_t numberOfLineNonNull(const size_t& i) const
  {
    return __lines[i].size();
  }

  void getDiagonal(BaseVector& X) const
  {
    throw ErrorHandler(__FILE__, __LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  
  void transposedTimesX(const BaseVector& X, BaseVector& Z) const
  {
    throw ErrorHandler(__FILE__, __LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }

  //! Computes z = A*x
  void timesX(const BaseVector& X,
	      BaseVector& Z) const
  {
   const Vector<real_t>& x =dynamic_cast<const Vector<real_t>&>(X);
   Vector<real_t>& z =dynamic_cast<Vector<real_t>&>(Z);
   z=0;
   for (size_t i=0; i<this->numberOfLines(); ++i) {
     for(std::map<size_t,real_t*>::const_iterator j = this->beginOfLine(i);
	 j != this->endOfLine(i); ++j) {
       z[i] += (*(*j).second)*x[(*j).first];
     }
   }
  }
  
  DoubleHashedMatrix(const size_t& i, const size_t& j)
    : BaseMatrix(BaseMatrix::doubleHashedMatrix,i)
  {
    __lines.resize(i);
    __columns.resize(j);
  }

  real_t& operator()(const size_t& i, const size_t& j)
  {
    ASSERT ((i<__lines.size())&&(j<__columns.size()));
    
    // seeks if the element is already stored
    std::map<DoubleEntry, real_t>::iterator elem
      = __elements.find(DoubleEntry(i,j));

    if (elem != __elements.end()) {
      return (*elem).second;
    } else {
      // create lines and columns
      __lines[i][j] = __columns[j][i] = &__elements[DoubleEntry(i,j)];

      return __elements[DoubleEntry(i,j)];
    }
  }

  ~DoubleHashedMatrix()
  {
    ;
  }
};

#endif // _DOUBLE_HASHED_MATRIX_HPP_

