//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DiagPrecond.hpp,v 1.4 2007/05/20 23:02:49 delpinux Exp $


// This is very simple preconditioner: Diagonale of Operator.

#ifndef _DIAGPRECOND_HPP_
#define _DIAGPRECOND_HPP_

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>
#include <Vector.hpp>
#include <Preconditioner.hpp>

/*!
  \class DiagPrecond
  This class implements a diagonal Preconditionner

  \author St�phane Del Pino
*/

class DiagPrecond
  : public Preconditioner
{
private:
  //! Contains the diagonal matrix.
  Vector<real_t> P;

  //! The matrix which contains the diagonale.
  const BaseMatrix& A;
public:

  std::string name() const
  {
    return "diagonal";
  }

  //! Initializes the DiagPrecond.
  void initializes()
  {
    Vector<real_t> diagonal(A.size());
    A.getDiagonal(diagonal);
    for (size_t i=0; i<A.size(); ++i) {
      if (diagonal[i] != 0)
	P[i] = 1./diagonal[i];
      else
      P[i] = 1.;
    }
  }

  //! Computes \f$ z = P^{-1} r \f$.
  void computes(const Vector<real_t>& r ,
		Vector<real_t>& z) const
  {
    ASSERT ((P.size() == r.size())&&(P.size() == z.size()));
    for (size_t i=0; i<P.size(); ++i) {
    z[i] = P[i]*r[i];
    }
  }

  //! Copy constructor.
  DiagPrecond(const DiagPrecond& D)
    : Preconditioner(D),
    P(D.P),
    A(D.A)
  {
    ;
  }

  //! Constructs a DiagPrecond using the pdeSystem
  DiagPrecond(const Problem& Pb,
	      const BaseMatrix& AA)
    : Preconditioner(Pb, diagonale),
    P(AA.size()),
    A(AA)
  {
    ;
  }

  //! Destructor.
  ~DiagPrecond()
  {
    ;
  }
};

#endif // _DIAGPRECOND_HPP_

