//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Array3DShape.hpp,v 1.4 2007/06/09 10:37:09 delpinux Exp $


#ifndef _ARRAY_3D_SHAPE_
#define _ARRAY_3D_SHAPE_

#include <TinyVector.hpp>
#include <StreamCenter.hpp>

/*!
  \class Array3DShape

  This class defines the shape of a 3d structure (mesh, vector, ...)

  \author St�phane Del Pino.
 */

class Array3DShape
{
private:
  //! The shape.
  TinyVector<3,size_t> __shape;

public:  
  friend std::ostream& operator << (std::ostream& os, const Array3DShape& a)
  {
    os << a.__shape;
    return os;
  }

  bool operator==(const Array3DShape& s) const
  {
    return (__shape==s.__shape);
  }

  /*! Returns the global number associated to the (i,j,k)
    representation */
  inline size_t operator()(const size_t& i,
			   const size_t& j,
			   const size_t& k) const
  {
    return (i*__shape[1]*__shape[2] + j*__shape[2] + k);
//     return (i + j*__shape[0] + k*__shape[0]*__shape[1]);
  }

  //! Access to the first component of the shape.
  inline const size_t& nx() const
  {
    return __shape[0];
  }

  //! Access to the second component of the shape.
  inline const size_t& ny() const
  {
    return __shape[1];
  }

  //! Access to the third component of the shape.
  inline const size_t& nz() const
  {
    return __shape[2];
  }

  inline const size_t& operator[](const size_t& i) const
  {
    ASSERT(i<3);
    return __shape[i];
  }

  //! Returns the product of shape components (ie: the number of elements when tensorized).
  inline size_t size() const
  {
    return __shape[0]*__shape[1]*__shape[2];
  }

  /*!  Computes the Array3DShape of an object for which 1 element is removed
    in each direction. Example: 
  */
  inline Array3DShape operator-(size_t i) const
  {
    ASSERT ((i<__shape[0])&&(i<__shape[1])&&(i<__shape[2]));
    return Array3DShape(__shape[0]-i, __shape[1]-i, __shape[2]-i);
  }

  /*!  Computes the Array3DShape of an object for which 1 element is removed
    in each direction. Example: 
  */
  inline Array3DShape& operator=(const Array3DShape& s)
  {
    __shape = s.__shape;
    return *this;
  }

  //! Constructs a Array3DShape.
  Array3DShape(const size_t& i,
	       const size_t& j,
	       const size_t& k)
  {
    __shape[0]=i;
    __shape[1]=j;
    __shape[2]=k;
  }

  //! Copy constructor
  Array3DShape(const Array3DShape& s)
    : __shape(s.__shape)
  {
    ;
  }

  //! Default Constructor.
  Array3DShape()
  {
    ;
  }

  ~Array3DShape()
  {
    ;
  }
};

#endif // _STUCTURED_3D_MESH_SHAPE_HPP_

