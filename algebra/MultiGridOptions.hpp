//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MultiGridOptions.hpp,v 1.3 2007/06/09 10:37:09 delpinux Exp $

#ifndef _MULTIGRID_OPTIONS_HPP_
#define _MULTIGRID_OPTIONS_HPP_

#include <ParametrizableObject.hpp>

class MultiGridOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  static const char* identifier()
  {
    // autodoc: "to change multigrid options (\textbf{Not working anymore!})"
    return "multigrid";
  }

  real_t epsilon()
  {
    real_t eps;
    get("epsilon",eps);
    return eps;
  }

  real_t omega()
  {
    real_t omega;
    get("omega",omega);
    return omega;
  }

  int maxiter()
  {
    int maxiter;
    get("maxiter",maxiter);
    return maxiter;
  }

  int level()
  {
    int level;
    get("level",level);
    return level;
  }

  int mu1()
  {
    int mu1;
    get("mu1",mu1);
    return mu1;
  }

  int mu2()
  {
    int mu2;
    get("mu2",mu2);
    return mu2;
  }

  int nu1()
  {
    int nu1;
    get("nu1",nu1);
    return nu1;
  }

  int nu2()
  {
    int nu2;
    get("nu2",nu2);
    return nu2;
  }

  explicit MultiGridOptions()
  {
    // autodoc: "maximum number of iterations"
    add(new IntegerParameter(1, "maxiter"));
    // autodoc: "grid level"
    add(new IntegerParameter(3, "level"));
    // autodoc: "$\nu_1$"
    add(new IntegerParameter(2, "nu1"));
    // autodoc: "$\nu_2$"
    add(new IntegerParameter(2, "nu2"));
    // autodoc: "$\mu_1$"
    add(new IntegerParameter(1, "mu1"));
    // autodoc: "$\mu_2$"
    add(new IntegerParameter(1, "mu2"));
    // autodoc: "the factor of reduction of the residu"
    add(new DoubleParameter (1E-4,"epsilon"));
    // autodoc: "$\omega$, the relaxation parameter for Jacobi solver"
    add(new DoubleParameter (2./3.,"omega"));
  }

  ~MultiGridOptions()
  {
    ;
  }

};

#endif // _MULTIGRID_OPTIONS_HPP_

