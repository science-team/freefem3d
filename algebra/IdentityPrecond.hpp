//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: IdentityPrecond.hpp,v 1.2 2005/11/26 19:36:09 delpinux Exp $


// This is a fake preconditioner.

#ifndef _IDENTITYPREDOND_HPP_
#define _IDENTITYPREDOND_HPP_

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>
#include <Vector.hpp>
#include <Preconditioner.hpp>

/*!
  \class IdentityPredond
  This class implements a diagonal Preconditionner

  \author St�phane Del Pino
*/
class IdentityPredond
  : public Preconditioner
{
private:

  //! Forbids the copy constructor.
  IdentityPredond(const IdentityPredond& D)
    : Preconditioner(D)
  {
    ;
  }

public:

  std::string name() const
  {
    return "none";
  }

  //! Initializes the IdentityPredond.
  void initializes()
  {
    ;
  }

  //! Computes \f$ z = P^{-1} r \f$ in the special case where P=I :-)
  void computes(const Vector<real_t>& r ,
		Vector<real_t>& z) const
  {
    z=r;
  }

  //! Constructs a IdentityPredond using the PDEProblem \a Pb.
  IdentityPredond(const Problem& Pb)
    : Preconditioner(Pb, none)
  {
    ;
  }

  //! Destructor.
  ~IdentityPredond()
  {
    ;
  }
};


#endif // _IDENTITYPREDOND_HPP_

