// This file is used to provide the low performances interface
// compatible with the blitz library interface to allow compilation
// on low performances computers

#ifndef _NOBLITZ_H_
#define _NOBLITZ_H_
#include <cassert>
#include <vector>

template <int N>
class VarShape {
private:
  std::vector <int> s;
public:
  const int& operator[](int i) const {
    assert ((i<N)&&(i>=0));
    return s[i];
  }

  int& operator[](int i) {
    assert ((i<N)&&(i>=0));
    return s[i];
  }

  VarShape() {
    s.resize(N);
  }

  VarShape(const VarShape& V) {
    s.resize(V.N);
    for (int i=0; i<N; i++)
      s[i] = V.s[i];
  }
};


template <class T, int N>
class Array {
};


template <class T>
class Array<T,3> {
private:
  T*** array;
  T*   the_array;
  VarShape<3> s;
public:

  Array() {
    for (int i=0; i<3; i++)
      shape()[i]=0;
  }

  Array(const Array<T,3>& a) {
    const int ni = a.shape()[0];
    const int nj = a.shape()[1];
    const int nk = a.shape()[2];

    resize(ni,nj,nk);

    for (int i=0; i<ni*nj*nk; i++)
      the_array[i] = a.the_array[i];
  }

  ~Array() {
    if (shape()[0]>0) {
      for (int i=0; i<shape()[0]; i++)
	delete[] array[i];
      delete [] array;
      delete [] the_array;
    }
  }

  inline T& operator()(const int& i, const int& j, const int& k) {
    return array[i][j][k];
  }

  inline const T& operator()(const int& i, const int& j, const int& k) const {
    return array[i][j][k];
  }

  inline T& operator[](const int& i) {
    return the_array[i];
  }

  inline const T& operator[](const int& i) const {
    return the_array[i];
  }

  inline Array<T,3> operator*(const real& d) const {
    Array<T,3> a;
    a.resize(shape()[0],shape()[1],shape()[2]);

    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++)
	  a.the_array[i] = the_array[i]*d;

    return a;
  }

  inline Array<T,3> operator-(const Array<T,3>& a) const {
    assert((shape()[0]==a.shape()[0])
	   &&(shape()[1]==a.shape()[1])
	   &&(shape()[2]==a.shape()[2]));

    Array<T,3> temp;
    temp.resize(shape()[0],shape()[1],shape()[2]);
    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++) {
      temp.the_array[i]
	= the_array[i] - a.the_array[i];
    }
    return temp;
  }

  inline Array<T,3>& operator-=(const Array<T,3>& a) {
    assert((shape()[0]==a.shape()[0])
	   &&(shape()[1]==a.shape()[1])
	   &&(shape()[2]==a.shape()[2]));

    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++){
      the_array[i] -= a.the_array[i];
    }
    return *this;
  }

  inline Array<T,3> operator+(const Array<T,3>& a) const {
    assert((shape()[0]==a.shape()[0])
	   &&(shape()[1]==a.shape()[1])
	   &&(shape()[2]==a.shape()[2]));

    Array<T,3> temp;
    temp.resize(shape()[0],shape()[1],shape()[2]);
    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++) {
      temp.the_array[i] 
	= the_array[i] + a.the_array[i];
    }
    return temp;
  }

  inline Array<T,3>& operator*=(const real& d) {

    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++) {
      the_array[i] *= d;
    }
    return *this;
  }

  inline Array<T,3>& operator+=(const Array<T,3>& a) {
    assert((shape()[0]==a.shape()[0])
	   &&(shape()[1]==a.shape()[1])
	   &&(shape()[2]==a.shape()[2]));

    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++) {
      the_array[i] += a.the_array[i];
    }
    return *this;
  }

  inline VarShape<3>& shape() {
    return s;
  }

  inline void resize(const int& ni, const int& nj, const int& nk) {
    if ((ni != shape()[0])&&(nj != shape()[1])&&(nk != shape()[2])) {

      // clean memory.
      if (shape()[0] > 0) {
	for (int i=0; i<shape()[0]; i++)
	  if (shape()[1] > 0) {
	    delete [] array[i];
	  }
	delete [] array;
	delete [] the_array;
      }

      shape()[0] = ni;
      shape()[1] = nj;
      shape()[2] = nk;
      array = new T**[ni];

      the_array = new T[ni*nj*nk];

      for (int i=0; i<ni; i++)
	array[i] = new T*[nj];
      for (int i=0; i<ni; i++)
	for (int j=0; j<nj; j++)
	  array[i][j]  = &the_array[i*nj*nk + j*nk];
    }
  }

  inline const VarShape<3>& shape() const {
    return s;
  }

  inline Array<T,3>& operator=(const Array<T,3>& a) {

    if (this != &a) { // if Vectors are the same one don't do anythings
      resize(a.shape()[0], a.shape()[1], a.shape()[2]);
      for (int i=0; i<a.shape()[0]*shape()[1]*shape()[2]; i++)
	the_array[i] = a.the_array[i];
    }

    return *this;
  }


  inline Array<T,3>& operator=(const real& d) {
    for (int i=0; i<shape()[0]*shape()[1]*shape()[2]; i++)
      the_array[i] = d;

    return *this;
  }

};

#endif // _NOBLITZ_H_

