//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: TinyVector.hpp,v 1.10 2007/06/09 10:37:08 delpinux Exp $

#ifndef TINY_VECTOR_HPP
#define TINY_VECTOR_HPP

#include <cmath>
#include <typeinfo>
#include <Assert.hpp>

#include <Types.hpp>
#include <StreamCenter.hpp>

/**
 * @file   TinyVector.hpp
 * @author St�phane Del Pino
 * @date   Wed Aug  6 17:36:14 2003
 * 
 * @brief this class defines small vectors of predefined size (ie:
 * known at compilation time)
 * 
 * @todo use more meta compution
 */

template <size_t N, typename T=real_t>
class TinyVector
{
public:
  /// The type of values stored in the TinyVector.
  typedef T ValueType;

  enum {
    Dimension = N		/**< The dimension of the vector */
  };

protected:
  /// stored data
  T __x[N];

public:

  /** 
   *  Read-only access to the dimension of the TinyVector.
   * 
   * @return N
   */  
  size_t size() const
  {
    return N;
  }

  /** 
   * Access to the \a i th coordinate.
   * 
   * @param i the component number
   * 
   * @return ith component
   */
  inline T& operator[](const size_t& i)
  {
    ASSERT (i<N);
    return __x[i];
  }

  /** 
   * Read-only access to the \a i th component.
   * 
   * @param i the component number
   * 
   * @return ith component
   */
  inline const T& operator[](const size_t& i) const
  {
    ASSERT (i<N);
    return __x[i];
  }

  /** 
   * Makes the current vector equal to a given one
   * 
   * @param V the vector to copy
   * 
   * @return the modified vector
   */
  inline const TinyVector<N, T>& operator = (const TinyVector<N, T>& V)
  {
    for (size_t i=0; i<N; i++)
      __x[i] = V.__x[i];
    return (*this);
  }

  /** 
   * Copies the value t to every component of the vector
   * 
   * @param t the value to copy
   * 
   * @return the result vector
   */
  inline const TinyVector<N, T>& operator = (const T& t)
  {
    for (size_t i=0; i<N; i++)
      __x[i] = t;
    return (*this);
  }

  /** 
   * Returns multiplication by a T from the right ...
   * 
   * @param t the value
   * 
   * @return \f$ t U \f$ where \f$ U\f$ is the current vector
   */
  inline TinyVector<N, T> operator*(const T& t) const
  {
    TinyVector<N, T> W;
    for (size_t i=0; i<N; i++)
      W.__x[i] = __x[i] * t;
    return W;
  }

  /** 
   * Computes the scalar product with another vector
   * 
   * @param V the other vector
   * 
   * @return \f$ U\cdot V\f$
   */
  inline real_t operator*(const TinyVector<N,T>& V) const
  {
    ASSERT(typeid(T) == typeid(real_t));
    real_t s=0;
    for (size_t i=0; i<N; i++)
      s += __x[i] * V.__x[i];
    return s;
  }

  /** 
   * Multiplies the TinyVector by a T
   * 
   * @param t the given value
   * 
   * @return \f$ U := t U\f$
   */
  inline const TinyVector<N, T>& operator*=(const T& t)
  {
    for (size_t i=0; i<N; i++)
      __x[i] *= t;
    return (*this);
  }

  /** 
   * Multiplies the TinyVector by the inverse of a T
   * 
   * @param t the given value
   * 
   * @return \f$ U := t^{-1} U\f$
   */
  inline const TinyVector<N, T>& operator/=(const T& t)
  {
    const T temp = 1./t;
    this->operator*=(temp);
    return (*this);
  }

  /** 
   * Computes the difference with an other vector
   * 
   * @param V the other vector
   * 
   * @return \f$ U - V \f$
   */
  inline TinyVector<N, T> operator-(const TinyVector<N, T>& V) const
  {
    TinyVector<N, T> v = 0;
    for (size_t i=0; i<N; i++)
      v[i] = __x[i] - V.__x[i];
    return v;
  }

  /** 
   *  Substracts a vector.
   * 
   * @param V the vector to remove
   * 
   * @return \f$ U := U - V\f$
   */
  inline const TinyVector<N, T>& operator-=(const TinyVector<N, T>& V)
  {
    for (size_t i=0; i<N; i++)
      __x[i] -= V.__x[i];
    return (*this);
  }

  /** 
   * Computes the vectorial product with a vector
   * 
   * @param V the given vector
   * 
   * @return \f$ U \land V \f$
   */
  inline TinyVector<N, T> operator^(const TinyVector<N, T>& V) const
  {
    TinyVector<N, T> W=0;
    for (size_t i=0; i<N; i++)
      W[i] += __x[(i+1)%N] * V.__x[(i+2)%N] - __x[(i+2)%N] * V.__x[(i+1)%N];
    return W;
  }

  /** 
   * Computes the sum with another vector
   * 
   * @param V the other vector
   * 
   * @return \f$ U+V \f$
   */
  inline TinyVector<N, T> operator + (const TinyVector<N, T>& V) const
  {
    TinyVector<N, T> W(*this);
    for (size_t i=0; i<N; i++)
      W.__x[i] += V.__x[i];
    return W;
  }

  /** 
   * Adds a vector
   * 
   * @param V the vector to add
   * 
   * @return \f$ U := U+V\f$
   */
  inline TinyVector<N, T>& operator += (const TinyVector<N, T>& V) {
    for (size_t i=0; i<N; i++)
      __x[i] += V.__x[i];
    return (*this);
  }

  /** 
   * Compares two vectors
   * 
   * @param V the vector to compare with
   * 
   * @return true if vector matches
   *
   * @note Use this function only for special purpose when vectors can
   * really match (not if results of numerical computations).
   */
  inline bool operator==(const TinyVector<N, T>& V) const
  {
    for (size_t i=0; i<N; i++) {
      if (__x[i] != V.__x[i]) {
	return false;
      }
    }
    return true;
  }

  /** 
   * Compares 2 vectors. true if they are not \b exactly the same
   * 
   * @param V the vector to compare with
   * 
   * @return true if vector do not match
   */
  inline bool operator!=(const TinyVector<N, T>& V) const
  {
    for (size_t i=0; i<N; i++) {
      if (__x[i] != V.__x[i]) {
	return true;
      }
    }
    return false;
  }

  /** 
   * Compares two vectors
   * 
   * @param V the vector to compare with
   * 
   * @return true is the vector is smaller than the given one
   * 
   * @note this function uses an arbitrary order
   */
  inline bool operator < (const TinyVector<N,T>& V) const
  {
    for (size_t i=0; i<N; ++i) {
      if(__x[i] != V.__x[i]) {
	return (__x[i] < V.__x[i]);
      }
    }
    return false;
  }

  /** 
   *  Returns the square root of a vector.
   * (ie: it returns the vector whoose componnents are square roots of arg)
   * 
   * @param V the given vector
   * 
   * @return \f$ \left( V_0^{\frac{1}{2}}, \ldots , V_N^{\frac{1}{2}} \right) \f$
   */
  inline friend TinyVector<N, T> sqrt(const TinyVector<N, T>& V)
  {
    using namespace std;
    TinyVector<N, T> W;
    for (size_t i=0; i<N; i++)
      W.__x[i] = sqrt(V.__x[i]);
    return W;
  }

  /** 
   * Returns the Euclidean norm of the vector.
   * 
   * @param V the vector which norm is to compute
   * 
   * @return \f$ ||V||_2 \f$
   */
  inline friend T Norm(const TinyVector<N, T>& V)
  {
    T t = 0;
    for (size_t i=0; i<N; i++) {
      t += V.__x[i]*V.__x[i];
    }

    using namespace std;
    return sqrt(t);
  }

  /** 
   * Returns the multiplication of a TinyVector by a T on the left.
   * 
   * @param a the multiplicative constant
   * @param V the vector to multiply
   * 
   * @return \f$ a V \f$
   */
  inline friend TinyVector<N, T> operator*(const T& a, const TinyVector<N, T>& V)
  {
    return (V*a);
  }

  /** 
   *  Prints out the transposed of a vector in a stream
   * 
   * @param os the given stream
   * @param V  the vector
   * 
   * @return the modified stream
   */
  inline friend std::ostream& operator<<(std::ostream& os,
					 const TinyVector<N, T>& V)
  {
    ASSERT (N>0);
    os << '(';
    for (size_t i=0; i<N-1; i++)
      os << V[i] << ", ";
    os << V[N-1] << ')';
    return os;
  }

  /** 
   * Default constructor.
   * 
   * @note data are not initialized
   */
  TinyVector()
  {
    ;
  }

  /** 
   * Construct the vector setting its coordinates to \a t.
   * 
   * @param t the given value
   */
  TinyVector(const T& t)
  {
    for (size_t i=0; i<N; i++) {
      __x[i] = t;
    }
  }

  /** 
   * Copy constructor.
   * 
   * @param V the original vector
   */
  TinyVector(const TinyVector<N, T>& V)
  {
    for (size_t i=0; i<N; i++) 
      __x[i] = V.__x[i];
  }

  /** 
   * Destructor
   * 
   */
  ~TinyVector()
  {
    ;
  }
};

/**
 * @class  TinyVector<3, T>
 * @author St�phane Del Pino
 * @date   Wed Aug  6 18:52:20 2003
 * 
 * @brief  specialization for three component vectors
 * 
 * 
 */
template <class T>
class TinyVector<3, T>
{
  /// The type of values stored in the TinyVector.
  typedef T ValueType;

  enum {
    Dimension = 3		/**< The dimension of the vector */
  };

protected:
  /// stored data
  T __x[3];

public:

  /** 
   *  Read-only access to the dimension of the TinyVector.
   * 
   * @return 3
   */  
  inline size_t size() const
  {
    return 3;
  }

  /** 
   * Access to the \a i th coordinate.
   * 
   * @param i the component number
   * 
   * @return ith component
   */
  inline T& operator[](const size_t& i)
  {
    ASSERT (i<3);
    return __x[i];
  }

  /** 
   * Read-only access to the \a i th component.
   * 
   * @param i the component number
   * 
   * @return ith component
   */
  inline const T& operator[](const size_t& i) const
  {
    ASSERT (i<3);
    return __x[i];
  }

  /** 
   * Makes the current vector equal to a given one
   * 
   * @param V the vector to copy
   * 
   * @return the modified vector
   */
  inline const TinyVector<3, T>& operator = (const TinyVector<3, T>& V)
  {
    for (size_t i=0; i<3; i++)
      __x[i] = V.__x[i];
    return (*this);
  }

  /** 
   * Copies the value t to every component of the vector
   * 
   * @param t the value to copy
   * 
   * @return the result vector
   */
  inline const TinyVector<3, T>& operator = (const T& t)
  {
    for (size_t i=0; i<3; i++)
      __x[i] = t;
    return (*this);
  }

  /** 
   * Returns multiplication by a T from the right ...
   * 
   * @param t the value
   * 
   * @return \f$ t U \f$ where \f$ U\f$ is the current vector
   */
  inline TinyVector<3, T> operator*(const T& t) const
  {
    TinyVector<3,T> W;
    for (size_t i=0; i<3; i++) {
      W.__x[i] = __x[i] * t;
    }
    return W;
  }

  /** 
   * Computes the scalar product with another vector
   * 
   * @param V the other vector
   * 
   * @return \f$ U\cdot V\f$
   */
  inline real_t operator*(const TinyVector<3,T>& V) const
  {
    ASSERT(typeid(T) == typeid(real_t));
    real_t s=0;
    for (size_t i=0; i<3; i++)
      s += __x[i] * V.__x[i];
    return s;
  }

  /** 
   * Multiplies the TinyVector by a T
   * 
   * @param t the given value
   * 
   * @return \f$ U := t U\f$
   */
  inline TinyVector<3, T>& operator*=(const T& t)
  {
    for (size_t i=0; i<3; i++)
      __x[i] *= t;
    return (*this);
  }

  /** 
   * Multiplies the TinyVector by the inverse of a T
   * 
   * @param t the given value
   * 
   * @return \f$ U := t^{-1} U\f$
   */
  inline TinyVector<3, T>& operator/=(const T& t)
  {
    const T temp = 1./t;
    this->operator*=(temp);
    return (*this);
  }

  /** 
   * Computes the difference with an other vector
   * 
   * @param V the other vector
   * 
   * @return \f$ U - V \f$
   */
  inline TinyVector<3, T> operator-(const TinyVector<3, T>& V) const
  {
    TinyVector<3, T> v = 0;
    for (size_t i=0; i<3; i++)
      v[i] = __x[i] - V.__x[i];
    return v;
  }

  /** 
   * Returns the opposed TinyVector.
   * 
   * 
   * @return \f$ -X \f$
   */
  inline TinyVector<3, T> operator-() const
  {
    return (TinyVector<3, T> (-__x[0],-__x[1],-__x[2]));
  }

  /** 
   *  Substracts a vector.
   * 
   * @param V the vector to remove
   * 
   * @return \f$ U := U - V\f$
   */
  inline const TinyVector<3, T>& operator-=(const TinyVector<3, T>& V)
  {
    for (size_t i=0; i<3; i++)
      __x[i] -= V.__x[i];
    return (*this);
  }

  /** 
   * Computes the vectorial product with a vector
   * 
   * @param V the given vector
   * 
   * @return \f$ U \land V \f$
   */
  inline TinyVector<3, T> operator^(const TinyVector<3, T>& V) const
  {
    TinyVector<3, T> W(0);
    for (size_t i=0; i<3; i++)
      W[i] += __x[(i+1)%3] * V.__x[(i+2)%3] - __x[(i+2)%3] * V.__x[(i+1)%3];
    return W;
  }

  /** 
   * Computes the sum with another vector
   * 
   * @param V the other vector
   * 
   * @return \f$ U+V \f$
   */
  inline TinyVector<3, T> operator + (const TinyVector<3, T>& V) const
  {
    TinyVector<3, T> W(*this);
    for (size_t i=0; i<3; i++)
      W.__x[i] += V.__x[i];
    return W;
  }

  /** 
   * Adds a vector
   * 
   * @param V the vector to add
   * 
   * @return \f$ U := U+V\f$
   */
  inline const TinyVector<3, T>& operator += (const TinyVector<3, T>& V)
  {
    for (size_t i=0; i<3; i++)
      __x[i] += V.__x[i];
    return (*this);
  }

  /** 
   * Compares two vectors
   * 
   * @param V the vector to compare with
   * 
   * @return true if vector matches
   *
   * @note Use this function only for special purpose when vectors can
   * really match (not if results of numerical computations).
   */
  inline bool operator==(const TinyVector<3, T>& V) const
  {
    return ((__x[0]==V.__x[0])&&(__x[1]==V.__x[1])&&(__x[2]==V.__x[2]));
  }

  /** 
   * Compares 2 vectors. true if they are not \b exactly the same
   * 
   * @param V the vector to compare with
   * 
   * @return true if vector do not match
   */
  inline bool operator!=(const TinyVector<3, T>& V) const
  {
    return ((__x[0]!=V.__x[0])||(__x[1]!=V.__x[1])||(__x[2]!=V.__x[2]));
  }

  /** 
   * Compares two vectors
   * 
   * @param V the vector to compare with
   * 
   * @return true is the vector is smaller than the given one
   * 
   * @note this function uses an arbitrary order
   */
  inline bool operator < (const TinyVector<3,T>& V) const
  {
    for (size_t i=0; i<3; ++i) {
      if(__x[i] != V.__x[i]) {
	return (__x[i] < V.__x[i]);
      }
    }
    return false;
  }

  /** 
   *  Returns the square root of a vector.
   * (ie: it returns the vector whoose componnents are square roots of arg)
   * 
   * @param V the given vector
   * 
   * @return \f$ \left( V_0^{\frac{1}{2}}, \ldots , V_N^{\frac{1}{2}} \right) \f$
   */
  inline friend TinyVector<3, T> sqrt(const TinyVector<3, T>& V)
  {
    using namespace std;
    TinyVector<3, T> W;
    for (size_t i=0; i<3; i++)
      W.__x[i] = sqrt(V.__x[i]);
    return W;
  }

  /** 
   * Returns the Euclidean norm of the vector.
   * 
   * @param V the vector which norm is to compute
   * 
   * @return \f$ ||V||_2 \f$
   */
  inline friend real_t Norm(const TinyVector<3, T>& V)
  {
    ASSERT(typeid(T) == typeid(real_t));
    real_t d = 0;
    for (size_t i=0; i<3; i++)
      d += V.__x[i]*V.__x[i];
    using namespace std;
    return sqrt(d);
  }

  /** 
   * Returns the multiplication of a TinyVector by a T on the left.
   * 
   * @param a the multiplicative constant
   * @param V the vector to multiply
   * 
   * @return \f$ a V \f$
   */
  inline friend TinyVector<3, T> operator*(const real_t& a,
					   const TinyVector<3, T>& V)
  {
    return (V*a);
  }

  /** 
   *  Prints out the transposed of a vector in a stream
   * 
   * @param os the given stream
   * @param V  the vector
   * 
   * @return the modified stream
   */
  inline friend std::ostream& operator<<(std::ostream& os,
					 const TinyVector<3, T>& V)
  {
    os << '(';
    for (size_t i=0; i<3-1; i++)
      os << V[i] << ", ";
    os << V[3-1] << ')';
    return os;
  }

  /** 
   * Constructs the vector using three values
   * 
   * @param x1 first component
   * @param x2 second component
   * @param x3 third component
   */
  TinyVector (const T& x1, const T& x2, const T& x3)
  {
    __x[0] = x1;
    __x[1] = x2;
    __x[2] = x3;
  }

  /** 
   * Default constructor.
   * 
   * @note data are not initialized
   */
  TinyVector()
  {
    ;
  }

  /** 
   * Construct the vector setting its coordinates to \a t.
   * 
   * @param t the given value
   */
  TinyVector(const T& t)
  {
    for (size_t i=0; i<3; i++)
      __x[i] = t;
  }

  /** 
   * Copy constructor.
   * 
   * @param V the original vector
   */
  TinyVector(const TinyVector<3, T>& V)
  {
    for (size_t i=0; i<3; i++) 
      __x[i] = V.__x[i];
  }

  /** 
   * Destructor
   * 
   */
  ~TinyVector()
  {
    ;
  }
};

/**
 * @class   TinyVector<1,real_t>
 * @author St�phane Del Pino
 * @date   Wed Aug  6 21:41:47 2003
 * 
 * @brief special vector of size 1 (ie lonely values)
 * 
 * 
 */
template<typename T>
class TinyVector<1, T>
{
  /// The type of values stored in the TinyVector.
  typedef T ValueType;

  enum {
    Dimension =1		/**< The dimension of the vector */
  };
protected:
  //! The value of the 
  T __x;
public:

  /** 
   * Cast operators to use TinyVector<1, T> as r_values. 
   * So, TinyVector<1,T> can be used as an argument for \b all 'T'
   * functions.
   * 
   * @return the value
   */
  inline operator T&()
  {
    return __x;
  }

  /** 
   * Cast operators to use TinyVector<1, T> as r_values. 
   * So, TinyVector<1,T> can be used as an argument for \b all 'T'
   * functions.
   * 
   * @return the value
   */
  inline operator const T&() const
  {
    return __x;
  }

  /** 
   * Compatibility access function
   * 
   * @param i the component
   * 
   * @return the ith value
   */
  inline T& operator[](const size_t& i)
  {
    ASSERT(i==0);
    return __x;
  }

  /** 
   * Compatibility access function
   * 
   * @param i the component
   * 
   * @return the ith value
   */
  inline const T& operator[](const size_t& i) const
  {
    ASSERT(i==0);
    return __x;
  }

  /** 
   *  Read-only access to the dimension of the TinyVector.
   * 
   * @return 1
   */  
  inline size_t size() const
  {
    return 1;
  }

  /** 
   * Constructs a TinyVector using a T
   * 
   * @param y the given value
   */
  TinyVector(const T& y)
    : __x(y)
  {
    ;
  }

  /** 
   * Default constructor.
   * 
   * @note data are not initialized
   */
  TinyVector()
  {
    ;
  }

  //! Copy constructor.
  /** 
   * Copy constructor.
   * 
   * @param V the original vector
   */
  TinyVector(const TinyVector<1, real_t>& V)
    : __x(V.__x)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~TinyVector()
  {
    ;
  }
};

#endif // TINY_VECTOR_HPP

