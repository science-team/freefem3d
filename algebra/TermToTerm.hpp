//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: TermToTerm.hpp,v 1.2 2007/06/09 10:37:08 delpinux Exp $

#ifndef _TERMTOTERM_HPP_
#define _TERMTOTERM_HPP_

template<class T>
class TermToTermProduct
{
private:
  T t;
public:
  inline const T& operator()(const T& t1, const T& t2)
  {
    for (size_t i=0; i<t1.size(); ++i)
      t[i] = t1[i]*t2[i];
    return t;
  }
};

template<>
class TermToTermProduct<real_t>
{
public:
  inline real_t operator()(const real_t& t1, const real_t& t2)
  {
    return t1*t2;
  }
};


template<class T>
class TermToTermDivision
{
private:
  T t;
public:
  inline const T& operator()(const T& t1, const T& t2)
  {
    for (size_t i=0; i<t1.size(); ++i)
      t = t1[i]/t2[i];
    return t;
  }
};

template<>
class TermToTermDivision<real_t>
{
public:
  inline real_t operator()(const real_t& t1, const real_t& t2)
  {
    return t1/t2;
  }
};



template<class T>
class TermToTermLess
{
  bool less;
public:
  inline bool operator()(const T& t1, const T& t2)
  {
    less = true;
    for (size_t i=0; i<t1.size(); ++i)
      less = less && t1[i]<t2[i];
    return less;
  }
};

template<>
class TermToTermLess<real_t>
{
public:
  inline real_t operator()(const real_t& t1, const real_t& t2)
  {
    return (t1<t2);
  }
};


#endif // _TERMTOTERM_HPP_

