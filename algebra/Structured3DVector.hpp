//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Structured3DVector.hpp,v 1.4 2007/06/09 10:37:08 delpinux Exp $


#ifndef _STRUCTURED_3D_VECTOR_HPP_
#define _STRUCTURED_3D_VECTOR_HPP_

#include <Vector.hpp>
#include <Array3DShape.hpp>

/*!
  \class Structured3DVector

  This class defines a template Vector class to store huge quantity of datas
  defined on a 3d structure (ie: defined on a 3d mesh). The stored elements
  type is the template argument.

  \author St�phane Del Pino.
*/

template <typename T>
class Structured3DVector
  : public Vector<T>
{
private:
  //! allows access to the data using the structured organization.
  T*** structuredAccess;

  //! The vector shape.
  const Array3DShape shape_;
public:

  size_t nx() const
  {
    return shape_.nx();
  }

  size_t ny() const
  {
    return shape_.ny();
  }

  size_t nz() const
  {
    return shape_.nz();
  }

  const Array3DShape& shape() const
  {
    return shape_;
  }

  const T& operator()(size_t i, size_t j, size_t k) const
  {
    ASSERT ((i<shape_.nx()) && (j<shape_.ny()) && (k<shape_.nz()));
    return structuredAccess[i][j][k];
  }

  T& operator()(size_t i, size_t j, size_t k)
  {
    ASSERT ((i<shape_.nx()) && (j<shape_.ny()) && (k<shape_.nz()));
    return structuredAccess[i][j][k];
  }

  void operator=(const T t)
  {
    Vector<T>::operator=(t);
  }

  const Structured3DVector<T>& operator=(const Structured3DVector<T>& v)
  {
    ASSERT (shape_==v.shape_);
    this->Vector<T>::operator=(v);
    return *this;
  }

  Structured3DVector(const Array3DShape& smShape)
    : Vector<T>(smShape.size()),
      shape_(smShape)
  {
    structuredAccess = new T**[shape_.nx()];
    for (size_t i=0; i < shape_.nx(); ++i) {
      structuredAccess[i] = new T* [shape_.ny()];
      for (size_t j=0; j < shape_.ny(); ++j) {
	structuredAccess[i][j]
	  = &Vector<T>::__values[i*shape_.ny()*shape_.nz() + j*shape_.nz()];
      }
    }
  }

  Structured3DVector(const Structured3DVector& v)
    : Vector<T>(v),
      shape_(v.shape_)
  {
    structuredAccess = new T**[shape_.nx()];
    for (size_t i=0; i < shape_.nx(); ++i) {
      structuredAccess[i] = new T* [shape_.ny()];
      for (size_t j=0; j < shape_.ny(); ++j) {
	structuredAccess[i][j]
	  = &Vector<T>::__values[i*shape_.ny()*shape_.nz() + j*shape_.nz()];
      }
    }
  }

  ~Structured3DVector()
  {
    // deletes the 3d structure informations.
    for (size_t i=0; i<shape_.nx(); ++i)
      delete[] structuredAccess[i];
    delete [] structuredAccess;
  }
};

#endif // _STRUCTURED_3D_VECTOR_HPP_

