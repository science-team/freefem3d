//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: UnknownExpressionDeclaration.hpp,v 1.2 2007/05/20 23:38:14 delpinux Exp $

#ifndef UNKNOWN_EXPRESSION_DECLARATION_HPP
#define UNKNOWN_EXPRESSION_DECLARATION_HPP

#include <UnknownExpression.hpp>

/**
 * @file   UnknownExpressionDeclaration.hpp
 * @author St�phane Del Pino
 * @date   Sat Feb 10 18:01:02 2007
 * 
 * @brief  Unknown expression declaration
 */
class UnknownExpressionDeclaration
  : public UnknownExpression
{
private:
  /** 
   * Pre-execute the expression
   * 
   */
  void __preExecute();

public:
  /** 
   * Constructor
   * 
   * @param name name of the unknown 
   * @param type discretization type
   */
  UnknownExpressionDeclaration(const std::string& name,
			       const DiscretizationType::Type& type
			       = DiscretizationType::undefined);

  /** 
   * Destructor
   * 
   */
  ~UnknownExpressionDeclaration();
};

#endif // UNKNOWN_EXPRESSION_DECLARATION_HPP
