//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryExpression.hpp,v 1.7 2006/08/27 21:33:53 delpinux Exp $

#ifndef BOUNDARY_EXPRESSION_HPP
#define BOUNDARY_EXPRESSION_HPP

#include <Expression.hpp>

#include <Vector3Expression.hpp>
#include <Variable.hpp>

#include <list>

class Boundary;

/**
 * @file   BoundaryExpression.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 16:10:49 2006
 * 
 * @brief This class defines the base class of Boundary expressions
 */
class BoundaryExpression
  : public Expression
{
public:
  enum BoundaryType {
    undefined,
    surfaceMesh,
    povray,
    references,
    list,
    variable
  };

private:
  BoundaryExpression::BoundaryType
  __boundaryType;		/**< type of the boundary */

protected:
  ReferenceCounting<Boundary>
  __boundary;			/**< Reference to the boundary */

public:
  /** 
   * Checks if the boundary has POVRay boundary
   * 
   * @return true if the boundary has POVRay boundary
   */
  virtual bool hasPOVBoundary() const = 0;

  /** 
   * Read only access to the type of the boundary
   * 
   * @return __boundaryType
   */
  const BoundaryExpression::BoundaryType& boundaryType() const
  {
    return __boundaryType;
  }

  /** 
   * Access to the boundary
   * 
   * @return __boundary
   */
  ReferenceCounting<Boundary> boundary();

  /** 
   * Copy constructor
   * 
   * @param e given boundary expression
   */
  BoundaryExpression(const BoundaryExpression& e);

  /** 
   * Constructor
   * 
   * @param t the type of the boundary
   */
  BoundaryExpression(const BoundaryExpression::BoundaryType& t);

  /** 
   * Destructor
   * 
   */
  virtual ~BoundaryExpression();
};

#endif // BOUNDARY_EXPRESSION_HPP
