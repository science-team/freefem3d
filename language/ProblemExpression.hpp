//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ProblemExpression.hpp,v 1.2 2006/03/12 20:17:23 delpinux Exp $

#ifndef PROBLEM_EXPRESSION_HPP
#define PROBLEM_EXPRESSION_HPP

#include <Expression.hpp>

class ProblemExpression
  : public Expression
{
public:
  enum ProblemType {
    pdeSystem,
    variationalProblem
  };

private:
  ProblemType __problemType;	/**< problem's type */

public:

  /** 
   * Checks if the ProblemExpression contains POVRay references. If
   * it does the execution is stopped. This verification is required
   * by standard FEM.
   */
  virtual bool hasPOVBoundary() const = 0;

  /** 
   * 
   * Access to the problem type
   * 
   * @return __problemType
   */
  ProblemExpression::ProblemType problemType()
  {
    return __problemType;
  }

  /** 
   * Constructor
   * 
   * @param t type of the problem
   * 
   */
  ProblemExpression(ProblemExpression::ProblemType t)
    : Expression(Expression::problem),
      __problemType(t)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param P another problem
   *
   */
  ProblemExpression(const ProblemExpression& P)
    : Expression(P),
      __problemType(P.__problemType)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~ProblemExpression()
  {
    ;
  }
};

#endif // PROBLEM_EXPRESSION_HPP

