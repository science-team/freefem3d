//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: IntegratedOperatorExpression.hpp,v 1.7 2007/06/10 14:59:57 delpinux Exp $

#ifndef INTEGRATED_OPERATOR_EXPRESSION_HPP
#define INTEGRATED_OPERATOR_EXPRESSION_HPP

#include <Expression.hpp>
#include <IntegratedExpression.hpp>

class IntegratedOperatorExpression
  : public Expression
{
public:
  enum OperatorType {
    undefined,
    orderZero,
    gradient,
    dx,
    dy,
    dz
  };

private:
  const OperatorType __operatorType;

protected:
  ReferenceCounting<IntegratedExpression> __integratedExpression;

private:
  bool __isMean;
  bool __isJump;

protected:
  std::string __vo;
  std::string __vf;


public:
  const bool& isJump() const
  {
    return __isJump;
  }

  void setJump()
  {
    __isJump=true;
    __vo = "[";
    __vf = "]";
  }

  const bool& isMean() const
  {
    return __isMean;
  }

  void setMean()
  {
    __isMean=true;
    __vo = "{";
    __vf = "}";
  }

  const OperatorType& operatorType() const
  {
    return __operatorType;
  }

  ConstReferenceCounting<IntegratedExpression> integratedExpression() const
  {
    return __integratedExpression;
  }

  IntegratedOperatorExpression(const IntegratedOperatorExpression::
			       OperatorType t,
			       ReferenceCounting<IntegratedExpression> i)
    : Expression(Expression::integratedOperator),
      __operatorType(t),
      __integratedExpression(i),
      __isMean(false),
      __isJump(false),
      __vo(""),
      __vf("")
  {
    ;
  }

  IntegratedOperatorExpression(const IntegratedOperatorExpression& I)
    : Expression(I),
      __operatorType(I.__operatorType),
      __integratedExpression(I.__integratedExpression),
      __isMean(I.__isMean),
      __isJump(I.__isJump),
      __vo(I.__vo),
      __vf(I.__vf)
  {
    ;
  }

  virtual ~IntegratedOperatorExpression()
  {
    ;
  }
};


class IntegratedOperatorExpressionOrderZero
  : public IntegratedOperatorExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << __vo << *__integratedExpression << __vf;
    return os;
  }
public:

  void execute()
  {
    ;
  }

  IntegratedOperatorExpressionOrderZero(ReferenceCounting<IntegratedExpression> I)
    : IntegratedOperatorExpression(IntegratedOperatorExpression::orderZero,
				   I)
  {
    ;
  }

  IntegratedOperatorExpressionOrderZero(const IntegratedOperatorExpressionOrderZero& I)
    : IntegratedOperatorExpression(I)
  {
    ;
  }

  ~IntegratedOperatorExpressionOrderZero()
  {
    ;
  }
};


class IntegratedOperatorExpressionGrad
  : public IntegratedOperatorExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << __vo << "grad(" << *__integratedExpression << ')' << __vf;
    return os;
  }
public:

  void execute()
  {
    ;
  }

  IntegratedOperatorExpressionGrad(ReferenceCounting<IntegratedExpression> I)
    : IntegratedOperatorExpression(IntegratedOperatorExpression::gradient, I)
  {
    ;
  }

  IntegratedOperatorExpressionGrad(const IntegratedOperatorExpressionGrad& I)
    : IntegratedOperatorExpression(I)
  {
    ;
  }

  ~IntegratedOperatorExpressionGrad()
  {
    ;
  }
};

class IntegratedOperatorExpressionDx
  : public IntegratedOperatorExpression
{
private:
  std::string __operatorName;

  std::ostream& put(std::ostream& os) const
  {
    os << __vo << __operatorName << '(' << *__integratedExpression << ')' << __vf;
    return os;
  }

  static IntegratedOperatorExpression::OperatorType
  getOperatorType(const std::string& operatorName)
  {
    if (operatorName == "dx") {
      return IntegratedOperatorExpression::dx;
    }
    if (operatorName == "dy") {
      return IntegratedOperatorExpression::dy;
    }
    if (operatorName == "dz") {
      return IntegratedOperatorExpression::dz;
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown operator",
		       ErrorHandler::unexpected);
    return IntegratedOperatorExpression::undefined;
  }
public:

  void execute()
  {
    ;
  }

  IntegratedOperatorExpressionDx(const std::string& operatorName,
				 ReferenceCounting<IntegratedExpression> I)
    : IntegratedOperatorExpression(IntegratedOperatorExpressionDx::getOperatorType(operatorName), I),
      __operatorName(operatorName)
  {
    ;
  }

  IntegratedOperatorExpressionDx(const IntegratedOperatorExpressionDx& I)
    : IntegratedOperatorExpression(I),
      __operatorName(I.__operatorName)
  {
    ;
  }

  ~IntegratedOperatorExpressionDx()
  {
    ;
  }
};

#endif // INTEGRATED_OPERATOR_EXPRESSION_HPP

