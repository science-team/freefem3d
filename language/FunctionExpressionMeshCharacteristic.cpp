//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionMeshCharacteristic.cpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#include <FunctionExpressionMeshCharacteristic.hpp>
#include <ScalarFunctionMeshCharacteristic.hpp>
#include <Mesh.hpp>

std::ostream&
FunctionExpressionMeshCharacteristic::
put(std::ostream& os) const
{
  os << "one(" << *__meshExpression <<')';
  return os;
}

void
FunctionExpressionMeshCharacteristic::
execute()
{
  __meshExpression->execute();
  __scalarFunction=new ScalarFunctionMeshCharacteristic(__meshExpression->mesh());
}

FunctionExpressionMeshCharacteristic::
FunctionExpressionMeshCharacteristic(ReferenceCounting<MeshExpression> meshExpression)
  : FunctionExpression(FunctionExpression::meshCharacteristic),
    __meshExpression(meshExpression)
{
  ;
}

FunctionExpressionMeshCharacteristic::
FunctionExpressionMeshCharacteristic(const FunctionExpressionMeshCharacteristic& f)
  : FunctionExpression(f),
    __meshExpression(f.__meshExpression)
{
  ;
}


FunctionExpressionMeshCharacteristic::
~FunctionExpressionMeshCharacteristic()
{
  ;
}

