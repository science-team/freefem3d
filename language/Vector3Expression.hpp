//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Vector3Expression.hpp,v 1.8 2007/06/09 10:37:08 delpinux Exp $

#ifndef VECTOR3_EXPRESSION_HPP
#define VECTOR3_EXPRESSION_HPP

#include <Expression.hpp>
#include <RealExpression.hpp>

#include <EmbededFunctions.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <cmath>

class Vector3Expression
  : public Expression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << '(' << this->value(0)
       << ',' << this->value(1)
       << ',' << this->value(2) << ')';
    return os;
  }

public:
  /**
   * Returns the value of the expression.
   * \todo Return type should use traits.
   */
  virtual real_t value(const size_t& i) const = 0;

  /**
   * Returns the value of the expression.
   * \todo Return type should use traits.
   */
  virtual ReferenceCounting<RealExpression>
  component(const size_t& i) const = 0;

  Vector3Expression(const Vector3Expression& e)
    : Expression(e)
  {
    ;
  }

  Vector3Expression()
    : Expression(Expression::vector3)
  {
    ;
  }

  virtual ~Vector3Expression()
  {
    ;
  }
};


class Vector3ExpressionValue
  : public Vector3Expression
{
private:
  ReferenceCounting<RealExpression> __v0;
  ReferenceCounting<RealExpression> __v1;
  ReferenceCounting<RealExpression> __v2;

public:
  real_t value(const size_t& i) const
  {
    ASSERT (i<3);
    switch (i) {
    case 0:
      return (*__v0).realValue();
    case 1:
      return (*__v1).realValue();
    case 2:
      return (*__v2).realValue();
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "trying to access a component not in set {0,1,2}",
			 ErrorHandler::unexpected);
    }
    }
    return 0;
  }

  ReferenceCounting<RealExpression> component(const size_t& i) const
  {
    ASSERT (i<3);
    switch (i) {
    case 0:
      return __v0;
    case 1:
      return __v1;
    case 2:
      return __v2;
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "trying to access a component not in set {0,1,2}",
			 ErrorHandler::unexpected);
    }
    }
    return 0;
  }

  void execute()
  {
    (*__v0).execute();
    (*__v1).execute();
    (*__v2).execute();
  }

  Vector3ExpressionValue(ReferenceCounting<RealExpression> v0,
			 ReferenceCounting<RealExpression> v1,
			 ReferenceCounting<RealExpression> v2)
    : __v0(v0),
      __v1(v1),
      __v2(v2)
  {
    ;
  }

  Vector3ExpressionValue(const Vector3ExpressionValue& re)
    : __v0(re.__v0),
      __v1(re.__v1),
      __v2(re.__v2)
  {
    ;
  }

  ~Vector3ExpressionValue()
  {
    ;
  }
};

class Vector3ExpressionTimesScalar
  : public Vector3Expression
{
private:
  ReferenceCounting<Vector3Expression> __vector;
  ReferenceCounting<RealExpression> __scalar;

public:
  real_t value(const size_t& i) const
  {
    ASSERT (i<3);
    return ((*__vector).value(i) * (*__scalar).realValue());
  }

  ReferenceCounting<RealExpression> component(const size_t& i) const
  {
    ASSERT (i<3);
    return new RealExpressionBinaryOperator<product>((*__vector).component(i),__scalar);
  }

  void execute()
  {
    (*__vector).execute();
    (*__scalar).execute();
  }

  Vector3ExpressionTimesScalar(ReferenceCounting<Vector3Expression> v,
			       ReferenceCounting<RealExpression> s)
    : __vector(v),
      __scalar(s)
  {
    ;
  }

  Vector3ExpressionTimesScalar(const Vector3ExpressionTimesScalar& re)
    : __vector(re.__vector),
      __scalar(re.__scalar)
  {
    ;
  }

  ~Vector3ExpressionTimesScalar()
  {
    ;
  }
};

class Vector3Variable;
class Vector3ExpressionVariable
  : public Vector3Expression
{
private:
  const std::string __vector3Name;
  ReferenceCounting<Vector3Variable> __vector3Variable;
  real_t __value[3];

public:
  real_t value(const size_t& i) const;

  ReferenceCounting<RealExpression> component(const size_t& i) const;

  void execute();

  Vector3ExpressionVariable(const std::string& vector3Name);

  Vector3ExpressionVariable(const Vector3ExpressionVariable& e);

  ~Vector3ExpressionVariable();
};

#include <cmath>

template <real_t(*B)(const real_t x, const real_t y)>
class Vector3ExpressionBinaryOperator
  : public Vector3Expression
{
private:
  ReferenceCounting<Vector3Expression> __r1;
  ReferenceCounting<Vector3Expression> __r2;

public:
  real_t value(const size_t& i) const
  {
    return B((*__r1).value(i), (*__r2).value(i));
  }

  ReferenceCounting<RealExpression> component(const size_t& i) const
  {
    return new RealExpressionBinaryOperator<B>((*__r1).component(i), (*__r2).component(i));
  }

  void execute()
  {
    (*__r1).execute();
    (*__r2).execute();
  }

  Vector3ExpressionBinaryOperator(ReferenceCounting<Vector3Expression> r1,
				  ReferenceCounting<Vector3Expression> r2)
    : __r1(r1),
      __r2(r2)
  {
    ;
  }

  Vector3ExpressionBinaryOperator(const Vector3ExpressionBinaryOperator<B>& e)
    : __r1(e.__r1),
      __r2(e.__r2)
  {
    ;
  }

  ~Vector3ExpressionBinaryOperator()
  {
    ;
  }
};

#endif // VECTOR3_EXPRESSION_HPP
