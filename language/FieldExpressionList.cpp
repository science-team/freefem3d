//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FieldExpressionList.cpp,v 1.2 2007/05/20 23:02:48 delpinux Exp $

#include <FieldExpressionList.hpp>

std::ostream& FieldExpressionList::
put(std::ostream& os) const
{
  ASSERT(__list.size()>0);
  if (__list.size()>1) {
    os << '{';
  }

  os << *__list[0];
  for (size_t i=1; i<__list.size(); ++i) {
    os << ',' << *__list[i];
  }

  if (__list.size()>1) {
    os << '}';
  }

  return os;
}

ReferenceCounting<FieldExpression> FieldExpressionList::
field(const size_t& i)
{
  ASSERT(i<__list.size());
  return __list[i];
}

ConstReferenceCounting<FieldExpression> FieldExpressionList::
field(const size_t& i) const
{
  ASSERT(i<__list.size());
  return __list[i];
}

void FieldExpressionList::
execute()
{
  for (size_t i=0; i<__list.size(); ++i) {
    __list[i]->execute();
  }
}

size_t FieldExpressionList::
numberOfFields() const
{
  return __list.size();
}

void FieldExpressionList::
add(ReferenceCounting<FieldExpression> field)
{
  __list.push_back(field);
}

FieldExpressionList::
FieldExpressionList()
  : Expression(Expression::fieldlist)
{
  ;
}

FieldExpressionList::
~FieldExpressionList()
{
  ;
}
