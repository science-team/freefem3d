//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionListExpression.cpp,v 1.5 2007/02/08 23:19:38 delpinux Exp $

#include <BoundaryConditionListExpression.hpp>
#include <BoundaryConditionSet.hpp>

#include <BoundaryList.hpp>

BoundaryConditionListExpression::
BoundaryConditionListExpression(const BoundaryConditionListExpression& e)
  : Expression(Expression::boundaryConditionList),
    __boundaryConditionListType(e.__boundaryConditionListType),
    __boundaryConditionSet(e.__boundaryConditionSet)
{
  ;
}

BoundaryConditionListExpression::
BoundaryConditionListExpression(const BoundaryConditionListExpression::
				BoundaryConditionListType& t)
  : Expression(Expression::boundaryConditionList),
    __boundaryConditionListType(t)
{
  ;
}

BoundaryConditionListExpression::
~BoundaryConditionListExpression()
{
  ;
}

ReferenceCounting<BoundaryConditionSet>
BoundaryConditionListExpression::boundaryConditionSet()
{
  return __boundaryConditionSet;
}

void
BoundaryConditionListExpressionSet::
__splitBoundaryList(BoundaryConditionSet& bcSet,
		    ConstReferenceCounting<PDECondition> pde,
		    ConstReferenceCounting<Boundary> b)
{
  const BoundaryList& boundaryList = dynamic_cast<const BoundaryList&>(*b);

  for (BoundaryList::List::const_iterator i = boundaryList.list().begin();
       i != boundaryList.list().end(); ++i) {
    ConstReferenceCounting<Boundary> boundary = *i;
    if (boundary->type() == Boundary::list) {
      this->__splitBoundaryList(bcSet, pde, boundary);
    } else {
      bcSet.addBoundaryCondition(new BoundaryCondition(pde, boundary));
    }
  }
}

bool BoundaryConditionListExpressionSet::hasPOVBoundary() const
{
  for (BoundaryConditionExpressionList::const_iterator
	 i = __boundaryConditionExpressionList.begin();
       i != __boundaryConditionExpressionList.end(); ++i) {
    if ((*(*i)).hasPOVBoundary()) {
      return true;
    }
  }
  return false;
}


void BoundaryConditionListExpressionSet::execute()
{
  __boundaryConditionSet = new BoundaryConditionSet();
  BoundaryConditionSet& bcSet = (*__boundaryConditionSet);

  for (BoundaryConditionExpressionList::iterator
	 i = __boundaryConditionExpressionList.begin();
       i != __boundaryConditionExpressionList.end(); ++i) {
    (*i)->execute();
    ConstReferenceCounting<BoundaryCondition> b = (*i)->boundaryCondition();
    if (b->boundary()->type() == Boundary::list) {
      ConstReferenceCounting<Boundary> bc = b->boundary();
      ConstReferenceCounting<PDECondition> pde = b->condition();

      this->__splitBoundaryList(bcSet, pde, bc);
    } else {
      bcSet.addBoundaryCondition((*i)->boundaryCondition());
    }
  }
}

BoundaryConditionListExpressionSet::
BoundaryConditionListExpressionSet(const BoundaryConditionListExpressionSet& e)
  : BoundaryConditionListExpression(e),
    __boundaryConditionExpressionList(e.__boundaryConditionExpressionList)
{
  ;
}

BoundaryConditionListExpressionSet::BoundaryConditionListExpressionSet()
  : BoundaryConditionListExpression(BoundaryConditionListExpression::set)
{
  ;
}

BoundaryConditionListExpressionSet::~BoundaryConditionListExpressionSet()
{
  ;
}

