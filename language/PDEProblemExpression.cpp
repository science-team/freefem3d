//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003, 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDEProblemExpression.cpp,v 1.5 2007/02/10 17:20:53 delpinux Exp $

#include <ProblemExpression.hpp>

#include <PDEProblemExpression.hpp>
#include <PDEProblem.hpp>

ReferenceCounting<PDEProblem> PDEProblemExpression::pdeProblem()
{
  return __pdeProblem;
}

PDEProblemExpression::PDEProblemExpression(const PDEProblemExpression& e)
  : Expression(e),
    __pdeProblemType(e.__pdeProblemType),
    __pdeProblem(e.__pdeProblem)
{
  ;
}

PDEProblemExpression::
PDEProblemExpression(const PDEProblemExpression::PDEProblemType& t)
  : Expression(Expression::pdeProblem),
    __pdeProblemType(t)
{
  ;
}

PDEProblemExpression::~PDEProblemExpression()
{
  ;
}


bool PDEProblemExpressionDescription::
hasPOVBoundary() const
{
  return (*__bcList).hasPOVBoundary();
}

ReferenceCounting<PDEEquationExpression>
PDEProblemExpressionDescription::pdeEquation()
{
  return __pdeEquation;
}

ReferenceCounting<BoundaryConditionListExpression>
PDEProblemExpressionDescription::bcList()
{
  return __bcList;
}

std::ostream& PDEProblemExpressionDescription::put(std::ostream& os) const
{
  os << "\n\t\tpde(" << __unknownName << "):\n";
  os << (*__pdeEquation) << '\n';
  os << (*__bcList) << '\n';

  return os;
}

void PDEProblemExpressionDescription::execute()
{
  (*__pdeEquation).execute();
  (*__bcList).execute();

  __pdeProblem = new PDEProblem((*__pdeEquation).pde(),
				(*__bcList).boundaryConditionSet());
}

PDEProblemExpressionDescription::
PDEProblemExpressionDescription(const std::string& unknownName,
				ReferenceCounting<PDEEquationExpression> pdeEquation,
				ReferenceCounting<BoundaryConditionListExpression> bcList)
  : PDEProblemExpression(PDEProblemExpression::description),
    __unknownName(unknownName),
    __pdeEquation(pdeEquation),
    __bcList(bcList)
{
  ;
}

PDEProblemExpressionDescription::
PDEProblemExpressionDescription(const PDEProblemExpressionDescription& e)
  : PDEProblemExpression(e),
    __unknownName(e.__unknownName),
    __pdeEquation(e.__pdeEquation),
    __bcList(e.__bcList)
{
  ;
}

PDEProblemExpressionDescription::
~PDEProblemExpressionDescription()
{
  ;
}

