//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionBinaryOperation.hpp,v 1.2 2006/07/30 22:23:10 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_BINARY_OPERATION_HPP
#define FUNCTION_EXPRESSION_BINARY_OPERATION_HPP

#include <FunctionExpression.hpp>
#include <BinaryOperation.hpp>

/**
 * @file   FunctionExpressionBinaryOperation.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:15:48 2006
 * 
 * @brief FunctionExpression described as binary operators
 * 
 */
class FunctionExpressionBinaryOperation
  : public FunctionExpression
{
private:
  BinaryOperation __type;	/**< type of binary operators */

  ReferenceCounting<FunctionExpression>
  __fExpression;		/**< first operand */

  ReferenceCounting<FunctionExpression>
 __gExpression;			/**< second operand */
public:
  /** 
   * Overload of the execution of Expression
   * 
   */
  void execute();

  /** 
   * Returns true if the function has a boundary
   * 
   * @return true if __fExpression or __gExpression has a boundary
   */
  bool hasBoundaryExpression() const;

  /** 
   * Constructor
   * 
   * @param operatorName the name of the operator 
   * @param f the first operand
   * @param g the second operand
   */
  FunctionExpressionBinaryOperation(const std::string& operatorName,
				    ReferenceCounting<FunctionExpression> f,
				    ReferenceCounting<FunctionExpression> g);

  /** 
   * Constructor
   * 
   * @param type the binary operation type 
   * @param f the first operand
   * @param g the second operand
   */
  FunctionExpressionBinaryOperation(const BinaryOperation::Type& type,
				    ReferenceCounting<FunctionExpression> f,
				    ReferenceCounting<FunctionExpression> g);

  /** 
   * Copy constructor
   * 
   * @param f the original binary operator function expression
   */
  FunctionExpressionBinaryOperation(const FunctionExpressionBinaryOperation& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionBinaryOperation();
};

#endif // FUNCTION_EXPRESSION_BINARY_OPERATION_HPP
