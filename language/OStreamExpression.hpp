//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OStreamExpression.hpp,v 1.3 2007/02/08 23:19:38 delpinux Exp $

#ifndef OSTREAM_EXPRESSION_HPP
#define OSTREAM_EXPRESSION_HPP

#include <Expression.hpp>
#include <OStream.hpp>

class OFStreamVariable;

/**
 * @file   OStreamExpression.hpp
 * @author Stephane Del Pino
 * @date   Sat Dec 30 01:01:35 2006
 * 
 * @brief general ostream expression management class
 */
class OStreamExpression
  : public Expression
{
private:
  const std::string
  __ofstreamName;		/**< name of the ofstream variable */

  OStream* __ostream;		/**< ff3d's standard stream */

  OFStreamVariable*
  __ofstreamVariable;		/**< ofstream variable */

  /** 
   * Overload of the put function
   * 
   * @param os givem stream
   * 
   * @return os
   */
  std::ostream & put(std::ostream & os) const;

public:
  /** 
   * execute the expression
   * 
   */
  void execute();

  /** 
   * Writes an expression to the stream
   * 
   * @param e given expression
   */
  void operator<<(Expression& e);

  /** 
   * Constructor for a standard ff3d's stream
   * 
   * @param os given standard ff3d's stream
   */
  OStreamExpression(OStream* os);

  /** 
   * Constructor that uses an ofstream
   * 
   * @param ofstreamName given ofstream
   */
  OStreamExpression(const std::string& ofstreamName);

  /** 
   * Copy constructor
   * 
   * @param os given OStreamExpression
   */
  OStreamExpression(const OStreamExpression& os);

  /** 
   * Destructor
   * 
   */
  ~OStreamExpression();
};

#endif // OSTREAM_EXPRESSION_HPP
