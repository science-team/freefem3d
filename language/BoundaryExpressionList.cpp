//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryExpressionList.cpp,v 1.1 2006/08/27 21:33:53 delpinux Exp $

#include <BoundaryExpressionList.hpp>

#include <BoundaryList.hpp>

void
BoundaryExpressionList::
execute()
{
  BoundaryList* b = new BoundaryList();

  for(List::iterator i = __boundaries.begin();
      i != __boundaries.end(); ++i) {
    (**i).execute();
    b->add((**i).boundary());
  }
  __boundary = b;
}

std::ostream&
BoundaryExpressionList::
put(std::ostream& os) const
{
  for (List::const_iterator i = __boundaries.begin();
       i != __boundaries.end(); ++i) {
    if (i != __boundaries.begin()) {
      os << ',';
    }
    os << (**i);
  }
  return os;
}

BoundaryExpressionList::
BoundaryExpressionList()
  : BoundaryExpression(BoundaryExpression::list)
{
  ;
}

BoundaryExpressionList::
BoundaryExpressionList(const BoundaryExpressionList& l)
  : BoundaryExpression(l),
    __boundaries(l.__boundaries)
{
  ;
}

BoundaryExpressionList::
~BoundaryExpressionList()
{
  ;
}
