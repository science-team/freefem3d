//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DomainExpressionSet.hpp,v 1.1 2006/09/28 19:05:34 delpinux Exp $

#ifndef DOMAIN_EXPRESSION_SET_HPP
#define DOMAIN_EXPRESSION_SET_HPP

#include <DomainExpression.hpp>

class SceneExpression;
class InsideListExpression;

class DomainExpressionSet
  : public DomainExpression
{
private:
  ReferenceCounting<SceneExpression> __scene;

  ReferenceCounting<InsideListExpression> __definition;

  std::ostream& put(std::ostream& os) const;

public:
  ReferenceCounting<SceneExpression> scene();

  void execute();

  DomainExpressionSet(ReferenceCounting<SceneExpression> s,
		      ReferenceCounting<InsideListExpression> def);

  DomainExpressionSet(const DomainExpressionSet& m);

  ~DomainExpressionSet();
};

#endif // DOMAIN_EXPRESSION_SET_HPP
