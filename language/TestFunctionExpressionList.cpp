//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: TestFunctionExpressionList.cpp,v 1.1 2007/02/10 17:20:53 delpinux Exp $

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <TestFunctionExpressionList.hpp>

std::ostream& TestFunctionExpressionList::
put(std::ostream& os) const
{
  os << "tests Functions: ";
  for(ListType::const_iterator i = __list.begin();
      i != __list.end(); ++i) {
    os << (*i)->name() << ' ';
  }
  return os;
}

size_t TestFunctionExpressionList::
size() const
{
  return __list.size();
}

size_t TestFunctionExpressionList::
number(const std::string& name) const
{
  size_t n = 0;
  for(ListType::const_iterator i = __list.begin(); i != __list.end(); ++i) {
    if ((*i)->name() == name) {
      return n;
    }
    ++n;
  }

  const std::string errorMsg
    = name+" is not a variable of the test function list";

  throw ErrorHandler(__FILE__,__LINE__,
		     errorMsg,
		     ErrorHandler::normal);

  return 0;
}


void TestFunctionExpressionList::
add(ReferenceCounting<TestFunctionVariable> t)
{
  __list.push_back(t);
}

void TestFunctionExpressionList::
execute()
{
  ;
}

TestFunctionExpressionList::
TestFunctionExpressionList()
  : Expression(Expression::testFunctionList)
{
  ;
}

TestFunctionExpressionList::
~TestFunctionExpressionList()
{
  ;
}
