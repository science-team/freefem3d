//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: StringExpression.cpp,v 1.2 2004/11/28 18:15:47 delpinux Exp $

#include <StringExpression.hpp>
#include <RealExpression.hpp>

#include <Stringify.hpp>

void StringExpressionReal::execute()
{
  (*__realExpression).execute();
  __stringExpressionValue = stringify((*__realExpression).realValue());
}

StringExpressionReal::StringExpressionReal(ReferenceCounting<RealExpression> r)
  : __realExpression(r)
{
  ;
}

StringExpressionReal::StringExpressionReal(const StringExpressionReal& re)
  : __stringExpressionValue(re.__stringExpressionValue),
    __realExpression(re.__realExpression)
{
  ;
}

StringExpressionReal::~StringExpressionReal()
{
  ;
}

