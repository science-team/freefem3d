//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDEEquationExpression.hpp,v 1.2 2003/05/04 18:09:01 delpinux Exp $

#ifndef _PDEEQUATION_EXPRESSION_HPP_
#define _PDEEQUATION_EXPRESSION_HPP_

#include <Expression.hpp>

#include <PDEOperatorSumExpression.hpp>
#include <FunctionExpression.hpp>
class PDE;
class PDEEquationExpression
  : public Expression
{
private:
  ReferenceCounting<PDEOperatorSumExpression> __pdeOperatorSum;

  ReferenceCounting<FunctionExpression> __secondMember;

protected:
  ReferenceCounting<PDE> __pde;

private:
  std::ostream& put(std::ostream& os) const
  {
    os << "\t\t" << (*__pdeOperatorSum) << " = " << (*__secondMember);
    return os;
  }

public:
  ReferenceCounting<PDE> pde();

  void execute();

  PDEEquationExpression(const PDEEquationExpression& e);

  PDEEquationExpression(ReferenceCounting<PDEOperatorSumExpression> __pdeOpSum,
			ReferenceCounting<FunctionExpression> __secondMember);

  ~PDEEquationExpression();
};

#endif // _PDEEQUATION_EXPRESSION_HPP_

