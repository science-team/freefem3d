//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryExpressionReferences.cpp,v 1.1 2006/08/27 21:33:53 delpinux Exp $

#include <BoundaryExpressionReferences.hpp>
#include <BoundaryReferences.hpp>

std::ostream&
BoundaryExpressionReferences::
put(std::ostream& os) const
{
  for (BoundaryExpressionReferences::ReferencesSet::const_iterator i
	 = __references.begin();
       i != __references.end(); ++i) {
    if (i != __references.begin()) {
      os  << ',';
    }
    os << (**i);
  }
  return os;
}

void
BoundaryExpressionReferences::
execute()
{
  for (BoundaryExpressionReferences::ReferencesSet::iterator i
	 = __references.begin();
       i != __references.end(); ++i)
    {
      ReferenceCounting<RealExpression> ref = *i;
      (*ref).execute();
    }

  BoundaryReferences* b= new BoundaryReferences();

  for (BoundaryExpressionReferences::ReferencesSet::iterator i
	 = __references.begin();
       i != __references.end(); ++i) {
    ReferenceCounting<RealExpression> ref = *i;
    const double value = (*ref).realValue();
    if ((value<0) or (value != int(value))) {
      throw ErrorHandler(__FILE__,__LINE__,
			 stringify(value)
			 +" is not a correct boundary reference\n"
			 +"Boundary references must be positive integers",
			 ErrorHandler::normal);
    }
    b->add(static_cast<size_t>(value));
  }
  __boundary = b;
}


BoundaryExpressionReferences::
BoundaryExpressionReferences()
  : BoundaryExpression(BoundaryExpression::references)
{
  ;
}

BoundaryExpressionReferences::
BoundaryExpressionReferences(const std::string& boundaryName)
  : BoundaryExpression(BoundaryExpression::references)
{
  if (boundaryName=="xmin") {
    __references.insert(new RealExpressionValue(0));
    return;
  }
  if (boundaryName=="xmax") {
    __references.insert(new RealExpressionValue(1));
    return;
  }
  if (boundaryName=="ymin") {
    __references.insert(new RealExpressionValue(2));
    return;
  }
  if (boundaryName=="ymax") {
    __references.insert(new RealExpressionValue(3));
    return;
  }
  if (boundaryName=="zmin") {
    __references.insert(new RealExpressionValue(4));
    return;
  }
  if (boundaryName=="zmax") {
    __references.insert(new RealExpressionValue(5));
    return;
  }
  throw ErrorHandler(__FILE__,__LINE__,
		     "unknown boundary name: '"+boundaryName+"'",
		     ErrorHandler::unexpected);
}

BoundaryExpressionReferences::
BoundaryExpressionReferences(const BoundaryExpressionReferences& r)
  : BoundaryExpression(r),
    __references(r.__references)
{
  ;
}

BoundaryExpressionReferences::
~BoundaryExpressionReferences()
{
  ;
}
