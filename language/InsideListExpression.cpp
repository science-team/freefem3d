//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: InsideListExpression.cpp,v 1.7 2007/02/08 23:19:38 delpinux Exp $

#include <InsideListExpression.hpp>

#include <Scene.hpp>
#include <Object.hpp>
#include <Union.hpp>
#include <Intersection.hpp>
#include <Not.hpp>

ReferenceCounting<Object> InsideListExpressionAnd::objects(const Scene& S)
{
  Intersection* intersection = new Intersection();

  (*intersection).push_back((*__node1).objects(S));
  (*intersection).push_back((*__node2).objects(S));

  return new Object(intersection);
}

ReferenceCounting<Object> InsideListExpressionOr::objects(const Scene& S)
{
  Union* merge = new Union();

  (*merge).push_back((*__node1).objects(S));
  (*merge).push_back((*__node2).objects(S));

  return new Object(merge);
}


ReferenceCounting<Object> InsideListExpressionNot::objects(const Scene& S)
{
  return new Object(new Not((*__node).objects(S)));
}

ReferenceCounting<Object> InsideListExpressionLeaf::objects(const Scene& S)
{
  Union* merge = new Union();

  TinyVector<3> reference = (*__leaf).reference();

  size_t numberOfLeafObjects = 0;
  for (size_t i = 0; i<S.nbObjects(); ++i) {
    if(S.object(i)->hasReference()) {
      if (S.object(i)->reference() == reference) {
	merge->push_back(S.object(i));
	numberOfLeafObjects++;
      }
    } else {
      ffout(0) << "Object " << i << ':' << S.object(i) << "\nhas no reference\n\n";
    }
  }

  if (numberOfLeafObjects==0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "no object has reference "+stringify(reference)+" in scene",
		       ErrorHandler::normal);
  }

  ConstReferenceCounting<Shape> leaves;
  if(numberOfLeafObjects == 1) { // leaf is just one object
    leaves = (*(*merge).begin())->shape();
  } else { // leaves are an union
    leaves = merge;
  }

  Object* o;
  if ((*__leaf).inside()) {
    o = new Object(leaves);
  } else {
    o = new Object(new Not(new Object(leaves)));
  }

  o->setReference(reference);
  return o;
}

