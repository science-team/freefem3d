//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionObjectCharacteristic.hpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_OBJECT_CHARACTERISTIC_HPP
#define FUNCTION_EXPRESSION_OBJECT_CHARACTERISTIC_HPP

#include <FunctionExpression.hpp>

class Vector3Expression;
class Scene;

/**
 * @file   FunctionExpressionObjectCharacteristic.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 00:49:31 2006
 * 
 * @brief Manipulates the expression of the characteristic function of
 * a given object
 * 
 */
class FunctionExpressionObjectCharacteristic
  : public FunctionExpression
{
private:
  ReferenceCounting<Vector3Expression> __reference; /**< The reference of the object */
  ReferenceCounting<Scene> __scene; /**< The scene that contain the object */

public:
  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param ref the reference of the object
   * 
   */
  FunctionExpressionObjectCharacteristic(ReferenceCounting<Vector3Expression> ref);

  /** 
   * Copy constructor
   * 
   * @param f given function expression
   */
  FunctionExpressionObjectCharacteristic(const FunctionExpressionObjectCharacteristic& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionObjectCharacteristic();
};

#endif // FUNCTION_EXPRESSION_OBJECT_CHARACTERISTIC_HPP
