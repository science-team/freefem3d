//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionExpression.hpp,v 1.9 2007/02/08 23:19:38 delpinux Exp $

#ifndef BOUNDARY_CONDITION_EXPRESSION_HPP
#define BOUNDARY_CONDITION_EXPRESSION_HPP

#include <Expression.hpp>
#include <Variable.hpp>

#include <BoundaryExpression.hpp>

/**
 * @file   BoundaryConditionExpression.hpp
 * @author Stephane Del Pino
 * @date   Wed Jan  5 20:09:56 2005
 * 
 * @brief This class defines the base class of BoundaryCondition
 * expressions
 */
class BoundaryCondition;
class BoundaryConditionExpression
  : public Expression
{
protected:
  ConstReferenceCounting<BoundaryCondition> __boundaryCondition;

  ReferenceCounting<BoundaryExpression> __boundary;

  const std::string __unknownName;

public:
  enum BoundaryConditionType {
    dirichlet,
    neumann,
    fourrier // also known as Robin
  };

  const std::string& unknownName() const
  {
    return __unknownName;
  }

private:
  BoundaryConditionExpression::BoundaryConditionType __boundaryConditionType;

public:
  bool hasPOVBoundary() const
  {
    return __boundary->hasPOVBoundary();
  }

  ConstReferenceCounting<BoundaryCondition> boundaryCondition() const;

  const BoundaryConditionExpression::BoundaryConditionType& boundaryConditionType() const
  {
    return __boundaryConditionType;
  }

  BoundaryConditionExpression(const BoundaryConditionExpression& e);

  BoundaryConditionExpression(ReferenceCounting<BoundaryExpression> boundary,
			      const std::string& unknownName,
			      const BoundaryConditionExpression::BoundaryConditionType& t);

  virtual ~BoundaryConditionExpression();
};

#endif // BOUNDARY_CONDITION_EXPRESSION_HPP
