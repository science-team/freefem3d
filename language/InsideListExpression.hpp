//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: InsideListExpression.hpp,v 1.6 2004/12/31 14:00:47 delpinux Exp $

#ifndef INSIDE_LIST_EXPRESSION_HPP
#define INSIDE_LIST_EXPRESSION_HPP

#include <Expression.hpp>
#include <InsideExpression.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

class Object;
class Scene;

/**
 * @file   InsideListExpression.hpp
 * @author Stephane Del Pino & Cecile Dobrzynski
 * @date   Fri Nov 15 15:21:58 2002
 * 
 * @brief  nodes and leaves of the domain tree
 * 
 */

class InsideListExpression
  : public Expression
{
protected:

  typedef std::map<TinyVector<3, real_t>, size_t> ReferenceList;

public:
  enum NodeType {
    NodeAnd,
    NodeOr,
    NodeNot,
    NodeLeaf
  };

private:
  const NodeType __type;	/**< node type */

public:

  /** 
   * overloading of Expression::execute()
   * 
   */
  void execute()
  {
    this->subExecute();
    this->checkReferencesUniqueness();
  }

  /** 
   * helps to break the execution into parts
   * 
   */
  virtual void subExecute() = 0;

  /** 
   * Checks that all given references are given only once
   * 
   */
  void checkReferencesUniqueness()
  {
    InsideListExpression::ReferenceList references;
    this->addReferences(references);
    for (ReferenceList::const_iterator i = references.begin();
	 i != references.end(); ++i) {
      if ((*i).second > 1) {
	std::string errorMsg
	  = "POV-Ray reference "+stringify((*i).first)+" is used "
		 +stringify((*i).second)+" times in the domain definition\n"
	    "only once is allowed";
	throw ErrorHandler(__FILE__,__LINE__,
			   errorMsg,
			   ErrorHandler::normal);
      }
    }
  }

  /** 
   * Fills a list which counts POV-Ray references
   * 
   * @param references the references and their number of instance
   * 
   */
  virtual void
  addReferences(InsideListExpression::ReferenceList& references) = 0;

  /** 
   * Evaluates the domain shape
   * 
   * @return a shape.
   */  
  virtual ReferenceCounting<Object> objects(const Scene& S) = 0;

  /** 
   * node constructor
   * 
   * @param type
   * 
   */
  InsideListExpression(const InsideListExpression::NodeType type)
    : Expression(Expression::insideListExpression),
      __type(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param ie 
   * 
   */
  InsideListExpression(const InsideListExpression& ie)
    : Expression(ie),
      __type(ie.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   * 
   */
  virtual ~InsideListExpression()
  {
    ;
  }
};


class InsideListExpressionAnd
  : public InsideListExpression
{
private:

  ReferenceCounting<InsideListExpression> __node1;

  ReferenceCounting<InsideListExpression> __node2;

  /** 
   * Overloading of Expression::put()
   * 
   * @param os 
   * 
   */
  std::ostream& put(std::ostream& os) const
  {
    os << '(' << *__node1 << " and " << *__node2 << ')';
    return os;
  }

public:

  ReferenceCounting<Object> objects(const Scene& S);

  /** 
   * Overloading of InsideListeExpression::subExecute()
   * 
   */
  void subExecute()
  {
    (*__node1).subExecute();
    (*__node2).subExecute();
  }

  /** 
   * Fills a list which counts POV-Ray references
   * 
   * @param references the references and their number of instance
   * 
   */
  void
  addReferences(InsideListExpression::ReferenceList& references)
  {
    (*__node1).addReferences(references);
    (*__node2).addReferences(references);
  }

  /** 
   * Constructor
   * 
   * @param node1 
   * @param node2 
   * 
   */
  InsideListExpressionAnd(ReferenceCounting<InsideListExpression> node1,
			  ReferenceCounting<InsideListExpression> node2)
    : InsideListExpression(InsideListExpression::NodeAnd),
      __node1(node1),
      __node2(node2)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param I 
   * 
   */
  InsideListExpressionAnd(const InsideListExpressionAnd& I)
    : InsideListExpression(I),
      __node1(I.__node1),
      __node2(I.__node2)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~InsideListExpressionAnd()
  {
    ;
  }
};


class InsideListExpressionOr
  : public InsideListExpression
{
private:

  ReferenceCounting<InsideListExpression> __node1;

  ReferenceCounting<InsideListExpression> __node2;

  /** 
   * Overloading of Expression::put()
   * 
   * @param os 
   * 
   */
  std::ostream& put(std::ostream& os) const
  {
    os << '(' << *__node1 << " or " << *__node2 << ')';
    return os;
  }

public:
  ReferenceCounting<Object> objects(const Scene& S);

  /** 
   * Overloading of InsideListeExpression::subExecute()
   * 
   */
  void subExecute()
  {
    (*__node1).subExecute();
    (*__node2).subExecute();
  }

  /** 
   * Fills a list which counts POV-Ray references
   * 
   * @param references the references and their number of instance
   * 
   */
  void
  addReferences(InsideListExpression::ReferenceList& references)
  {
    (*__node1).addReferences(references);
    (*__node2).addReferences(references);
  }

  /** 
   * Constructor
   * 
   * @param node1 
   * @param node2 
   * 
   */
  InsideListExpressionOr(ReferenceCounting<InsideListExpression> node1,
			  ReferenceCounting<InsideListExpression> node2)
    : InsideListExpression(InsideListExpression::NodeOr),
      __node1(node1),
      __node2(node2)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param I 
   * 
   */
  InsideListExpressionOr(const InsideListExpressionOr& I)
    : InsideListExpression(I),
      __node1(I.__node1),
      __node2(I.__node2)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~InsideListExpressionOr()
  {
    ;
  }
};



class InsideListExpressionNot
  : public InsideListExpression
{
private:

  ReferenceCounting<InsideListExpression> __node;

  /** 
   * Overloading of Expression::put()
   * 
   * @param os 
   * 
   */
  std::ostream& put(std::ostream& os) const
  {
    os << '!' << *__node;
    return os;
  }

public:

  ReferenceCounting<Object> objects(const Scene& S);

  /** 
   * Overloading of InsideListeExpression::subExecute()
   * 
   */
  void subExecute()
  {
    (*__node).subExecute();
  }

  /** 
   * Fills a list which counts POV-Ray references
   * 
   * @param references the references and their number of instance
   * 
   */
  void
  addReferences(InsideListExpression::ReferenceList& references)
  {
    (*__node).addReferences(references);
  }

  /** 
   * Constructor
   * 
   * @param node
   * 
   */
  InsideListExpressionNot(ReferenceCounting<InsideListExpression> node)
    : InsideListExpression(InsideListExpression::NodeNot),
      __node(node)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param I 
   * 
   */
  InsideListExpressionNot(const InsideListExpressionNot& I)
    : InsideListExpression(I),
      __node(I.__node)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~InsideListExpressionNot()
  {
    ;
  }
};


class InsideListExpressionLeaf
  : public InsideListExpression
{
private:

  ReferenceCounting<InsideExpression> __leaf;

  /** 
   * Overloading of Expression::put()
   * 
   * @param os 
   * 
   */
  std::ostream& put(std::ostream& os) const
  {
    os << *__leaf;
    return os;
  }

public:
  ReferenceCounting<Object> objects(const Scene& S);

  /** 
   * Overloading of InsideListeExpression::subExecute()
   * 
   */
  void subExecute()
  {
    (*__leaf).execute();
  }

  /** 
   * Fills a list which counts POV-Ray references
   * 
   * @param references the references and their number of instance
   * 
   */
  void
  addReferences(InsideListExpression::ReferenceList& references)
  {
    references[(*__leaf).reference()]++;
  }

  /** 
   * Constructor
   * 
   * @param leaf
   * 
   */
  InsideListExpressionLeaf(ReferenceCounting<InsideExpression> leaf)
    : InsideListExpression(InsideListExpression::NodeLeaf),
      __leaf(leaf)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param I 
   * 
   */
  InsideListExpressionLeaf(const InsideListExpressionLeaf& I)
    : InsideListExpression(I),
      __leaf(I.__leaf)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~InsideListExpressionLeaf()
  {
    ;
  }
};

#endif // INSIDE_LIST_EXPRESSION_HPP
