//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OFStreamExpressionUndefined.hpp,v 1.1 2006/12/30 00:17:05 delpinux Exp $

#ifndef OFSTREAM_EXPRESSION_UNDEFINED_HPP
#define OFSTREAM_EXPRESSION_UNDEFINED_HPP

#include <OFStreamExpression.hpp>

/**
 * @file   OFStreamExpressionUndefined.hpp
 * @author Stephane Del Pino
 * @date   Sat Dec 30 01:08:19 2006
 * 
 * @brief  Undefined ofstream
 */
class OFStreamExpressionUndefined
  : public OFStreamExpression
{
public:
  /** 
   * Execution requires no action
   * 
   */
  void execute()
  {
    ;
  }

  /** 
   * Constructor
   * 
   */
  OFStreamExpressionUndefined()
    : OFStreamExpression(OFStreamExpression::undefined)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param ofs given undefined ofstream expression
   */
  OFStreamExpressionUndefined(const OFStreamExpressionUndefined& ofs)
    : OFStreamExpression(ofs)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~OFStreamExpressionUndefined()
  {
    ;
  }
};

#endif // OFSTREAM_EXPRESSION_UNDEFINED_HPP
