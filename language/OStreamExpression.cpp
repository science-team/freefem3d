//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OStreamExpression.cpp,v 1.6 2007/05/20 23:02:48 delpinux Exp $

#include <OStreamExpression.hpp>

#include <OFStreamExpression.hpp>
#include <Variable.hpp>

#include <VariableRepository.hpp>

std::ostream&
OStreamExpression::
put(std::ostream & os) const
{
  return os;
}

void
OStreamExpression::
execute()
{
  if (__ostream == 0) {
    __ofstreamVariable
      = VariableRepository::instance().findVariable<OFStreamVariable>(__ofstreamName);
  }
}

void
OStreamExpression::
operator<<(Expression& E)
{
  if (__ostream != 0) {
    (*__ostream) << E;
  } else {
    ASSERT(__ofstreamVariable->expression()->ofstream() != 0);
    (*__ofstreamVariable->expression()->ofstream()) << E << std::flush;
  }
}

OStreamExpression::
OStreamExpression(OStream* os)
  : Expression(Expression::ostreamexpression),
    __ofstreamName("none"),
    __ostream(os),
    __ofstreamVariable(0)
{
  ;
}

OStreamExpression::
OStreamExpression(const std::string& ofstreamName)
  : Expression(Expression::ostreamexpression),
    __ofstreamName(ofstreamName),
    __ostream(0),
    __ofstreamVariable(0)
{
  ;
}

OStreamExpression::
OStreamExpression(const OStreamExpression& os)
  : Expression(*this),
    __ofstreamName(os.__ofstreamName),
    __ostream(os.__ostream),
    __ofstreamVariable(os.__ofstreamVariable)
{
  ;
}

OStreamExpression::
~OStreamExpression()
{
  ;
}

