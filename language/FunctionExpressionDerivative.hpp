//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionDerivative.hpp,v 1.2 2006/07/23 22:14:19 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_DERIVATIVE_HPP
#define FUNCTION_EXPRESSION_DERIVATIVE_HPP

#include <FunctionExpression.hpp>
#include <ErrorHandler.hpp>

/**
 * @file   FunctionExpressionDerivative.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:43:55 2006
 * 
 * @brief Computes the derivative of a function expression
 * 
 */
class FunctionExpressionDerivative
  : public FunctionExpression
{
public:
  enum Direction {
    x,
    y,
    z,
    undefined
  };

private:
  FunctionExpressionDerivative::Direction __direction; /**< direction of derivation */

  ReferenceCounting<FunctionExpression> __derivedFunction; /**< function to derive */

  /** 
   * returns @b true if the function to derive has a boundary
   * 
   * @return true if the function to derive has a boundary
   */
  bool hasBoundaryExpression() const
  {
    return __derivedFunction->hasBoundaryExpression();
  }

  /** 
   * converts the string direction name to a the type Direction
   * 
   * @param name name of the direction
   * 
   * @return associated enumerate value
   */
  static FunctionExpressionDerivative::Direction
  getDirection(const std::string& name)
  {
    if (name == "dx") return x;
    if (name == "dy") return y;
    if (name == "dz") return z;
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown derivation direction",
		       ErrorHandler::unexpected);
    return undefined;
  }

public:
  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param directionName derivation direction
   * @param function function to derive
   */
  FunctionExpressionDerivative(const std::string& directionName,
			       ReferenceCounting<FunctionExpression> function);

  /** 
   * Copy constructor
   * 
   * @param f the derivative function expression
   */
  FunctionExpressionDerivative(const FunctionExpressionDerivative& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionDerivative();
};

#endif // FUNCTION_EXPRESSION_DERIVATIVE_HPP
