//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionMeshReferences.cpp,v 1.2 2007/05/20 23:22:26 delpinux Exp $

#include <FunctionExpressionMeshReferences.hpp>
#include <MeshExpression.hpp>

#include <FEMFunctionBuilder.hpp>
#include <Mesh.hpp>

#include <ScalarFunctionMeshElementsReferences.hpp>

FunctionExpressionMeshReferences::ItemType
FunctionExpressionMeshReferences::
__getItemType()
{
  if (__itemName == "elements") {
    return FunctionExpressionMeshReferences::element;
  }
  if (__itemName == "vertices") {
    return FunctionExpressionMeshReferences::vertex;
  }
  return FunctionExpressionMeshReferences::undefined;
}


std::ostream&
FunctionExpressionMeshReferences::
put(std::ostream& os) const
{
  os << "reference(" << __itemName << ',' << *__mesh << ',' << *__referenceSet << ')';
  return os;
}

void FunctionExpressionMeshReferences::
execute()
{
  __referenceSet->execute();
  __mesh->execute();

  ScalarFunctionMeshElementsReferences::FunctionMap functionMap;

  for (ReferencesSet::FunctionReferences::const_iterator
	 i = __referenceSet->functionReferences().begin();
       i != __referenceSet->functionReferences().end(); ++i) {
    size_t referenceNumber = size_t(i->first->realValue());
    if (functionMap.find(referenceNumber) != functionMap.end()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "reference "+
			 stringify(referenceNumber)+
			 " multiply defined in '"+
			 stringify(*this)+
			 "'",
			 ErrorHandler::normal);
    }
    functionMap[referenceNumber] = i->second->function();
  }

  switch (this->__getItemType()) {
  case FunctionExpressionMeshReferences::element: {
    __scalarFunction = new ScalarFunctionMeshElementsReferences(functionMap,__mesh->mesh());
    break;
  }
  case FunctionExpressionMeshReferences::vertex: {
    const Mesh* m = __mesh->mesh();

    Vector<real_t> values(m->numberOfVertices());
    for (size_t i=0; i<values.size(); ++i) {
      const Vertex& x = m->vertex(i);
      ScalarFunctionMeshElementsReferences::FunctionMap::const_iterator
	f = functionMap.find(x.reference());
      if (f != functionMap.end()) {
	values[i] = (*f->second)(x);
      } else { // reference was not found in list
	values[i] = 0;
      }
    }

    FEMFunctionBuilder builder;
    DiscretizationType d(DiscretizationType::lagrangianFEM1);
    builder.build(d,m,values);
    __scalarFunction = builder.getBuiltScalarFunction();
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown item type",
		       ErrorHandler::unexpected);
  }
  }
}


FunctionExpressionMeshReferences::
FunctionExpressionMeshReferences(const std::string itemName,
				 ReferenceCounting<MeshExpression> mesh,
				 ReferenceCounting<ReferencesSet> ref)
  : FunctionExpression(FunctionExpression::meshReferences),
    __itemName(itemName),
    __mesh(mesh),
    __referenceSet(ref)
{
  ;
}

FunctionExpressionMeshReferences::
FunctionExpressionMeshReferences(const FunctionExpressionMeshReferences& f)
  : FunctionExpression(f),
    __itemName(f.__itemName),
    __mesh(f.__mesh),
    __referenceSet(f.__referenceSet)
{
  ;
}

FunctionExpressionMeshReferences::
~FunctionExpressionMeshReferences()
{
  ;
}
