//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: UnknownExpressionFunction.hpp,v 1.2 2007/05/20 23:37:29 delpinux Exp $

#ifndef UNKNOWN_EXPRESSION_FUNCTION_HPP
#define UNKNOWN_EXPRESSION_FUNCTION_HPP

#include <UnknownExpression.hpp>
#include <string>

class UnknownExpressionFunction
  : public UnknownExpression
{
private:
  void __preExecute();
  static DiscretizationType::Type __getFunctionType(const std::string& functionName);
public:
  UnknownExpressionFunction(const std::string& name,
			    const DiscretizationType::Type& type = DiscretizationType::functionLike);

  ~UnknownExpressionFunction();
};

#endif // UNKNOWN_EXPRESSION_FUNCTION_HPP
