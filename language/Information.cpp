//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Information.cpp,v 1.6 2007/05/20 23:02:48 delpinux Exp $

#include <Information.hpp>

#include <Mesh.hpp>
#include <UnknownListExpression.hpp>
#include <Scene.hpp>

ConstReferenceCounting<Mesh> Information::
getMesh()
{
  ASSERT(not __mesh.empty());
  return __mesh.top();
}

void Information::
setMesh(ConstReferenceCounting<Mesh> mesh)
{
  __mesh.push(mesh);
}

void Information::
unsetMesh()
{
  __mesh.pop();
}

bool Information::
usesMesh() const
{
  return not __mesh.empty();
}

ConstReferenceCounting<Scene> Information::
getScene()
{
  return __scene;
}

void Information::
setScene(ConstReferenceCounting<Scene> scene)
{
  __scene = scene;
}

bool Information::
usesScene() const
{
  return __scene != 0;
}

void Information::
unsetScene()
{
  __scene = 0;
}

ReferenceCounting<UnknownListExpression> Information::
getUnknownList()
{
  return __unknownListExpression;
}

void Information::
setUnknownList(ReferenceCounting<UnknownListExpression> u)
{
  __unknownListExpression = u;
}

bool Information::
usesUnknownList() const
{
  return __unknownListExpression != 0;
}


void Information::
unsetUnknownList()
{
  __unknownListExpression = 0;
}


const bool& Information::
coarseMesh() const
{
  return __coarseMesh;
}


void Information::
setCoarseMesh(const bool& coarse)
{
  __coarseMesh = coarse;
}

Information::
Information()
  : __scene(0),
    __unknownListExpression(0),
    __coarseMesh(true)
{
  ;
}

Information::
Information(const Information& I)
  : __mesh(I.__mesh),
    __scene(I.__scene),
    __unknownListExpression(I.__unknownListExpression),
    __coarseMesh(I.__coarseMesh)
{
  ;
}

Information::
~Information()
{
  ;
}

