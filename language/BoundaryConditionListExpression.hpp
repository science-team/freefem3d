//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionListExpression.hpp,v 1.6 2007/02/08 23:19:38 delpinux Exp $

#ifndef BOUNDARYCONDITIONLIST_EXPRESSION_HPP
#define BOUNDARYCONDITIONLIST_EXPRESSION_HPP

#include <Expression.hpp>
#include <list>
#include <Variable.hpp>

#include <BoundaryConditionExpression.hpp>

/*!
  \class BoundaryConditionListExpression

  This class defines the base class of BoundaryConditionList expressions

  \author Stephane Del Pino
 */
class BoundaryConditionSet;
class BoundaryConditionListExpression
  : public Expression
{
public:
  enum BoundaryConditionListType {
    set,
    variable,
    undefined
  };

private:
  BoundaryConditionListType __boundaryConditionListType;

protected:
  ReferenceCounting<BoundaryConditionSet> __boundaryConditionSet;

public:
  virtual bool hasPOVBoundary() const = 0;

  ReferenceCounting<BoundaryConditionSet> boundaryConditionSet();

  const BoundaryConditionListExpression::BoundaryConditionListType&
  boundaryConditionListType() const
  {
    return __boundaryConditionListType;
  }

  virtual void add(ReferenceCounting<BoundaryConditionExpression>) = 0;

  BoundaryConditionListExpression(const BoundaryConditionListExpression& e);

  BoundaryConditionListExpression(const BoundaryConditionListExpression::
				  BoundaryConditionListType& t);

  virtual ~BoundaryConditionListExpression();
};

/*!
  \class BoundaryConditionListExpressionSet

  This class defines the base class of BoundaryConditionList expressions

  \author Stephane Del Pino
 */
class PDECondition;
class BoundaryConditionListExpressionSet
  : public BoundaryConditionListExpression
{
private:
  typedef std::list<ReferenceCounting<BoundaryConditionExpression> > BoundaryConditionExpressionList;
  BoundaryConditionExpressionList __boundaryConditionExpressionList;

  std::ostream& put(std::ostream& os) const
  {
    for (BoundaryConditionExpressionList::const_iterator
	   i = __boundaryConditionExpressionList.begin();
	 i != __boundaryConditionExpressionList.end(); ++i)
      os  << (**i) << '\n';
    return os;
  }

  void __splitBoundaryList(BoundaryConditionSet& bcSet,
			   ConstReferenceCounting<PDECondition> pde,
			   ConstReferenceCounting<Boundary> b);

public:
  bool hasPOVBoundary() const;

  void execute();

  void add(ReferenceCounting<BoundaryConditionExpression> b)
  {
    __boundaryConditionExpressionList.push_back(b);
  }

  BoundaryConditionListExpressionSet(const BoundaryConditionListExpressionSet& e);

  BoundaryConditionListExpressionSet();

  ~BoundaryConditionListExpressionSet();
};

#endif // BOUNDARYCONDITIONLIST_EXPRESSION_HPP

