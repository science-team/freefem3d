//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionIntegrate.hpp,v 1.2 2006/07/23 22:17:07 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_INTEGRATE_HPP
#define FUNCTION_EXPRESSION_INTEGRATE_HPP

#include <FunctionExpression.hpp>

/**
 * @file   FunctionExpressionIntegrate.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul 18 00:01:26 2006
 * 
 * @brief This class describes expressions of function integration in
 * a given direction @f$ \int_a^b f dx_i @f$
 * 
 */
class FunctionExpressionIntegrate
  : public FunctionExpression
{
public:
  enum Direction {
    x,
    y,
    z
  };

private:
  ReferenceCounting<FunctionExpression>
  __lowerBound;			/**< lower bound of integration @f$ a @f$ */
  ReferenceCounting<FunctionExpression>
  __upperBound;			/**< upper bound of integration @f$ b @f$ */
  ReferenceCounting<FunctionExpression>
  __integratedFunction;		/**< expression to integrate */
  Direction __direction;	/**< Direction of integration */
  

public:
  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Construtor
   * 
   * @param lowerBound lower bound of integration @f$ a @f$
   * @param upperBound upper bound of integration @f$ b @f$
   * @param integratedFunction function to integrate @f$ f @f$
   * @param directionName name of the direction of integration
   */
  FunctionExpressionIntegrate(ReferenceCounting<FunctionExpression> lowerBound,
			      ReferenceCounting<FunctionExpression> upperBound,
			      ReferenceCounting<FunctionExpression> integratedFunction,
			      const std::string& directionName);

  /** 
   * Copy constructor
   * 
   * @param f original expression
   */
  FunctionExpressionIntegrate(const FunctionExpressionIntegrate& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionIntegrate();
};

#endif // FUNCTION_EXPRESSION_INTEGRATE_HPP
