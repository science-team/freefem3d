//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MultiLinearExpressionSum.hpp,v 1.1 2007/06/10 14:59:56 delpinux Exp $

#ifndef MULTI_LINEAR_EXPRESSION_SUM_HPP
#define MULTI_LINEAR_EXPRESSION_SUM_HPP

#include <Expression.hpp>

#include <MultiLinearExpression.hpp>

#include <LinearExpression.hpp>

#include <list>

class MultiLinearExpressionSum
  : public Expression
{
private:
  typedef std::list<ReferenceCounting<MultiLinearExpression> > ListType;

public:
  typedef ListType::iterator iterator;

  MultiLinearExpressionSum::iterator beginPlus()
  {
    return __listPlus.begin();
  }

  MultiLinearExpressionSum::iterator endPlus()
  {
    return __listPlus.end();
  }

  MultiLinearExpressionSum::iterator beginMinus()
  {
    return __listMinus.begin();
  }

  MultiLinearExpressionSum::iterator endMinus()
  {
    return __listMinus.end();
  }

private:
  ListType __listPlus;
  ListType __listMinus;

  std::ostream& put(std::ostream& os) const;

public:
  void plus(ReferenceCounting<MultiLinearExpression> m) {
    __listPlus.push_back(m);
  }

  void minus(ReferenceCounting<MultiLinearExpression> m) {
    __listMinus.push_back(m);
  }

  void check();

  void execute();

  MultiLinearExpressionSum()
    : Expression(Expression::multiLinearExpSum)
  {
    ;
  }

  MultiLinearExpressionSum(const MultiLinearExpressionSum& M)
    : Expression(M),
      __listPlus(M.__listPlus),
      __listMinus(M.__listMinus)
  {
    ;
  }

  ~MultiLinearExpressionSum()
  {
    ;
  }
};

#endif // MULTI_LINEAR_EXPRESSION_SUM_HPP
