//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDEEquationExpression.cpp,v 1.4 2007/02/08 23:19:38 delpinux Exp $

#include <PDEEquationExpression.hpp>
#include <PDE.hpp>

PDEEquationExpression::PDEEquationExpression(const PDEEquationExpression& e)
  : Expression(e),
    __pdeOperatorSum(e.__pdeOperatorSum),
    __secondMember(e.__secondMember)
{
  ;
}

PDEEquationExpression::
PDEEquationExpression(ReferenceCounting<PDEOperatorSumExpression> pdeOperatorSum,
		      ReferenceCounting<FunctionExpression> secondMember)
  : Expression(Expression::pdeEquation),
    __pdeOperatorSum(pdeOperatorSum),
    __secondMember(secondMember)
{
  ;
}

PDEEquationExpression::~PDEEquationExpression()
{
  ;
}

ReferenceCounting<PDE>
PDEEquationExpression::pde()
{
  return __pde;
}

void PDEEquationExpression::execute()
{
  (*__pdeOperatorSum).execute();
  (*__secondMember).execute();

  if (__secondMember->hasBoundaryExpression()) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot evaluate the expression \""
		       +stringify((*__secondMember))
		       +"\" in the volume",
		       ErrorHandler::normal);
  }

  __pde = new PDE(__pdeOperatorSum->vectorPDEOperator(), __secondMember->function());
}

