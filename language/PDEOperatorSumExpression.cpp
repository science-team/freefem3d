//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDEOperatorSumExpression.cpp,v 1.5 2007/02/08 23:19:38 delpinux Exp $

#include <PDEOperatorSumExpression.hpp>
#include <PDESolution.hpp>

#include <VectorialPDEOperator.hpp>

#include <Information.hpp>

#include <UnknownListExpression.hpp>

ReferenceCounting<VectorialPDEOperator>
PDEOperatorSumExpression::vectorPDEOperator()
{
  return __vectorPDEOperator;
}

void PDEOperatorSumExpression::execute()
{
  ReferenceCounting<UnknownListExpression> unknownListExpression
    = Information::instance().getUnknownList();

  const UnknownListExpression& uList = *unknownListExpression;

  __vectorPDEOperator = new VectorialPDEOperator(uList.size());
  for(PDEOperatorExpressionList::iterator i = (*__sumList).begin();
      i != (*__sumList).end(); ++i) {
    PDEOperatorExpression& theOperator = (*(*i));
    theOperator.execute();
    switch(theOperator.pdeOperatorType()) {
    case (PDEOperatorExpression::scalar): {
      PDEScalarOperatorExpression& theScalarOperator
	= dynamic_cast<PDEScalarOperatorExpression&>(theOperator);
      size_t n = uList.number(theScalarOperator.unknownName());
      (*__vectorPDEOperator)[n]->add(theScalarOperator.pdeOperator());
      break;
    }
    case (PDEOperatorExpression::vectorial): {
      PDEVectorialOperatorExpression& theVectorialOperator
	= dynamic_cast<PDEVectorialOperatorExpression&>(theOperator);
      typedef
	PDEVectorialOperatorExpression::PDEOperatorList::iterator
	Iterator;

      for (Iterator i = theVectorialOperator.pdeOperatorList().begin();
	   i != theVectorialOperator.pdeOperatorList().end(); ++i) {
	size_t n = uList.number((*i).first);
	(*__vectorPDEOperator)[n]->add((*i).second);
      }
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    }
  }

  for(PDEOperatorExpressionList::iterator i = (*__differenceList).begin();
      i != (*__differenceList).end(); ++i) {
    PDEOperatorExpression& theOperator = (*(*i));
    theOperator.execute();
    switch(theOperator.pdeOperatorType()) {
    case (PDEOperatorExpression::scalar): {
      PDEScalarOperatorExpression& theScalarOperator
	= dynamic_cast<PDEScalarOperatorExpression&>(theOperator);
      size_t n = uList.number(theScalarOperator.unknownName());
      (*__vectorPDEOperator)[n]->add(-(*theScalarOperator.pdeOperator()));
      break;
    }
    case (PDEOperatorExpression::vectorial): {
      PDEVectorialOperatorExpression& theVectorialOperator
	= dynamic_cast<PDEVectorialOperatorExpression&>(theOperator);
      typedef
	PDEVectorialOperatorExpression::PDEOperatorList::iterator
	Iterator;

      for (Iterator i = theVectorialOperator.pdeOperatorList().begin();
	   i != theVectorialOperator.pdeOperatorList().end(); ++i) {
	size_t n = uList.number((*i).first);
	(*__vectorPDEOperator)[n]->add(-(*(*i).second));
      }
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    }
  }
}

PDEOperatorSumExpression::
PDEOperatorSumExpression(const PDEOperatorSumExpression& e)
  : Expression(e),
    __vectorPDEOperator(e.__vectorPDEOperator),
    __sumList(e.__sumList),
    __differenceList(e.__differenceList)
{
  ;
}

PDEOperatorSumExpression::PDEOperatorSumExpression()
  : Expression(Expression::pdeOperatorSum),
    __sumList(new PDEOperatorExpressionList),
    __differenceList(new PDEOperatorExpressionList)
{
  ;
}

PDEOperatorSumExpression::~PDEOperatorSumExpression()
{
  ;
}

void PDEOperatorSumExpression::add(ReferenceCounting<PDEOperatorSumExpression> P)
{
  for(PDEOperatorExpressionList::iterator i = (*(*P).__sumList).begin();
      i != (*(*P).__sumList).end(); ++i) {
    (*__sumList).push_back(*i);
  }

  for(PDEOperatorExpressionList::iterator i = (*(*P).__differenceList).begin();
      i != (*(*P).__differenceList).end(); ++i) {
    (*__differenceList).push_back(*i);
  }
}

void PDEOperatorSumExpression::minus(ReferenceCounting<PDEOperatorSumExpression> P)
{
  for(PDEOperatorExpressionList::iterator i = (*(*P).__sumList).begin();
      i != (*(*P).__sumList).end(); ++i) {
    (*__differenceList).push_back(*i);
  }

  for(PDEOperatorExpressionList::iterator i = (*(*P).__differenceList).begin();
      i != (*(*P).__differenceList).end(); ++i) {
    (*__sumList).push_back(*i);
  }
}

