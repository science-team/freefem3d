//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Expression.hpp,v 1.5 2007/02/26 01:05:51 delpinux Exp $

#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <Types.hpp>
#include <ReferenceCounting.hpp>
#include <StreamCenter.hpp>

/**
 * @file   Expression.hpp
 * @author Stephane Del Pino
 * @date   Sun Feb  9 16:47:26 2003
 * 
 * @brief  This is the base class for all expressions
 * 
 * 
 */

class Expression
{
public:
  enum  Type {
    boolean,
    boundary,
    boundaryCondition,
    boundaryConditionList,

    domain,

    field,
    fieldlist,
    function,

    integrated,
    integratedOperator,
    insideExpression,
    insideListExpression,

    linearExp,

    mesh,
    multiLinearExp,
    multiLinearExpSum,
    multiLinearForm,
    multiLinearFormSum,

    ofstreamexpression,
    option,
    ostreamexpression,
    ostreamExpressionList,

    pdeEquation,
    pdeOperator,
    pdeOperatorSum,
    pdeProblem,
    problem,

    real,

    scene,
    solver,
    solverOptions,
    string,
    subOption,
    subOptionList,

    testFunctionList,

    unknown,
    unknownList,

    variationalFormula,
    variationalBilinearOperator,
    variationalLinearOperator,
    variationalDirichlet,
    vector3
  };

private:
  //! The type of the expression.
  Expression::Type __type;

protected:
  /*!
    Traits conversion of types.
    \todo IMPLEMENT IT.
   */
  static Expression::Type
  getType(ReferenceCounting<Expression> e1,
	  ReferenceCounting<Expression> e2)
  {
    return Expression::real;
  }

  //! Writes *this to the output os.
  virtual std::ostream& put(std::ostream& os) const = 0;

public:
  //! return the type.
  const Expression::Type& type() const
  {
    return __type;
  }

  /*!
    Executes the expression.
   */
  virtual void execute() = 0;

  //! this function allows to output expressions.
  friend std::ostream& operator<< (std::ostream& os, const Expression& e)
  {
    return e.put(os);
  }

  Expression(const Expression::Type& t)
    : __type(t)
  {
    ;
  }

  Expression(const Expression& e)
    : __type(e.__type)
  {
    ;
  }

  virtual ~Expression()
  {
    ;
  }
};

#endif // EXPRESSION_HPP
