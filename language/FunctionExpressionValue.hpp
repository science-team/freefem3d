//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionValue.hpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_VALUE_HPP
#define FUNCTION_EXPRESSION_VALUE_HPP

#include <FunctionExpression.hpp>

/**
 * @file   FunctionExpressionValue.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 11:25:48 2006
 * 
 * @brief  Embeddes the value of a function expression.
 * 
 * This class does not know anymore which expression was used to
 * create the function it juste embeddes the ScalarFunctionBase
 */
class FunctionExpressionValue
  : public FunctionExpression
{
private:
  const bool __hasBoundary;	/**< @b true if the original function
				   expression had a boundary*/

public:
  /** 
   * Executes the expression.
   * @note nothing to do ;-)
   */
  void execute()
  {
    ;
  }

  /** 
   * Checks wether this expression requieres a boundary for evaluation
   * 
   * @return __hasBoundary
   */
  bool hasBoundaryExpression() const
  {
    return __hasBoundary;
  }

  /** 
   * Constructor
   * 
   * @param f a ScalarFunctionBase
   * @param hasBoundary does @a f requires a boundary
   */
  FunctionExpressionValue(ConstReferenceCounting<ScalarFunctionBase> f,
			  const bool& hasBoundary);

  /** 
   * Copy constructor
   * 
   * @param f a given expression value
   */
  FunctionExpressionValue(const FunctionExpressionValue& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionValue();
};

#endif // FUNCTION_EXPRESSION_VALUE_HPP
