//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OFStreamExpressionValue.hpp,v 1.1 2006/12/30 00:17:05 delpinux Exp $

#ifndef OFSTREAM_EXPRESSION_VALUE_HPP
#define OFSTREAM_EXPRESSION_VALUE_HPP

#include <Expression.hpp>
#include <StringExpression.hpp>

#include <fstream>
/**
 * @file   OFStreamExpressionValue.hpp
 * @author Stephane Del Pino
 * @date   Sat Dec 30 00:27:13 2006
 * 
 * @brief  ofstream expression instanciation
 */
class OFStreamExpressionValue
  : public OFStreamExpression
{
private:
  ReferenceCounting<StringExpression>
  __filename;			/**< expression for the filename */

public:
  /** 
   * Creates the ofstream
   * 
   */
  void execute()
  {
    __filename->execute();
    __fout = new std::ofstream(__filename->value());
  }

  /** 
   * Constructor
   * 
   * @param filename given filename expression
   */
  OFStreamExpressionValue(ReferenceCounting<StringExpression> filename)
    : OFStreamExpression(OFStreamExpression::value),
      __filename(filename)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param ofs 
   */
  OFStreamExpressionValue(const OFStreamExpressionValue& ofs)
    : OFStreamExpression(ofs),
      __filename(ofs.__filename)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~OFStreamExpressionValue()
  {
    ;
  }
};

#endif // OFSTREAM_EXPRESSION_VALUE_HPP
