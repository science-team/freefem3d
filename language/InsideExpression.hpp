//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: InsideExpression.hpp,v 1.2 2003/05/04 18:09:01 delpinux Exp $

#ifndef INSIDE_EXPRESSION_HPP
#define INSIDE_EXPRESSION_HPP

#include <Expression.hpp>
#include <Vector3Expression.hpp>

#include <TinyVector.hpp>
/**
 * @file   InsideExpression.hpp
 * @author Stephane Del Pino
 * @date   Tue Nov 12 16:06:48 2002
 * 
 * @brief This class is used to build caracteristic function of sets
 * of objects defined by a given color.
 * 
 * 
 */

class InsideExpression
  : public Expression
{
private:
  ReferenceCounting<Vector3Expression> __reference; /**< The reference */

  const bool __inside;		/**< true for inside, false for outside */

  /** 
   * Expression::put function overloading
   * 
   * @param os 
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    if (__inside) {
      os << "inside(";
    } else {
      os << "outside(";
    }
    os << *__reference;
    os << ')';

    return os;
  }
public:

  /** 
   * Returns the reference
   * 
   * 
   * @return x
   */
  const TinyVector<3> reference() const
  {
    TinyVector<3> x;
    for (size_t i=0; i<3; ++i)
      x[i] = (*__reference).value(i);

    return x;
  }

  /** 
   * Specialization of execute
   * 
   */
  void execute()
  {
    (*__reference).execute();
  }

  /** 
   * Returns true if one wants to caracterize the inside of the
   * domain, false for outside
   * 
   * @return __inside
   */
  const bool& inside() const
  {
    return __inside;
  }

  /** 
   * Constructor
   * 
   * @param inside 
   * @param ref 
   * 
   */
  InsideExpression(const bool inside,
		   ReferenceCounting<Vector3Expression> ref)
    : Expression(Expression::insideExpression),
      __reference(ref),
      __inside(inside)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param IE 
   * 
   */
  InsideExpression(const InsideExpression& IE)
    : Expression(IE),
      __reference(IE.__reference),
      __inside(IE.__inside)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~InsideExpression()
  {
    ;
  }  
};

#endif // INSIDE_EXPRESSION_HPP
