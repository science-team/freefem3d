//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: UnknownListExpression.hpp,v 1.7 2007/02/08 23:19:38 delpinux Exp $

#ifndef UNKNOWN_LIST_EXPRESSION_HPP
#define UNKNOWN_LIST_EXPRESSION_HPP

#include <Expression.hpp>
#include <Variable.hpp>

#include <UnknownExpression.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <list>

/*!
  \class UnknownlistExpression

  This class defines the base class of Unknownlist expressions.

  \author Stephane Del Pino
 */
class UnknownListExpression
  : public Expression
{
public:
  typedef std::list<ReferenceCounting<UnknownExpression> > listType;

  typedef listType::iterator iterator;
  typedef listType::const_iterator const_iterator;

  virtual UnknownListExpression::iterator begin() = 0;
  virtual UnknownListExpression::const_iterator begin() const = 0;
  virtual UnknownListExpression::const_iterator end() const = 0;

  virtual size_t size() const = 0;

  virtual size_t number(const std::string& name) const = 0;

  UnknownListExpression()
    : Expression(Expression::unknownList)
  {
    ;
  }

  UnknownListExpression(const UnknownListExpression& u)
    : Expression(u)
  {
    ;
  }

  virtual ~UnknownListExpression()
  {
    ;
  }
};

/*
class UnknownListExpressionVariable
  : public UnknownListExpression
{
private:
  ReferenceCounting<UnknownListVariable> __unknownlistVariable;

  std::ostream& put(std::ostream& os) const
  {
    os << (*__unknownlistVariable).name() << ": " << (*(*__unknownlistVariable).expression());
    return os;
  }

public:
  void execute()
  {
    ;
  }

  UnknownListExpressionVariable(ReferenceCounting<UnknownListVariable> r);

  UnknownListExpressionVariable(const UnknownListExpressionVariable& e);

  ~UnknownListExpressionVariable();
};
*/

class UnknownListExpressionSet
  : public UnknownListExpression
{
protected:
  listType __list;

private:
  std::ostream& put(std::ostream& os) const;

public:
  UnknownListExpressionSet::listType::iterator begin()
  {
    return __list.begin();
  }

  UnknownListExpressionSet::listType::const_iterator begin() const
  {
    return __list.begin();
  }

  UnknownListExpressionSet::listType::const_iterator end() const
  {
    return __list.end();
  }

  inline size_t size() const
  {
    return __list.size();
  }

  inline size_t number(const std::string& name) const
  {
    size_t n = 0;
    for(listType::const_iterator i = __list.begin(); i != __list.end(); ++i) {
      if ((*i)->name() == name) {
	return n;
      }
      ++n;
    }

    const std::string errorMsg
      = "\""+name+"\" is not a variable of the unkown list";

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg,
		       ErrorHandler::unexpected);
    return 0;
  }

  listType& giveList()
  {
    return __list;
  }

  void add(ReferenceCounting<UnknownExpression> u) {
    for (listType::const_iterator i =  __list.begin();
	 i != __list.end(); ++i) {
      if (*i == u) {
	const std::string errorMsg
	  = stringify(*u)+" has been declared twice in unknown list";

	throw ErrorHandler(__FILE__,__LINE__,
			   errorMsg,
			   ErrorHandler::normal);
      }
    }
    __list.push_back(u);
  }

  void execute()
  {
    ;
  }

  UnknownListExpressionSet()
    : UnknownListExpression()
  {
    ;
  }

  UnknownListExpressionSet(const UnknownListExpressionSet& l)
    : UnknownListExpression(l),
      __list(l.__list)
  {
    ;
  }

  ~UnknownListExpressionSet()
  {
    ;
  }
};

#endif // UNKNOWN_LIST_EXPRESSION_HPP
