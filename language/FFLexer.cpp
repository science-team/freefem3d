//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FFLexer.cpp,v 1.47 2007/06/09 10:37:08 delpinux Exp $

#include <StreamCenter.hpp>

#include <Expression.hpp>
#include <RealExpression.hpp>
#include <BooleanExpression.hpp>
#include <Vector3Expression.hpp>

#include <FunctionExpression.hpp>
#include <FunctionExpressionConstant.hpp>
#include <FunctionExpressionMeshReferences.hpp>

#include <MeshExpression.hpp>

#include <DomainExpression.hpp>
#include <InsideExpression.hpp>
#include <InsideListExpression.hpp>

#include <FieldExpression.hpp>
#include <FieldExpressionList.hpp>

#include <BoundaryExpression.hpp>
#include <BoundaryConditionExpression.hpp>
#include <BoundaryConditionListExpression.hpp>

#include <PDEOperatorExpression.hpp>

#include <PDESystemExpression.hpp>
#include <PDEEquationExpression.hpp>

#include <SolverOptionsExpression.hpp>
#include <SubOptionListExpression.hpp>
#include <SubOptionExpression.hpp>

#include <Instruction.hpp>

#include <TestFunctionExpressionList.hpp>
#include <IntegratedExpression.hpp>
#include <IntegratedOperatorExpression.hpp>

#include <LinearExpression.hpp>
#include <MultiLinearExpression.hpp>
#include <MultiLinearFormExpression.hpp>

#include <UnknownExpression.hpp>
#include <UnknownListExpression.hpp>

#include <VariationalFormulaExpression.hpp>
#include <VariationalProblemExpression.hpp>

#include <VariationalDirichletListExpression.hpp>

#include <OStreamExpressionList.hpp>

#include <ScalarFunctionNormal.hpp>

#include <parse.ff.h>

#include <cstdlib>
#include <cstring>
#include <cctype>

#include <FFLexer.hpp>

#include <sstream>

#include <VariableLexerRepository.hpp>

#include <ParameterCenter.hpp>

void FFLexer::define(Lexer::KeyWordList& kw)
{
  kw["x"] = XYZ;
  kw["y"] = XYZ;
  kw["z"] = XYZ;
  kw["double"] = DOUBLE;
  kw["extract"] = EXTRACT;
  kw["femfunction"] = FEMFUNCTION;
  kw["function"] = FUNCTION;
  kw["mesh"] = MESH;
  kw["periodic"] = PERIODIC;
  kw["sfunction"] = SFUNCTION;
  kw["simplify"] = SIMPLIFY;
  kw["spectral"] = SPECTRALMESH;
  kw["structured"] = STRUCTMESH;
  kw["surface"] = SURFMESH;
  kw["tetrahedrize"] = TETRAHEDRIZE;
  kw["transform"] = TRANSFORM;

 // Those two keywords are equivalent!
  kw["vector"] = VECTOR;
  kw["vertex"] = VECTOR;

  kw["scene"] = SCENE;
  kw["domain"] = DOMAIN_;

  kw["ofstream"] = OFSTREAM;

  // statements
  kw["do"] = DO;
  kw["while"] = WHILE;
  kw["for"] = FOR;
  kw["if"] = IF;
  kw["else"] = ELSE;

  kw["true"] = TRUE;
  kw["false"] = FALSE;
  kw["not"] = NOT;
  kw["and"] = AND;
  kw["or"] = OR;
  kw["xor"] = XOR;

  kw["solve"] = SOLVE;

  kw["div"] = DIV;
  kw["grad"] = GRAD;
  kw["dnu"] = DNU;

  kw["int"] = INT;

  kw["min"] = MINMAX;
  kw["max"] = MINMAX;

  kw["convect"] = CONVECT;

  kw["pde"] = PDEQ;

  kw["dx"] = DXYZ;
  kw["dy"] = DXYZ;
  kw["dz"] = DXYZ;

  kw["nx"] = NORMALCOMPONENT;
  kw["ny"] = NORMALCOMPONENT;
  kw["nz"] = NORMALCOMPONENT;

  kw["in"] = IN;
  kw["on"] = ON;
  kw["by"] = BY;
  kw["using"] = USING;

  kw["P0"] = DISCRETIZATIONTYPE;
  kw["P1"] = DISCRETIZATIONTYPE;
  kw["P2"] = DISCRETIZATIONTYPE;

  kw["coarsemesh"] = COARSEMESH;
  kw["finemesh"] = FINEMESH;

  kw["exit"] = EXIT;

  kw["inside"] = INSIDE;
  kw["outside"] = OUTSIDE;

  kw["cout"] = COUT;
  kw["cerr"] = CERR;

  kw["read"] = READ;
  kw["save"] = SAVE;
  kw["plot"] = PLOT;
  kw["exec"] = EXEC;
  kw["cat"]  = CAT;

  kw["pov"]    = POV;

  kw["gmsh"]   = FILEFORMAT;
  kw["medit"]  = FILEFORMAT;
  kw["am_fmt"] = FILEFORMAT;
  kw["opendx"] = FILEFORMAT;
  kw["raw"]    = FILEFORMAT;
  kw["vtk"]    = FILEFORMAT;

  kw["abs"] = CFUNCTION;
  kw["sin"] = CFUNCTION;
  kw["cos"] = CFUNCTION;
  kw["tan"] = CFUNCTION;
  kw["asin"] = CFUNCTION;
  kw["acos"] = CFUNCTION;
  kw["atan"] = CFUNCTION;
  kw["sqrt"] = CFUNCTION;
  kw["exp"] = CFUNCTION;
  kw["log"] = CFUNCTION;

  kw["one"] = ONE;
  kw["reference"] = REFERENCE;
  kw["vertices"] = ITEMS;
  kw["elements"] = ITEMS;

  kw["xmin"] = FACE;
  kw["xmax"] = FACE;
  kw["ymin"] = FACE;
  kw["ymax"] = FACE;
  kw["zmin"] = FACE;
  kw["zmax"] = FACE;

  kw["dos"]    = FILETYPE;
  kw["unix"]   = FILETYPE;
  kw["mac"]    = FILETYPE;
  kw["binary"] = FILETYPE;

  kw["test"] = TESTS;
}

//! Constructs a FFLexer for given std::istream and std::ostream.
FFLexer::FFLexer(std::istream& In,
		 std::ostream& Out)
  : Lexer(In,Out)
{
  FFLexer::define(__keyWordList);
  //! Get the list of keywords
  ParameterCenter::instance().get(__optionsKeyWords);
}

void FFLexer::switchToContext(const FFLexer::Context context)
{
  switch (context) {
  case (FFLexer::defaultContext): {
    __keyWordList["dx"] = DXYZ;
    __keyWordList["dy"] = DXYZ;
    __keyWordList["dz"] = DXYZ;
    break;
  }
  case (FFLexer::variationalContext): {
    __keyWordList["dx"] = DXYZOP;
    __keyWordList["dy"] = DXYZOP;
    __keyWordList["dz"] = DXYZOP;
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "should not reach that context",
		       ErrorHandler::unexpected);
  }
  }
}

/*! This function returns the token associated to the name "word" if it
  corresponds to a Keyword, else, it returns "-1".
*/
int FFLexer::isKeyword(const std::string& word)
{
  KeyWordList::iterator theKeyWord
    = __keyWordList.find(word.c_str());

  if(theKeyWord != __keyWordList.end()) { // if it was found
    switch (theKeyWord->second) {
    case FILETYPE: {
      fflval.fileType = FileDescriptor::fileType(word.c_str());
      break;
    }
    case FILEFORMAT: {
      fflval.formatType = FileDescriptor::formatType(word.c_str());
      break;
    }
    case CFUNCTION:
    case DXYZ:
    case DXYZOP:
    case FACE:
    case ITEMS:
    case MINMAX:
    case XYZ: {
      fflval.str = new char[strlen(word.c_str())+1];
      strcpy (fflval.str, word.c_str());
      break;
    }
    case NORMALCOMPONENT: {
      const char* c = word.c_str();
      if (std::strcmp(c,"nx") == 0) {
	fflval.normalComponent = ScalarFunctionNormal::x;
	break;
      }
      if (std::strcmp(c,"ny") == 0) {

	fflval.normalComponent = ScalarFunctionNormal::y;
	break;
      }
      if (std::strcmp(c,"nz") == 0) {
	fflval.normalComponent = ScalarFunctionNormal::z;
	break;
      }
      // Should not reach this place
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected discretization type",
			 ErrorHandler::unexpected);
    }
    case DISCRETIZATIONTYPE: {
      const char* c = word.c_str();
      if (std::strcmp(c,"P0") == 0) {
	fflval.discretizationType = DiscretizationType::lagrangianFEM0;
	break;
      }
      if (std::strcmp(c,"P1") == 0) {
	fflval.discretizationType = DiscretizationType::lagrangianFEM1;
	break;
      }
      if (std::strcmp(c,"P2") == 0) {
	fflval.discretizationType = DiscretizationType::lagrangianFEM2;
	break;
      }
      if (std::strcmp(c,"Legendre") == 0) {
	fflval.discretizationType = DiscretizationType::spectralLegendre;
	break;
      }
      // Should not reach this place
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected discretization type",
			 ErrorHandler::unexpected);
    }
    default:{
      ;
    }
    }

    return theKeyWord->second;
  }
  return -1;			// Means this is not a keyword!
}

/*! This function returns the token associated to the name "word" if it
  corresponds to an Option, else, it returns "-1".
*/
int FFLexer::isOption(const std::string& word)
{
  IdentifierSet::iterator theKeyWord
    = __optionsKeyWords.find(word.c_str());
  if(theKeyWord != __optionsKeyWords.end()) { // if it was found
    return OPTION;
  }
  return -1;			// Means this is not a keyword!
}

/*!
  This function returns the token associated to the name "word" if it
  corresponds to a variable, else, it returns "-1".
*/
int FFLexer::isVariable(const std::string& word)
{
  const int token = VariableLexerRepository::instance().find(word);
  if (token != -1) {
//     if (fflval.str != 0) {
//       delete fflval.str;
//     }
    fflval.str = new char[strlen(word.c_str())+1];
    strcpy(fflval.str,word.c_str());
  }
  return token;
}

//! The lexer function.
int FFLexer::yylex()
{
  char readChar = ' ';
  in.unsetf(std::ios::skipws);

  while ((in)&&(isspace(readChar))) {
    in >> readChar;
    if (readChar=='\n')
      linenumber++;
  }

  char nextChar = in.peek(); // read next Char

  if (in) {

    // Is input a Number?
    if (isdigit(readChar) || ((readChar=='.') && isdigit(nextChar))) {
      in.unget();

      if (in >> fflval.value) {
	std::stringstream is;
	is << fflval.value << std::ends;
	yytext = is.str();

	return NUM;
      } else {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);
      }
    }

    if (isalpha(readChar) or (readChar == '_')) {
      std::string is;
      while (isalnum(readChar) or (readChar == '_')) {
	is += readChar;
	in >> readChar;
      }
      in.unget();

      yytext = is;
      // is the word a Keyword?
      int TOKEN = isKeyword(yytext);
      if (TOKEN != -1) {
	return TOKEN;
      }
      // reaching this place means this is not a Keyword!

      // Is the TOKEN an option ?
      TOKEN = isOption(yytext);
      if(TOKEN != -1) {
	fflval.str = new char[strlen(yytext.c_str())+1];
	strcpy (fflval.str, yytext.c_str());
	return TOKEN;
      }
	
      // is the word a Variable?
      TOKEN = isVariable(yytext);
      if (TOKEN != -1) {
	return TOKEN;
      } else {
	fflval.aString = new std::string(yytext);
	return NAME;
      }
      // reaching this place means this is not a Variable!

    }

    // Comments treatment.
    switch(readChar) {
    case '/': {
      if (nextChar == '/') {	// end of line comment
	while ((readChar != '\n') and (not(in.eof())))
	  in >> readChar;
	in.unget();
	return yylex();	// use recursion
      }

      if (nextChar == '*') {	// multi-line comment
	bool endOfComment = false;
	size_t commentLines = 0; // Counts lines within comment

	in >> readChar;
	while (!endOfComment) {
	  if(in >> readChar) {
	    switch (readChar) {
	    case '*': {
	      if  (in.peek() == '/') {
		endOfComment = true;
	      }
	      break;
	    }
	    case '\n': {
	      commentLines++;
	      break;
	    }
	    }
	  } else {
	    throw ErrorHandler(__FILE__,linenumber,
			       "opened comment never closed",
			       ErrorHandler::compilation);
	  }
	}
	in >> readChar;

	linenumber += commentLines;
	return yylex();
      }
    }
    case '+': {
      if (nextChar == '+') {	// increment operator
	in >> readChar;
	return INCR;
      }
      break;
    }
    case '{': {
      yytext = "{";
      return OPENBLOCK;
    }
    case '}': {
      yytext = "}";
      return CLOSEBLOCK;
    }
    case '-': {
      if (nextChar == '-') {	// increment operator
	in >> readChar;
	yytext = "--";
	return DECR;
      }
      break;
    }
    case '<':{
      if (nextChar == '=') {	// increment operator
	in >> readChar;
	yytext = "<=";
	return LEQ;
      } else if (nextChar == '<') {	// increment operator
	in >> readChar;
	yytext = "<<";
	return LRAFTER;
      }
      yytext = "<";
      return LT;
    }
    case '>':{
      if (nextChar == '=') {	// increment operator
	in >> readChar;
	yytext = ">=";
	return GEQ;
      } else if (nextChar == '>') {	// increment operator
	in >> readChar;
	yytext = ">>";
	return RRAFTER;
      }
      yytext = ">";
      return GT;
    }
    case '=': {
      if (nextChar == '=') {	// increment operator
	in >> readChar;
	yytext = "==";
	return EQ;
      }
      break;
    }
    case '!': {
      if (nextChar == '=') {	// increment operator
	in >> readChar;
	yytext = "!=";
	return NE;
      }
      return NOT;
    }
    case '|': {
      if (nextChar == '|') {	// increment operator
	yytext = "||";
	in >> readChar;
	return OR;
      }
      break;
    }
    case '&': {
      if (nextChar == '&') {	// increment operator
	in >> readChar;
	yytext = "&&";
	return AND;
      }
      break;
    }

    case ')': {
      if (declarationToken == SOLVE)	// Declaration has been achieved.
	declarationToken = -1;        // Must remove Type storage!
      return ')';
    }

    case '"': {
      bool endOfString = false;
      size_t stringLines = 0; // Counts lines within the string
      std::stringstream is;

      while (!endOfString) {
	if(in >> readChar) {
	  switch (readChar) {
	  case '"': {
	    endOfString = true;
	    break;
	  }
	  case '\n': {
	    stringLines++;
	    is << '\n';
	    break;
	  }
	  case '\\': {
	    int peekC = in.peek();
	    switch (peekC) {
	    case 'n': {
	      is << '\n';
	      in >> readChar;
	      break;
	    }
	    case 't': {
	      is << '\t';
	      in >> readChar;
	      break;
	    }
	    case 'v': {
	      is << '\v';
	      in >> readChar;
	      break;
	    }
	    case 'b': {
	      is << '\b';
	      in >> readChar;
	      break;
	    }
	    case 'r': {
	      is << '\r';
	      in >> readChar;
	      break;
	    }
	    case 'f': {
	      is << '\f';
	      in >> readChar;
	      break;
	    }
	    case 'a': {
	      is << '\a';
	      in >> readChar;
	      break;
	    }
	    case '\'': {
	      is << '\'';
	      in >> readChar;
	      break;
	    }
	    case '"': {
	      is << '"';
	      in >> readChar;
	      break;
	    }
	    case '\\': {
	      is << '\\';
	      in >> readChar;
	      break;
	    }
	    default: {
	      throw ErrorHandler(__FILE__,linenumber,
				 "unknown escape sequence",
				 ErrorHandler::compilation);
	    }
	    }
	    break;
	  }
	  default: {
	    is << readChar;
	  }
	  }
	} else {
	      throw ErrorHandler(__FILE__,linenumber,
				 "opened string never closed",
				 ErrorHandler::compilation);
	}
      }
      is << std::ends;

      fflval.str = new char[is.str().size()+1];
      strcpy (fflval.str, is.str().c_str());

      linenumber += stringLines;
      //	yytext = fflval.str;
      return STRING;
    }

    }

    // Others characters treatment
    yytext = readChar;
    return readChar;
  } else {
    return EOF;
  }

  return 0;
}

