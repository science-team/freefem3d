//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionIntegrate.cpp,v 1.2 2006/07/23 22:17:07 delpinux Exp $

#include <FunctionExpressionIntegrate.hpp>
#include <ScalarFunctionIntegrate.hpp>

void
FunctionExpressionIntegrate::
execute()
{
  __lowerBound->execute();
  __upperBound->execute();
  __integratedFunction->execute();

  switch (__direction) {
  case FunctionExpressionIntegrate::x: {
    __scalarFunction = new ScalarFunctionIntegrate(__lowerBound->function(),
						   __upperBound->function(),
						   __integratedFunction->function(),
						   ScalarFunctionIntegrate::x);
    break;
  }
  case FunctionExpressionIntegrate::y: {
    __scalarFunction = new ScalarFunctionIntegrate(__lowerBound->function(),
						   __upperBound->function(),
						   __integratedFunction->function(),
						   ScalarFunctionIntegrate::y);
    break;
  }
  case FunctionExpressionIntegrate::z: {
    __scalarFunction = new ScalarFunctionIntegrate(__lowerBound->function(),
						   __upperBound->function(),
						   __integratedFunction->function(),
						   ScalarFunctionIntegrate::z);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Unknown integration direction",
		       ErrorHandler::unexpected);
  }
  }
}

FunctionExpressionIntegrate::
FunctionExpressionIntegrate(ReferenceCounting<FunctionExpression> lowerBound,
			    ReferenceCounting<FunctionExpression> upperBound,
			    ReferenceCounting<FunctionExpression> integratedFunction,
			    const std::string& directionName)
  : FunctionExpression(FunctionExpression::integrate),
    __lowerBound(lowerBound),
    __upperBound(upperBound),
    __integratedFunction(integratedFunction)
{
  if (directionName.size() == 2) {
    if (directionName[0] == 'd') {
      switch (directionName[1]) {
      case 'x': {
	__direction = FunctionExpressionIntegrate::x;
	return;
      }
      case 'y': {
	__direction = FunctionExpressionIntegrate::y;
	return;
      }
      case 'z': {
	__direction = FunctionExpressionIntegrate::z;
	return;
      }
      }
    }
  }
  throw ErrorHandler(__FILE__,__LINE__,
		     "unknown direction name: "+directionName,
		     ErrorHandler::unexpected);
}
			      
FunctionExpressionIntegrate::
FunctionExpressionIntegrate(const FunctionExpressionIntegrate& f)
  : FunctionExpression(f),
    __lowerBound(f.__lowerBound),
    __upperBound(f.__upperBound),
    __integratedFunction(f.__integratedFunction),
    __direction(f.__direction)
{
  ;
}

FunctionExpressionIntegrate::
~FunctionExpressionIntegrate()
{
  ;
}
