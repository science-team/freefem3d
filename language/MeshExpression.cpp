//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshExpression.cpp,v 1.40 2007/05/20 23:23:19 delpinux Exp $

#include <MeshExpression.hpp>
#include <Variable.hpp>

#include <Mesh.hpp>

#include <MeshOfHexahedra.hpp>
#include <MeshOfTetrahedra.hpp>
#include <SpectralMesh.hpp>
#include <Structured3DMesh.hpp>

#include <SurfaceMeshOfQuadrangles.hpp>
#include <SurfaceMeshOfTriangles.hpp>

#include <Domain.hpp>
#include <Scene.hpp>

#include <DomainExpression.hpp>

#include <Union.hpp>

#include <MeshDomainTetrahedrizor.hpp>
#include <MeshTetrahedrizor.hpp>
#include <MeshPeriodizer.hpp>
#include <MeshSimplifier.hpp>

#include <MeshReader.hpp>
#include <MeshReaderAM_FMTFormat.hpp>
#include <MeshFormatReader.hpp>
#include <GmshFormatReader.hpp>

#include <MeshTransformer.hpp>

#include <MeshExtractor.hpp>

#include <SurfaceMeshGenerator.hpp>
#include <RealExpression.hpp>

#include <FieldExpression.hpp>
#include <ScalarFunctionBase.hpp>

#include <StringExpression.hpp>

#include <VariableRepository.hpp>

ReferenceCounting<Mesh>
MeshExpression::
mesh() const
{
  ASSERT(__typeOfMesh != MeshExpression::undefined);
  return __mesh;
}

MeshExpression::
MeshExpression(const MeshExpression& e)
  : Expression(e),
    __mesh(e.__mesh),
    __typeOfMesh(e.__typeOfMesh)
{
  ;
}

MeshExpression::
MeshExpression(ReferenceCounting<Mesh> mesh,
	       const MeshExpression::TypeOfMesh& t)
  : Expression(Expression::mesh),
    __mesh(mesh),
    __typeOfMesh(t)
{
  ;
}

MeshExpression::
~MeshExpression()
{
  ;
}


std::ostream&
MeshExpressionStructured::
put(std::ostream& os) const
{
  const Structured3DMesh& smesh = static_cast<const Structured3DMesh&>(*__mesh);
  os << "structured mesh of {(" << smesh.shape().shape().nx() << ','
     << smesh.shape().shape().ny() << ','<< smesh.shape().shape().nz()
     << ");" << smesh.shape().a() << ';' << smesh.shape().b() << '}';
  return os;
}

void MeshExpressionStructured::
execute()
{
  TinyVector<3> a;
  TinyVector<3> b;
  TinyVector<3,int> n;
  (*__corner1).execute();
  (*__corner2).execute();
  (*__meshSize).execute();
  for (size_t i=0; i<3; ++i) {
    a[i] = (*__corner1).value(i);
    b[i] = (*__corner2).value(i);
    n[i] = static_cast<int>((*__meshSize).value(i));
  }
  ffout(2) << "Building Mesh ... ";
  Structured3DMeshShape S(n[0], n[1], n[2], a, b);
  __mesh
    = new Structured3DMesh(S,
			   new VerticesCorrespondance(S.numberOfVertices()));
  ffout(2) << "done\n";
}

MeshExpressionStructured::
MeshExpressionStructured(ReferenceCounting<Vector3Expression> size,
			 ReferenceCounting<Vector3Expression> corner1,
			 ReferenceCounting<Vector3Expression> corner2)
  : MeshExpression(0, MeshExpression::structured),
    __meshSize(size),
    __corner1(corner1),
    __corner2(corner2)
{
  ;
}


MeshExpressionStructured::
MeshExpressionStructured(const MeshExpressionStructured& m)
  : MeshExpression(m),
    __meshSize(m.__meshSize),
    __corner1(m.__corner1),
    __corner2(m.__corner2)
{
  ;
}

MeshExpressionStructured::
~MeshExpressionStructured()
{
  ;
}



std::ostream&
MeshExpressionSpectral::
put(std::ostream& os) const
{
  const SpectralMesh& smesh = static_cast<const SpectralMesh&>(*__mesh);
  os << "spectral mesh of {(" << smesh.shape().shape().nx() << ','
     << smesh.shape().shape().ny() << ','<< smesh.shape().shape().nz()
     << ");" << smesh.shape().a() << ';' << smesh.shape().b() << '}';
  return os;
}

void MeshExpressionSpectral::
execute()
{
  TinyVector<3> a;
  TinyVector<3> b;
  TinyVector<3,int> n;
  __corner1->execute();
  __corner2->execute();
  __degree->execute();
  for (size_t i=0; i<3; ++i) {
    a[i] = __corner1->value(i);
    b[i] = __corner2->value(i);
    n[i] = static_cast<int>(__degree->value(i));
  }
  ffout(2) << "Building Mesh ... ";
  Structured3DMeshShape degrees(n[0], n[1], n[2], a, b);
  Structured3DMeshShape shape  (n[0]+2, n[1]+2, n[2]+2, a, b);
  __mesh
    = new SpectralMesh(degrees,new VerticesCorrespondance(shape.numberOfVertices()));
  ffout(2) << "done\n";
}

MeshExpressionSpectral::
MeshExpressionSpectral(ReferenceCounting<Vector3Expression> degree,
		       ReferenceCounting<Vector3Expression> corner1,
		       ReferenceCounting<Vector3Expression> corner2)
  : MeshExpression(0, MeshExpression::spectral),
    __degree(degree),
    __corner1(corner1),
    __corner2(corner2)
{
  ;
}


MeshExpressionSpectral::
MeshExpressionSpectral(const MeshExpressionSpectral& m)
  : MeshExpression(m),
    __degree(m.__degree),
    __corner1(m.__corner1),
    __corner2(m.__corner2)
{
  ;
}

MeshExpressionSpectral::
~MeshExpressionSpectral()
{
  ;
}

std::ostream& MeshExpressionSurface::
put(std::ostream& os) const
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "not implemented",
		     ErrorHandler::unexpected);
  return os;
}

template <typename MeshType>
void MeshExpressionSurface::
__getSurfaceMesh(MeshType& mesh)
{
  if (mesh.hasSurfaceMesh()) {
    __mesh = mesh.surfaceMesh();
  } else {
    throw ErrorHandler(__FILE__,__LINE__,
		       "mesh \""+stringify(*__volumeMesh)+"\" has no surface mesh",
		       ErrorHandler::normal);
  }
}


void MeshExpressionSurface::
execute()
{
  __volumeMesh->execute();

  if (__volumeMesh->mesh()->family() != Mesh::volume) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Cannot build a surface mesh using mesh \""+
		       stringify(*__volumeMesh)+
		       "\" a volume mesh is mandatory!",
		       ErrorHandler::normal);
  }

  if (__domain == 0) {
    
    switch (__volumeMesh->mesh()->type()) {
    case Mesh::cartesianHexahedraMesh: {
      this->__getSurfaceMesh(dynamic_cast<Structured3DMesh&>(*__volumeMesh->mesh()));
      break;
    }
    case Mesh::hexahedraMesh: {
      this->__getSurfaceMesh(dynamic_cast<MeshOfHexahedra&>(*__volumeMesh->mesh()));
      break;
    }
    case Mesh::tetrahedraMesh: {
      this->__getSurfaceMesh(dynamic_cast<MeshOfTetrahedra&>(*__volumeMesh->mesh()));
      break;
    }
    case Mesh::spectralMesh: {
      this->__getSurfaceMesh(dynamic_cast<SpectralMesh&>(*__volumeMesh->mesh()));
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot get boundary of mesh \""+stringify(*__volumeMesh)+"\"",
			 ErrorHandler::unexpected);
    }
    }
  } else {
    __domain->execute();

    Domain& omega = *__domain->domain();

    if(omega.isR3()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "trying to mesh the boundary of R3!",
			 ErrorHandler::normal);
    }

    switch (__volumeMesh->mesh()->type()) {
    case Mesh::cartesianHexahedraMesh: {
      __mesh = new SurfaceMeshOfTriangles();
      Structured3DMesh& volumeMesh
	= dynamic_cast<Structured3DMesh&>(*__volumeMesh->mesh());
      SurfaceMeshOfTriangles& surfaceMesh
	= dynamic_cast<SurfaceMeshOfTriangles&>(*__mesh);

      SurfaceMeshGenerator S;
      S.generateSurfacicMesh(omega,
			     volumeMesh,
			     surfaceMesh);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "mesh generation not yet implemented for \""+
			 stringify(*__volumeMesh)+"\"'s type",
			 ErrorHandler::normal);
    }
    }
  }
}

MeshExpressionSurface::
MeshExpressionSurface(ReferenceCounting<DomainExpression> domain,
		      ReferenceCounting<MeshExpression> volumeMesh)
  : MeshExpression(0, MeshExpression::surface),
    __domain(domain),
    __volumeMesh(volumeMesh)
{
  ;
}

MeshExpressionSurface::
MeshExpressionSurface(ReferenceCounting<MeshExpression> volumeMesh)
  : MeshExpression(0, MeshExpression::surface),
    __domain(0),
    __volumeMesh(volumeMesh)
{
  ;
}


MeshExpressionSurface::
MeshExpressionSurface(const MeshExpressionSurface& m)
  : MeshExpression(m),
    __domain(m.__domain),
    __volumeMesh(m.__volumeMesh)
{
  ;
}

MeshExpressionSurface::
~MeshExpressionSurface()
{
  ;
}

void MeshExpressionVariable::
execute()
{
  __meshVariable = VariableRepository::instance().findVariable<MeshVariable>(__meshName);
  __mesh = __meshVariable->expression()->mesh();
}

MeshExpressionVariable::
MeshExpressionVariable(const std::string& meshName)
  : MeshExpression(0,MeshExpression::variable),
    __meshName(meshName),
    __meshVariable(0)
{
  ;
}

MeshExpressionVariable::
MeshExpressionVariable(const MeshExpressionVariable& e)
  : MeshExpression(e),
    __meshName(e.__meshName),
    __meshVariable(e.__meshVariable)
{
  ;
}

MeshExpressionVariable::
~MeshExpressionVariable()
{
  ;
}

std::ostream&
MeshExpressionRead::
put(std::ostream& os) const
{
  os << "read(" << __fileDescriptor->toString() << ",\"" << __filename->value() << "\")";
  return os;
}

void MeshExpressionRead::
execute()
{
  (*__filename).execute();
  const std::string filename = (*__filename).value();

  try {
    ReferenceCounting<MeshReader> M;
    // pas encore de support pour les modes binaire/ascii
    
    switch ((*__fileDescriptor).format()) {
    case FileDescriptor::am_fmt:
      M = new MeshReaderAM_FMTFormat(filename);
      break;
    case FileDescriptor::medit:
      M = new MeshFormatReader(filename);
      break;
    case FileDescriptor::gmsh:
      M = new GmshFormatReader(filename);
      break;
    default:
      throw MeshReader::Error("no support for reading '"+(*__fileDescriptor).toString()+"' format");
      break;
    }
    __mesh = (*M).mesh();
  } catch (MeshReader::Error & e) {
    throw ErrorHandler(__FILE__,__LINE__,
		       e.message(),
		       ErrorHandler::normal);
  }
}

MeshExpressionRead::
MeshExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
		   ReferenceCounting<StringExpression> filename)
  : MeshExpression(0, MeshExpression::read),
    __fileDescriptor(descriptor),
    __filename(filename)
{
  ;
}

MeshExpressionRead::
MeshExpressionRead(const MeshExpressionRead& m)
  : MeshExpression(m),
    __fileDescriptor(m.__fileDescriptor),
    __filename(m.__filename)
{
  ;
}

MeshExpressionRead::~MeshExpressionRead()
{
  ;
}

std::ostream&
MeshExpressionSimplify::
put(std::ostream& os) const
{
  os << "simplify("
     << (*__originalMesh) << ')';
  return os;
}

void
MeshExpressionSimplify::
execute()
{
  (*__originalMesh).execute();
  MeshSimplifier M((*__originalMesh).mesh());
  __mesh = M.mesh();
}

MeshExpressionSimplify::
MeshExpressionSimplify(ReferenceCounting<MeshExpression> me)
  : MeshExpression(0, MeshExpression::simplify),
    __originalMesh(me)
{
  ;
}

MeshExpressionSimplify::
MeshExpressionSimplify(const MeshExpressionSimplify& e)
  : MeshExpression(e),
    __originalMesh(e.__originalMesh)
{
  ;
}

MeshExpressionSimplify::
~MeshExpressionSimplify()
{
  ;
}


std::ostream&
MeshExpressionExtract::
put(std::ostream& os) const
{
  os << "extract(" << (*__originalMesh) << ','
     << (*__referenceToExtract) << ')';
  return os;
}

template <typename ExtractedMeshType>
void MeshExpressionExtract::__extract()
{
  Mesh* mesh = __originalMesh->mesh();
   MeshExtractor<ExtractedMeshType>
     extractor(dynamic_cast<ExtractedMeshType*>(mesh));

   const int ref = static_cast<int>(__referenceToExtract->realValue());
   if (ref < 0) {
     throw ErrorHandler(__FILE__,__LINE__,
			"Element references have to be positives",
			ErrorHandler::normal);
   }
   std::set<size_t> refSet;
   refSet.insert(static_cast<size_t>(ref));
   __mesh = extractor(refSet);
}

void
MeshExpressionExtract::
execute()
{
  (*__originalMesh).execute();
  (*__referenceToExtract).execute();

  switch ((*(*__originalMesh).mesh()).type()) {
//   case Mesh::cartesianHexahedraMesh: {
//     this->__extract<Structured3DMesh>();
//     break;
//   }
//   case Mesh::hexahedraMesh: {
//     this->__extract<MeshOfHexahedra>();
//     break;
//   }
  case Mesh::tetrahedraMesh: {
    this->__extract<MeshOfTetrahedra>();
    break;
  }
  case Mesh::surfaceMeshQuadrangles: {
    this->__extract<SurfaceMeshOfQuadrangles>();
    break;
  }
  case Mesh::surfaceMeshTriangles: {
    this->__extract<SurfaceMeshOfTriangles>();
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}

MeshExpressionExtract::
MeshExpressionExtract(ReferenceCounting<MeshExpression> me,
		      ReferenceCounting<RealExpression> re)
  : MeshExpression(0, MeshExpression::extract),
    __originalMesh(me),
    __referenceToExtract(re)
{
  ;
}

MeshExpressionExtract::
MeshExpressionExtract(const MeshExpressionExtract& e)
  : MeshExpression(e),
    __originalMesh(e.__originalMesh),
    __referenceToExtract(e.__referenceToExtract)
{
  ;
}

MeshExpressionExtract::
~MeshExpressionExtract()
{
  ;
}



void
MeshExpressionTetrahedrize::execute()
{
  (*__inputMesh).execute();
  ffout(3) << "Converting to tetrahedra "
	   << (*__inputMesh) << '\n';
  MeshTetrahedrizor mt((*__inputMesh).mesh());

  mt.run();

  __mesh = mt.mesh();
  ffout(3) << "Converting to tetrahedra done\n";
}

MeshExpressionTetrahedrize::
MeshExpressionTetrahedrize(ReferenceCounting<MeshExpression> m)
  : MeshExpression(0,MeshExpression::tetrahedrize),
    __inputMesh(m)
{
  ;
}

MeshExpressionTetrahedrize::
MeshExpressionTetrahedrize(const MeshExpressionTetrahedrize& m)
  : MeshExpression(m),
    __inputMesh(m.__inputMesh)
{
  ;
}

MeshExpressionTetrahedrize::
~MeshExpressionTetrahedrize()
{
  ;
}




void
MeshExpressionTetrahedrizeDomain
::execute()
{
  (*__inputMesh).execute();
  (*__domain).execute();
  ffout(3) << "Building domain tetrahedral mesh\n";
  MeshDomainTetrahedrizor mt((*__inputMesh).mesh(),
			     (*__domain).domain());

  mt.run();

  __mesh = mt.mesh();
  ffout(3) << "Building domain tetrahedral mesh: done\n";
}

MeshExpressionTetrahedrizeDomain::
MeshExpressionTetrahedrizeDomain(ReferenceCounting<MeshExpression> m,
				 ReferenceCounting<DomainExpression> d)
  : MeshExpression(0,MeshExpression::tetrahedrizeDomain),
    __inputMesh(m),
    __domain(d)
{
  ;
}

MeshExpressionTetrahedrizeDomain::
MeshExpressionTetrahedrizeDomain(const MeshExpressionTetrahedrizeDomain& m)
  : MeshExpression(m),
    __inputMesh(m.__inputMesh),
    __domain(m.__domain)
{
  ;
}

MeshExpressionTetrahedrizeDomain::
~MeshExpressionTetrahedrizeDomain()
{
  ;
}



void
MeshExpressionTransform::execute()
{
  __inputMesh->execute();
  __transformationField->execute();

  if (__transformationField->numberOfComponents() != 3) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "mesh transformation needs a 3 component field:\n"
		       +stringify(*__transformationField)+"has "
		       +stringify(__transformationField->numberOfComponents())+" components",
		       ErrorHandler::normal);
  }

  MeshTransformer transformer(__inputMesh->mesh(),
			      __transformationField->field());

  transformer.transform();

  __mesh = transformer.mesh();
}

MeshExpressionTransform::
MeshExpressionTransform(ReferenceCounting<MeshExpression> m,
			ReferenceCounting<FieldExpression> f)
  : MeshExpression(0,MeshExpression::transform),
    __inputMesh(m),
    __transformationField(f)
{
  ;
}

MeshExpressionTransform::
MeshExpressionTransform(const MeshExpressionTransform& m)
  : MeshExpression(m),
    __inputMesh(m.__inputMesh),
    __transformationField(m.__transformationField)
{
  ;
}

MeshExpressionTransform::
~MeshExpressionTransform()
{
  ;
}



void
MeshExpressionPeriodic::execute()
{
  (*__inputMesh).execute();

  MeshPeriodizer::ReferencesMapping referencesMapping;

  MappedReferencesList& mappedReferences = (*__mappedReferences);
  for (MappedReferencesList::iterator i = mappedReferences.begin();
       i != mappedReferences.end(); ++i) {
    (*(*i).first).execute();
    (*(*i).second).execute();
    const real_t first = (*(*i).first).realValue();
    const real_t second= (*(*i).second).realValue();
    if ((first != int(first)) or (second != int(second))) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "invalid periodic references ("
			 +stringify(first)+"<->"+stringify(second)
			 +") You must give integers value",
			 ErrorHandler::normal);
    }
    referencesMapping.push_back(std::make_pair(static_cast<size_t>(first),
					       static_cast<size_t>(second)));
  }

  MeshPeriodizer periodizer((*__inputMesh).mesh(), referencesMapping);
  periodizer.run();
  __mesh = periodizer.mesh();
}

MeshExpressionPeriodic::
MeshExpressionPeriodic(ReferenceCounting<MeshExpression> m,
		       ReferenceCounting<MappedReferencesList> l)
  : MeshExpression(0,MeshExpression::periodic),
    __inputMesh(m),
    __mappedReferences(l)
{
  ;
}

MeshExpressionPeriodic::
MeshExpressionPeriodic(const MeshExpressionPeriodic& m)
  : MeshExpression(m),
    __inputMesh(m.__inputMesh),
    __mappedReferences(m.__mappedReferences)
{
  ;
}

MeshExpressionPeriodic::
~MeshExpressionPeriodic()
{
  ;
}


MeshExpressionUndefined::MeshExpressionUndefined()
  : MeshExpression(0, MeshExpression::undefined)
{
  ;
}

MeshExpressionUndefined
::MeshExpressionUndefined(const MeshExpressionUndefined& m)
  : MeshExpression(m)
{
  ;
}

MeshExpressionUndefined
::~MeshExpressionUndefined()
{
  ;
}

