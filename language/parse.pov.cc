/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Substitute the variable and function names.  */
#define yyparse povparse
#define yylex   povlex
#define yyerror poverror
#define yylval  povlval
#define yychar  povchar
#define yydebug povdebug
#define yynerrs povnerrs


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     NUM = 258,
     STRING = 259,
     KEYWORD = 260,
     SPHERE = 261,
     BOX = 262,
     CYLINDER = 263,
     CONE = 264,
     PLANE = 265,
     TORUS = 266,
     UNION = 267,
     INTERSECTION = 268,
     DIFFERENCE = 269,
     OBJECT = 270,
     INVERSE = 271,
     NAME = 272,
     DECLARE = 273,
     TRANSF = 274,
     MATRIXTRANSF = 275,
     PIGMENT = 276,
     COLOR = 277,
     RGB = 278,
     RGBF = 279,
     IF = 280,
     IFDEF = 281,
     IFNDEF = 282,
     ELSE = 283,
     END = 284,
     WHILE = 285,
     AND = 286,
     OR = 287,
     NOT = 288,
     LE = 289,
     GE = 290,
     EQ = 291,
     NE = 292,
     NEG = 293
   };
#endif
/* Tokens.  */
#define NUM 258
#define STRING 259
#define KEYWORD 260
#define SPHERE 261
#define BOX 262
#define CYLINDER 263
#define CONE 264
#define PLANE 265
#define TORUS 266
#define UNION 267
#define INTERSECTION 268
#define DIFFERENCE 269
#define OBJECT 270
#define INVERSE 271
#define NAME 272
#define DECLARE 273
#define TRANSF 274
#define MATRIXTRANSF 275
#define PIGMENT 276
#define COLOR 277
#define RGB 278
#define RGBF 279
#define IF 280
#define IFDEF 281
#define IFNDEF 282
#define ELSE 283
#define END 284
#define WHILE 285
#define AND 286
#define OR 287
#define NOT 288
#define LE 289
#define GE 290
#define EQ 291
#define NE 292
#define NEG 293




/* Copy the first part of user declarations.  */
#line 1 "/home/delpino/src/ff3d/language/parse.pov.yy"
 /* -*- c++ -*- */

   /*
    *  This file is part of ff3d - http://www.freefem.org/ff3d
    *  Copyright (C) 2001, 2002, 2003 St�phane Del Pino
    * 
    *  This program is free software; you can redistribute it and/or modify
    *  it under the terms of the GNU General Public License as published by
    *  the Free Software Foundation; either version 2, or (at your option)
    *  any later version.
    * 
    *  This program is distributed in the hope that it will be useful,
    *  but WITHOUT ANY WARRANTY; without even the implied warranty of
    *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    *  GNU General Public License for more details.
    * 
    *  You should have received a copy of the GNU General Public License
    *  along with this program; if not, write to the Free Software Foundation,
    *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
    * 
    *  $Id: parse.pov.yy,v 1.12 2007/04/25 22:13:12 delpinux Exp $
    * 
    */

#define YYDEBUG 1

#include <cstdlib>
#include <sstream>

#include <iostream>
#include <stack>
#include <cctype>
#include <string>

#include <parse.pov.hpp>
#include <SceneBox.hpp>

#include <Scene.hpp>

#include <Sphere.hpp>
#include <Cube.hpp>
#include <Cylinder.hpp>
#include <Cone.hpp>
#include <Plane.hpp>
#include <Torus.hpp>

#include <Not.hpp>
#include <Union.hpp>
#include <Intersection.hpp>
#include <Difference.hpp>

#include <stdlib.h>
#include <stdio.h>

#include <POVLexer.hpp>

#include <ErrorHandler.hpp>
#include <Stringify.hpp>

 extern std::vector<std::string> path;       // include files paths
 bool execute = true;

 variable * variables; 
 int nbvariables;

 extern POVLexer* povlexer;

 void yyerror (char* s);
 void declarevar (char* name, char* val);
 void expend(std::string& expendval);

 extern Scene* pScene;
 SceneBox SB;

 enum statement {
   condition,
   loop
 };

 std::stack<bool> ExecStack;
 std::stack<statement> StatementStack;
 std::stack<long int> StreamPosStack;

 bool BoolOp = false; // used if the actual object is a
                      // construction object that belongs
                      // to a boolean operation
 std::stack<bool> BoolOpStack;
 std::stack<std::vector<ReferenceCounting<Object> > > OpStack; // Used to store temporarly objects

#define povlex povlexer->povlex



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 94 "/home/delpino/src/ff3d/language/parse.pov.yy"
{
  real_t val;
  real_t vect[3];
  real_t vect4[4];
  real_t mat[12];
  char* str;
  int integer;
  long int stream_pos;
  parsetrans trans;
}
/* Line 187 of yacc.c.  */
#line 284 "parse.pov.cc"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 297 "parse.pov.cc"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   492

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  52
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  17
/* YYNRULES -- Number of rules.  */
#define YYNRULES  69
/* YYNRULES -- Number of states.  */
#define YYNSTATES  230

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   293

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      47,    48,    42,    41,    51,    40,     2,    43,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    50,
      38,    49,    39,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    45,     2,    46,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    44
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,    10,    13,    18,    23,    28,
      30,    32,    37,    43,    51,    58,    65,    66,    69,    73,
      77,    80,    88,    96,   104,   114,   126,   134,   135,   142,
     143,   150,   151,   158,   159,   166,   168,   171,   173,   177,
     181,   185,   189,   193,   196,   198,   202,   206,   210,   214,
     218,   222,   226,   230,   233,   237,   263,   271,   274,   278,
     282,   286,   290,   294,   304,   307,   311,   315,   319,   323
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      53,     0,    -1,    -1,    53,    55,    -1,    53,    58,    -1,
      53,    54,    -1,    26,    47,    17,    48,    -1,    27,    47,
      17,    48,    -1,    25,    47,    65,    48,    -1,    28,    -1,
      29,    -1,    30,    47,    65,    48,    -1,    18,    17,    49,
      64,    50,    -1,    18,    17,    49,     6,    45,    17,    46,
      -1,    21,    45,    22,    23,    67,    46,    -1,    21,    45,
      22,    24,    68,    46,    -1,    -1,    57,    56,    -1,    57,
      19,    67,    -1,    57,    20,    66,    -1,    57,    16,    -1,
       6,    45,    67,    51,    64,    57,    46,    -1,    11,    45,
      64,    51,    64,    57,    46,    -1,     7,    45,    67,    51,
      67,    57,    46,    -1,     8,    45,    67,    51,    67,    51,
      64,    57,    46,    -1,     9,    45,    67,    51,    64,    51,
      67,    51,    64,    57,    46,    -1,    10,    45,    67,    51,
      64,    57,    46,    -1,    -1,    12,    45,    59,    63,    57,
      46,    -1,    -1,    13,    45,    60,    63,    57,    46,    -1,
      -1,    14,    45,    61,    63,    57,    46,    -1,    -1,    15,
      45,    62,    58,    57,    46,    -1,    58,    -1,    58,    63,
      -1,     3,    -1,    64,    41,    64,    -1,    64,    40,    64,
      -1,    64,    42,    64,    -1,    64,    43,    64,    -1,    47,
      64,    48,    -1,    40,    64,    -1,    64,    -1,    64,    39,
      64,    -1,    64,    38,    64,    -1,    64,    35,    64,    -1,
      64,    34,    64,    -1,    64,    36,    64,    -1,    64,    37,
      64,    -1,    65,    31,    65,    -1,    65,    32,    65,    -1,
      33,    65,    -1,    47,    65,    48,    -1,    38,    64,    51,
      64,    51,    64,    51,    64,    51,    64,    51,    64,    51,
      64,    51,    64,    51,    64,    51,    64,    51,    64,    51,
      64,    39,    -1,    38,    64,    51,    64,    51,    64,    39,
      -1,    40,    67,    -1,    67,    40,    67,    -1,    67,    41,
      67,    -1,    67,    42,    64,    -1,    67,    43,    64,    -1,
      64,    42,    67,    -1,    38,    64,    51,    64,    51,    64,
      51,    64,    39,    -1,    40,    68,    -1,    68,    40,    68,
      -1,    68,    41,    68,    -1,    68,    42,    64,    -1,    68,
      43,    64,    -1,    64,    42,    68,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   141,   141,   142,   143,   144,   147,   165,   183,   190,
     195,   208,   219,   244,   255,   261,   270,   279,   311,   372,
     405,   433,   464,   495,   528,   560,   593,   624,   624,   660,
     660,   697,   697,   735,   735,   778,   779,   782,   783,   784,
     785,   786,   787,   788,   791,   792,   793,   794,   795,   796,
     797,   798,   799,   800,   801,   805,   824,   829,   834,   839,
     844,   849,   854,   861,   867,   871,   875,   879,   883,   887
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUM", "STRING", "KEYWORD", "SPHERE",
  "BOX", "CYLINDER", "CONE", "PLANE", "TORUS", "UNION", "INTERSECTION",
  "DIFFERENCE", "OBJECT", "INVERSE", "NAME", "DECLARE", "TRANSF",
  "MATRIXTRANSF", "PIGMENT", "COLOR", "RGB", "RGBF", "IF", "IFDEF",
  "IFNDEF", "ELSE", "END", "WHILE", "AND", "OR", "NOT", "LE", "GE", "EQ",
  "NE", "'<'", "'>'", "'-'", "'+'", "'*'", "'/'", "NEG", "'{'", "'}'",
  "'('", "')'", "'='", "';'", "','", "$accept", "input", "iftok",
  "variable", "objref", "modifier", "object", "@1", "@2", "@3", "@4",
  "objects", "exp", "boolexp", "matrix", "vector", "vector4", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,    60,    62,
      45,    43,    42,    47,   293,   123,   125,    40,    41,    61,
      59,    44
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    52,    53,    53,    53,    53,    54,    54,    54,    54,
      54,    54,    55,    55,    56,    56,    57,    57,    57,    57,
      57,    58,    58,    58,    58,    58,    58,    59,    58,    60,
      58,    61,    58,    62,    58,    63,    63,    64,    64,    64,
      64,    64,    64,    64,    65,    65,    65,    65,    65,    65,
      65,    65,    65,    65,    65,    66,    67,    67,    67,    67,
      67,    67,    67,    68,    68,    68,    68,    68,    68,    68
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     2,     2,     4,     4,     4,     1,
       1,     4,     5,     7,     6,     6,     0,     2,     3,     3,
       2,     7,     7,     7,     9,    11,     7,     0,     6,     0,
       6,     0,     6,     0,     6,     1,     2,     1,     3,     3,
       3,     3,     3,     2,     1,     3,     3,     3,     3,     3,
       3,     3,     3,     2,     3,    25,     7,     2,     3,     3,
       3,     3,     3,     9,     2,     3,     3,     3,     3,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     9,    10,     0,
       5,     3,     4,     0,     0,     0,     0,     0,     0,    27,
      29,    31,    33,     0,     0,     0,     0,     0,    37,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    44,     0,     0,
       0,     0,     0,    43,    57,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    43,
       0,     0,    35,    16,    16,    16,    16,     0,     0,    53,
      44,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       8,     6,     7,    11,     0,    42,    39,    38,    40,    62,
      41,    58,    59,    60,    61,    16,    16,     0,     0,    16,
      40,    16,    36,     0,     0,     0,     0,     0,    12,    54,
      48,    47,    49,    50,    46,    45,    51,    52,     0,     0,
       0,     0,     0,     0,     0,    20,     0,     0,     0,    28,
      17,    30,    32,    34,     0,     0,    21,    23,    16,     0,
      26,    22,    18,     0,    19,     0,    13,     0,     0,     0,
       0,     0,    56,    24,    16,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    25,     0,    14,     0,    43,
      64,     0,     0,     0,     0,     0,    15,     0,     0,    40,
      69,    65,    66,    67,    68,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    63,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    55
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,    20,    21,   150,   123,    82,    50,    51,    52,
      53,    83,    42,    58,   164,    43,   184
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -45
static const yytype_int16 yypact[] =
{
     -45,   203,   -45,   -44,   -42,   -26,    -3,     2,     6,    10,
      18,    36,    38,    -7,   -15,    39,    49,   -45,   -45,    52,
     -45,   -45,   -45,    71,    71,    71,    71,    71,    81,   -45,
     -45,   -45,   -45,    65,    40,   100,   103,    40,   -45,    81,
      71,    81,   164,   -18,   220,   225,   255,   260,    81,   267,
     432,   432,   432,   432,    76,    40,    40,   414,   -27,    50,
      74,   -17,   272,   -45,   -35,   418,    81,    81,    71,    81,
      71,    71,    81,    81,    81,    71,    71,    81,    81,   -45,
      81,    81,   432,   -45,   -45,   -45,   -45,    79,   387,   -45,
     251,    98,    81,    81,    81,    81,    81,    81,    40,    40,
     -45,   -45,   -45,   -45,    81,   -45,   -13,   -13,   -45,   -45,
     -45,   -35,   -35,   -45,   -45,   441,   445,   279,   284,   441,
     -45,   441,   -45,    69,    85,   123,   136,   108,   -45,   -45,
     441,   441,   441,   441,   441,   441,   -45,   -45,   291,   152,
     204,    81,    71,   218,   224,   -45,    71,    88,    87,   -45,
     -45,   -45,   -45,   -45,    90,    81,   -45,   -45,   441,   296,
     -45,   -45,   445,    81,   -45,   111,   -45,   240,   232,    81,
     303,    13,   -45,   -45,   441,    81,    71,    72,   238,   308,
     422,    81,    72,   449,   429,   -45,    81,   -45,   315,   -45,
      51,    72,    72,    72,    81,    81,   -45,   320,    81,   -45,
     -45,    51,    51,   -45,   -45,    81,   327,   332,    81,    81,
     339,   344,    81,    81,   393,   351,   -45,    81,   356,    81,
     363,    81,   368,    81,   375,    81,   380,    81,   437,   -45
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -45,   -45,   -45,   -45,   -45,   -24,     1,   -45,   -45,   -45,
     -45,   -34,   -28,   -21,   -45,    32,   -31
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      49,    23,    22,    24,    98,    99,    57,    72,    73,    57,
      33,    62,    63,    65,    98,    99,    61,    84,    85,    25,
      79,   100,    70,    71,    72,    73,    88,    57,    90,    80,
      69,   103,    34,    74,    89,    91,   176,   177,   106,   107,
     108,   110,    26,    38,   113,   114,   115,    27,   122,   118,
     119,    28,   120,   121,    86,    29,    44,    45,    46,    47,
     124,   125,   126,    30,   130,   131,   132,   133,   134,   135,
      57,    57,    64,    55,    38,    38,   138,   136,   137,    38,
      48,    31,    87,    32,    38,   145,    35,    56,   146,   147,
     148,   139,   140,   194,   195,   143,    36,   144,   101,    37,
     109,   145,   111,   112,   146,   147,   148,   116,   117,    39,
     181,    40,   182,   158,    54,   149,    48,    59,    41,    41,
      60,    48,   102,    41,   127,   154,   163,   167,    41,    98,
      99,   151,   165,   171,   168,   170,   166,     0,     0,   145,
       0,   174,   146,   147,   148,     0,   129,   179,     0,   183,
     178,   190,   145,   188,   189,   146,   147,   148,   197,     0,
     200,   201,   202,   199,   183,   183,   203,   204,   145,   152,
     206,   146,   147,   148,   159,     0,     0,   207,   162,     0,
     210,   211,   153,     0,   214,   215,     0,     0,     0,   218,
       0,   220,     0,   222,     0,   224,     0,   226,   156,   228,
       0,     0,     0,     2,    66,    67,    68,    69,   180,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,     0,
     145,    13,     0,   146,   147,   148,     0,     0,    14,    15,
      16,    17,    18,    19,   145,     0,     0,   146,   147,   148,
     145,     0,     0,   146,   147,   148,     0,     0,   145,     0,
     157,   146,   147,   148,   145,     0,     0,   146,   147,   148,
      70,    71,    72,    73,   160,    70,    71,    72,    73,     0,
     161,    75,     0,     0,     0,     0,    76,     0,   173,   172,
      66,    67,    80,    69,   185,    92,    93,    94,    95,    96,
      97,    66,    67,    80,    69,    70,    71,    72,    73,   105,
      70,    71,    72,    73,     0,     0,    77,    66,    67,    80,
      69,    78,    66,    67,    80,    69,     0,     0,    81,    70,
      71,    72,    73,   104,    66,    67,    80,    69,     0,     0,
     141,    66,    67,    80,    69,   142,    70,    71,    72,    73,
       0,     0,   155,    66,    67,    80,    69,   169,    66,    67,
      80,    69,     0,     0,   175,    66,    67,    80,    69,   186,
      66,    67,    80,    69,     0,     0,   198,    66,    67,    80,
      69,   205,    66,    67,    80,    69,     0,     0,   208,    66,
      67,    80,    69,   209,    66,    67,    80,    69,     0,     0,
     212,    66,    67,    80,    69,   213,    66,    67,    80,    69,
       0,     0,   217,    66,    67,    80,    69,   219,    66,    67,
      80,    69,     0,     0,   221,    66,    67,    80,    69,   223,
      66,    67,    80,    69,     0,     0,   225,    66,    67,    80,
      69,   227,   216,    66,    67,    80,    69,   128,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    92,    93,
      94,    95,    96,    97,    66,    67,    80,    69,    66,    67,
      80,    69,    70,    71,    72,    73,   105,     0,   187,   192,
     193,   194,   195,     0,     0,   196,   229,    66,    67,    80,
      69,    66,    67,    80,    69,    70,    71,    72,    73,    66,
      67,   191,    69
};

static const yytype_int16 yycheck[] =
{
      28,    45,     1,    45,    31,    32,    34,    42,    43,    37,
      17,    39,    40,    41,    31,    32,    37,    51,    52,    45,
      48,    48,    40,    41,    42,    43,    54,    55,    56,    42,
      43,    48,    47,    51,    55,    56,    23,    24,    66,    67,
      68,    69,    45,     3,    72,    73,    74,    45,    82,    77,
      78,    45,    80,    81,    53,    45,    24,    25,    26,    27,
      84,    85,    86,    45,    92,    93,    94,    95,    96,    97,
      98,    99,    40,    33,     3,     3,   104,    98,    99,     3,
      40,    45,     6,    45,     3,    16,    47,    47,    19,    20,
      21,   115,   116,    42,    43,   119,    47,   121,    48,    47,
      68,    16,    70,    71,    19,    20,    21,    75,    76,    38,
      38,    40,    40,   141,    49,    46,    40,    17,    47,    47,
      17,    40,    48,    47,    45,    17,    38,   155,    47,    31,
      32,    46,    45,    22,   158,   163,    46,    -1,    -1,    16,
      -1,   169,    19,    20,    21,    -1,    48,   175,    -1,   177,
     174,   182,    16,   181,   182,    19,    20,    21,   186,    -1,
     191,   192,   193,   191,   192,   193,   194,   195,    16,    46,
     198,    19,    20,    21,   142,    -1,    -1,   205,   146,    -1,
     208,   209,    46,    -1,   212,   213,    -1,    -1,    -1,   217,
      -1,   219,    -1,   221,    -1,   223,    -1,   225,    46,   227,
      -1,    -1,    -1,     0,    40,    41,    42,    43,   176,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    -1,
      16,    18,    -1,    19,    20,    21,    -1,    -1,    25,    26,
      27,    28,    29,    30,    16,    -1,    -1,    19,    20,    21,
      16,    -1,    -1,    19,    20,    21,    -1,    -1,    16,    -1,
      46,    19,    20,    21,    16,    -1,    -1,    19,    20,    21,
      40,    41,    42,    43,    46,    40,    41,    42,    43,    -1,
      46,    51,    -1,    -1,    -1,    -1,    51,    -1,    46,    39,
      40,    41,    42,    43,    46,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    40,    41,    42,    43,    48,
      40,    41,    42,    43,    -1,    -1,    51,    40,    41,    42,
      43,    51,    40,    41,    42,    43,    -1,    -1,    51,    40,
      41,    42,    43,    51,    40,    41,    42,    43,    -1,    -1,
      51,    40,    41,    42,    43,    51,    40,    41,    42,    43,
      -1,    -1,    51,    40,    41,    42,    43,    51,    40,    41,
      42,    43,    -1,    -1,    51,    40,    41,    42,    43,    51,
      40,    41,    42,    43,    -1,    -1,    51,    40,    41,    42,
      43,    51,    40,    41,    42,    43,    -1,    -1,    51,    40,
      41,    42,    43,    51,    40,    41,    42,    43,    -1,    -1,
      51,    40,    41,    42,    43,    51,    40,    41,    42,    43,
      -1,    -1,    51,    40,    41,    42,    43,    51,    40,    41,
      42,    43,    -1,    -1,    51,    40,    41,    42,    43,    51,
      40,    41,    42,    43,    -1,    -1,    51,    40,    41,    42,
      43,    51,    39,    40,    41,    42,    43,    50,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    40,    41,
      42,    43,    40,    41,    42,    43,    48,    -1,    46,    40,
      41,    42,    43,    -1,    -1,    46,    39,    40,    41,    42,
      43,    40,    41,    42,    43,    40,    41,    42,    43,    40,
      41,    42,    43
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    53,     0,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    18,    25,    26,    27,    28,    29,    30,
      54,    55,    58,    45,    45,    45,    45,    45,    45,    45,
      45,    45,    45,    17,    47,    47,    47,    47,     3,    38,
      40,    47,    64,    67,    67,    67,    67,    67,    40,    64,
      59,    60,    61,    62,    49,    33,    47,    64,    65,    17,
      17,    65,    64,    64,    67,    64,    40,    41,    42,    43,
      40,    41,    42,    43,    51,    51,    51,    51,    51,    64,
      42,    51,    58,    63,    63,    63,    58,     6,    64,    65,
      64,    65,    34,    35,    36,    37,    38,    39,    31,    32,
      48,    48,    48,    48,    51,    48,    64,    64,    64,    67,
      64,    67,    67,    64,    64,    64,    67,    67,    64,    64,
      64,    64,    63,    57,    57,    57,    57,    45,    50,    48,
      64,    64,    64,    64,    64,    64,    65,    65,    64,    57,
      57,    51,    51,    57,    57,    16,    19,    20,    21,    46,
      56,    46,    46,    46,    17,    51,    46,    46,    64,    67,
      46,    46,    67,    38,    66,    45,    46,    64,    57,    51,
      64,    22,    39,    46,    64,    51,    23,    24,    57,    64,
      67,    38,    40,    64,    68,    46,    51,    46,    64,    64,
      68,    42,    40,    41,    42,    43,    46,    64,    51,    64,
      68,    68,    68,    64,    64,    51,    64,    64,    51,    51,
      64,    64,    51,    51,    64,    64,    39,    51,    64,    51,
      64,    51,    64,    51,    64,    51,    64,    51,    64,    39
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 3:
#line 142 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { }
    break;

  case 4:
#line 143 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { }
    break;

  case 5:
#line 144 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { }
    break;

  case 6:
#line 147 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
       ExecStack.push(execute);
       StatementStack.push(condition);
       if (execute) {
	 bool defined_var = false;
	 for (int i=0; i<nbvariables; i++) {
	   if ( strcmp(variables[i].name,(yyvsp[(3) - (4)].str)) ) {
	     defined_var = true;
	     break;
	   }
	 }
	 if (defined_var) {
	   execute = true;
	 } else {
	   execute = false;
	 }
       }
      }
    break;

  case 7:
#line 165 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	ExecStack.push(execute);
	StatementStack.push(condition);
	if (execute) {
	  bool defined_var = false;
	  for (int i=0; i<nbvariables; i++) {
	    if ( strcmp(variables[i].name,(yyvsp[(3) - (4)].str)) ) {
	      defined_var = true;
	      break;
	    }
	  }
	  if (!defined_var) {
	    execute = true;
	  } else {
	    execute = false;
	  }
	}
      }
    break;

  case 8:
#line 183 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	ExecStack.push(execute);
	StatementStack.push(condition);
	if (execute) {
	  execute = (yyvsp[(3) - (4)].integer);
	}
      }
    break;

  case 9:
#line 190 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	if (ExecStack.top()) { // This means that the IF THEN ELSE statment is not to completly ignore
          execute = ! execute;
	}
      }
    break;

  case 10:
#line 195 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	if (StatementStack.top()==loop) {
	  fferr(1) << __FILE__ << ':' << __LINE__
		   << ": Not implemented\n";
	  std::exit(1);
// 	  if (execute)
// 	    fseek(yyin, StreamPosStack.top(),SEEK_SET);
// 	  StreamPosStack.pop();
	}
	execute = ExecStack.top();
	StatementStack.pop();
	ExecStack.pop();
      }
    break;

  case 11:
#line 208 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	ExecStack.push(execute);
	StatementStack.push(loop);
	if (execute) {
	  StreamPosStack.push((yyvsp[(1) - (4)].stream_pos)); // To go back to position in file.
	  execute = (yyvsp[(3) - (4)].integer);
	}
      }
    break;

  case 12:
#line 219 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
                if (execute) {
		  std::string varname = (yyvsp[(2) - (5)].str);
		  int varnum = 0;

		  bool found = false;
		  for (int i=0; i<nbvariables; i++)
		    if (varname == variables[i].name) {
		      found = true;
		      varnum = i;
		      if (variables[i].type == 1) { // var was a string
			free(variables[i].val.s);
		      }
		      break;
		    }
		  if (!found) {
		    varnum = nbvariables;
		    nbvariables++;
		    variables = (variable*)realloc(variables, nbvariables);
		  }
		  variables[varnum].name = new char[strlen((yyvsp[(2) - (5)].str))+1];
		  strcpy(variables[varnum].name, (yyvsp[(2) - (5)].str));
		  variables[varnum].val.d = (yyvsp[(4) - (5)].val);
                }
              }
    break;

  case 13:
#line 244 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	      if (execute) {
		declarevar((yyvsp[(2) - (7)].str), (yyvsp[(6) - (7)].str)); // not complete
		std::cerr << __FILE__ << ':' << __LINE__
		     << ':' << "Incorrect implementation\n";
		std::exit(1);
	      }
	    }
    break;

  case 14:
#line 256 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	       for (size_t i=0; i<3; ++i)
		 (yyval.vect4)[i] = (yyvsp[(5) - (6)].vect)[i];
	       (yyval.vect4)[3] = 0;
	     }
    break;

  case 15:
#line 262 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
	       for (size_t i=0; i<4; ++i)
		 (yyval.vect4)[i] = (yyvsp[(5) - (6)].vect4)[i];
	     }
    break;

  case 16:
#line 270 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    (yyval.trans.number) = 0;
		    (yyval.trans.hasRef) = false;
		    (yyval.trans.inverse) = false;
		    for (size_t i=0; i<4; ++i)
		      (yyval.trans.ref)[i] = 0;
		  }
		}
    break;

  case 17:
#line 280 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    (yyval.trans.number) = (yyvsp[(1) - (2)].trans.number);
		    (yyval.trans.type) = new TransType[((yyval.trans.number))];
		    (yyval.trans.vect)[0] = new real_t[((yyval.trans.number))];
		    (yyval.trans.vect)[1] = new real_t[((yyval.trans.number))];
		    (yyval.trans.vect)[2] = new real_t[((yyval.trans.number))];

		    for (size_t i=0; i<12; ++i)
		      (yyval.trans.mat)[i] = (yyvsp[(1) - (2)].trans.mat)[i];

		    /* copy old modifier values */
		    for (int i=0; i<(yyvsp[(1) - (2)].trans.number); i++) {
		      (yyval.trans.type)[i] = (yyvsp[(1) - (2)].trans.type)[i];
		      (yyval.trans).vect[0][i] = (yyvsp[(1) - (2)].trans).vect[0][i];
		      (yyval.trans).vect[1][i] = (yyvsp[(1) - (2)].trans).vect[1][i];
		      (yyval.trans).vect[2][i] = (yyvsp[(1) - (2)].trans).vect[2][i];
		    }
		    if ((yyvsp[(1) - (2)].trans.number) > 0) {
		      delete[] (yyvsp[(1) - (2)].trans.type); /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] (yyvsp[(1) - (2)].trans.vect)[i];
		    }
		    (yyval.trans.inverse) = (yyvsp[(1) - (2)].trans.inverse);

		    (yyval.trans.hasRef) = true;

		    for (size_t i=0; i<4; ++i)
		      (yyval.trans.ref)[i] = (yyvsp[(2) - (2)].vect4)[i];
		  }
		}
    break;

  case 18:
#line 312 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {

		    TransType* savetype;
		    real_t* savevect[3];
		  
		    savetype = new TransType[(yyval.trans.number)+1];
		    for (int i=0; i<3; i++)
		      savevect[i] = new real_t[(yyval.trans.number)+1];

		    for (int i=0; i<(yyval.trans.number); i++) {
		      savetype[i] = (yyval.trans.type)[i]; /* keeps old values */
		      savevect[0][i] = (yyval.trans.vect)[0][i];
		      savevect[1][i] = (yyval.trans.vect)[1][i];
		      savevect[2][i] = (yyval.trans.vect)[2][i];
		    }

		    if ((yyval.trans.number) > 0) {
		      delete[] (yyval.trans.type); /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] (yyval.trans.vect)[i];
		    }

		    (yyval.trans.type) = new TransType[((yyval.trans.number)+1)];
		    (yyval.trans.vect)[0] = new real_t[((yyval.trans.number)+1)];
		    (yyval.trans.vect)[1] = new real_t[((yyval.trans.number)+1)];
		    (yyval.trans.vect)[2] = new real_t[((yyval.trans.number)+1)];

		    /* restore old values */
		    for (int i=0; i<(yyval.trans.number); i++) {
		      (yyval.trans.type)[i] = savetype[i];
		      (yyval.trans.vect)[0][i] = savevect[0][i];
		      (yyval.trans.vect)[1][i] = savevect[1][i];
		      (yyval.trans.vect)[2][i] = savevect[2][i];
		    }

		    (yyval.trans.hasRef) = (yyvsp[(1) - (3)].trans.hasRef);
		    (yyval.trans.inverse) = (yyvsp[(1) - (3)].trans.inverse);
		    for (size_t i=0; i<4; ++i)
		      (yyval.trans.ref)[i] = (yyvsp[(1) - (3)].trans.ref)[i];

		    delete[] savetype;
		    for (int i=0; i<3; i++)
		      delete [] savevect[i];
		    if (!strcmp ((yyvsp[(2) - (3)].str),"rotate")) {
		      (yyval.trans.type)[(yyval.trans.number)] = rotation;
		    } else if (!strcmp ((yyvsp[(2) - (3)].str),"translate")) {
		      (yyval.trans.type)[(yyval.trans.number)] = translation;
		    } else if (!strcmp ((yyvsp[(2) - (3)].str),"scale")) {
		      (yyval.trans.type)[(yyval.trans.number)] = scale;
		    } else {
		      std::cerr << "parse error unknown "
				<< (yyvsp[(2) - (3)].str) << " check parsing in combo.y\n";
		    }
		    for (int i=0; i<3; i++)
		      (yyval.trans.vect)[i][(yyval.trans.number)] = (yyvsp[(3) - (3)].vect)[i];
		    
		    (yyval.trans.number)++;
		  }
		}
    break;

  case 19:
#line 373 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    (yyval.trans.number) = (yyvsp[(1) - (3)].trans.number);
		    (yyval.trans.type) = new TransType[((yyval.trans.number))];
		    (yyval.trans.vect)[0] = new real_t[((yyval.trans.number))];
		    (yyval.trans.vect)[1] = new real_t[((yyval.trans.number))];
		    (yyval.trans.vect)[2] = new real_t[((yyval.trans.number))];

		    /* copy old modifier values */
		    for (int i=0; i<(yyvsp[(1) - (3)].trans.number); i++) {
		      (yyval.trans.type)[i] = (yyvsp[(1) - (3)].trans.type)[i];
		      (yyval.trans).vect[0][i] = (yyvsp[(1) - (3)].trans).vect[0][i];
		      (yyval.trans).vect[1][i] = (yyvsp[(1) - (3)].trans).vect[1][i];
		      (yyval.trans).vect[2][i] = (yyvsp[(1) - (3)].trans).vect[2][i];
		    }
		    if ((yyvsp[(1) - (3)].trans.number) > 0) {
		      delete[] (yyvsp[(1) - (3)].trans.type); /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] (yyvsp[(1) - (3)].trans.vect)[i];
		    }
		    (yyval.trans.inverse) = (yyvsp[(1) - (3)].trans.inverse);
		    (yyval.trans.hasRef) = (yyvsp[(1) - (3)].trans.hasRef);

		    for (size_t i=0; i<12; ++i)
		      (yyval.trans.mat)[i] = (yyvsp[(3) - (3)].mat)[i];

		    (yyval.trans.type) = new TransType[1];
		    (yyval.trans.type)[0] = matrix;

 		    (yyval.trans.number)=0;
 		  }
		}
    break;

  case 20:
#line 406 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    (yyval.trans.number) = (yyvsp[(1) - (2)].trans.number);
		    (yyval.trans.type) = new TransType[((yyval.trans.number))];
		    (yyval.trans.vect)[0] = new real_t[((yyval.trans.number))];
		    (yyval.trans.vect)[1] = new real_t[((yyval.trans.number))];
		    (yyval.trans.vect)[2] = new real_t[((yyval.trans.number))];

		    /* copy old modifier values */
		    for (int i=0; i<(yyvsp[(1) - (2)].trans.number); i++) {
		      (yyval.trans.type)[i] = (yyvsp[(1) - (2)].trans.type)[i];
		      (yyval.trans).vect[0][i] = (yyvsp[(1) - (2)].trans).vect[0][i];
		      (yyval.trans).vect[1][i] = (yyvsp[(1) - (2)].trans).vect[1][i];
		      (yyval.trans).vect[2][i] = (yyvsp[(1) - (2)].trans).vect[2][i];
		    }
		    if ((yyvsp[(1) - (2)].trans.number) > 0) {
		      delete[] (yyvsp[(1) - (2)].trans.type); /* deallocate memory if needed */
		      for (int i=0; i<3; i++)
			delete[] (yyvsp[(1) - (2)].trans.vect)[i];
		    }
		    (yyval.trans.hasRef) = (yyvsp[(1) - (2)].trans.hasRef);

		    (yyval.trans.inverse) = not((yyvsp[(1) - (2)].trans.inverse));
 		  }		  
		}
    break;

  case 21:
#line 434 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {

		    if ((yyvsp[(6) - (7)].trans.ref)[3] == 0) { // means that the object is not transparent!
		      Vertex c((yyvsp[(3) - (7)].vect)[0], (yyvsp[(3) - (7)].vect)[1], (yyvsp[(3) - (7)].vect)[2]);
		      real_t& r = (yyvsp[(5) - (7)].val);
		      parsetrans& transformations = (yyvsp[(6) - (7)].trans);

		      Shape* s = new Sphere(c,r);
		      (*s).parseTransform(transformations);

		      if ((yyvsp[(6) - (7)].trans.inverse)) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ((yyvsp[(6) - (7)].trans.hasRef)) {
			O->setReference(TinyVector<3>((yyvsp[(6) - (7)].trans.ref)[0],
						      (yyvsp[(6) - (7)].trans.ref)[1],
						      (yyvsp[(6) - (7)].trans.ref)[2]));
		      }

		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
    break;

  case 22:
#line 465 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {

		    if ((yyvsp[(6) - (7)].trans.ref)[3] == 0) { // means that the object is not transparent!
		      real_t& r1 = (yyvsp[(3) - (7)].val);
		      real_t& r2 = (yyvsp[(5) - (7)].val);
		      parsetrans& transformations = (yyvsp[(6) - (7)].trans);

		      Shape* s = new Torus(r1,r2);
		      (*s).parseTransform(transformations);

		      if ((yyvsp[(6) - (7)].trans.inverse)) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
 		      if ((yyvsp[(6) - (7)].trans.hasRef)) {
			O->setReference(TinyVector<3>((yyvsp[(6) - (7)].trans.ref)[0],
						      (yyvsp[(6) - (7)].trans.ref)[1],
						      (yyvsp[(6) - (7)].trans.ref)[2]));
		      }

		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
    break;

  case 23:
#line 496 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    Vertex a((yyvsp[(3) - (7)].vect)[0], (yyvsp[(3) - (7)].vect)[1], (yyvsp[(3) - (7)].vect)[2]);
		    Vertex b((yyvsp[(5) - (7)].vect)[0], (yyvsp[(5) - (7)].vect)[1], (yyvsp[(5) - (7)].vect)[2]);
		    if ((yyvsp[(6) - (7)].trans.ref)[3] == 0) { // means the object is not transparent
		      parsetrans& transformations = (yyvsp[(6) - (7)].trans);

		      Shape* s = new Cube(a,b);
		      (*s).parseTransform(transformations);

		      if ((yyvsp[(6) - (7)].trans.inverse)) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ((yyvsp[(6) - (7)].trans.hasRef)) {
			O->setReference(TinyVector<3>((yyvsp[(6) - (7)].trans.ref)[0],
						      (yyvsp[(6) - (7)].trans.ref)[1],
						      (yyvsp[(6) - (7)].trans.ref)[2]));
		      }

		      /* put the cube in the list */
		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    } else {
		      SB = SceneBox(a, b);
		    }
		  }
		}
    break;

  case 24:
#line 529 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    if ((yyvsp[(8) - (9)].trans.ref)[3] == 0) { // means the object is not transparent
		      Vertex a((yyvsp[(3) - (9)].vect)[0], (yyvsp[(3) - (9)].vect)[1], (yyvsp[(3) - (9)].vect)[2]);
		      Vertex b((yyvsp[(5) - (9)].vect)[0], (yyvsp[(5) - (9)].vect)[1], (yyvsp[(5) - (9)].vect)[2]);
		      real_t radius = (yyvsp[(7) - (9)].val);
		      parsetrans& transformations = (yyvsp[(8) - (9)].trans);

		      Shape* s = new Cylinder(a,b,radius);
		      (*s).parseTransform(transformations);

		      if ((yyvsp[(8) - (9)].trans.inverse)) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ((yyvsp[(8) - (9)].trans.hasRef)) {
			O->setReference( TinyVector<3>((yyvsp[(8) - (9)].trans.ref)[0],
						       (yyvsp[(8) - (9)].trans.ref)[1],
						       (yyvsp[(8) - (9)].trans.ref)[2]));
		      }

		      /* put the cylinder in the list */
		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  } 
		}
    break;

  case 25:
#line 561 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {
		    if ((yyvsp[(8) - (11)].trans.ref)[3] == 0) { // means the object is not transparent
		      Vertex a((yyvsp[(3) - (11)].vect)[0], (yyvsp[(3) - (11)].vect)[1], (yyvsp[(3) - (11)].vect)[2]);
		      Vertex b((yyvsp[(7) - (11)].vect)[0], (yyvsp[(7) - (11)].vect)[1], (yyvsp[(7) - (11)].vect)[2]);
		      real_t radius1 = (yyvsp[(5) - (11)].val);
		      real_t radius2 = (yyvsp[(9) - (11)].val);
		      parsetrans& transformations = (yyvsp[(10) - (11)].trans);

		      Shape* s = new Cone(a,b,radius1, radius2);
		      (*s).parseTransform(transformations);

		      if ((yyvsp[(8) - (11)].trans.inverse)) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ((yyvsp[(10) - (11)].trans.hasRef)) {
			O->setReference( TinyVector<3>((yyvsp[(10) - (11)].trans.ref)[0],
						       (yyvsp[(10) - (11)].trans.ref)[1],
						       (yyvsp[(10) - (11)].trans.ref)[2]));
		      }

		      /* put the cylinder in the list */
		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
    break;

  case 26:
#line 594 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		  if (execute) {

		    if ((yyvsp[(6) - (7)].trans.ref)[3] == 0) { // means that the object is not transparent!
		      Vertex c((yyvsp[(3) - (7)].vect)[0], (yyvsp[(3) - (7)].vect)[1], (yyvsp[(3) - (7)].vect)[2]);
		      real_t& r = (yyvsp[(5) - (7)].val);
		      parsetrans& transformations = (yyvsp[(6) - (7)].trans);

		      Shape* s = new Plane(c,r);
		      (*s).parseTransform(transformations);

		      if ((yyvsp[(6) - (7)].trans.inverse)) {
			s = new Not(new Object(s));
		      }

		      Object* O = new Object(s);
		      if ((yyvsp[(6) - (7)].trans.hasRef)) {
			O->setReference( TinyVector<3>((yyvsp[(6) - (7)].trans.ref)[0],
						       (yyvsp[(6) - (7)].trans.ref)[1],
						       (yyvsp[(6) - (7)].trans.ref)[2]));
		      }

		      if (BoolOp) {
			OpStack.top().push_back(O);
		      } else {
			pScene->add(O);
		      }
		    }
		  }
		}
    break;

  case 27:
#line 624 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
    break;

  case 28:
#line 632 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
		 if (execute) {
		   if ((yyvsp[(5) - (6)].trans.ref)[3] == 0) { // means the object is not transparent
		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Union* u = new Union();
		     for (size_t i=0; i<OpStack.top().size(); i++)
		       u->push_back(OpStack.top()[i]);
		     OpStack.pop();

		     parsetrans& transformations = (yyvsp[(5) - (6)].trans);
		     u->parseTransform(transformations);

		     Object* O = new Object(u);
		     if ((yyvsp[(5) - (6)].trans.hasRef)) {
		       O->setReference(TinyVector<3>((yyvsp[(5) - (6)].trans.ref)[0],
						    (yyvsp[(5) - (6)].trans.ref)[1],
						    (yyvsp[(5) - (6)].trans.ref)[2]));
		     }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
    break;

  case 29:
#line 660 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
    break;

  case 30:
#line 668 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
		 if (execute) {
		   if ((yyvsp[(5) - (6)].trans.ref)[3] == 0) { // means the object is not transparent
		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Intersection* I = new Intersection();

		     for (size_t i=0; i<OpStack.top().size(); i++)
		       I->push_back(OpStack.top()[i]);
		     OpStack.pop();

		     parsetrans& transformations = (yyvsp[(5) - (6)].trans);
		     (*I).parseTransform(transformations);

		     Object* O = new Object(I);
		      if ((yyvsp[(5) - (6)].trans.hasRef)) {
			O->setReference( TinyVector<3>((yyvsp[(5) - (6)].trans.ref)[0],
						       (yyvsp[(5) - (6)].trans.ref)[1],
						       (yyvsp[(5) - (6)].trans.ref)[2]));
		      }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
    break;

  case 31:
#line 697 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
    break;

  case 32:
#line 705 "/home/delpino/src/ff3d/language/parse.pov.yy"
    {
		 if (execute) {
		   if ((yyvsp[(5) - (6)].trans.ref)[3] == 0) { // means the object is not transparent
		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Difference* d = new Difference();
	
		     for (size_t i=0; i<OpStack.top().size(); i++) {
		       (*d).push_back(OpStack.top()[i]);
		     }
		     OpStack.pop();

		     parsetrans& transformations = (yyvsp[(5) - (6)].trans);
		     (*d).parseTransform(transformations);

		     Object* O = new Object(d);
		     if ((yyvsp[(5) - (6)].trans.hasRef)) {
		       O->setReference(TinyVector<3>((yyvsp[(5) - (6)].trans.ref)[0],
						     (yyvsp[(5) - (6)].trans.ref)[1],
						     (yyvsp[(5) - (6)].trans.ref)[2]));
		     }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
    break;

  case 33:
#line 735 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // object is treated like an union limited to one object
#ifdef    __GNUC__
#warning REWORK OBJECT TREATMENT
#endif // __GNUC__
		        if (execute) {
			  BoolOpStack.push(BoolOp);     // store previous context
			  OpStack.push(std::vector<ReferenceCounting<Object> >()); // start new vetor
		          BoolOp = true;
			}
	              }
    break;

  case 34:
#line 746 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
		 if (execute) {
		   if ((yyvsp[(5) - (6)].trans.ref)[3] == 0) { // means the object is not transparent

		     BoolOp = BoolOpStack.top(); // revert to previous context
		     BoolOpStack.pop();
		     Union* u = new Union(); 
		     for (size_t i=0; i<OpStack.top().size(); i++)
		       (*u).push_back(OpStack.top()[i]);
		     OpStack.pop();

		     parsetrans& transformations = (yyvsp[(5) - (6)].trans);
		     (*u).parseTransform(transformations);

		     Object* O = new Object(u);
		     if ((yyvsp[(5) - (6)].trans.hasRef)) {
		       O->setReference(TinyVector<3>((yyvsp[(5) - (6)].trans.ref)[0],
						     (yyvsp[(5) - (6)].trans.ref)[1],
						     (yyvsp[(5) - (6)].trans.ref)[2]));
		     }

		     if (BoolOp) {
		       OpStack.top().push_back(O);
		     } else {
		       pScene->add(O);
		     }
		   }
		 }
	      }
    break;

  case 37:
#line 782 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = (yyvsp[(1) - (1)].val);         }
    break;

  case 38:
#line 783 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = (yyvsp[(1) - (3)].val) + (yyvsp[(3) - (3)].val);    }
    break;

  case 39:
#line 784 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = (yyvsp[(1) - (3)].val) - (yyvsp[(3) - (3)].val);    }
    break;

  case 40:
#line 785 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = (yyvsp[(1) - (3)].val) * (yyvsp[(3) - (3)].val);    }
    break;

  case 41:
#line 786 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = (yyvsp[(1) - (3)].val) / (yyvsp[(3) - (3)].val);    }
    break;

  case 42:
#line 787 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = (yyvsp[(2) - (3)].val) ;        }
    break;

  case 43:
#line 788 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.val) = -(yyvsp[(2) - (2)].val);        }
    break;

  case 44:
#line 791 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = (fabs((yyvsp[(1) - (1)].val))>1E-6); }
    break;

  case 45:
#line 792 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = ((yyvsp[(1) - (3)].val) > (yyvsp[(3) - (3)].val));       }
    break;

  case 46:
#line 793 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = ((yyvsp[(1) - (3)].val) < (yyvsp[(3) - (3)].val));       }
    break;

  case 47:
#line 794 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = ((yyvsp[(1) - (3)].val) >= (yyvsp[(3) - (3)].val));      }
    break;

  case 48:
#line 795 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = ((yyvsp[(1) - (3)].val) <= (yyvsp[(3) - (3)].val));      }
    break;

  case 49:
#line 796 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = ((yyvsp[(1) - (3)].val) == (yyvsp[(3) - (3)].val));      }
    break;

  case 50:
#line 797 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = ((yyvsp[(1) - (3)].val) != (yyvsp[(3) - (3)].val));      }
    break;

  case 51:
#line 798 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = (yyvsp[(1) - (3)].integer) && (yyvsp[(3) - (3)].integer);        }
    break;

  case 52:
#line 799 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = (yyvsp[(1) - (3)].integer) || (yyvsp[(3) - (3)].integer);        }
    break;

  case 53:
#line 800 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = !((yyvsp[(2) - (2)].integer));           }
    break;

  case 54:
#line 801 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { (yyval.integer) = (yyvsp[(2) - (3)].integer);              }
    break;

  case 55:
#line 808 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
                                             (yyval.mat)[ 0] = (yyvsp[(2) - (25)].val);
					     (yyval.mat)[ 1] = (yyvsp[(4) - (25)].val);
					     (yyval.mat)[ 2] = (yyvsp[(6) - (25)].val);
                                             (yyval.mat)[ 3] = (yyvsp[(8) - (25)].val);
					     (yyval.mat)[ 4] = (yyvsp[(10) - (25)].val);
					     (yyval.mat)[ 5] = (yyvsp[(12) - (25)].val);
                                             (yyval.mat)[ 6] = (yyvsp[(14) - (25)].val);
					     (yyval.mat)[ 7] = (yyvsp[(16) - (25)].val);
					     (yyval.mat)[ 8] = (yyvsp[(18) - (25)].val);
                                             (yyval.mat)[ 9] = (yyvsp[(20) - (25)].val);
					     (yyval.mat)[10] = (yyvsp[(22) - (25)].val);
					     (yyval.mat)[11] = (yyvsp[(24) - (25)].val);
              }
    break;

  case 56:
#line 824 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
                                             (yyval.vect)[0] = (yyvsp[(2) - (7)].val);
					     (yyval.vect)[1] = (yyvsp[(4) - (7)].val);
					     (yyval.vect)[2] = (yyvsp[(6) - (7)].val);
              }
    break;

  case 57:
#line 829 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // opposit vector
                                             (yyval.vect)[0] = - (yyvsp[(2) - (2)].vect)[0];
					     (yyval.vect)[1] = - (yyvsp[(2) - (2)].vect)[1];
					     (yyval.vect)[2] = - (yyvsp[(2) - (2)].vect)[2];	        
              }
    break;

  case 58:
#line 834 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // difference between 2 vectors
                                             (yyval.vect)[0] = (yyvsp[(1) - (3)].vect)[0] - (yyvsp[(3) - (3)].vect)[0];
					     (yyval.vect)[1] = (yyvsp[(1) - (3)].vect)[1] - (yyvsp[(3) - (3)].vect)[1];
					     (yyval.vect)[2] = (yyvsp[(1) - (3)].vect)[2] - (yyvsp[(3) - (3)].vect)[2];
              }
    break;

  case 59:
#line 839 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // sum of 2 vectors
                                             (yyval.vect)[0] = (yyvsp[(1) - (3)].vect)[0] + (yyvsp[(3) - (3)].vect)[0];
					     (yyval.vect)[1] = (yyvsp[(1) - (3)].vect)[1] + (yyvsp[(3) - (3)].vect)[1];
					     (yyval.vect)[2] = (yyvsp[(1) - (3)].vect)[2] + (yyvsp[(3) - (3)].vect)[2];
              }
    break;

  case 60:
#line 844 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // product by a scalar on right
                                             (yyval.vect)[0] = (yyvsp[(1) - (3)].vect)[0] * (yyvsp[(3) - (3)].val);
					     (yyval.vect)[1] = (yyvsp[(1) - (3)].vect)[1] * (yyvsp[(3) - (3)].val);
					     (yyval.vect)[2] = (yyvsp[(1) - (3)].vect)[2] * (yyvsp[(3) - (3)].val);
              }
    break;

  case 61:
#line 849 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // division by a scalar on right
                                             (yyval.vect)[0] = (yyvsp[(1) - (3)].vect)[0] / (yyvsp[(3) - (3)].val);
					     (yyval.vect)[1] = (yyvsp[(1) - (3)].vect)[1] / (yyvsp[(3) - (3)].val);
					     (yyval.vect)[2] = (yyvsp[(1) - (3)].vect)[2] / (yyvsp[(3) - (3)].val);
              }
    break;

  case 62:
#line 854 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // product by a scalar on left
                                             (yyval.vect)[0] = (yyvsp[(3) - (3)].vect)[0] * (yyvsp[(1) - (3)].val);
					     (yyval.vect)[1] = (yyvsp[(3) - (3)].vect)[1] * (yyvsp[(1) - (3)].val);
					     (yyval.vect)[2] = (yyvsp[(3) - (3)].vect)[2] * (yyvsp[(1) - (3)].val);
              }
    break;

  case 63:
#line 861 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { 
                                             (yyval.vect4)[0] = (yyvsp[(2) - (9)].val);
					     (yyval.vect4)[1] = (yyvsp[(4) - (9)].val);
					     (yyval.vect4)[2] = (yyvsp[(6) - (9)].val);
					     (yyval.vect4)[3] = (yyvsp[(8) - (9)].val);
              }
    break;

  case 64:
#line 867 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // opposit vector4
  	                                   for (size_t i=0; i<4; ++i)
                                             (yyval.vect4)[i] = - (yyvsp[(2) - (2)].vect4)[i];
              }
    break;

  case 65:
#line 871 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // difference between 2 vector4s
  	                                   for (size_t i=0; i<4; ++i)
                                             (yyval.vect4)[i] = (yyvsp[(1) - (3)].vect4)[i] - (yyvsp[(3) - (3)].vect4)[i];
              }
    break;

  case 66:
#line 875 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // sum of 2 vector4s
  	                                   for (size_t i=0; i<4; ++i)
                                             (yyval.vect4)[i] = (yyvsp[(1) - (3)].vect4)[i] + (yyvsp[(3) - (3)].vect4)[i];
              }
    break;

  case 67:
#line 879 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // product by a scalar on right
  	                                   for (size_t i=0; i<4; ++i)
                                             (yyval.vect4)[i] = (yyvsp[(1) - (3)].vect4)[i] * (yyvsp[(3) - (3)].val);
              }
    break;

  case 68:
#line 883 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // division by a scalar on right
  	                                   for (size_t i=0; i<4; ++i)
                                             (yyval.vect4)[i] = (yyvsp[(1) - (3)].vect4)[i] / (yyvsp[(3) - (3)].val);
              }
    break;

  case 69:
#line 887 "/home/delpino/src/ff3d/language/parse.pov.yy"
    { // product by a scalar on left
  	                                   for (size_t i=0; i<4; ++i)
                                             (yyval.vect4)[i] = (yyvsp[(3) - (3)].vect4)[i] * (yyvsp[(1) - (3)].val);
              }
    break;


/* Line 1267 of yacc.c.  */
#line 2674 "parse.pov.cc"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 892 "/home/delpino/src/ff3d/language/parse.pov.yy"


     
void yyerror(char * s) {
   throw ErrorHandler("PARSED FILE",povlexer->lineno(),
		      stringify(s)+"'"+povlexer->YYText()+"' unexpected",
		      ErrorHandler::compilation);
}
void declarevar (char* name, char* val) {
  bool found = false;
  int varnum = 0;

  std::string expendval = val;
  // Substitute variables value in varname.
  expend(expendval);

  for (int i=0; i<nbvariables; ++i)
    if (strcmp(variables[i].name, name) == 0) {
      // The Variable's value is going to change
      found = true;
      delete [] variables[i].val.s;
      varnum = i;
    }
  if (!found) {
    // storing new variable
    varnum = nbvariables;
    nbvariables++;
    variables = (variable*)realloc(variables, nbvariables);
    variables[varnum].name = new char[strlen(name)+1];
    strcpy(variables[varnum].name, name);
  }
  // Affecting value
  variables[varnum].val.s = new char[expendval.size() + 1];
  strcpy(variables[varnum].val.s, expendval.c_str());
}

void expend(std::string& expendval) {
  int i=0;
  while (i<nbvariables) {
    std::string var = variables[i].name;
    std::string val = variables[i].val.s;
    int begining = expendval.find_first_of(variables[i].name);
    if (begining >= 0) {
      bool replace = true;
      if (begining > 0) {
	replace = !isalnum(expendval[begining-1]);
      }
      replace = (replace) && isalnum(expendval[begining+var.size()]);
      if (replace) {

	std::stringstream tempstr;
	for (int i=0; i<begining; i++)
	  tempstr << expendval[i];
	tempstr << val;
	for (size_t i=begining+var.size()-1; i < expendval.size(); i++)
	  tempstr << expendval[i];
	tempstr << std::ends;

	expendval = tempstr.str();
      }

    } else {
      i++;
    }
  }
}



