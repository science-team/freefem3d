//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionSpectral.cpp,v 1.2 2007/04/29 19:19:36 yakoubix Exp $

#include <FunctionExpressionSpectral.hpp>

#include <MeshExpression.hpp>
#include <Information.hpp>

#include <SpectralFunction.hpp>
#include <ScalarFunctionBase.hpp>

#include <Mesh.hpp>

#include <ErrorHandler.hpp>

void FunctionExpressionSpectral::
execute()
{
  __mesh->execute();

  Information::instance().setMesh(__mesh->mesh());

  if (__mesh->mesh()->type() != Mesh::spectralMesh) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot built spectral function on non spectral mesh",
		       ErrorHandler::normal);
  }

  const Mesh* mesh = __mesh->mesh();
  ConstReferenceCounting<SpectralMesh> smesh
    = dynamic_cast<const SpectralMesh*>(mesh);

  SpectralFunction* s = new SpectralFunction(smesh);
  if (__functionExpression != 0) {
    __functionExpression->execute();

    (*s) = *(__functionExpression->function());
  }


  __scalarFunction = s;

  // function has now been evaluated.
  __functionExpression = 0;
  Information::instance().unsetMesh();
}

FunctionExpressionSpectral::
FunctionExpressionSpectral(ReferenceCounting<MeshExpression> mesh,
			   ReferenceCounting<FunctionExpression> function)
  : FunctionExpression(FunctionExpression::spectral),
    __mesh(mesh),
    __functionExpression(function)
{
  ;
}

FunctionExpressionSpectral::
FunctionExpressionSpectral(const FunctionExpressionSpectral& spectralFunction)
  : FunctionExpression(spectralFunction),
    __mesh(spectralFunction.__mesh),
    __functionExpression(spectralFunction.__functionExpression)
{
  ;
}

FunctionExpressionSpectral::
~FunctionExpressionSpectral()
{
  ;
}
