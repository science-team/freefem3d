//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDESystemExpression.hpp,v 1.4 2007/06/09 10:37:08 delpinux Exp $

#ifndef PDE_SYSTEM_EXPRESSION_HPP
#define PDE_SYSTEM_EXPRESSION_HPP

#include <Expression.hpp>
#include <Variable.hpp>

#include <ProblemExpression.hpp>

#include <PDEProblemExpression.hpp>


/**
 * @file   PDESystemExpression.hpp
 * @author Stephane Del Pino
 * @date   Sat Mar 11 18:54:22 2006
 * 
 * @brief This class allows manipulation of systems of PDEs and the
 * associated boundary conditions
 * 
 */
class PDESystem;
class PDESystemExpression
  : public ProblemExpression
{
private:
  ReferenceCounting<PDESystem> __pdeSystem; /**< The PDE system */

  typedef std::list<ReferenceCounting<PDEProblemExpression> > SystemList;
  SystemList __system;		/**< The system of PDE problems */

  /** 
   * Allows to write a PDESystemExpression
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    for (SystemList::const_iterator i = __system.begin(); i!= __system.end();
	 ++i)
      os << (*(*i));
    return os;
  }

public:
  /** 
   * Checks if the PDESystemExpression contains POVRay references. If
   * it does the execution is stopped. This verification is required
   * by standard FEM.
   */
  bool hasPOVBoundary() const;

  /** 
   * Returns the number of equations of the system
   * 
   * @return the system size
   */
  size_t size() const
  {
    return __system.size();
  }

  /** 
   * Access to the PDE system
   * 
   * @return the PDE system
   */
  ReferenceCounting<PDESystem> pdeSystem();

  /** 
   * Execute the expression
   * 
   */
  void execute();

  /** 
   * Addes a PDE problem to the system
   * 
   * @param p the given PDE problem
   */
  void add(ReferenceCounting<PDEProblemExpression> p)
  {
    __system.push_back(p);
  }

  /** 
   * The default constructor
   * 
   */    
  PDESystemExpression();

  /** 
   * The copy constructor
   * 
   * @param S a given PDE system
   * 
   */
  PDESystemExpression(const PDESystemExpression& S);

  /** 
   * The destructor
   * 
   */
  ~PDESystemExpression();
};

#endif // PDE_SYSTEM_EXPRESSION_HPP
