//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SolverOptionsExpression.hpp,v 1.2 2003/05/04 18:09:01 delpinux Exp $

#ifndef _SOLVEROPTIONS_EXPRESSION_HPP_
#define _SOLVEROPTIONS_EXPRESSION_HPP_

#include <Expression.hpp>
#include <OptionExpression.hpp>

#include <list>
/*!
  \class SolverOptionsExpression

  This class defines the base class of SolverOptions expressions.

  \author Stephane Del Pino
 */
class SolverOptionsExpression
  : public Expression
{
private:
  typedef std::list<ReferenceCounting<OptionExpression> > OptionList;

  OptionList __options;

  std::ostream& put(std::ostream& os) const
  {
    for(OptionList::const_iterator i = __options.begin();
	i != __options.end(); ++i) {
      os << (*(*i)) << '\n';
    }
    return os;
  }

public:

  void execute()
  {
    for(OptionList::iterator i = __options.begin();
	i != __options.end(); ++i) {
      (*(*i)).execute();
    }
  }
  void add(ReferenceCounting<OptionExpression> o)
  {
    __options.push_back(o);
  }

  SolverOptionsExpression(const SolverOptionsExpression& soe)
    : Expression(soe),
      __options(soe.__options)
  {
    ;
  }

  SolverOptionsExpression()
    : Expression(Expression::solverOptions)
  {
    ;
  }

  ~SolverOptionsExpression()
  {
    ;
  }
};

#endif // _SOLVEROPTIONS_EXPRESSION_HPP_

