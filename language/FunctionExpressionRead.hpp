//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionRead.hpp,v 1.2 2007/06/10 14:59:09 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_READ_HPP
#define FUNCTION_EXPRESSION_READ_HPP

#include <FunctionExpression.hpp>

class FileDescriptor;
class StringExpression;
class RealExpression;
class MeshExpression;
/**
 * @file   FunctionExpressionRead.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 00:53:00 2006
 * 
 * @brief  Describes function expression readers
 * 
 */

class FunctionExpressionRead
  : public FunctionExpression
{
private:
  ReferenceCounting<FileDescriptor> __fileDescriptor; /**< file descriptor */

  ReferenceCounting<StringExpression> __fileName; /**< file name */

  ReferenceCounting<StringExpression> __functionName; /**< function name */

  ReferenceCounting<RealExpression>   __componentNumber; /**< component numberxo */

  ReferenceCounting<MeshExpression>  __mesh; /**< mesh associated */

public:
  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param descriptor file descriptor
   * @param fileName file name
   * @param mesh the mesh
   */
  FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
			 ReferenceCounting<StringExpression> fileName,
			 ReferenceCounting<MeshExpression> mesh);

  /** 
   * Constructor
   * 
   * @param descriptor file descriptor
   * @param fileName file name
   * @param functionName function name
   * @param mesh the mesh
   */
  FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
			 ReferenceCounting<StringExpression> fileName,
			 ReferenceCounting<StringExpression> functionName,
			 ReferenceCounting<MeshExpression> mesh);

  /** 
   * Constructor
   * 
   * @param descriptor file descriptor
   * @param fileName file name
   * @param functionName function name
   * @param componentNumber component number
   * @param mesh the mesh
   */
  FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
			 ReferenceCounting<StringExpression> fileName,
			 ReferenceCounting<StringExpression> functionName,
			 ReferenceCounting<RealExpression> componentNumber,
			 ReferenceCounting<MeshExpression> mesh);

  /** 
   * Constructor
   * 
   * @param descriptor file descriptor
   * @param fileName file name
   * @param functionName function name
   * @param componentNumber component number
   * @param mesh the mesh
   */
  FunctionExpressionRead(ReferenceCounting<FileDescriptor> descriptor,
			 ReferenceCounting<StringExpression> fileName,
			 ReferenceCounting<RealExpression> componentNumber,
			 ReferenceCounting<MeshExpression> mesh);

  /** 
   * Copy constructor
   * 
   * @param f given function expression
   */
  FunctionExpressionRead(const FunctionExpressionRead& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionRead();
};

#endif // FUNCTION_EXPRESSION_READ_HPP
