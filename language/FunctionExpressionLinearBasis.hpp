//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionLinearBasis.hpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_LINEAR_BASIS_HPP
#define FUNCTION_EXPRESSION_LINEAR_BASIS_HPP

#include <FunctionExpression.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   FunctionExpressionLinearBasis.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul 18 00:05:24 2006
 * 
 * @brief  basis linear function \f$(x,y \mbox{or} z)\f$
 * 
 */
class FunctionExpressionLinearBasis
  : public FunctionExpression
{
public:
  enum BasisType {
    x,
    y,
    z
  };

private:
  FunctionExpressionLinearBasis::BasisType __basisType;	/**< type of the basis function */

public:
  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param name name of the linear basis function
   * 
   */
  FunctionExpressionLinearBasis(const std::string& name);

  /** 
   * Copy constructor
   * 
   * @param f original expression
   * 
   */
  FunctionExpressionLinearBasis(const FunctionExpressionLinearBasis& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionLinearBasis();
};

#endif // FUNCTION_EXPRESSION_LINEAR_BASIS_HPP
