//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: EmbededFunctions.cpp,v 1.4 2007/03/30 18:25:52 delpinux Exp $

#include <EmbededFunctions.hpp>
// unary operators

real_t unaryMinus(real_t x)
{
  return -x;
}

bool not_(bool a)
{
  return not(a);
}

// binary operators
real_t modulo(real_t x, real_t y)
{
  int a = static_cast<int>(x);
  int b = static_cast<int>(y);
  return a%b;
}

real_t sum(real_t x, real_t y)
{
  return x+y;
}

real_t difference(real_t x, real_t y)
{
  return x-y;
}

real_t division(real_t x, real_t y)
{
  return x/y;
}

real_t product(real_t x, real_t y)
{
  return x*y;
}

// Binary Bool operators.
bool gt(real_t x, real_t y)
{
  return (x > y);
}

bool lt(real_t x, real_t y)
{
  return (x < y);
}

bool ge(real_t x, real_t y)
{
  return (x >= y);
}

bool le(real_t x, real_t y)
{
  return (x <= y);
}

bool eq(real_t x, real_t y)
{
  return (x == y);
}

bool ne(real_t x, real_t y)
{
  return (x != y);
}

bool and_(bool a, bool b)
{
  return (a and b);
}

bool or_(bool a, bool b)
{
  return (a or b);
}

bool xor_(bool a, bool b)
{
  return (a xor b);
}

template<>
std::ostream& functionName<std::log>(std::ostream& os) {
  os << "log";
  return os;
}

template<>
std::ostream& functionName<std::sin>(std::ostream& os) {
  os << "sin";
  return os;
}

template<>
std::ostream& functionName<std::cos>(std::ostream& os) {
  os << "cos";
  return os;
}

template<>
std::ostream& functionName<std::tan>(std::ostream& os) {
  os << "tan";
  return os;
}

template<>
std::ostream& functionName<std::asin>(std::ostream& os) {
  os << "asin";
  return os;
}

template<>
std::ostream& functionName<std::acos>(std::ostream& os) {
  os << "acos";
  return os;
}

template<>
std::ostream& functionName<std::atan>(std::ostream& os) {
  os << "atan";
  return os;
}

template<>
std::ostream& functionName<std::exp>(std::ostream& os) {
  os << "exp";
  return os;
}

template<>
std::ostream& functionName<std::abs>(std::ostream& os) {
  os << "abs";
  return os;
}

template<>
std::ostream& functionName<std::sqrt>(std::ostream& os) {
  os << "sqrt";
  return os;
}

template <>
std::ostream& operatorName<gt>(std::ostream& os) {
  os << '>';
  return os;
}

template <>
std::ostream& operatorName<lt>(std::ostream& os) {
  os << '<';
  return os;
}

template <>
std::ostream& operatorName<ge>(std::ostream& os) {
  os << ">=";
  return os;
}
template <>
std::ostream& operatorName<le>(std::ostream& os) {
  os << "<=";
  return os;
}
template <>
std::ostream& operatorName<eq>(std::ostream& os) {
  os << "==";
  return os;
}
template <>
std::ostream& operatorName<ne>(std::ostream& os) {
  os << "!=";
  return os;
}

template <>
std::ostream& binaryBooleanName<and_>(std::ostream& os)
{
  os << "and";
  return os;
}

template <>
std::ostream& binaryBooleanName<or_>(std::ostream& os)
{
  os << "or";
  return os;
}

template <>
std::ostream& binaryBooleanName<xor_>(std::ostream& os)
{
  os << "xor";
  return os;
}

template <>
std::ostream& unaryBooleanName<not_>(std::ostream& os)
{
  os << "not";
  return os;
}
