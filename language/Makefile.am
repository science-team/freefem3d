# -*- Mode: makefile  -*-

#  This file is part of ff3d - http://www.freefem.org/ff3d
#  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id: Makefile.am,v 1.27 2007/06/10 15:00:07 delpinux Exp $

noinst_HEADERS	=					\
	BooleanExpression.hpp				\
	BoundaryConditionExpression.hpp			\
	BoundaryConditionExpressionDirichlet.hpp	\
	BoundaryConditionExpressionFourrier.hpp		\
	BoundaryConditionExpressionNeumann.hpp		\
	BoundaryConditionListExpression.hpp		\
	BoundaryExpression.hpp				\
	BoundaryExpressionList.hpp			\
	BoundaryExpressionPOVRay.hpp			\
	BoundaryExpressionSurfaceMesh.hpp		\
	BoundaryExpressionReferences.hpp		\
	DomainExpression.hpp				\
	DomainExpressionAnalytic.hpp			\
	DomainExpressionSet.hpp				\
	DomainExpressionUndefined.hpp			\
	DomainExpressionVariable.hpp			\
	EmbededFunctions.hpp				\
	Expression.hpp					\
	FFLexer.hpp					\
	FieldExpression.hpp				\
	FieldExpressionList.hpp				\
	FunctionExpression.hpp				\
	FunctionExpressionBinaryOperation.hpp		\
	FunctionExpressionCFunction.hpp			\
	FunctionExpressionComposed.hpp			\
	FunctionExpressionConstant.hpp			\
	FunctionExpressionConvection.hpp		\
	FunctionExpressionDerivative.hpp		\
	FunctionExpressionDomainCharacteristic.hpp	\
	FunctionExpressionFEM.hpp			\
	FunctionExpressionIntegrate.hpp			\
	FunctionExpressionLinearBasis.hpp		\
	FunctionExpressionMeshCharacteristic.hpp	\
	FunctionExpressionMeshReferences.hpp		\
	FunctionExpressionNormalComponent.hpp		\
	FunctionExpressionNot.hpp			\
	FunctionExpressionObjectCharacteristic.hpp	\
	FunctionExpressionRead.hpp			\
	FunctionExpressionSpectral.hpp			\
	FunctionExpressionUnaryMinus.hpp		\
	FunctionExpressionValue.hpp			\
	FunctionExpressionVariable.hpp			\
	Information.hpp					\
	InsideExpression.hpp				\
	InsideListExpression.hpp			\
	Instruction.hpp					\
	IntegratedExpression.hpp			\
	IntegratedOperatorExpression.hpp		\
	Lexer.hpp					\
	LinearExpression.hpp				\
	MeshExpression.hpp				\
	MultiLinearExpression.hpp			\
	MultiLinearExpressionSum.hpp			\
	MultiLinearFormExpression.hpp			\
	OptionExpression.hpp				\
	OFStreamExpression.hpp				\
	OFStreamExpressionUndefined.hpp			\
	OFStreamExpressionValue.hpp			\
	OFStreamExpressionVariable.hpp			\
	OStreamExpression.hpp				\
	OStreamExpressionList.hpp			\
	PDEEquationExpression.hpp			\
	PDEOperatorExpression.hpp			\
	PDEOperatorSumExpression.hpp			\
	PDEProblemExpression.hpp			\
	PDESystemExpression.hpp				\
	POVLexer.hpp					\
	ProblemExpression.hpp				\
	RealExpression.hpp				\
	SceneExpression.hpp				\
	SolutionExpression.hpp				\
	SolverExpression.hpp				\
	SolverOptionsExpression.hpp			\
	StringExpression.hpp				\
	SubOptionExpression.hpp				\
	SubOptionListExpression.hpp			\
	TestFunctionExpressionList.hpp			\
	UnknownExpression.hpp				\
	UnknownExpressionDeclaration.hpp		\
	UnknownExpressionFunction.hpp			\
	UnknownListExpression.hpp			\
	Variable.hpp					\
	VariableLexerRepository.hpp			\
	VariableRepository.hpp				\
	VariationalDirichletListExpression.hpp		\
	VariationalFormulaExpression.hpp		\
	VariationalOperatorExpression.hpp		\
	VariationalProblemExpression.hpp		\
	Vector3Expression.hpp				\
	XMLLexer.hpp					\
	parse.ff.hpp					\
	parse.pov.hpp

noinst_LIBRARIES=				\
	libfflanguage.a				\
	libpovlanguage.a			\
	libxmllanguage.a

libfflanguage_a_SOURCES=				\
	FFLexer.cpp					\
							\
	BooleanExpression.cpp				\
	BoundaryConditionExpression.cpp			\
	BoundaryConditionExpressionDirichlet.cpp	\
	BoundaryConditionExpressionFourrier.cpp		\
	BoundaryConditionExpressionNeumann.cpp		\
	BoundaryConditionListExpression.cpp		\
	BoundaryExpression.cpp				\
	BoundaryExpressionList.cpp			\
	BoundaryExpressionPOVRay.cpp			\
	BoundaryExpressionReferences.cpp		\
	BoundaryExpressionSurfaceMesh.cpp		\
	DomainExpression.cpp				\
	DomainExpressionAnalytic.cpp			\
	DomainExpressionSet.cpp				\
	DomainExpressionUndefined.cpp			\
	DomainExpressionVariable.cpp			\
	EmbededFunctions.cpp				\
	FieldExpression.cpp				\
	FieldExpressionList.cpp				\
	FunctionExpression.cpp				\
	FunctionExpressionBinaryOperation.cpp		\
	FunctionExpressionCFunction.cpp			\
	FunctionExpressionComposed.cpp			\
	FunctionExpressionConstant.cpp			\
	FunctionExpressionConvection.cpp		\
	FunctionExpressionDerivative.cpp		\
	FunctionExpressionDomainCharacteristic.cpp	\
	FunctionExpressionFEM.cpp			\
	FunctionExpressionIntegrate.cpp			\
	FunctionExpressionLinearBasis.cpp		\
	FunctionExpressionMeshCharacteristic.cpp	\
	FunctionExpressionMeshReferences.cpp		\
	FunctionExpressionNormalComponent.cpp		\
	FunctionExpressionNot.cpp			\
	FunctionExpressionObjectCharacteristic.cpp	\
	FunctionExpressionRead.cpp			\
	FunctionExpressionSpectral.cpp			\
	FunctionExpressionUnaryMinus.cpp		\
	FunctionExpressionValue.cpp			\
	FunctionExpressionVariable.cpp			\
	Information.cpp					\
	Instruction.cpp					\
	InsideListExpression.cpp			\
	MeshExpression.cpp				\
	MultiLinearExpression.cpp			\
	MultiLinearExpressionSum.cpp			\
	OFStreamExpressionVariable.cpp			\
	OStreamExpression.cpp                           \
	PDEOperatorExpression.cpp			\
	PDEOperatorSumExpression.cpp			\
	PDEEquationExpression.cpp			\
	PDEProblemExpression.cpp			\
	PDESystemExpression.cpp				\
	RealExpression.cpp				\
	SceneExpression.cpp				\
	SolverExpression.cpp				\
	StringExpression.cpp				\
	SubOptionExpression.cpp				\
	TestFunctionExpressionList.cpp			\
	UnknownExpressionDeclaration.cpp		\
	UnknownExpressionFunction.cpp			\
	UnknownListExpression.cpp			\
	Variable.cpp					\
	VariableLexerRepository.cpp			\
	VariableRepository.cpp				\
	VariationalDirichletListExpression.cpp          \
	VariationalFormulaExpression.cpp		\
	VariationalProblemExpression.cpp		\
	Vector3Expression.cpp				\
	parse.ff.yy 

libpovlanguage_a_SOURCES=			\
	POVLexer.cpp				\
	parse.pov.yy

libxmllanguage_a_SOURCES=			\
	XMLLexer.cpp				\
	parse.xml.yy


CLEANFILES=					\
	parse.ff.cc				\
	parse.ff.h				\
	parse.ff.output				\
	parse.pov.cc				\
	parse.pov.h				\
	parse.pov.output			\
	parse.xml.cc				\
	parse.xml.h				\
	parse.xml.output

BUILT_SOURCES=					\
	parse.ff.cc				\
	parse.ff.h				\
	parse.pov.cc				\
	parse.pov.h				\
	parse.xml.cc				\
	parse.xml.h

parse.ff.cc parse.ff.h: $(srcdir)/parse.ff.yy
	$(YACC) -p ff -o parse.ff.cc --defines="parse.ff.h" -v $<

parse.pov.cc parse.pov.h: $(srcdir)/parse.pov.yy
	$(YACC) -p pov -o parse.pov.cc --defines="parse.pov.h" -v $<

parse.xml.cc parse.xml.h: $(srcdir)/parse.xml.yy
	$(YACC) -p xml -o parse.xml.cc --defines="parse.xml.h" -v $<

