//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionDomainCharacteristic.cpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#include <FunctionExpressionDomainCharacteristic.hpp>
#include <ScalarFunctionDomainCharacteristic.hpp>

#include <DomainExpression.hpp>
#include <Domain.hpp>

std::ostream& FunctionExpressionDomainCharacteristic::
put(std::ostream& os) const
{
  os << "one(" << (*__domain) << ')';
  return os;
}

void FunctionExpressionDomainCharacteristic::
execute()
{
  __domain->execute();
  __scalarFunction = new ScalarFunctionDomainCharacteristic(__domain->domain());
}

FunctionExpressionDomainCharacteristic
::FunctionExpressionDomainCharacteristic(ReferenceCounting<DomainExpression> D)
  : FunctionExpression(FunctionExpression::domainCharacteristic),
    __domain(D)
{
  ;
}

FunctionExpressionDomainCharacteristic::
FunctionExpressionDomainCharacteristic(const FunctionExpressionDomainCharacteristic& f)
  : FunctionExpression(FunctionExpression::domainCharacteristic),
    __domain(f.__domain)
{
  ;
}

FunctionExpressionDomainCharacteristic::
~FunctionExpressionDomainCharacteristic()
{
  ;
}

