//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionExpressionFourrier.hpp,v 1.2 2007/02/08 23:19:38 delpinux Exp $

#ifndef BOUNDARY_CONDITION_EXPRESSION_FOURRIER_HPP
#define BOUNDARY_CONDITION_EXPRESSION_FOURRIER_HPP

#include <BoundaryConditionExpression.hpp>

/**
 * @file   BoundaryConditionExpressionFourrier.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 13:08:34 2006
 * 
 * @brief  This class defines the class of fourrier BoundaryCondition
 * expressions
 */
class BoundaryConditionExpressionFourrier
  : public BoundaryConditionExpression
{
private:
  ReferenceCounting<FunctionExpression>
  __Alpha;			/**< @f$ \alpha @f$ */
  ReferenceCounting<FunctionExpression>
  __g;				/**< @f$ g @f$ */

  /** 
   * Writes the expression to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param unknownName the unknown expression
   * @param alpha @f$ \alpha @f$
   * @param g @f$ g @f$
   * @param boundary the boundary expression
   */
  BoundaryConditionExpressionFourrier(const std::string& unknownName,
				      ReferenceCounting<FunctionExpression> alpha,
				      ReferenceCounting<FunctionExpression> g,
				      ReferenceCounting<BoundaryExpression> boundary);
  /** 
   * Copy constructor
   * 
   * @param f given fourrier expression
   */
  BoundaryConditionExpressionFourrier(const BoundaryConditionExpressionFourrier& f);

  /** 
   * Destructor
   * 
   */
  ~BoundaryConditionExpressionFourrier();
};

#endif // BOUNDARY_CONDITION_EXPRESSION_FOURRIER_HPP
