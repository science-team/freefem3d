//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DomainExpressionVariable.hpp,v 1.2 2007/02/08 23:19:38 delpinux Exp $

#ifndef DOMAIN_EXPRESSION_VARIABLE_HPP
#define DOMAIN_EXPRESSION_VARIABLE_HPP

#include <DomainExpression.hpp>
#include <Variable.hpp>

class DomainExpressionVariable
  : public DomainExpression
{
private:
  const std::string __domainName;
  ReferenceCounting<DomainVariable> __domainVariable;

  std::ostream& put(std::ostream& os) const
  {
    os << __domainName;
    return os;
  }

public:
  void execute();
  ReferenceCounting<DomainExpression> domainExpression() const;

  DomainExpressionVariable(const std::string& domainName);

  DomainExpressionVariable(const DomainExpressionVariable& e);

  ~DomainExpressionVariable();
};

#endif // DOMAIN_EXPRESSION_VARIABLE_HPP
