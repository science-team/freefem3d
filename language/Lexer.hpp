//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Lexer.hpp,v 1.6 2007/06/09 10:37:08 delpinux Exp $

// This is the Lexer replacement to Flex. The reason for changing flex
// is the portability: it is not available at this very moment using CodeWarrior 6.
// This is a base class. Its derived classes will be used for different languages.

#ifndef LEXER_HPP
#define LEXER_HPP

#include <cstring>
#include <map>

#include <StreamCenter.hpp>

#include <cstddef>

/*! 
  \class Lexer
  This class is the base class for all lexers.

  \remark It has been written to replace the use of flex which did not worked
  under MS-Windows (using cygwin). This should make the porting of the code
  easier.

  \author St�phane Del Pino.
*/
class Lexer
{
private:
  //! The maximum autorized length for a "word".
  size_t WordMaxLenght;

  struct eqstr
  {
    bool operator()(const char* s1, const char* s2) const
    {
      return std::strcmp(s1, s2) < 0;
    }
  };

protected:
  //! The input std::istream.
  std::istream& in;

  //! The output std::istream.
  std::ostream& out;

  //! The current number line.
  size_t linenumber;

  //! The current "word".
  std::string yytext;

public:
  /*! a list of known Keyword.
   */
  typedef std::map<const char*, int,
		   Lexer::eqstr> KeyWordList;

protected:
  KeyWordList __keyWordList;

  /*!  This contains the token associated to a type. This is used for
    variables declaration to know its type.
  */
  int declarationToken;
public:

  //! Constructs a Lexer.
  Lexer(std::istream& In,
	std::ostream& Out = std::cout,
	const size_t WMaxL = 255)
    : WordMaxLenght(WMaxL),
      in(In),
      out(Out)      
  {
    //    yytext = new char[WMaxL+1];
    linenumber = 1;
  }

  //! Destructor.
  virtual ~Lexer()
  {
    ;
  }

  //! Interface with parser.
  virtual int yylex() = 0;

  //! Read-only access to the current line number.
  const size_t& lineno() const
  {
    return linenumber;
  }

  //! Read-only access to the current Word
  const char* YYText() const
  {
    return yytext.c_str();
  }
};

#endif // LEXER_HPP
