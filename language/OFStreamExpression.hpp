//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OFStreamExpression.hpp,v 1.3 2007/02/08 23:19:38 delpinux Exp $

#ifndef OFSTREAM_EXPRESSION_HPP
#define OFSTREAM_EXPRESSION_HPP

#include <Expression.hpp>
#include <StringExpression.hpp>
#include <ErrorHandler.hpp>

#include <fstream>

/**
 * @file   OFStreamExpression.hpp
 * @author Stephane Del Pino
 * @date   Fri Dec 29 20:56:43 2006
 * 
 * @brief  Base class for ofstream expression
 */
class OFStreamExpression
  : public Expression
{
public:
  enum Type {
    undefined,
    value,
    variable
  };

protected:
  const Type __type;		/**< type of the expression */
  ReferenceCounting<std::ofstream> __fout;

  virtual std::ostream&
  put(std::ostream & os) const
  {
    return os;
  }

public:

  /** 
   * Access to the file stream pointer
   * 
   * @return __fout
   */
  std::ofstream* ofstream()
  {
    if (__type == undefined) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "using undefined stream",
			 ErrorHandler::normal);
    }
    return __fout;
  }

  OFStreamExpression(const OFStreamExpression::Type& type)
    : Expression(Expression::ofstreamexpression),
      __type(type),
      __fout(0)
  {
    ;
  }

  OFStreamExpression(const OFStreamExpression& ofs)
    : Expression(ofs),
      __type(ofs.__type),
      __fout(ofs.__fout)
  {
    ;
  }

  virtual ~OFStreamExpression()
  {
    ;
  }
};

#endif // OFSTREAM_EXPRESSION_HPP
