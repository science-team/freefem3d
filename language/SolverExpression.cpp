//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SolverExpression.cpp,v 1.24 2007/05/28 18:24:22 yakoubix Exp $

#include <SolverExpression.hpp>

#include <Solver.hpp>
#include <PDEProblem.hpp>
#include <PDESystem.hpp>
#include <SolverDriver.hpp>
#include <BoundaryConditionSet.hpp>

#include <FunctionExpressionValue.hpp>

#include <PDESystemExpression.hpp>
#include <VariationalProblemExpression.hpp>

#include <ScalarFunctionBase.hpp>

#include <FEMSolution.hpp>
#include <LegendreSolution.hpp>

#include <VariationalProblem.hpp>

#include <SceneExpression.hpp>
#include <Scene.hpp>

#include <Structured3DMesh.hpp>
#include <Information.hpp>

#include <DegreeOfFreedomSetBuilder.hpp>

#include <ParameterCenter.hpp>

#include <SpectralFunction.hpp>
#include <FEMFunction.hpp>
#include <FEMFunctionBuilder.hpp>

#include <DomainExpression.hpp>
#include <DomainExpressionSet.hpp>
#include <DomainExpressionVariable.hpp>
#include <DomainExpressionAnalytic.hpp>

#include <VariableRepository.hpp>


std::ostream& SolverExpression::
put(std::ostream& os) const
{
  os << __FILE__ << ':' << __LINE__ << ": NOT IMPLEMENTED\n";
  return os;
}


SolverExpression::
SolverExpression(ReferenceCounting<UnknownListExpression> unknownList,
		 ReferenceCounting<MeshExpression> mesh,
		 ReferenceCounting<SolverOptionsExpression> solverOptions,
		 ReferenceCounting<ProblemExpression> problemExpression,
		 ReferenceCounting<DomainExpression> domainExpression)
  : Expression(Expression::solver),
    __unknownList(unknownList),
    __mesh(mesh),
    __solverOptions(solverOptions),
    __problemExpression(problemExpression),
    __domain(domainExpression)
{
  ;
}

SolverExpression::
~SolverExpression()
{
  ;
}

void SolverExpression::
__setScene(ReferenceCounting<DomainExpression> domainExp) const
{
  switch(domainExp->domainType()) {
  case DomainExpression::set: {
    DomainExpressionSet& D
      = dynamic_cast<DomainExpressionSet&>(*domainExp);

    const SceneExpression& SE
      = dynamic_cast<const SceneExpression&>(*(D.scene()));
    Information::instance().setScene(SE.scene());
    break;
  }
  case DomainExpression::analytic: {
    DomainExpressionAnalytic& D
      = dynamic_cast<DomainExpressionAnalytic&>(*domainExp);

    Information::instance().setScene(D.domain()->scene());
    break;
  }
  case DomainExpression::variable: {
    DomainExpressionVariable& D
      = dynamic_cast<DomainExpressionVariable&>(*domainExp);
    __setScene(D.domainExpression());
    break;
  }
  case DomainExpression::undefined: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "undefined domain",
		       ErrorHandler::normal);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected domain type",
		       ErrorHandler::unexpected);
  }
  }
}

void SolverExpression::
execute()
{
  //! The unknowns
  Information::instance().setUnknownList(__unknownList);

  //! Reset solver options.
  ParameterCenter::instance().reset();

  __mesh->execute();

  //! A mesh is used there. We inform others about that ...
  Information::instance().setMesh(__mesh->mesh());

  __unknownList->execute();

  if (__domain != 0) {
    __domain->execute();
    __setScene(__domain);
  }

  __solverOptions->execute();
  __problemExpression->execute();

  if (__domain != 0) {
    switch (__problemExpression->problemType()) {
    case ProblemExpression::pdeSystem: {
      (*dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem()).setDomain(__domain->domain());
      break;
    }
    case ProblemExpression::variationalProblem: {
      (*dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem()).setDomain(__domain->domain());
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected problem type",
			 ErrorHandler::unexpected);
    }
    }
  } else {
    if (__problemExpression->hasPOVBoundary()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot use POVRay references in standard FEM discretization.\n"
			 "You probably forgot to specify the domain in the 'solve' bloc.\n"
			 "Refere to the Fictitious Domain part of the documentation",
			 ErrorHandler::normal);
    }
  }


  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;
    unknown.execute();

    DiscretizationType discretization
      = DiscretizationType::getDefault(unknown.discretizationType());

    if (discretization.type() == DiscretizationType::functionLike) {
      UnknownExpression& unknown = **__unknownList->begin();
      unknown.execute();

      ReferenceCounting<FunctionVariable> unknownVariable
	= VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

      ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();
      switch (f->type()) {
      case ScalarFunctionBase::femfunction: {
	discretization = dynamic_cast<const FEMFunctionBase&>(*f).discretizationType();
	break;
      }
      case ScalarFunctionBase::spectral: {
	discretization = dynamic_cast<const SpectralFunction&>(*f).discretizationType();
	break;
      }
      default: {
	discretization = DiscretizationType::lagrangianFEM1;
      }
      }
      unknown.setDiscretizationType(discretization.type());
    }
  }

  DiscretizationType discretization
    = DiscretizationType::getDefault((*__unknownList->begin())->discretizationType());

  switch (discretization.type()) {
  case DiscretizationType::lagrangianFEM0:
  case DiscretizationType::lagrangianFEM1:
  case DiscretizationType::lagrangianFEM2: {
    this->__solveFEM(discretization);
    break;
  }
  case DiscretizationType::spectralLegendre: {
    this->__solveLegendre(discretization);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "'"+DiscretizationType::name(discretization)+"' discretization not implemented",
		       ErrorHandler::unexpected);
  }
  }

  //! The unknown are no more there.
  Information::instance().unsetUnknownList();

  //! The mesh is no more used in this region.
  Information::instance().unsetMesh();
}

void SolverExpression::
__solveFEM(const DiscretizationType& discretization)
{
  ReferenceCounting<DegreeOfFreedomSetBuilder> dofBuilder;
  if (__domain != 0) {
    dofBuilder = new DegreeOfFreedomSetBuilder(__unknownList->size(),
					       discretization,
					       *__mesh->mesh(),
					       *__domain->domain());
  } else {
    dofBuilder = new DegreeOfFreedomSetBuilder(__unknownList->size(),
					       discretization,
					       *__mesh->mesh());
  }

  const DegreeOfFreedomSet& degreeOfFreedomSet
    = dofBuilder->degreeOfFreedomSet();

  FEMSolution u(degreeOfFreedomSet);

  typedef
    std::vector<ReferenceCounting<FEMFunctionBase> >
    UnknownList;
  UnknownList UL;
  ConstReferenceCounting<Mesh> mesh = __mesh->mesh();

  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();

    FEMFunctionBuilder femFunctionBuilder;
    femFunctionBuilder.build(discretization, mesh, *f);
    UL.push_back(femFunctionBuilder.getBuiltFEMFunction());
  }

  u.setUserFunction(UL);

  ConstReferenceCounting<Problem> P;
  switch (__problemExpression->problemType()) {
  case ProblemExpression::pdeSystem: {
    P = (PDESystem*)(dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem());
    break;
  }
  case ProblemExpression::variationalProblem: {
    P = (VariationalProblem*)(dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem());
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected problem type",
		       ErrorHandler::unexpected);
  }
  }

  ffout(2) << "Solving Problem:\n" << '\n';
  ffout(2) << "\tUnknowns: " << *__unknownList << '\n';
  ffout(2) << "\tProblem:  " << *__problemExpression << '\n';

  if (__domain != 0) {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fictitiousFEM);
    sd.run();
  } else {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fem);
    sd.run();
  }

  u.getUserFunction(UL);

  UnknownListExpressionSet& UList
    = dynamic_cast<UnknownListExpressionSet&>(*__unknownList);
  size_t i=0;
  for(UnknownListExpressionSet::listType::iterator iunknown = UList.giveList().begin();
      iunknown != UList.giveList().end(); ++iunknown) {
    UnknownExpression& unknown = **iunknown;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    const FEMFunctionBase* value = UL[i];
    (*unknownVariable)
      = new FunctionExpressionValue(value, false);

    ++i;
  }
}

void SolverExpression::
__solveLegendre(const DiscretizationType& discretization)
{
  ReferenceCounting<DegreeOfFreedomSetBuilder> dofBuilder;
  if (__domain != 0) {
    dofBuilder = new DegreeOfFreedomSetBuilder(__unknownList->size(),
					       discretization,
					       *__mesh->mesh(),
					       *__domain->domain());
  } else {
    dofBuilder = new DegreeOfFreedomSetBuilder(__unknownList->size(),
					       discretization,
					       *__mesh->mesh());
  }

  if (__mesh->mesh()->type() != Mesh::spectralMesh) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot use spectral method on '"+__mesh->mesh()->typeName()+"'",
		       ErrorHandler::normal);
  }

  ConstReferenceCounting<SpectralMesh> mesh
    = dynamic_cast<const SpectralMesh*>(static_cast<const Mesh*>(__mesh->mesh()));

  const DegreeOfFreedomSet& degreeOfFreedomSet
    = dofBuilder->degreeOfFreedomSet();

  LegendreSolution u(degreeOfFreedomSet);

  typedef
    std::vector<ReferenceCounting<SpectralFunction> >
    UnknownList;
  UnknownList UL;

  for (UnknownListExpression::iterator i= __unknownList->begin();
       i != __unknownList->end(); ++i) {
    UnknownExpression& unknown = **i;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    ConstReferenceCounting<ScalarFunctionBase> f = unknownVariable->expression()->function();

    UL.push_back(new SpectralFunction(mesh,*f));
  }

  u.setUserFunction(UL);

  ConstReferenceCounting<Problem> P;
  switch (__problemExpression->problemType()) {
  case ProblemExpression::pdeSystem: {
    P = (PDESystem*)(dynamic_cast<PDESystemExpression&>(*__problemExpression).pdeSystem());
    break;
  }
  case ProblemExpression::variationalProblem: {
    P = (VariationalProblem*)(dynamic_cast<VariationalProblemExpression&>(*__problemExpression).variationalProblem());
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected problem type",
		       ErrorHandler::unexpected);
  }
  }

  ffout(2) << "Solving Problem:\n" << '\n';
  ffout(2) << "\tUnknowns: " << *__unknownList << '\n';
  ffout(2) << "\tProblem:  " << *__problemExpression << '\n';

  if (__domain != 0) {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::fictitiousSpectral);
    sd.run();
  } else {
    SolverDriver sd(P, u, discretization,__mesh->mesh(), degreeOfFreedomSet,
		    SolverDriver::spectral);
    sd.run();
  }

  u.getUserFunction(UL);

  UnknownListExpressionSet& UList
    = dynamic_cast<UnknownListExpressionSet&>(*__unknownList);
  size_t i=0;
  for(UnknownListExpressionSet::listType::iterator iunknown = UList.giveList().begin();
      iunknown != UList.giveList().end(); ++iunknown) {
    UnknownExpression& unknown = **iunknown;

    ReferenceCounting<FunctionVariable> unknownVariable
      = VariableRepository::instance().findVariable<FunctionVariable>(unknown.name());

    const SpectralFunction* value = UL[i];
    (*unknownVariable)
      = new FunctionExpressionValue(value, false);

    ++i;
  }
}
