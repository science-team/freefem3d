//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionVariable.hpp,v 1.3 2007/05/20 23:15:36 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_VARIABLE_HPP
#define FUNCTION_EXPRESSION_VARIABLE_HPP

#include <Variable.hpp>
#include <FunctionExpression.hpp>

#include <DiscretizationType.hpp>

/**
 * @file   FunctionExpressionVariable.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 11:44:32 2006
 * 
 * @brief  Manages function expression variables
 * 
 */
class FunctionExpressionVariable
  : public FunctionExpression
{
private:
  /** 
   * writes an expression
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

  const std::string __functionName;

  ReferenceCounting<FunctionVariable>
  __functionVariable;		/**< the function variable */

  DiscretizationType::Type
  __discretizationType;		/**< Variables can be related to
				   discretization types */

public:
  /** 
   * Checks if the variable requires a boundary for evaluation
   * 
   * 
   * @return @b true if __functionVariable requires a boundary
   */
  bool hasBoundaryExpression() const
  {
    return __functionVariable->expression()->hasBoundaryExpression();
  }

  /** 
   * Subscribe the variable
   * 
   */
  void subscribe()
  {
    __functionVariable->subscribe();
  }

  /** 
   * Un-substrcibe the variable
   * 
   */
  void unsubscribe()
  {
    __functionVariable->unsubscribe();
  }

  /** 
   * Read only access to the discretization type
   * 
   * @return __discretizationType
   */
  const DiscretizationType::Type&
  discretizationType() const
  {
    return __discretizationType;
  }

  /** 
   * Read only access to the variable
   * 
   * @return __functionVariable
   */
  ConstReferenceCounting<FunctionVariable>
  variable() const;

  /** 
   * Access to the variable
   * 
   * @return __functionVariable
   */
  ReferenceCounting<FunctionVariable>
  variable();

  /** 
   * Executes the function expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param functionName name of the variable
   * @param type a discretization type
   */
  FunctionExpressionVariable(const std::string& functionName,
			     const DiscretizationType::Type& type
			     = DiscretizationType::lagrangianFEM1);

  /** 
   * Copy constructor
   * 
   * @param functionVariable name of the variable
   */
  FunctionExpressionVariable(const FunctionExpressionVariable& functionVariable);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionVariable();
};

#endif // FUNCTION_EXPRESSION_VARIABLE_HPP
