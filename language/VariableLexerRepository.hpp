//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariableLexerRepository.hpp,v 1.3 2007/02/14 21:16:56 delpinux Exp $

#ifndef VARIABLE_LEXER_REPOSITORY_HPP
#define VARIABLE_LEXER_REPOSITORY_HPP

#include <string>
#include <map>
#include <vector>

#include <ThreadStaticBase.hpp>
/**
 * @file   VariableLexerRepository.hpp
 * @author St�phane Del Pino
 * @date   Sat Feb 10 13:17:05 2007
 * 
 * @brief Variable repository used during lexical analysis
 */
class VariableLexerRepository
  : public ThreadStaticBase<VariableLexerRepository>
{
private:
  typedef int Type;

  //! The container of variables.
  typedef std::map<std::string,
		   Type> Container;

  std::vector<Container>
  __repository;			/**< per level variable lists */

  size_t __blockLevel;		/**< current level */

public:
  /** 
   * Search for a variable in the list
   * 
   * @param name name of the variable to find
   * 
   * @return the type of the variable
   */
  Type find(const std::string& name) const;

  /** 
   * Adds a variable to the list
   * 
   * @param name name of the variable
   * @param type type of the variable
   */
  void add(const std::string& name, const Type& type);

  /** 
   * Marks a function as a variable
   * 
   * @param name name of the variable
   */
  void markAsUnknown(const std::string& name);

  /** 
   * Begins a new block
   * 
   */
  void beginBlock();

  /** 
   * Ends a block
   * 
   */
  void endBlock();

  /** 
   * Constructor
   * 
   */
  VariableLexerRepository();

  /** 
   * Destructor
   * 
   */
  ~VariableLexerRepository();
};

#endif // VARIABLE_LEXER_REPOSITORY_HPP
