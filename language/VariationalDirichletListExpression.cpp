//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalDirichletListExpression.cpp,v 1.1 2006/08/27 21:38:58 delpinux Exp $

#include <VariationalDirichletListExpression.hpp>
#include <BoundaryConditionExpressionDirichlet.hpp>

std::ostream&
VariationalDirichletListExpression::
put(std::ostream& os) const
{
  for (ListType::const_iterator i = __list.begin();
       i != __list.end(); ++i) {
    os << *(*i) << '\n';
  }
  return os;
}

bool
VariationalDirichletListExpression::
hasPOVBoundary() const
{
  for (ListType::const_iterator i = __list.begin();
       i != __list.end(); ++i) {
    if ((*(*i)).hasPOVBoundary())
      return true;
  }
  return false;
}

VariationalDirichletListExpression::iterator
VariationalDirichletListExpression::
begin()
{
  return __list.begin();
}

VariationalDirichletListExpression::const_iterator
VariationalDirichletListExpression::
begin() const
{
  return __list.begin();
}

VariationalDirichletListExpression::const_iterator
VariationalDirichletListExpression::
end() const
{
  return __list.end();
}

void
VariationalDirichletListExpression::
execute()
{
  for (ListType::iterator i = __list.begin();
       i != __list.end(); ++i) {
    (*(*i)).execute();
  }    
}

void
VariationalDirichletListExpression::
add(ReferenceCounting<BoundaryConditionExpressionDirichlet> d)
{
  __list.push_back(d);
}

VariationalDirichletListExpression::
VariationalDirichletListExpression()
  : Expression(Expression::variationalDirichlet)
{
  ;
}

VariationalDirichletListExpression::
VariationalDirichletListExpression(const VariationalDirichletListExpression& V)
  : Expression(V),
    __list(V.__list)
{
  ;
}

VariationalDirichletListExpression::
~VariationalDirichletListExpression()
{
  ;
}
