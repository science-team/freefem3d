//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Instruction.hpp,v 1.18 2007/05/01 11:33:17 delpinux Exp $

#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP

#include <list>
#include <string>
#include <Expression.hpp>
#include <BooleanExpression.hpp>

#include <SceneExpression.hpp>
#include <Variable.hpp>
#include <OStreamExpression.hpp>

#include <FileDescriptor.hpp>
#include <FieldExpressionList.hpp>

#include <ErrorHandler.hpp>

/**
 * @file   Instruction.hpp
 * @author Stephane Del Pino
 * @date   Sat Aug 28 00:58:55 2004
 * 
 * @brief  Utility instructions
 * 
 */
class Instruction
{
public:
  enum Type {
    list,
    ifStatement,
    doWhileStatement,
    whileStatement,
    forStatement,
    exitStatement,
    coarseMesh,

    declaration,
    affectation,
    increment,
    decrement,
    evaluation,

    blockBegin,
    blockEnd,

    output,
    Using,
    exec,
    cat,
    plot,
    save,
    none
  };

protected:
  const Instruction::Type __type;

public:
  const Instruction::Type& type() const
  {
    return __type;
  }

  virtual void execute() = 0;

  Instruction(const Instruction::Type& t)
    : __type(t)
  {
    ;
  }

  Instruction(const Instruction& I)
    : __type(I.__type)
  {
    ;
  }

  virtual ~Instruction()
  {
    ;
  }
};

class InstructionList
  : public Instruction
{
public:
  typedef std::list<ReferenceCounting<Instruction> > InstructionSet;
  InstructionSet __instructions;

public:
  void execute()
  {
    for(InstructionSet::iterator i = __instructions.begin();
	i != __instructions.end(); ++i) {
      (*i)->execute();
    }
  }

  void add(ReferenceCounting<Instruction> i)
  {
    __instructions.push_back(i);
  }

  InstructionList()
    : Instruction(Instruction::list)
  {
    ;
  }

  InstructionList(const InstructionList& I)
    : Instruction(I),
      __instructions(I.__instructions)
  {
    ;
  }

  ~InstructionList()
  {
    ;
  }
};

class InstructionIfStatement
  : public Instruction
{
public:
  ReferenceCounting<BooleanExpression> __booleanExpression;
  ReferenceCounting<Instruction> __firstStatement;
  ReferenceCounting<Instruction> __secondStatement;

public:
  void execute()
  {
    (*__booleanExpression).execute();
    if ((*__booleanExpression).boolValue()) {
      (*__firstStatement).execute();
    } else {
      (*__secondStatement).execute();
    }
  }

  InstructionIfStatement(ReferenceCounting<BooleanExpression> b,
			 ReferenceCounting<Instruction> first,
			 ReferenceCounting<Instruction> second)
    : Instruction(Instruction::ifStatement),
      __booleanExpression(b),
      __firstStatement(first),
      __secondStatement(second)
  {
    ;
  }

  InstructionIfStatement(const InstructionIfStatement& I)
    : Instruction(I),
      __booleanExpression(I.__booleanExpression),
      __firstStatement(I.__firstStatement),
      __secondStatement(I.__secondStatement)
  {
    ;
  }

  ~InstructionIfStatement()
  {
    ;
  }
};

class InstructionDoWhileStatement
  : public Instruction
{
public:
  ReferenceCounting<Instruction> __statement;
  ReferenceCounting<BooleanExpression> __booleanExpression;

public:
  void execute()
  {
    do {
      __statement->execute();
      __booleanExpression->execute();
    } while (__booleanExpression->boolValue());
  }

  InstructionDoWhileStatement(ReferenceCounting<Instruction> statement,
			      ReferenceCounting<BooleanExpression> b)
    : Instruction(Instruction::doWhileStatement),
      __statement(statement),
      __booleanExpression(b)
  {
    ;
  }

  InstructionDoWhileStatement(const InstructionDoWhileStatement& I)
    : Instruction(I),
      __statement(I.__statement),
      __booleanExpression(I.__booleanExpression)
  {
    ;
  }

  ~InstructionDoWhileStatement()
  {
    ;
  }
};

class InstructionForStatement
  : public Instruction
{
public:
  ReferenceCounting<Instruction> __instruction1;
  ReferenceCounting<BooleanExpression> __booleanExpression;
  ReferenceCounting<Instruction> __instruction2;
  ReferenceCounting<Instruction> __statement;

public:
  void execute()
  {
    for (__instruction1->execute();
	 __booleanExpression->execute(),
	   __booleanExpression->boolValue();
	 __instruction2->execute())
      {
	__statement->execute();
      }
  }

  InstructionForStatement(ReferenceCounting<Instruction> a,
			  ReferenceCounting<BooleanExpression> b,
			  ReferenceCounting<Instruction> c,
			  ReferenceCounting<Instruction> statement)
    : Instruction(Instruction::forStatement),
      __instruction1(a),
      __booleanExpression(b),
      __instruction2(c),
      __statement(statement)
  {
    ;
  }

  InstructionForStatement(const InstructionForStatement& I)
    : Instruction(I),
      __instruction1(I.__instruction1),
      __booleanExpression(I.__booleanExpression),
      __instruction2(I.__instruction2),
      __statement(I.__statement)
  {
    ;
  }

  ~InstructionForStatement()
  {
    ;
  }
};

class InstructionWhileStatement
  : public Instruction
{
public:
  ReferenceCounting<Instruction> __statement;
  ReferenceCounting<BooleanExpression> __booleanExpression;

public:
  void execute()
  {
    __booleanExpression->execute();
    while (__booleanExpression->boolValue()) {
      __statement->execute();
      __booleanExpression->execute();
    }
  }

  InstructionWhileStatement(ReferenceCounting<BooleanExpression> b,
			      ReferenceCounting<Instruction> statement)
    : Instruction(Instruction::whileStatement),
      __statement(statement),
      __booleanExpression(b)
  {
    ;
  }

  InstructionWhileStatement(const InstructionWhileStatement& I)
    : Instruction(I),
      __statement(I.__statement),
      __booleanExpression(I.__booleanExpression)
  {
    ;
  }

  ~InstructionWhileStatement()
  {
    ;
  }
};

class InstructionOutput
  : public Instruction
{
private:
  ReferenceCounting<OStreamExpression> __ostream;
  ReferenceCounting<Expression> __expression;

public:
  void execute()
  {
    __ostream->execute();
    __expression->execute();
    *__ostream << *__expression;
  }

  InstructionOutput(ReferenceCounting<OStreamExpression> o,
		    ReferenceCounting<Expression> e)
    : Instruction(Instruction::output),
      __ostream(o),
      __expression(e)
  {
    ;
  }

  InstructionOutput(const InstructionOutput& I)
    : Instruction(I),
      __ostream(I.__ostream),
      __expression(I.__expression)    
  {
    ;
  }

  ~InstructionOutput()
  {
    ;
  }
};

/**
 * @class  InstructionSave
 * @author Stephane Del Pino
 * @date   Sun Feb  9 17:24:28 2003
 * 
 * @brief  Mother class for saving instruction
 * 
 */

class InstructionSave
  : public Instruction
{
protected:
  ReferenceCounting<FileDescriptor>  __fileDescriptor;
  ReferenceCounting<StringExpression>  __fileName;
  ReferenceCounting<MeshExpression> __mesh;
  
public:

  InstructionSave(ReferenceCounting<FileDescriptor> descriptor,
		  ReferenceCounting<StringExpression> fileName,
		  ReferenceCounting<MeshExpression> mesh);

  InstructionSave(const InstructionSave& I);

  ~InstructionSave();
};

/**
 * @class   InstructionExec
 * @author Stephane Del Pino
 * @date   Mon Jan 26 17:40:26 2004
 * 
 * @brief  Executes shell commands
 */
class InstructionExec
  : public Instruction
{
protected:
  ReferenceCounting<StringExpression> __command;
  
public:
  void execute();

  InstructionExec(ReferenceCounting<StringExpression> command);

  InstructionExec(const InstructionExec& I);

  ~InstructionExec();
};

/**
 * @class   InstructionExec
 * @author Stephane Del Pino
 * @date   Mon Jan 26 17:40:26 2004
 * 
 * @brief  Executes shell commands
 */
class InstructionCat
  : public Instruction
{
protected:
  ReferenceCounting<StringExpression> __filename;
  
public:
  void execute();

  InstructionCat(ReferenceCounting<StringExpression> command);

  InstructionCat(const InstructionCat& I);

  ~InstructionCat();
};

/**
 * @class  InstructionSaveMesh
 * @author Stephane Del Pino
 * @date   Sun Feb  9 17:24:55 2003
 * 
 * @brief  Saves meshes in files according to a given format
 * 
 */

class InstructionSaveMesh
  : public InstructionSave
{
public:
  void execute();

  InstructionSaveMesh(ReferenceCounting<FileDescriptor> descriptor,
		      ReferenceCounting<StringExpression> fileName,
		      ReferenceCounting<MeshExpression> mesh);

  InstructionSaveMesh(const InstructionSaveMesh& I);

  ~InstructionSaveMesh();
};

/**
 * @class  InstructionSaveField
 * @author Stephane Del Pino
 * @date   Sun Feb  9 17:25:44 2003
 * 
 * @brief  Saves fields of function according to a given format.
 */
class InstructionSaveFieldList
  : public InstructionSave
{
private:
  ReferenceCounting<FieldExpressionList> __fieldList;

public:
  void execute();

  InstructionSaveFieldList(ReferenceCounting<FileDescriptor> descriptor,
			   ReferenceCounting<StringExpression> fileName,
			   ReferenceCounting<FieldExpressionList> fieldList,
			   ReferenceCounting<MeshExpression> mesh);

  InstructionSaveFieldList(const InstructionSaveFieldList& I);

  ~InstructionSaveFieldList();
};


/**
 * @class InstructionPlot
 * 
 * This class is designed to plot data using graphic libraries
 *
 * @author Stephane Del Pino
 */

class InstructionPlot
  : public Instruction
{
private:
  ReferenceCounting<MeshExpression> __mesh;
  ReferenceCounting<FunctionExpression> __f;

public:
  /** 
   * Expression::execute() specialization
   * 
   */
  void execute();

  /** 
   * 
   * Constructor
   * 
   */  
  InstructionPlot(ReferenceCounting<MeshExpression> mesh,
		  ReferenceCounting<FunctionExpression> f = 0);

  /** 
   * Copy Constructor
   * 
   */
  InstructionPlot(const InstructionPlot& __instruction);

  /** 
   * Destructor
   * 
   */
  ~InstructionPlot();
};


/*!
  \class InstructionNone

  This class is used for istructions which does nothing

  \author Stephane Del Pino
 */
class InstructionNone
  : public Instruction
{
public:
  void execute()
  {
    ;
  }

  InstructionNone()
    : Instruction(Instruction::none)
  {
    ;
  }

  InstructionNone(const InstructionNone& I)
    : Instruction(I)
  {
    ;
  }

  ~InstructionNone()
  {
    ;
  }
};

#include <VariableRepository.hpp>

template <typename DataType, typename VarType>
class InstructionDeclaration
  : public Instruction
{
private:
  const std::string __name;
  ReferenceCounting<DataType> __expression;

public:
  void execute()
  {
    ReferenceCounting<Variable> a
      = new VarType(__name, __expression);
    VariableRepository::instance().add(a);

    ReferenceCounting<VarType> V 
      = VariableRepository::instance().findVariable<VarType>(__name);

    __expression->execute();
    (*V) = __expression;
  }

  InstructionDeclaration(const std::string name,
			 ReferenceCounting<DataType> e)
    : Instruction(Instruction::declaration),
      __name(name),
      __expression(e)
  {
    ;
  }

  ~InstructionDeclaration()
  {
    ;
  }
};

template<typename DataType,
	 typename VarType>
class InstructionAffectation
  : public Instruction
{
private:
  std::string __variableName;
  ReferenceCounting<DataType> __expression;

public:
  void execute()
  {
    __expression->execute();

    VarType* V = VariableRepository::instance().findVariable<VarType>(__variableName);
    (*V) = __expression;
  }

  InstructionAffectation(const std::string& variableName,
			 ReferenceCounting<DataType> e)
    : Instruction(Instruction::affectation),
      __variableName(variableName),
      __expression(e)
  {
    ;
  }

  InstructionAffectation(const InstructionAffectation<DataType, VarType>& I)
    : Instruction(I),
      __variableName(I.__variableName),
      __expression(I.__expression)
  {
    ;
  }

  ~InstructionAffectation()
  {
    ;
  }
};

class InstructionRealExpressionIncrement
  : public Instruction
{
private:
  std::string __variableName;

public:
  void execute()
  {
    RealVariable* realVariable
      = VariableRepository::instance().findVariable<RealVariable>(__variableName);;

    const real_t r = realVariable->expression()->realValue();
    (*realVariable) = new RealExpressionValue(r+1);
  }

  InstructionRealExpressionIncrement(const std::string& variableName)
    : Instruction(Instruction::increment),
      __variableName(variableName)
  {
    ;
  }

  InstructionRealExpressionIncrement(const InstructionRealExpressionIncrement& I)
    : Instruction(I),
      __variableName(I.__variableName)
  {
    ;
  }

  ~InstructionRealExpressionIncrement()
  {
    ;
  }
};

class InstructionRealExpressionDecrement
  : public Instruction
{
private:
  std::string __variableName;

public:
  void execute()
  {
    RealVariable* realVariable = VariableRepository::instance().findVariable<RealVariable>(__variableName);

    const real_t r = realVariable->expression()->realValue();
    (*realVariable) = new RealExpressionValue(r-1);
  }

  InstructionRealExpressionDecrement(const std::string& variableName)
    : Instruction(Instruction::decrement),
      __variableName(variableName)
  {
    ;
  }

  InstructionRealExpressionDecrement(const InstructionRealExpressionDecrement& I)
    : Instruction(I),
      __variableName(I.__variableName)
  {
    ;
  }

  ~InstructionRealExpressionDecrement()
  {
    ;
  }
};

class FunctionExpression;
class FunctionExpressionFEM;
template<>
class InstructionAffectation<FunctionExpression, FunctionVariable>
  : public Instruction
{
private:
  std::string __variableName;
  ReferenceCounting<FunctionExpression> __expression;

public:
  void execute();

  InstructionAffectation(const std::string& variableName,
			 ReferenceCounting<FunctionExpression> e)
    : Instruction(Instruction::affectation),
      __variableName(variableName),
      __expression(e)
  {
    ;
  }

  InstructionAffectation(const InstructionAffectation<FunctionExpression,
			                              FunctionVariable>& I)
    : Instruction(I),
      __variableName(I.__variableName),
      __expression(I.__expression)
  {
    ;
  }

  ~InstructionAffectation()
  {
    ;
  }
};


template<typename ExpType>
class InstructionEvaluation
  : public Instruction
{
private:
  ReferenceCounting<ExpType> __expression;

public:
  void execute()
  {
    __expression->execute();
  }

  InstructionEvaluation(ReferenceCounting<ExpType> e)
    : Instruction(Instruction::evaluation),
      __expression(e)
  {
    ;
  }

  InstructionEvaluation(const InstructionEvaluation<ExpType>& I)
    : Instruction(I),
      __expression(I.__expression)
  {
    ;
  }

  ~InstructionEvaluation()
  {
    ;
  }
};


class InstructionUsingScene
  : public Instruction
{
private:
  ReferenceCounting<SceneExpression> __sceneExpression;

public:
  void execute();

  InstructionUsingScene(ReferenceCounting<SceneExpression> e);

  InstructionUsingScene(const InstructionUsingScene& I);

  ~InstructionUsingScene();
};

class InstructionExit
  : public Instruction
{
private:
  ReferenceCounting<RealExpression> __realExpression;

public:
  void execute()
  {
    if (__realExpression != 0) {
      __realExpression->execute();
      throw ErrorHandler(__FILE__,__LINE__,
			 "exit called (argument ignored)",
			 ErrorHandler::asked);
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "exit called",
		       ErrorHandler::asked);
  }

  InstructionExit()
    : Instruction(Instruction::exitStatement),
      __realExpression(0)
  {
    ;
  }

  InstructionExit(ReferenceCounting<RealExpression> e)
    : Instruction(Instruction::exitStatement),
      __realExpression(e)
  {
    ;
  }

  InstructionExit(const InstructionExit& I)
    : Instruction(I),
      __realExpression(I.__realExpression)
  {
    ;
  }

  ~InstructionExit()
  {
    ;
  }
};


class InstructionCoarseMesh
  : public Instruction
{
private:
  bool __coarseMesh;

public:
  void execute();

  InstructionCoarseMesh(const bool& fm)
    : Instruction(Instruction::coarseMesh),
      __coarseMesh(fm)
  {
    ;
  }

  InstructionCoarseMesh(const InstructionCoarseMesh& I)
    : Instruction(I),
      __coarseMesh(I.__coarseMesh)
  {
    ;
  }

  ~InstructionCoarseMesh()
  {
    ;
  }
};

class InstructionBlockBegin
  : public Instruction
{
public:
  void execute();

  InstructionBlockBegin()
    : Instruction(Instruction::blockBegin)
  {
    ;
  }

  InstructionBlockBegin(const InstructionBlockBegin& I)
    : Instruction(I)
  {
    ;
  }

  ~InstructionBlockBegin()
  {
    ;
  }
};

class InstructionBlockEnd
  : public Instruction
{
public:
  void execute();

  InstructionBlockEnd()
    : Instruction(Instruction::blockEnd)
  {
    ;
  }

  InstructionBlockEnd(const InstructionBlockEnd& I)
    : Instruction(I)
  {
    ;
  }

  ~InstructionBlockEnd()
  {
    ;
  }
};
#endif // INSTRUCTION_HPP
