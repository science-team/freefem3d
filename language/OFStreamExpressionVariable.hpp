//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OFStreamExpressionVariable.hpp,v 1.2 2007/02/08 23:19:38 delpinux Exp $

#ifndef OFSTREAM_EXPRESSION_VARIABLE_HPP
#define OFSTREAM_EXPRESSION_VARIABLE_HPP

#include <OFStreamExpression.hpp>
#include <Variable.hpp>

/**
 * @file   OFStreamExpressionVariable.hpp
 * @author Stephane Del Pino
 * @date   Sat Dec 30 01:10:27 2006
 * 
 * @brief  ofstream expression variable
 */
class OFStreamExpressionVariable
  : public OFStreamExpression
{
private:
  const std::string __ofstreamName;

  ReferenceCounting<OFStreamVariable>
  __ofstreamVariable;		/**< the variable */

  /** 
   * Overload of the put function
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const
  {
    os << __ofstreamVariable->name();
    return os;
  }

public:
  /** 
   * Executes the variable expression
   * 
   */
  void execute();

  /** 
   * Access to the ofstream expression related to this variable
   * 
   * @return __ofstreamVariable
   */
  ConstReferenceCounting<OFStreamExpression>
  ofstreamExpression() const;

  /** 
   * Constructor
   * 
   * @param ofstreamName given variable name
   */
  OFStreamExpressionVariable(const std::string& ofstreamName);

  /** 
   * Copy constructor
   * 
   * @param e given expression
   */
  OFStreamExpressionVariable(const OFStreamExpressionVariable& e);

  /** 
   * Destructor
   * 
   */
  ~OFStreamExpressionVariable();
};

#endif // OFSTREAM_EXPRESSION_VARIABLE_HPP
