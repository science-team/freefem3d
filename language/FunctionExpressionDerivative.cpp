//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionDerivative.cpp,v 1.2 2006/07/23 22:14:19 delpinux Exp $

#include <FunctionExpressionDerivative.hpp>
#include <ScalarFunctionDerivative.hpp>

void
FunctionExpressionDerivative::
execute()
{
  __derivedFunction->execute();

  switch (__direction) {
  case FunctionExpressionDerivative::x: {
    __scalarFunction = new ScalarFunctionDerivative(ScalarFunctionDerivative::x,
						    __derivedFunction->function());
    break;
  }
  case FunctionExpressionDerivative::y: {
    __scalarFunction = new ScalarFunctionDerivative(ScalarFunctionDerivative::y,
						    __derivedFunction->function());
    break;
  }
  case FunctionExpressionDerivative::z: {
    __scalarFunction = new ScalarFunctionDerivative(ScalarFunctionDerivative::z,
						    __derivedFunction->function());
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown derivation direction",
		       ErrorHandler::unexpected);
  }
  }
}

FunctionExpressionDerivative::
FunctionExpressionDerivative(const std::string& operatorName,
			     ReferenceCounting<FunctionExpression> derivedFunction)
  : FunctionExpression(FunctionExpression::derivative),
    __direction(FunctionExpressionDerivative::getDirection(operatorName)),
    __derivedFunction(derivedFunction)
{
  ;
}

FunctionExpressionDerivative::
FunctionExpressionDerivative(const FunctionExpressionDerivative& f)
  : FunctionExpression(f),
    __direction(f.__direction),
    __derivedFunction(f.__derivedFunction)
{
  ;
}

FunctionExpressionDerivative::
~FunctionExpressionDerivative()
{
  ;
}
