//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryExpressionSurfaceMesh.hpp,v 1.1 2006/08/27 21:33:53 delpinux Exp $

#ifndef BOUNDARY_EXPRESSION_SURFACE_MESH_HPP
#define BOUNDARY_EXPRESSION_SURFACE_MESH_HPP

#include <BoundaryExpression.hpp>

/**
 * @file   BoundaryExpressionSurfaceMesh.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 15:40:38 2006
 * 
 * @brief Describes surface mesh boundary expressions
 */
class BoundaryExpressionSurfaceMesh
  : public BoundaryExpression
{
private:
  ReferenceCounting<MeshExpression>
  __surfaceMeshExpression;	/**< surface mesh expression */

  /** 
   * Writes the expression to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Checks if the boundary is a POVRay boundary
   * 
   * @return false
   */
  bool hasPOVBoundary() const
  {
    return false;
  }

  /** 
   * Executes the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param m the mesh
   */
  BoundaryExpressionSurfaceMesh(ReferenceCounting<MeshExpression> m);

  /** 
   * Copy constructor
   * 
   * @param b given boundary expression
   */
  BoundaryExpressionSurfaceMesh(const BoundaryExpressionSurfaceMesh& b);

  /** 
   * Destuctor
   * 
   */
  ~BoundaryExpressionSurfaceMesh();
};

#endif // BOUNDARY_EXPRESSION_SURFACE_MESH_HPP
