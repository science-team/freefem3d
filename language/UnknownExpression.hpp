//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: UnknownExpression.hpp,v 1.5 2007/05/20 23:39:23 delpinux Exp $

#ifndef UNKNOWN_EXPRESSION_HPP
#define UNKNOWN_EXPRESSION_HPP

#include <Expression.hpp>
#include <DiscretizationType.hpp>

#include <string>

class UnknownExpression
  : public Expression
{
public:
  enum Type {
    declaration,
    existingFunction
  };

private:
  virtual void __preExecute() = 0;

  std::ostream& put(std::ostream& os) const
  {
    os << __name << ':' << DiscretizationType::name(__discretizationType);
    return os;
  }

protected:
  const Type __type;

  const std::string __name;
  DiscretizationType::Type __discretizationType;

public:
  void setDiscretizationType(const DiscretizationType::Type& type)
  {
    __discretizationType = type;
  }

  const std::string& name() const
  {
    return __name;
  }

  const DiscretizationType::Type& discretizationType() const
  {
    return __discretizationType;
  }

  void execute()
  {
    this->__preExecute();
  }

  UnknownExpression(const Type& type,
		    const std::string& name,
		    const DiscretizationType::Type& discretization)
    : Expression(Expression::unknown),
      __type(type),
      __name(name),
      __discretizationType(discretization)
  {
    ;
  }

  virtual ~UnknownExpression()
  {
    ;
  }
};

#endif // UNKNOWN_EXPRESSION_HPP
