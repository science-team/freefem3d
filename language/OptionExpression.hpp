//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OptionExpression.hpp,v 1.2 2003/05/04 18:09:01 delpinux Exp $

#ifndef _OPTIONS_EXPRESSION_HPP_
#define _OPTIONS_EXPRESSION_HPP_

#include <StringExpression.hpp>
#include <SubOptionListExpression.hpp>

class OptionExpression
  : public Expression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << *__name << '(' << *__options << ')';
    return os;
  }

  ReferenceCounting<StringExpression> __name;
  ReferenceCounting<SubOptionListExpression> __options;
public:

  void execute()
  {
    (*__name).execute();
    (*__options).execute();

    //! Passes the base name to options.
    (*__options).evaluate((*__name).value());
  }

  OptionExpression(ReferenceCounting<StringExpression> name,
		    ReferenceCounting<SubOptionListExpression> option)
    : Expression(Expression::option),
      __name(name),
      __options(option)
  {
    ;
  }

  OptionExpression(const OptionExpression& o)
    : Expression(o),
      __name(o.__name),
      __options(o.__options)
  {
    ;
  }

  ~OptionExpression()
  {
    ;
  }
};

#endif // _OPTIONS_EXPRESSION_HPP_

