//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: XMLLexer.cpp,v 1.1 2007/06/10 14:59:09 delpinux Exp $

#include <Types.hpp>
#include <ErrorHandler.hpp>

#include <XMLLexer.hpp>

#include <XMLTag.hpp>
#include <XMLAttribute.hpp>

#include <list>
typedef std::list<XMLAttribute*> XMLAttributeList;

#include <parse.xml.h>

//! Constructs a XMLLexer for given std::istream and std::ostream.
XMLLexer::
XMLLexer(std::istream& In,
	 std::ostream& Out)
  : Lexer(In,Out),
    __definingToken(false)
{
  in.unsetf(std::ios::skipws);
}

//! The lexer function.
int XMLLexer::
yylex()
{
  char readChar = ' ';

  if (__definingToken) {
    if (in) {
      while ((in)&&(isspace(readChar))) {
	in >> readChar;
	if ((readChar==' ') or (readChar=='\t')) {
	  return SPACE;
	}
	if (readChar=='\n') {
	  linenumber++;
	  return SPACE;
	}
      }

      char nextChar = in.peek(); // read next Char

      if (isalpha(readChar) or (readChar == '_')) {
	std::string is;
	while (isalnum(readChar) or (readChar == '_')) {
	  is += readChar;
	  in >> readChar;
	}
	in.unget();

	yytext = is;
	xmllval.str = new std::string(yytext);
	return NAME;
      }

      switch(readChar) {
      case '<':{
	if (nextChar == '!') {	// increment operator
	  in >> readChar;
	  in >> readChar;
	  if (readChar == '-') {
	    in >> readChar;
	    if (readChar == '-') {
	      const size_t startingLine = linenumber;
	      in >> readChar;
	      while (true) {
		while (readChar != '-') {
		  if (readChar == '\n') {
		    linenumber++;
		  }
		  in >> readChar;

		  if (not in) {
		    throw ErrorHandler(__FILE__,startingLine,
				       "runnaway comment",
				       ErrorHandler::compilation);
		  }
		}
		in >> readChar;
		if (readChar == '-') {
		  in >> readChar;
		  if (readChar == '>') {
		    return yylex();
		  }
		}
	      }
	    }
	  }
	}

	yytext = "<";
	return LT;
      }
      case '>':{
	yytext = ">";
	__definingToken = false;
	return GT;
      }
      case '"': {
	bool endOfString = false;
	size_t stringLines = 0; // Counts lines within the string
	std::stringstream is;

	while (!endOfString) {
	  if(in >> readChar) {
	    switch (readChar) {
	    case '"': {
	      endOfString = true;
	      break;
	    }
	    case '\n': {
	      stringLines++;
	      is << '\n';
	      break;
	    }
	    case '\\': {
	      int peekC = in.peek();
	      switch (peekC) {
	      case 'n': {
		is << '\n';
		in >> readChar;
		break;
	      }
	      case 't': {
		is << '\t';
		in >> readChar;
		break;
	      }
	      case 'v': {
		is << '\v';
		in >> readChar;
		break;
	      }
	      case 'b': {
		is << '\b';
		in >> readChar;
		break;
	      }
	      case 'r': {
		is << '\r';
		in >> readChar;
		break;
	      }
	      case 'f': {
		is << '\f';
		in >> readChar;
		break;
	      }
	      case 'a': {
		is << '\a';
		in >> readChar;
		break;
	      }
	      case '\'': {
		is << '\'';
		in >> readChar;
		break;
	      }
	      case '"': {
		is << '"';
		in >> readChar;
		break;
	      }
	      case '\\': {
		is << '\\';
		in >> readChar;
		break;
	      }
	      default: {
		throw ErrorHandler(__FILE__,linenumber,
				   "unknown escape sequence",
				   ErrorHandler::compilation);
	      }
	      }
	      break;
	    }
	    default: {
	      is << readChar;
	    }
	    }
	  } else {
	    throw ErrorHandler(__FILE__,linenumber,
			       "opened string never closed",
			       ErrorHandler::compilation);
	  }
	}

	xmllval.str = new std::string(is.str());
	linenumber += stringLines;

	return STRING;
      }
      }

      // Others characters treatment
      yytext = readChar;
      return readChar;
    }
    return 0;
  } else {
    bool hasChar = false;
    std::istream::pos_type position;

    in >> readChar;
    while (in and (readChar != '<')) {
      if (readChar=='\n') {
	linenumber++;
      } else {
	if (not hasChar) {
	  if ((readChar!=' ') and (readChar!='\t')) {
	    hasChar = true;
	    position = in.tellg();
	  }
	}
      }
      in >> readChar;
    }

    if (readChar == '<') {
      in.unget();
      __definingToken = true;

      if (hasChar) {
	xmllval.position = new std::istream::pos_type(position);
	return POSITION;
      }

      return yylex();
    }
  }

  return 0;
}

