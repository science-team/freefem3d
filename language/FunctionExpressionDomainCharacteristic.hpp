//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionDomainCharacteristic.hpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_DOMAIN_CHARACTERISTIC_HPP
#define FUNCTION_EXPRESSION_DOMAIN_CHARACTERISTIC_HPP

#include <FunctionExpression.hpp>

class DomainExpression;

/**
 * @file   FunctionExpressionDomainCharacteristic.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:47:23 2006
 * 
 * @brief  Characteristic function of a domain
 * 
 */
class FunctionExpressionDomainCharacteristic
  : public FunctionExpression
{
private:
  ReferenceCounting<DomainExpression> __domain;	/**< domain expression */

  /** 
   * Overloading of the put function
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * execute the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param D domain expression
   * 
   */
  FunctionExpressionDomainCharacteristic(ReferenceCounting<DomainExpression> D);

  /** 
   * Copy constructor
   * 
   * @param f given function expression of the characteristic function of a domain 
   * 
   */
  FunctionExpressionDomainCharacteristic(const FunctionExpressionDomainCharacteristic& f);

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionDomainCharacteristic();
};

#endif // FUNCTION_EXPRESSION_DOMAIN_CHARACTERISTIC_HPP
