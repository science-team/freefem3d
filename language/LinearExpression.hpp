//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: LinearExpression.hpp,v 1.4 2007/02/08 23:19:38 delpinux Exp $

#ifndef LINEAR_EXPRESSION_HPP
#define LINEAR_EXPRESSION_HPP

#include <Expression.hpp>
#include <ReferenceCounting.hpp>

#include <RealExpression.hpp>

#include <FunctionExpression.hpp>
#include <IntegratedOperatorExpression.hpp>

class LinearExpression
  : public Expression
{
public:
  enum LinearType {
    elementary,
    elementaryTimesFunction,
    elementaryTimesReal,
    elementaryTimesFunctionOperator
  };

private:
  LinearExpression::LinearType __formType;

protected:
  ReferenceCounting<IntegratedOperatorExpression> __integrated;

public:
  const LinearExpression::LinearType& formType() const
  {
    return __formType;
  }

  ConstReferenceCounting<IntegratedOperatorExpression> integrated() const
  {
    return __integrated;
  }

  LinearExpression(const LinearExpression::LinearType t,
		   ReferenceCounting<IntegratedOperatorExpression> i)
    : Expression(linearExp),
      __formType(t),
      __integrated(i)
  {
    ;
  }

  LinearExpression(const LinearExpression& LF)
    : Expression(LF),
      __formType(LF.__formType),
      __integrated(LF.__integrated)
  {
    ;
  }

  virtual ~LinearExpression()
  {
    ;
  }
};


class LinearExpressionElementary
  : public LinearExpression
{
private:

  std::ostream& put (std::ostream& os) const
  {
    os << *__integrated;
    return os;
  }

public:
  void execute()
  {
    (*__integrated).execute();
  }

  LinearExpressionElementary(ReferenceCounting<IntegratedOperatorExpression> i)
    : LinearExpression(LinearExpression::elementary, i)
  {
    ;
  }

  LinearExpressionElementary(const LinearExpressionElementary& LF)
    : LinearExpression(LF)
  {
    ;
  }

  ~LinearExpressionElementary()
  {
    ;
  }
};


class LinearExpressionElementaryTimesFunction
  : public LinearExpression
{
private:
  ReferenceCounting<FunctionExpression> __function;


  std::ostream& put (std::ostream& os) const
  {
    os << *__function << '*' << *__integrated;
    return os;
  }

public:

  void execute()
  {
    (*__integrated).execute();
    (*__function).execute();
  }

  ReferenceCounting<FunctionExpression> function()
  {
    return __function;
  }

  LinearExpressionElementaryTimesFunction
  (ReferenceCounting<IntegratedOperatorExpression> i,
   ReferenceCounting<FunctionExpression> f)
    : LinearExpression(LinearExpression::elementaryTimesFunction, i),
      __function(f)
  {
    ;
  }

  LinearExpressionElementaryTimesFunction(const LinearExpressionElementaryTimesFunction& LF)
    : LinearExpression(LF),
      __function(LF.__function)
  {
    ;
  }

  ~LinearExpressionElementaryTimesFunction()
  {
    ;
  }
};


class LinearExpressionElementaryTimesReal
  : public LinearExpression
{
private:
  ReferenceCounting<RealExpression> __realexp;

  std::ostream& put (std::ostream& os) const
  {
    os << *__realexp << '*' << *__integrated;
    return os;
  }

public:

  void execute()
  {
    (*__realexp).execute();
    (*__integrated).execute();
  }

  ReferenceCounting<RealExpression> real_t()
  {
    return __realexp;
  }

  LinearExpressionElementaryTimesReal
  (ReferenceCounting<IntegratedOperatorExpression> i,
   ReferenceCounting<RealExpression> r)
    : LinearExpression(LinearExpression::elementaryTimesReal, i),
      __realexp(r)
  {
    ;
  }

  LinearExpressionElementaryTimesReal(const LinearExpressionElementaryTimesReal& LF)
    : LinearExpression(LF),
      __realexp(LF.__realexp)
  {
    ;
  }

  ~LinearExpressionElementaryTimesReal()
  {
    ;
  }
};

#endif // LINEAR_EXPRESSION_HPP

