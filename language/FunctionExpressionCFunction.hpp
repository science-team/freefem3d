//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionCFunction.hpp,v 1.1 2006/07/20 17:29:18 delpinux Exp $

#ifndef FUNCTION_EXPRESSION_C_FUNCTION_HPP
#define FUNCTION_EXPRESSION_C_FUNCTION_HPP

#include <FunctionExpression.hpp>
#include <string>

/**
 * @file   FunctionExpressionCFunction.hpp
 * @author Stephane Del Pino
 * @date   Tue Jul  4 01:20:23 2006
 * 
 * @brief  Function expression of standard cmath functions
 * 
 * 
 */
class FunctionExpressionCFunction
  : public FunctionExpression
{
private:
  const std::string __cfunction; /**< the name of the cmath function */
  ReferenceCounting<FunctionExpression> __functionExpression; /**< the argument function */

public:
  /** 
   * Overload of the execution of the expression
   * 
   */
  void execute();

  /** 
   * Constructor
   * 
   * @param cfunction the cmath function name
   * @param functionExpression the argument of the C function
   * 
   */
  FunctionExpressionCFunction(const std::string& cfunction,
			      ReferenceCounting<FunctionExpression> functionExpression)
    : FunctionExpression(FunctionExpression::cfunction),
      __cfunction(cfunction),
      __functionExpression(functionExpression)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param f the original C function
   * 
   */
  FunctionExpressionCFunction(const FunctionExpressionCFunction& f)
    :FunctionExpression(f),
     __cfunction(f.__cfunction),
     __functionExpression(f.__functionExpression)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~FunctionExpressionCFunction()
  {
    ;
  }
};

#endif // FUNCTION_EXPRESSION_C_FUNCTION_HPP
