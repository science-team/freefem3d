//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DomainExpression.cpp,v 1.6 2007/05/20 23:02:48 delpinux Exp $

#include <DomainExpression.hpp>
#include <Variable.hpp>

#include <Scene.hpp>
#include <Domain.hpp>

ReferenceCounting<Domain> DomainExpression::domain() const
{
  ASSERT(__domainType != DomainExpression::undefined);
  return __domain;
}

DomainExpression::
DomainExpression(const DomainExpression& e)
  : Expression(e),
    __domain(e.__domain),
    __domainType(e.__domainType)
{
  ;
}

DomainExpression::
DomainExpression(const DomainExpression::DomainType& t)
  : Expression(Expression::domain),
    __domainType(t)
{
  ;
}

DomainExpression::
~DomainExpression()
{
  ;
}
