//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpressionBinaryOperation.cpp,v 1.2 2006/07/30 22:23:10 delpinux Exp $

#include <FunctionExpressionBinaryOperation.hpp>

#include <ScalarFunctionBuilder.hpp>

void
FunctionExpressionBinaryOperation::
execute()
{
  __fExpression->execute();
  __gExpression->execute();

  ScalarFunctionBuilder functionBuilder;
  functionBuilder.setFunction(__fExpression->function());
  functionBuilder.setBinaryOperation(__type,
				     __gExpression->function());
  __scalarFunction = functionBuilder.getBuiltFunction();
}

bool
FunctionExpressionBinaryOperation::
hasBoundaryExpression() const
{
  return (__fExpression->hasBoundaryExpression()
	  or __gExpression->hasBoundaryExpression());
}

FunctionExpressionBinaryOperation::
FunctionExpressionBinaryOperation(const std::string& operatorName,
				  ReferenceCounting<FunctionExpression> f,
				  ReferenceCounting<FunctionExpression> g)
  : FunctionExpression(FunctionExpression::binaryOperation),
    __type(operatorName),
    __fExpression(f),
    __gExpression(g)
{
  ;
}

FunctionExpressionBinaryOperation::
FunctionExpressionBinaryOperation(const BinaryOperation::Type& type,
				  ReferenceCounting<FunctionExpression> f,
				  ReferenceCounting<FunctionExpression> g)
  : FunctionExpression(FunctionExpression::binaryOperation),
    __type(type),
    __fExpression(f),
    __gExpression(g)
{
  ;
}

FunctionExpressionBinaryOperation::
FunctionExpressionBinaryOperation(const FunctionExpressionBinaryOperation& f)
  : FunctionExpression(f),
    __type(f.__type),
    __fExpression(f.__fExpression),
    __gExpression(f.__gExpression)
{
  ;
}

FunctionExpressionBinaryOperation::
~FunctionExpressionBinaryOperation()
{
  ;
}
