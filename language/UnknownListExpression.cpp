//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: UnknownListExpression.cpp,v 1.7 2007/02/10 17:20:53 delpinux Exp $

#include <Variable.hpp>
#include <UnknownListExpression.hpp>

std::ostream& UnknownListExpressionSet::
put(std::ostream& os) const
{
  os << '[';
  for (listType::const_iterator i = __list.begin(); i != __list.end(); ++i) {
    const UnknownExpression& u = **i;
    if (i != __list.begin()) {
      os << ',';
    }
    os << u.name() << ':'
       << DiscretizationType::name(u.discretizationType());
  }
  os << ']';
  return os;
}
