//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDESystemExpression.cpp,v 1.2 2006/03/12 20:17:23 delpinux Exp $

#include <PDESystemExpression.hpp>
#include <PDESystem.hpp>

ReferenceCounting<PDESystem> PDESystemExpression::pdeSystem()
{
  return __pdeSystem;
}

bool PDESystemExpression::hasPOVBoundary() const
{
  for (SystemList::const_iterator i = __system.begin(); i!= __system.end();
       ++i) {
    if ((*(*i)).hasPOVBoundary()) return true;
  }
  return false;
}

void PDESystemExpression::execute()
{
  size_t dimension = 0;

  for (SystemList::iterator i = __system.begin(); i!= __system.end();
       ++i, ++dimension) {
    (*(*i)).execute();
  }
  __pdeSystem = new PDESystem(dimension);

  for (SystemList::iterator i = __system.begin(); i!= __system.end();
       ++i) {
    (*__pdeSystem).add((*(*i)).pdeProblem());
  }
}

PDESystemExpression::PDESystemExpression()
  : ProblemExpression(ProblemExpression::pdeSystem)
{
  ;
}

PDESystemExpression::PDESystemExpression(const PDESystemExpression& S)
  : ProblemExpression(S),
    __pdeSystem(PDESystemExpression::__pdeSystem),
    __system(S.__system)
{
  ;
}

PDESystemExpression::~PDESystemExpression()
{
  ;
}

