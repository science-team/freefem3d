//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FunctionExpression.cpp,v 1.30 2007/05/20 23:02:48 delpinux Exp $

#include <FunctionExpression.hpp>
#include <FunctionExpressionValue.hpp>

#include <ScalarFunctionBase.hpp>
#include <ErrorHandler.hpp>

std::ostream&
FunctionExpression::
put(std::ostream& os) const
{
  os << *__scalarFunction;
  return os;
}

bool
FunctionExpression::
hasBoundaryExpression() const
{
  return false;
}

const FunctionExpression::FunctionType&
FunctionExpression::
type() const
{
  return __type;
}

ConstReferenceCounting<ScalarFunctionBase>
FunctionExpression::
function() const
{
  ASSERT(__scalarFunction != 0);
  return __scalarFunction;
}

ReferenceCounting<FunctionExpression>
FunctionExpression::
value()
{
  return new FunctionExpressionValue(__scalarFunction,
				     this->hasBoundaryExpression());
}

FunctionExpression::
FunctionExpression(const FunctionExpression& e)
  : Expression(e),
    __type(e.__type),
    __unknown(e.__unknown),
    __scalarFunction(e.__scalarFunction)
{
  ;
}

FunctionExpression::
FunctionExpression(const FunctionExpression::FunctionType& type)
  : Expression(Expression::function),
    __type(type),
    __unknown(false),
      __scalarFunction(0)
{
  ;
}

FunctionExpression::
~FunctionExpression()
{
  ;
}
