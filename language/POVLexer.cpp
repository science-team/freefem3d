//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: POVLexer.cpp,v 1.5 2005/11/03 21:13:15 delpinux Exp $

#include <StreamCenter.hpp>

#include <parse.pov.hpp>
#include <parse.pov.h>
#include <POVLexer.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <cctype>
#include <sstream>

//! Constructs a POVLexer for given std::istream and std::ostream.
POVLexer::POVLexer(std::istream& In,
		   std::ostream& Out)
  : Lexer(In,Out)
{
  // Unused KeyWords
  __keyWordList["global_settings"] = KEYWORD;
  __keyWordList["assumed_gamma"] = KEYWORD;
  __keyWordList["camera"] = KEYWORD;
  __keyWordList["location"] = KEYWORD;
  __keyWordList["direction"] = KEYWORD;
  __keyWordList["look_at"] = KEYWORD;
  __keyWordList["light_source"] = KEYWORD;
  __keyWordList["finish"] = KEYWORD;

  // Transformations
  __keyWordList["rotate"] = TRANSF;
  __keyWordList["scale"] = TRANSF;
  __keyWordList["translate"] = TRANSF;
  __keyWordList["matrix"] = MATRIXTRANSF;
  __keyWordList["inverse"] = INVERSE;

  // Objects.
  __keyWordList["sphere"] = SPHERE;
  __keyWordList["box"] = BOX;
  __keyWordList["cylinder"] = CYLINDER;
  __keyWordList["cone"] = CONE;
  __keyWordList["plane"] = PLANE;
  __keyWordList["torus"] = TORUS;

  __keyWordList["union"] = UNION;
  __keyWordList["merge"] = UNION;
  __keyWordList["intersection"] = INTERSECTION;
  __keyWordList["difference"] = DIFFERENCE;
  __keyWordList["object"] = OBJECT;

  // Other keywords.
  __keyWordList["#declare"] = DECLARE;
  __keyWordList["pigment"] = PIGMENT;
  __keyWordList["color"] = COLOR;
  __keyWordList["rgb"] = RGB;
  __keyWordList["rgbf"] = RGBF;
  __keyWordList["#if"] = IF;
  __keyWordList["#ifdef"] = IFDEF;
  __keyWordList["#ifndef"] = IFNDEF;
  __keyWordList["#else"] = ELSE;
  __keyWordList["#end"] = END;
  __keyWordList["#while"] = WHILE;
}

/*! This function returns the token associated to the name "word" if it
  corresponds to a Keyword, else, it returns "-1".
*/
int POVLexer::isKeyword(const std::string& word)
{
  KeyWordList::iterator theKeyWord
    = __keyWordList.find(word.c_str());
  if (theKeyWord != __keyWordList.end()) {
      switch (theKeyWord->second) {
      case TRANSF: {
	povlval.str = new char [word.size() + 1];
	strcpy (povlval.str, theKeyWord->first);
	break;
      }
      }
      return theKeyWord->second;
    }
  return -1;			// Means this is not a keyword!
}

//! The lexer function.
int POVLexer::yylex()
{
  char readChar = ' ';
  in.unsetf(std::ios::skipws);

  while ((in)&&(isspace(readChar))) {
    in >> readChar;
    if (readChar=='\n')
      linenumber++;
  }

  char nextChar = in.peek(); // read next Char

  if (in) {
    // Is input a Number?
    if (isdigit(readChar) || ((readChar=='.') && isdigit(nextChar))) {
      in.unget();

      if (in >> povlval.val) {
	std::stringstream is;
	is << povlval.val << std::ends;
	yytext = is.str();
	return NUM;
      } else {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);
      }
    }

    if (isalpha(readChar)||readChar=='#') {
      std::stringstream is;
      while (isalnum(readChar)||readChar=='_') {
	is << readChar;
	in >> readChar;
      }
      in.unget();

      is << std::ends;

      yytext = is.str();

      // is the word a Keyword?
      int TOKEN = isKeyword(yytext);
	

      if (TOKEN != -1) {
	if (TOKEN != KEYWORD) {
	  return TOKEN;
	} else {		// If it is an ignored KEYWORD,
	  return yylex();
	}
      }

      // reaching this place means this is not a Keyword!
      povlval.str = new char[yytext.size()+1];
      strcpy(povlval.str, yytext.c_str());
      return NAME;

      // Variables are not actually supported.
      // 	// is the word a Variable?
      // 	TOKEN = isVariable(yytext);
      // 	if (TOKEN != -1) {
      // 	  return TOKEN;
      // 	}
      // reaching this place means this is not a Variable!

    }

    // Comments treatment.
    switch(readChar) {
    case '/': {
      if (nextChar == '/') {	// end of line comment
	while (readChar!='\n')
	  in >> readChar;
	in.unget();
	return yylex();	// use recursion
      }

      if (nextChar == '*') {	// multi-line comment
	in >> readChar;
	in >> readChar;
	if (readChar == '*') {
	  in >> readChar;
	  if (readChar == '/') { // ff3d-special comment line
	    while (readChar!='\n')
	      in >> readChar;
	    in.unget();
	    return yylex();	// use recursion
	  }
	}
	bool endOfComment = false;
	size_t commentLines = 0; // Counts lines within comment
	while (!endOfComment) {
	  if(in >> readChar) {
	    switch (readChar) {
	    case '*': {
	      if  (in.peek() == '/') {
		endOfComment = true;
	      }
	      break;
	    }
	    case '\n': {
	      commentLines++;
	      break;
	    }
	    }
	  } else {
	    throw ErrorHandler(__FILE__,__LINE__,
			       stringify(linenumber)
			       +": opened comment never closed",
			       ErrorHandler::normal);
	  }
	}
	in >> readChar;

	linenumber += commentLines;
	return yylex();
      }
    }
    case '"': {
      bool endOfString = false;
      size_t stringLines = 0; // Counts lines within the string
      std::stringstream is;

      while (!endOfString) {
	if (in >> readChar) {
	  switch (readChar) {
	  case '"': {
	    endOfString = true;
	    break;
	  }
	  case '\n': {
	    stringLines++;
	    is << '\n';
	    break;
	  }
	  case '\\': {
	    if(in.peek()=='"') {
	      is << '"';
	      in >> readChar;
	    }
	    else
	      is << '\\';
	    break;
	  }
	  default: {
	    is << readChar;
	  }
	  }
	} else {
	  throw ErrorHandler(__FILE__,__LINE__,
			     stringify(linenumber)+": opened string never closed",
			     ErrorHandler::normal);
	}
      }
      is << std::ends;

      povlval.str = new char[is.str().size()+1];
      strcpy (povlval.str, is.str().c_str());

      yytext = povlval.str;
      linenumber += stringLines;
      return STRING;
    }

    }

    // Others characters treatment
    yytext = readChar;
    return readChar;

  } else {
    return EOF;
  }

  return 0;
}

