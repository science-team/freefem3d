//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SceneExpression.hpp,v 1.4 2007/02/08 23:19:38 delpinux Exp $

#ifndef SCENE_EXPRESSION_HPP
#define SCENE_EXPRESSION_HPP

#include <Expression.hpp>
#include <StringExpression.hpp>
#include <Variable.hpp>

/*!
  \class SceneExpression

  This class defines the base class of Scene expressions.

  \author Stephane Del Pino
 */
class Scene;
class SceneExpression
  : public Expression
{
protected:
  ReferenceCounting<Scene> __scene;

public:
  enum SceneType {
    undefined,
    povray,
    transform,
    variable
  };

private:
  SceneExpression::SceneType __sceneType;

public:
  ReferenceCounting<Scene> scene() const;

  const SceneExpression::SceneType& sceneType() const
  {
    return __sceneType;
  }

  SceneExpression(const SceneExpression& e);

  SceneExpression(ReferenceCounting<Scene> m,
		  const SceneExpression::SceneType& t);

  virtual ~SceneExpression();
};

/*!
  \class SceneExpressionPOVRay

  This class defines the class of povray Scene expressions.

  \author Stephane Del Pino
 */
class SceneExpressionPOVRay
  : public SceneExpression
{
private:
  ReferenceCounting<StringExpression> __filename;

  std::ostream& put(std::ostream& os) const;

public:
  void execute();

  SceneExpressionPOVRay(ReferenceCounting<StringExpression> s);

  SceneExpressionPOVRay(const SceneExpressionPOVRay& m);

  ~SceneExpressionPOVRay();
};

class SceneExpressionVariable
  : public SceneExpression
{
private:
  const std::string __sceneName;
  ReferenceCounting<SceneVariable> __sceneVariable;

  std::ostream& put(std::ostream& os) const
  {
    os << __sceneVariable->name() << ": " << (*__sceneVariable->expression());
    return os;
  }

public:
  void execute();

  SceneExpressionVariable(const std::string& sceneName);

  SceneExpressionVariable(const SceneExpressionVariable& e);

  ~SceneExpressionVariable();
};

class FieldExpression;
class SceneExpressionTransform
  : public SceneExpression
{
private:
  ReferenceCounting<SceneExpression> __sceneExpression;
  ReferenceCounting<FieldExpression> __fieldExpression;

  std::ostream& put(std::ostream& os) const;

public:
  void execute();

  SceneExpressionTransform(const SceneExpressionTransform& s);

  SceneExpressionTransform(ReferenceCounting<SceneExpression> sceneExpression,
			   ReferenceCounting<FieldExpression> fieldExpression);

  ~SceneExpressionTransform();
};

class SceneExpressionUndefined
  : public SceneExpression
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << "undefined scene";
    return os;
  }

public:

  void execute()
  {
    ;
  }

  SceneExpressionUndefined();

  SceneExpressionUndefined(const SceneExpressionUndefined& m);

  ~SceneExpressionUndefined();
};

#endif // SCENE_EXPRESSION_HPP
