//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Variable.cpp,v 1.7 2007/02/08 23:19:38 delpinux Exp $

#include <ReferenceCounting.hpp>

#include <Variable.hpp>

#include <RealExpression.hpp>
#include <Vector3Expression.hpp>
#include <FunctionExpression.hpp>
#include <MeshExpression.hpp>
#include <SceneExpression.hpp>
#include <DomainExpression.hpp>
#include <OFStreamExpression.hpp>

ReferenceCounting<RealExpression>
RealVariable::
expression() const
{
  return __expression;
}

ConstReferenceCounting<RealExpression>
RealVariable::operator=(ReferenceCounting<RealExpression> e)
{
  __expression=e->value();
  return __expression;
}

RealVariable::RealVariable(const std::string& name,
			   ReferenceCounting<RealExpression> expression)
  : Variable(name, Variable::real),
    __expression(expression)
{
  ;
}

RealVariable::RealVariable(const RealVariable& rv)
  : Variable(rv),
    __expression(rv.__expression)    
{
  ;
}

RealVariable::~RealVariable()
{
  ;
}

ReferenceCounting<Vector3Expression>
Vector3Variable::
expression() const
{
  return __expression;
}

ConstReferenceCounting<Vector3Expression>
Vector3Variable::operator=(ReferenceCounting<Vector3Expression> e)
{
  __expression=e;
  return __expression;
}

Vector3Variable::Vector3Variable(const std::string& name,
				 ReferenceCounting<Vector3Expression> expression)
  : Variable(name, Variable::vector3),
    __expression(expression)
{
  ;
}

Vector3Variable::Vector3Variable(const Vector3Variable& rv)
  : Variable(rv),
    __expression(rv.__expression)
{
  ;
}

Vector3Variable::~Vector3Variable()
{
  ;
}



ReferenceCounting<FunctionExpression>
FunctionVariable::expression() const
{
  return __expression;
}

ConstReferenceCounting<FunctionExpression>
FunctionVariable::operator=(ReferenceCounting<FunctionExpression> e)
{
  __expression = e->value();
  return __expression;
}

FunctionVariable::FunctionVariable(const std::string& name,
				   ReferenceCounting<FunctionExpression> expression)
  : Variable(name, Variable::function),
    __expression(expression),
    __subscribed(false)
{
  ;
}

FunctionVariable::FunctionVariable(const FunctionVariable& rv)
  : Variable(rv),
    __expression(rv.__expression),
    __subscribed(rv.__subscribed)
{
  ;
}

FunctionVariable::~FunctionVariable()
{
  ;
}

TestFunctionVariable::
TestFunctionVariable(const std::string& name)
  : Variable(name, Variable::testFuncion)
{
  ;
}

TestFunctionVariable::
TestFunctionVariable(const TestFunctionVariable& rv)
  : Variable(rv)
{
  ;
}

TestFunctionVariable::~TestFunctionVariable()
{
  ;
}



ReferenceCounting<MeshExpression>
MeshVariable::
expression() const
{
  if(__expression == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "undefined mesh '"+name()+"'",
		       ErrorHandler::normal);
  }
  return __expression;
}

ConstReferenceCounting<MeshExpression>
MeshVariable::operator=(ReferenceCounting<MeshExpression> e)
{
  __expression = e;
  return __expression;
}

MeshVariable::MeshVariable(const std::string& name)
  : Variable(name, Variable::mesh),
    __expression(0)
{
  ;
}

MeshVariable::MeshVariable(const std::string& name,
			   ReferenceCounting<MeshExpression> expression)
  : Variable(name, Variable::mesh),
    __expression(expression)
{
  ;
}

MeshVariable::MeshVariable(const MeshVariable& rv)
  : Variable(rv),
    __expression(rv.__expression)
{
}

MeshVariable::~MeshVariable()
{
  ;
}



ReferenceCounting<SceneExpression>
SceneVariable::
expression() const
{
  if(__expression == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "undefined scene '"+name()+"'",
		       ErrorHandler::normal);
  }
  return __expression;
}

ConstReferenceCounting<SceneExpression>
SceneVariable::operator=(ReferenceCounting<SceneExpression> e)
{
  __expression = e;
  return __expression;
}

SceneVariable::SceneVariable(const std::string& name)
  : Variable(name, Variable::scene),
    __expression(0)
{
  fferr(2) << "\nstoring scene " << name << '\n';
}

SceneVariable::SceneVariable(const std::string& name,
			   ReferenceCounting<SceneExpression> expression)
  : Variable(name, Variable::scene),
    __expression(expression)
{
  ;
}

SceneVariable::SceneVariable(const SceneVariable& rv)
  : Variable(rv),
    __expression(rv.__expression)
{
}

SceneVariable::~SceneVariable()
{
  ;
}



ReferenceCounting<DomainExpression>
DomainVariable::
expression() const
{
  if(__expression == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "undefined domain '"+name()+"'",
		       ErrorHandler::normal);
  }
  return __expression;
}

ConstReferenceCounting<DomainExpression>
DomainVariable::
operator=(ReferenceCounting<DomainExpression> e)
{
  __expression = e;
  return __expression;
}

DomainVariable::DomainVariable(const std::string& name)
  : Variable(name, Variable::domain),
    __expression(0)
{
  ;
}

DomainVariable::DomainVariable(const std::string& name,
			       ReferenceCounting<DomainExpression> expression)
  : Variable(name, Variable::domain),
    __expression(expression)
{
  ;
}

DomainVariable::DomainVariable(const DomainVariable& rv)
  : Variable(rv),
    __expression(rv.__expression)
{
}

DomainVariable::~DomainVariable()
{
  ;
}




ReferenceCounting<OFStreamExpression>
OFStreamVariable::
expression() const
{
  if(__expression == 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "undefined ofstream '"+name()+"'",
		       ErrorHandler::normal);
  }
  return __expression;
}

ConstReferenceCounting<OFStreamExpression>
OFStreamVariable::
operator=(ReferenceCounting<OFStreamExpression> e)
{
  __expression = e;
  return __expression;
}

OFStreamVariable::OFStreamVariable(const std::string& name)
  : Variable(name, Variable::ofstream),
    __expression(0)
{
  ;
}

OFStreamVariable::OFStreamVariable(const std::string& name,
			       ReferenceCounting<OFStreamExpression> expression)
  : Variable(name, Variable::ofstream),
    __expression(expression)
{
  ;
}

OFStreamVariable::OFStreamVariable(const OFStreamVariable& rv)
  : Variable(rv),
    __expression(rv.__expression)
{
}

OFStreamVariable::~OFStreamVariable()
{
  ;
}
