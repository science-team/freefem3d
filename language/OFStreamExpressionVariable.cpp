//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OFStreamExpressionVariable.cpp,v 1.3 2007/02/10 17:20:53 delpinux Exp $

#include <OFStreamExpressionVariable.hpp>

#include <VariableRepository.hpp>

ConstReferenceCounting<OFStreamExpression>
OFStreamExpressionVariable::ofstreamExpression() const
{
  return __ofstreamVariable->expression();
}

OFStreamExpressionVariable::
OFStreamExpressionVariable(const std::string& ofstreamName)
  : OFStreamExpression(OFStreamExpression::variable),
    __ofstreamName(ofstreamName),
    __ofstreamVariable(0)
{
  ;
}

OFStreamExpressionVariable::
OFStreamExpressionVariable(const OFStreamExpressionVariable& e)
  : OFStreamExpression(e),
    __ofstreamName(e.__ofstreamName),
    __ofstreamVariable(e.__ofstreamVariable)
{
  ;
}

OFStreamExpressionVariable::
~OFStreamExpressionVariable()
{
  ;
}

void OFStreamExpressionVariable::
execute()
{
  __ofstreamVariable = VariableRepository::instance().findVariable<OFStreamVariable>(__ofstreamName);
  __fout = __ofstreamVariable->expression()->ofstream();
}
