//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryExpressionReferences.hpp,v 1.1 2006/08/27 21:33:53 delpinux Exp $

#ifndef BOUNDARY_EXPRESSION_REFERENCES_HPP
#define BOUNDARY_EXPRESSION_REFERENCES_HPP

#include <BoundaryExpression.hpp>
#include <set>

/**
 * @file   BoundaryExpressionReferences.hpp
 * @author Stephane Del Pino
 * @date   Mon Aug  7 16:22:01 2006
 * 
 * @brief This class defines boundary expression reference
 */
class BoundaryExpressionReferences
  : public BoundaryExpression
{
public:
  typedef
  std::set<ReferenceCounting<RealExpression> >
  ReferencesSet;

private:
  ReferencesSet __references;	/**< set of references */

  /** 
   * Writes the expression to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& put(std::ostream& os) const;

public:
  /** 
   * Check is boundary has POVRay references
   * 
   * @return false
   */
  bool hasPOVBoundary() const
  {
    return false;
  }

  /** 
   * Eexecutes the expression
   * 
   */
  void execute();

  /** 
   * Adds a reference to the set
   * 
   * @param reference added reference
   */
  void add(ReferenceCounting<RealExpression> reference)
  {
    __references.insert(reference);
  }

  /** 
   * Constructor
   * 
   */
  BoundaryExpressionReferences();

  /** 
   * Constructor
   * 
   * @param borderName name of the border
   */
  BoundaryExpressionReferences(const std::string& borderName);

  /** 
   * Copy constructor
   * 
   * @param r given boundary expression reference
   */
  BoundaryExpressionReferences(const BoundaryExpressionReferences& r);

  /** 
   * Destructor
   * 
   */
  ~BoundaryExpressionReferences();
};

#endif // BOUNDARY_EXPRESSION_REFERENCES_HPP
