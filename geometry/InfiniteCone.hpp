//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: InfiniteCone.hpp,v 1.2 2006/10/01 23:25:58 delpinux Exp $

#ifndef INFINITE_CONE_HPP
#define INFINITE_CONE_HPP

#include <TinyVector.hpp>
#include <Shape.hpp>

#include <iostream>

class Cone;

/**
 * @file   InfiniteCone.hpp
 * @author Stephane Del Pino
 * @date   Mon Oct  2 00:12:50 2006
 * 
 * @brief describes an infinite cone. It does not exist in POVRay and is used internally
 */
class InfiniteCone
  : public Shape
{
private:
  const Vertex __center1;	/**< first face center */
  const Vertex __center2;	/**< second face center */

  const TinyVector<3, real_t>
  __axisVector;			/**< vector defining the cone axis */

  const real_t __height;	/**< height of the infinite cone. This
				   is useful to check if the point is
				   inside the InfiniteCone */

  const real_t __radius1;	/**< first face radius */
  const real_t __radius2;	/**< second face radius */

protected:
  /** 
   * Checks if a point is in the InfiniteCone
   * 
   * @param x given point
   * 
   * @return true if @f$ x\in S @f$
   */
  bool __inShape(const TinyVector<3, real_t>& x) const;

  /** 
   * Writes the InfiniteCone to a stream
   * 
   * @param os given strea,
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "internal infinite cone";
    return os;
  }

  /** 
   * gets a copy of the InfiniteCone
   * 
   * @return deep copy of the InfiniteCone
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Extracts a from the cone \a c an InfiniteCone
   * 
   * @param c the given cone
   * 
   */
  InfiniteCone(const Cone& c);

  /** 
   * Copy constructor
   * 
   * @param c a given InfiniteCone
   * 
   */
  InfiniteCone(const InfiniteCone& c);

  /** 
   * 
   * Destructor
   * 
   */
  ~InfiniteCone()
  {
    ;
  }
};

#endif // INFINITE_CONE_HPP
