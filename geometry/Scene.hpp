//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Scene.hpp,v 1.7 2007/06/09 10:37:08 delpinux Exp $

#ifndef SCENE_HPP
#define SCENE_HPP

#include <Object.hpp>
#include <Index.hpp>

#include <StreamCenter.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <map>

/**
 * @file   Scene.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun  9 19:22:48 2002
 * 
 * @brief  Set of ojects
 *
 * The Scene class is the "main" class of the computation.  It
 * contains all objects of the scene, the mesh, boundary conditions,
 * variables, ...
 * 
 */
class Scene
{
private:
  //! The vector containing objects.
  std::vector<ConstReferenceCounting<Object> > __objects;

  struct VectorOrder
  {
    bool operator()(const TinyVector<3>& a,
		    const TinyVector<3>& b) const
    {
      bool less;
      if (a[0] < b[0]) {
	less = true;
      } else if (a[0] == b[0]) {
	if (a[1] < b[1]) {
	  less = true;
	} else if (a[1] == b[1]) {
	  if (a[2] < b[2]) {
	    less = true;
	  } else {
	    less = false;
	  }
	} else {
	  less = false;
	}
      } else {
	less = false;
      }
      return less;
    }
  };

  //! references list
  typedef std::map<TinyVector<3,real_t>, int,
		   Scene::VectorOrder> ReferenceMap;
  ReferenceMap __references;

public:
  //! Returns the number of Object.
  inline size_t nbObjects() const
  {
    return __objects.size();
  }

  /** 
   * Stores the object o and checks its reference
   * 
   * @param o the object to add
   */
  inline void add(ConstReferenceCounting<Object> o)
  {
    if (o->hasReference()) {
      ReferenceMap::iterator i = __references.find(o->reference());
      if (i == __references.end()) {
	int size = __references.size();
	__references[o->reference()] = size;
      }
    }

    // finaly store the object
    __objects.push_back(o);
  }

  //! Read-only access to the Object \a i.
  ConstReferenceCounting<Object> object(const size_t& i) const
  {
    ASSERT (i<__objects.size());
    return __objects[i];
  }

  int getReferenceNumber(const TinyVector<3,real_t>& t) const
  {
    ReferenceMap::const_iterator i = __references.find(t);
    if (i == __references.end()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 stringify(t)+" reference has not been found",
			 ErrorHandler::normal);
    }
    return i->second;
  }

  /** 
   * Default Constructor
   * 
   */
  Scene();

  /** 
   * Copy constructor
   * 
   * @param S a Scene
   */
  Scene(const Scene& S);

  /** 
   * Destructor
   * 
   */
  ~Scene()
  {
    ;
  }
};

#endif // SCENE_HPP
