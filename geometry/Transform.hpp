//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Transform.hpp,v 1.4 2006/10/01 23:25:58 delpinux Exp $

// This class is the base class for all transformations such as
// Rotations, Translations, ...

#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include <string>

#include <Vertex.hpp>
#include <TransType.h>

#include <ReferenceCounting.hpp>
#include <ErrorHandler.hpp>

/**
 * @file   Transform.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 16:42:37 2006
 * 
 * @brief This class is the base class for transformations that can be
 * applied to Virtual Reality objects.
 * 
 * @todo
 * @li try to move the enum TransType inside the class.
 * @li make virtual function instead of switch in @p TypeName (then remove
 * TransType?)
 * @li change the name to Transformation?
 * @li check if the use of Default constructor is needed in children classes.
 */
class Transform
{
protected:
  const TransType __type;	/**< Transformation type */

public:
  /** 
   * Read-only access to the type of the transformation.
   * 
   * @return __type
   */
  inline const TransType& type() const
  {
    return __type;
  }

  /** 
   * Writes the POVRay Transform to a string
   * 
   * @return POVRay Transform string
   */
  virtual std::string povWrite() const = 0;

  /** 
   * Applies the transformation to a point 
   * 
   * @param x given point
   * 
   * @return @f$ T(x) @f$
   */
  virtual TinyVector<3,real_t> operator()(const TinyVector<3>& x) const = 0;

  /** 
   * Applies the inverse transformation to a point 
   * 
   * @param x given point
   * 
   * @return @f$ T^{-1}(x) @f$
   */
  virtual TinyVector<3,real_t> inverse(const TinyVector<3>& x) const = 0;

  /** 
   * Gets a copy of the Transform
   * 
   * @return deep copy of the Transform
   */
  virtual ReferenceCounting<Transform> getCopy() const = 0;

  /** 
   * Constructor
   * 
   * @param type transformation type
   */
  Transform(const TransType& type)
    : __type(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param t given Transform
   */
  Transform(const Transform& t)
    : __type(t.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~Transform()
  {
    ;
  }
};

#endif // TRANSFORM_HPP
