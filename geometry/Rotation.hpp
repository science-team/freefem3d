//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Rotation.hpp,v 1.3 2006/10/01 23:25:58 delpinux Exp $

#ifndef ROTATION_HPP
#define ROTATION_HPP

#include <Transform.hpp>
#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

/**
 * @file   Rotation.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 16:45:17 2006
 * 
 * @brief This class defines rotations using the same description that
 * @p POV-Ray. It means that rotations are performed using:
 * 
 * @li an angle of @a angles[0] around the X axis;
 * @li an angle of @a angles[1] around the Y axis;
 * @li an angle of @a angles[2] around the Z axis.
 */
class Rotation
  : public Transform
{
private:
  const TinyVector<3,real_t>
  __angles;			/**< Rotation angles as given in
				     POVRay */

  /*! 
  */
  TinyMatrix<3,3> __matrix;	/**< The inverse of the rotation is
				   stored in the matrix. This way one
				   can know if a vertex is the image
				   of a vertex contained in a base
				   shape; so, if a vertex is in the
				   image of shape! */

public:
  /** 
   * Applies the Rotation to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ R(x) @f$
   */
  TinyVector<3,real_t> operator()(const TinyVector<3>& x) const;

  /** 
   * Applies the inverse Rotation to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ R^{-1}(x) @f$
   */
  TinyVector<3,real_t> inverse(const TinyVector<3>& v) const;

  /** 
   *  Prints Rotation informations to a string.
   * 
   * @return Rotations information string
   */
  std::string povWrite() const;

  /** 
   * Gets a copy of the Rotation
   * 
   * @return deep copy of the rotation
   */
  ReferenceCounting<Transform> getCopy() const;

  /** 
   * Constructor
   * 
   * @param r given POVRay angles
   */
  Rotation(const TinyVector<3>& r);

  /** 
   * Copy Constructor
   * 
   * @param r given rotation
   */
  Rotation(const Rotation& r);

  /** 
   * Destructor
   * 
   */
  ~Rotation()
  {
    ;
  }
};

#endif // ROTATION_HPP
