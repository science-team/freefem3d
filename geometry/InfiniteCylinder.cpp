//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: InfiniteCylinder.cpp,v 1.3 2006/10/01 23:25:58 delpinux Exp $

#include <InfiniteCylinder.hpp>
#include <Cylinder.hpp>

InfiniteCylinder::
InfiniteCylinder(const Cylinder& c)
  : Shape(InfiniteCylinder::infiniteCylinder),
    __center(c.__center),
    __unaryVector(c.__unaryVector),
    __radius2(c.__radius2)
{
  this->setTransformationsList(c.transformationsList());
}

InfiniteCylinder::
InfiniteCylinder(const InfiniteCylinder& c)
  : Shape(c),
    __center(c.__center),
    __unaryVector(c.__unaryVector),
    __radius2(c.__radius2)
{
  ;
}

bool InfiniteCylinder::
__inShape(const TinyVector<3, real_t>& v) const
{
  TinyVector<3, real_t> x = v-__center;

  real_t ux = __unaryVector * x;
  TinyVector<3, real_t> normal (x - (ux * __unaryVector));
  return (normal * normal < __radius2);
}

ReferenceCounting<Shape> InfiniteCylinder::
__getCopy() const
{
  return new InfiniteCylinder(*this);
}
