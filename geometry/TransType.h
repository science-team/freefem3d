/* $Id: TransType.h,v 1.3 2004/02/01 19:24:58 delpinux Exp $ */


#ifndef _TRANSTYPE_HPP_
#define _TRANSTYPE_HPP_

#include <Types.hpp>

/*
  This must be defined separatly
  Because the lexer is generated in C code
*/

enum TransType {
  translation,
  rotation,
  scale,
  matrix
};

typedef struct parsetrans {
  int number;
#ifdef __cplusplus
  /* hack to provide single interface to C and C++ :-( */
  TransType* type;
#endif /* __cplusplus */
  real_t *vect[3];
  bool hasRef;   /* true if the object has a reference */
  bool inverse;  /* true if inverse */
  real_t ref[4]; /* The first 3 components contains the REF vector */
                 /* The other field is used to detect if the object is
		     to be taken into account in the scene */
  real_t mat[12];
} parsetrans;

typedef struct {
  char* name;
  int type; // 0 for real_t, 1 for char*
  union {
    real_t d;
    char* s;
  } val;
} variable;



#endif /* _TRANSTYPE_HPP_ */
