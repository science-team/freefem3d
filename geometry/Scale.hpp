//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Scale.hpp,v 1.3 2006/10/01 23:25:58 delpinux Exp $

#ifndef SCALE_HPP
#define SCALE_HPP

#include <Transform.hpp>
#include <TinyVector.hpp>

/**
 * @file   Scale.hpp
 * @author Stephane Del Pino
 * @date   Mon Oct  2 00:47:31 2006
 * 
 * @brief    This class defines POVRay scalings
 * 
 * It means that scales are performed using:
 * @li an scale of \a angles[0] by the X axis;
 * @li an scale of \a angles[1] by the Y axis;
 * @li an scale of \a angles[2] by the Z axis.
 */
class Scale
  : public Transform
{
private:
  TinyVector<3,real_t>
  __scale;			/**< POVRay scale values */

public:
  /** 
   * Applied the Scale to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ S(x) @f$
   */
  TinyVector<3,real_t> operator()(const TinyVector<3,real_t>& x) const;

  /** 
   * Applies the inverse of a Scale to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ S^{-1}(x) @f$
   */
  TinyVector<3,real_t> inverse(const TinyVector<3,real_t>& x) const;

  /** 
   * Writes the POVRay expression to a stream
   * 
   * @return POVRay expression
   */
  std::string povWrite() const;

  /** 
   * Gets a copy of the Scale
   * 
   * @return deep copy of the Scale
   */
  ReferenceCounting<Transform> getCopy() const;

  /** 
   * Constructor
   * 
   * @param x given Scale vector
   */
  Scale(const TinyVector<3,real_t>& x);

  /** 
   * Copy Constructor
   * 
   * @param S given Scale
   */
  Scale(const Scale& S);

  /** 
   * Destructor
   * 
   */
  ~Scale()
  {
    ;
  }
};

#endif // SCALE_HPP
