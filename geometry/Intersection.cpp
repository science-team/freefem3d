//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Intersection.cpp,v 1.3 2006/10/01 23:25:58 delpinux Exp $

#include <Intersection.hpp>

bool Intersection::
__inShape (const TinyVector<3, real_t>& x) const
{
  for (Intersection::const_iterator i = __objects.begin();
       i != __objects.end(); ++i) {
    if (not((*i)->inside(x))) {
      return false;
    }
  }

  return true;
}

std::ostream& Intersection::
__put(std::ostream& s) const
{
  s << "intersection {\n";
  for (Intersection::const_iterator i = __objects.begin();
       i != __objects.end(); ++i) {
    s << (*(*i));
  }
  for (size_t i=0; i<numberOfTransformations(); i++)
    s << __trans[i]->povWrite() << '\n';
  s << "}\n";
  return s;
}

Intersection::
Intersection()
  : Shape(Shape::intersection)
{
  ;
}

Intersection::
Intersection(const Intersection& I)
  : Shape(I)
{
  for (ObjectList::const_iterator i = I.__objects.begin();
       i != I.__objects.end(); ++i) {
    __objects.push_back((*i)->getCopy());
  }
}

ReferenceCounting<Shape> Intersection::
__getCopy() const
{
  return new Intersection(*this);
}
