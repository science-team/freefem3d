//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Shape.hpp,v 1.11 2007/04/21 14:49:11 delpinux Exp $

#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <ReferenceCounting.hpp>
#include <Transform.hpp>

#include <vector>

/**
 * @file   Shape.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 14:51:41 2006
 * 
 * @brief  This virtual class allow to manipulate shapes (primitives and set operations).
 */
class Shape
{
public:
  /**
   * @enum ShapeType
   * This is the list of identifiers used for shapes.
   * Identifiers are mean to be explicit ...
   * @li union_ is used instead of 'union' because 'union' is a C++ keyword.
   * @li not_ is used instead of 'not' because 'not' is a C++ keyword.
   */
  enum ShapeType { 
    sphere          = 1,
    cylinder        = 2,
    cone            = 3,
    cube            = 4,
    plane           = 5,
    torus           = 6,
    union_          = 7,    
    difference      = 8,
    intersection    = 9,
    not_            =10,
    infiniteCylinder=11,
    infiniteCone    =12,
    analytic        =13
  };

  typedef std::vector<ConstReferenceCounting<Transform> > TransformationsList;

protected:
  const ShapeType __type;	/**< Type of the transformation */

  TransformationsList __trans;	/**< Stores transformation set */

  /** 
   * Returns true if @a x is IN the PRIMITIVE shape.
   * 
   * @param x point
   * 
   * @return true if @f$ x\in S@f$
   */
  virtual bool __inShape(const TinyVector<3, real_t>& x) const = 0;

  /** 
   * Return overloaded output.
   * 
   * @param os stream
   * 
   * @return os
   */
  virtual std::ostream& __put(std::ostream& os) const = 0;


  /** 
   * Copies only the shape
   * 
   * @return shape copy
   */
  virtual ReferenceCounting<Shape> __getCopy() const = 0;

public:
  /** 
   * Creates a copy of the shape
   * 
   * @return copy of the shape
   */
  ReferenceCounting<Shape> getCopy() const
  {
    ReferenceCounting<Shape> shape = this->__getCopy();
    TransformationsList transformationsList;
    for (TransformationsList::const_iterator i=__trans.begin();
	 i != __trans.end(); ++i) {
      const ConstReferenceCounting<Transform>& t = *i;
      transformationsList.push_back(t->getCopy());
    }
    shape->setTransformationsList(transformationsList);

    return shape;
  }

  /** 
   * read only Access to the transformation lists
   * 
   * @return __trans
   */
  const TransformationsList& transformationsList() const
  {
    return __trans;
  }

  /** 
   * sets the transformation list
   * 
   */
  void setTransformationsList(const TransformationsList& l)
  {
    __trans = l;
  }

  /** 
   * Checks if a point is in the shape
   * 
   * @param x the point
   * 
   * @return true if @f$ x\in S @f$
   */
  inline bool inside(const TinyVector<3,real_t>& x) const
  {
    TinyVector<3,real_t> X = x;
    for (size_t i = numberOfTransformations()-1; i<numberOfTransformations(); --i)
      X = inverseTransformation(i, X);
    return this->__inShape(X);
  }

  /** 
   * Read only access to the type of the shape
   * 
   * @return __type
   */
  const ShapeType& type() const
  { 
    return __type;
  }

  /** 
   * Gets the number of transformations applied to the shape
   * 
   * @return number of transformations
   */
  inline size_t numberOfTransformations() const
  {
    return __trans.size();
  }

  /** 
   * Adds a transformation to the shape
   * 
   * @param t given transformation 
   */
  void setTransformation(const Transform& t);

  /** 
   * Applies inverse of transformation @a i to the point @a x.
   * 
   * @param i number of the transformation
   * @param x the point to inverse
   * 
   * @return the inverse
   */
  TinyVector<3, real_t> inverseTransformation(const size_t& i,
					      const TinyVector<3,real_t>& x) const
  {
    return __trans[i]->inverse(x);
  }

  /** 
   * Prints a shape to a stream
   * 
   * @param os stream
   * @param s shape
   * 
   * @return os
   */
  friend std::ostream& operator << (std::ostream& os,
				    const Shape& s);

  /** 
   * Builds the transformations vector using informations stored in @a trans.
   * 
   * @param trans transformation parser
   */
  void parseTransform(const parsetrans& trans);

  /** 
   * Constructor
   * 
   * @param type fiven shape type
   */
  Shape(const ShapeType& type)
    : __type(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param shape given shape
   */
  Shape(const Shape& shape)
    : __type(shape.__type),
      __trans(shape.__trans)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~Shape()
  {
    ;
  }
};

#endif // SHAPE_HPP
