//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Cube.hpp,v 1.6 2006/10/01 23:25:58 delpinux Exp $

#ifndef CUBE_HPP
#define CUBE_HPP

#include <Shape.hpp>
#include <Vertex.hpp>

/**
 * @file   Cube.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 19:19:19 2006
 * 
 * @brief  POVRay Cube
 */
class Cube
  : public Shape
{
private:
  const Vertex __lower;		/**< lower corner */
  const Vertex __higher;	/**< higher corner */

protected:
  /** 
   * returns true if a point is inside the shape.
   * 
   * @param x point to test
   * 
   * @return true if @a x is inside the shape
   */
  bool __inShape(const TinyVector<3, real_t>& x) const
  {
    return ((x[0] <= __higher.x()) and
	    (x[1] <= __higher.y()) and
	    (x[2] <= __higher.z()) and
	    (x[0] >= __lower.x())  and
	    (x[1] >= __lower.y())  and
	    (x[2] >= __lower.z()));
  }

  /** 
   * Prints the Cube to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

  /** 
   * gets a copy of the Cube
   * 
   * @return deep copy of the cube
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Gets lower corner's @a i th coordinate
   * 
   * @param i coorinate number
   * 
   * @return coordinate value
   */
  const real_t& lowerCorner(const size_t& i) const
  {
    return __lower[i];
  }

  /** 
   * Gets higher corner's @a i th coordinate
   * 
   * @param i coorinate number
   * 
   * @return coordinate value
   */
  const real_t& higherCorner(const size_t& i) const
  {
    return __higher[i];
  }

  /** 
   * Builds a Cube based on vertices @a a and @a b
   * 
   * @param a first vertex
   * @param b second vertex
   */
  Cube(const Vertex& a,
       const Vertex& b);

  /** 
   * Copy constructor
   * 
   * @param C given cube
   */
  Cube(const Cube& C);

  /** 
   * Destructor
   * 
   */
  ~Cube()
  {
    ;
  }
};

#endif // CUBE_HPP
