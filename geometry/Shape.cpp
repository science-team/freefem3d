//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Shape.cpp,v 1.3 2006/10/01 23:25:58 delpinux Exp $

#include <Shape.hpp>

#include <Translation.hpp>
#include <Rotation.hpp>
#include <Scale.hpp>
#include <TransformMatrix.hpp>

#include <Scene.hpp>

void Shape::
setTransformation(const Transform& T)
{
  __trans.push_back(T.getCopy());
}

std::ostream& operator << (std::ostream& os, const Shape& shape)
{
  return shape.__put(os);
}

void Shape::
parseTransform(const parsetrans& trans)
{
  for (int i=0; i<trans.number ; i++) {
    switch (trans.type[i]) {
    case matrix: {
      TransformMatrix M(trans.mat);
      this->setTransformation(M);
      break;
    }
    case rotation: {
      TinyVector<3> v( trans.vect[0][i],
		       trans.vect[1][i],
		       trans.vect[2][i]);
      Rotation R(v);
      this->setTransformation(R);
      break;
    }
    case translation: {
      TinyVector<3> v( trans.vect[0][i],
		       trans.vect[1][i],
		       trans.vect[2][i]);
      Translation T(v);
      this->setTransformation(T);
      break;
    }
    case scale: {
      TinyVector<3> v( trans.vect[0][i],
		       trans.vect[1][i],
		       trans.vect[2][i]);
      Scale S(v);
      this->setTransformation(S);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unknown transformation",
			 ErrorHandler::unexpected);
    }
    }
  }
}

