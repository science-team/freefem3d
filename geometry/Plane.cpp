//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Plane.cpp,v 1.5 2006/10/01 23:25:58 delpinux Exp $

#include <Plane.hpp>
#include <Cylinder.hpp>
#include <Cone.hpp>
#include <Cube.hpp>

bool Plane::
__inShape(const TinyVector<3, real_t>& p) const
{
  const real_t d = __unitNormal * (p-__origine) ;
  return (d < 0); // __unitNormal is exterior
}

Plane::
Plane(const Cube& c,
      const size_t& faceNumber)
  : Shape(Shape::plane)
{
  switch(faceNumber) {
  case 0: {
    __unitNormal = __normal = TinyVector<3, real_t>(-1,0,0);
    __distance = -c.lowerCorner(0);
    __origine = __distance*__unitNormal;
    break;
  }
  case 1: {
    __unitNormal = __normal = TinyVector<3, real_t>(0,-1,0);
    __distance = -c.lowerCorner(1);
    __origine = __distance*__unitNormal;
    break;
  }
  case 2: {
    __unitNormal = __normal = TinyVector<3, real_t>(0,0,-1);
    __distance = -c.lowerCorner(2);
    __origine = __distance*__unitNormal;
    break;
  }
  case 3: {
    __unitNormal = __normal = TinyVector<3, real_t>(1,0,0);
    __distance = c.higherCorner(0);
    __origine = __distance*__unitNormal;
    break;
  }
  case 4: {
    __unitNormal = __normal = TinyVector<3, real_t>(0,1,0);
    __distance = c.higherCorner(1);
    __origine = __distance*__unitNormal;
    break;
  }
  case 5: {
    __unitNormal = __normal = TinyVector<3, real_t>(0,0,1);
    __distance = c.higherCorner(2);
    __origine = __distance*__unitNormal;
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
  this->setTransformationsList(c.transformationsList());
}

Plane::
Plane(const Cylinder& c,
      const size_t& faceNumber)
  : Shape(Shape::plane)
{
  switch(faceNumber) {
  case 0: {
    TinyVector<3, real_t> x = c.__c1 - c.__center;
    __unitNormal = __normal = (1./Norm(x)) * x;
    __origine = c.__c1;
    __distance = __unitNormal*__origine;
    break;
  }
  case 1: {
    TinyVector<3, real_t> x = c.__c2 - c.__center;
    __unitNormal = __normal = (1./Norm(x)) * x;
    __origine = c.__c2;
    __distance = __unitNormal*__origine;
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
  this->setTransformationsList(c.transformationsList());
}


Plane::
Plane(const Cone& c,
      const size_t& faceNumber)
  : Shape(Shape::plane)
{
  switch(faceNumber) {
  case 0: {
    TinyVector<3, real_t> x = c.__center1 - c.__center2;
    __unitNormal = __normal = (1./Norm(x)) * x;
    __origine = c.__center1;
    __distance = __unitNormal*__origine;
    break;
  }
  case 1: {
    TinyVector<3, real_t> x = c.__center2 - c.__center1;
    __unitNormal = __normal = (1./Norm(x)) * x;
    __origine = c.__center2;
    __distance = __unitNormal*__origine;
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
  this->setTransformationsList(c.transformationsList());
}

Plane::
Plane(const Vertex& normal,
      const real_t& distance)
  : Shape(plane),
    __normal(normal),
    __distance(distance),
    __unitNormal(1./Norm(__normal)*__normal),
    __origine(distance*__unitNormal)
{
  ;
}

Plane::
Plane(const Plane& P)
  : Shape(P),
    __normal(P.__normal),
    __distance(P.__distance),
    __unitNormal(P.__unitNormal),
    __origine(P.__origine)
{
  ;
}

std::ostream& Plane::
__put(std::ostream& s) const
{
  s << "plane {\n" << __normal << ',' << __distance << "\n}\n";
  return s;
}

ReferenceCounting<Shape> Plane::
__getCopy() const
{
  return new Plane(*this);
}
