//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Cylinder.cpp,v 1.4 2006/10/01 23:25:58 delpinux Exp $

#include <Cylinder.hpp>
#include <TinyVector.hpp>

#include <cmath>

Cylinder::
Cylinder(const TinyVector<3, real_t>& a,
	 const TinyVector<3, real_t>& b,
	 const real_t& r)
  : Shape(cylinder),
    __c1(a), __c2(b),
    __center(0.5*(a+b)),
    __radius (r),
    __radius2(r*r),
    __half_height(Norm(__center - __c1)),
    __unaryVector(1./(2.*__half_height)*(b-a))
{
  ;
}

Cylinder::
Cylinder(const Cylinder& C)
  : Shape(C),
    __c1(C.__c1),
    __c2(C.__c2),
    __center(C.__center),
    __radius(C.__radius),
    __radius2(C.__radius2),
    __half_height(C.__half_height),
    __unaryVector(C.__unaryVector)
{
  ;
}

std::ostream& Cylinder::
__put(std::ostream& os) const
{
  os << "cylinder {\n" << __c1
    << ", " << __c2  << ", " << __radius << '\n';
  for (size_t i=0; i<numberOfTransformations(); i++)
    os << __trans[i]->povWrite() << '\n';
  os << "}\n";
  return os;
}

inline bool Cylinder::
__inShape(const TinyVector<3, real_t>& v) const
{
  TinyVector<3, real_t> x = v-__center;

  real_t ux = __unaryVector * x;
  TinyVector<3, real_t> normal (x - (ux * __unaryVector));
  return ((std::abs(ux) < __half_height) && (normal * normal < __radius2));
}

ReferenceCounting<Shape> Cylinder::
__getCopy() const
{
  return new Cylinder(*this);
}
