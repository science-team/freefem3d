//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Translation.hpp,v 1.3 2006/10/01 23:25:58 delpinux Exp $

#ifndef TRANSLATION_HPP
#define TRANSLATION_HPP

#include <Transform.hpp>
#include <TinyVector.hpp>

/**
 * @file   Translation.hpp
 * @author Stephane Del Pino
 * @date   Mon Oct  2 01:13:41 2006
 * 
 * @brief  POVRay translation
 */
class Translation
  : public Transform
{
private:
  const TinyVector<3,real_t>
  __vect;			/**< Translation vector */

public:
  /** 
   * Applies the translation to a vector
   * 
   * @param x given vector
   * 
   * @return @f$ x+b @f$
   */
  TinyVector<3,real_t> operator()(const TinyVector<3,real_t>& x) const;

  /** 
   * Applies the translation to a vector
   * 
   * @param x 
   * 
   * @return @f$ x-b @f$
   */
  TinyVector<3,real_t> inverse(const TinyVector<3,real_t>& x) const;

  /** 
   * Writes the Translation to a string
   * 
   * @return Translation string
   */
  std::string povWrite() const;

  /** 
   * Gets a copy of the Translation
   * 
   * @return deep copy of the Translation
   */
  ReferenceCounting<Transform> getCopy() const;

  /** 
   * Constructor
   * 
   * @param t translation
   */
  Translation(const TinyVector<3,real_t>& t);

  /** 
   * Copy constructor
   * 
   * @param T given Translation
   */
  Translation(const Translation& T);

  /** 
   * Destructor
   * 
   */
  ~Translation()
  {
    ;
  }
};

#endif // TRANSLATION_HPP
