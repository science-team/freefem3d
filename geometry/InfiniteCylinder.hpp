//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: InfiniteCylinder.hpp,v 1.2 2006/10/01 23:25:58 delpinux Exp $

#ifndef INFINITE_CYLINDER_HPP
#define INFINITE_CYLINDER_HPP

#include <TinyVector.hpp>
#include <Shape.hpp>

#include <iostream>

class Cylinder;

/**
 * @file   InfiniteCylinder.hpp
 * @author Stephane Del Pino
 * @date   Sat Feb  7 18:31:15 2004
 * 
 * @brief used to extract the infinite cylinder part of a finite
 * cylinder
 */
class InfiniteCylinder
  : public Shape
{
private:
  TinyVector<3, real_t> __center; /**< point on the axis */
  TinyVector<3, real_t> __unaryVector; /**< unary vector colinear to the axis */

  real_t __radius2;		/**< square of the cylinder radius */

protected:
  /** 
   * True if the point @a x is inside the shape
   * 
   * @param x a given point
   * 
   * @return true is @a x is inside the cylinder
   */
  bool __inShape(const TinyVector<3, real_t>& x) const;

  /** 
   * Writes a InfiniteCylinder to a stream
   * 
   * @param os given strea,
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "internal infinite cylinder";
    return os;
  }

  /** 
   * Gets a copy of the InfiniteCylinder
   * 
   * @return deep copy of the InfiniteCylinder
   */
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Extracts a from the cylinder \a c an InfiniteCylinder
   * 
   * @param c the given cylinder
   * 
   */
  InfiniteCylinder(const Cylinder& c);

  /** 
   * Copy constructor
   * 
   * @param c a given InfiniteCylinder
   * 
   */
  InfiniteCylinder(const InfiniteCylinder& c);

  /** 
   * Destructor
   * 
   */
  ~InfiniteCylinder()
  {
    ;
  }
};

#endif // INFINITE_CYLINDER_HPP
