//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Sphere.hpp,v 1.4 2006/10/01 23:25:58 delpinux Exp $

// This class provides a toolkit to manipulate Spheres. It inherits from the
// Shape class.

#ifndef SPHERE_HPP
#define SPHERE_HPP

#include <Shape.hpp>

/**
 * @file   Sphere.hpp
 * @author Stephane Del Pino
 * @date   Sun Oct  1 14:37:27 2006
 * 
 * @brief  This is the class which defines a POVRay Sphere.
 */
class Sphere
  : public Shape
{
private:
  Vertex __center;		/**< center of the Sphere */
  const real_t __radius;	/**< radius of the Sphere. */
  const real_t __radius2;	/**< square of the @a radius. */

  /** 
   * Returns @p true if the point @a x is inside the Sphere.
   * 
   * @param x point to localize 
   * 
   * @return true if @f$ x\in S @f$
   */
  inline bool __inShape(const TinyVector<3, real_t>& x) const
  {
    return (__center.distance2(x) <= __radius2);
  }

  //! 
  /** 
   * Prints the Sphere to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const;

protected:
  ReferenceCounting<Shape> __getCopy() const;

public:
  /** 
   * Default constructor
   * 
   */
  Sphere();

  /** 
   * Constructor
   * 
   * @param center center
   * @param radius radius
   */
  Sphere(const Vertex& center,
	 const real_t& radius);

  /** 
   * Copy constructor
   * 
   * @param sphere original sphere 
   */
  Sphere(const Sphere& sphere);

  /** 
   * Destructor
   * 
   */
  ~Sphere()
  {
    ;
  }
};

#endif // SPHERE_HPP
