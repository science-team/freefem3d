//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003, 2004 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ObjectTransformer.hpp,v 1.1 2004/04/25 20:03:16 delpinux Exp $

#ifndef OBJECT_TRANSFORMER_HPP
#define OBJECT_TRANSFORMER_HPP

#include <Shape.hpp>
#include <ReferenceCounting.hpp>

class Object;

/**
 * @file   ObjectTransformer.hpp
 * @author Stephane Del Pino
 * @date   Sun Apr 25 14:31:46 2004
 * 
 * @brief extracts object from boolean operation or compound object
 * 
 * ObjectTransformer is an inner ff3d class used to transfer set
 * operations to primitives. This is useful for meshing surfaces for
 * example.
 */

class ObjectTransformer
{
private:
  Shape::TransformationsList __addedTransformations;

  ObjectTransformer(const ObjectTransformer& s);
public:
  ReferenceCounting<Object>
  operator()(const Object& objectToCopy) const;

  ObjectTransformer(const Shape::TransformationsList& t)
    : __addedTransformations(t)
  {
    ;
  }

  ~ObjectTransformer()
  {
    ;
  }
};

#endif // OBJECT_TRANSFORMER_HPP
