//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Torus.hpp,v 1.4 2006/10/01 23:25:58 delpinux Exp $

#ifndef TORUS_HPP
#define TORUS_HPP

/**
 * @file   Torus.hpp
 * @author Stephane Del Pino
 * @date   Mon Oct  2 00:55:32 2006
 * 
 * @brief  Torus POVRay object
 */
class Torus
  : public Shape
{
private:
  const real_t __radius1;	/**< medium radius */
  const real_t __radius2;	/**< section radius */

protected:
  /** 
   * Checks if a point is inside the Torus
   * 
   * @param p given point
   * 
   * @return true if @f$ p\in T @f$
   */
  inline bool __inShape(const TinyVector<3, real_t>& p) const
  {
    const real_t d2 = p[0]*p[0]+p[2]*p[2];
    if (d2 != 0) {
      const real_t d = std::sqrt(d2);
      const real_t r=(1.-__radius1/d);
      TinyVector<3> x (r*p[0],
		       p[1],
		       r*p[2]);
      return (Norm(x) < __radius2);
    } else {
      return (Norm(p) < __radius2);
    }
  }

  /** 
   * Writes the Torus to a stream
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "torus (" << __radius1 << ',' << __radius2 << ')';
    return os;
  }

  /** 
   * Gets a copy of the Torus
   * 
   * @return deep copy of the Torus
   */
  ReferenceCounting<Shape> __getCopy() const
  {
    return new Torus(*this);
  }

public:
  /** 
   * Constructor
   * 
   * @param r1 medium radius
   * @param r2 section radius
   */
  Torus(const real_t& r1,
	const real_t& r2)
    : Shape(Shape::torus),
      __radius1(r1),
      __radius2(r2)
  {
    ;
  }

  /** 
   * Copy Constructor
   * 
   * @param T given Torus
   */
  Torus(const Torus& T)
    : Shape(T),
      __radius1(T.__radius1),
      __radius2(T.__radius2)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Torus()
  {
    ;
  }
};

#endif // TORUS_HPP
