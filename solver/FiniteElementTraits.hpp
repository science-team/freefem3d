//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FiniteElementTraits.hpp,v 1.8 2007/05/20 23:15:36 delpinux Exp $

#ifndef FINITE_ELEMENT_TRAITS_HPP
#define FINITE_ELEMENT_TRAITS_HPP

#include <Cell.hpp>
#include <Hexahedron.hpp>
#include <Tetrahedron.hpp>
#include <Triangle.hpp>
#include <Quadrangle.hpp>

#include <DiscretizationType.hpp>
#include <ConformTransformation.hpp>

#include <Q0HexahedronFiniteElement.hpp>
#include <Q1HexahedronFiniteElement.hpp>
#include <Q2HexahedronFiniteElement.hpp>

#include <P0TetrahedronFiniteElement.hpp>
#include <P1TetrahedronFiniteElement.hpp>
#include <P2TetrahedronFiniteElement.hpp>

#include <Q0Quadrangle3DFiniteElement.hpp>
#include <Q1Quadrangle3DFiniteElement.hpp>
#include <Q2Quadrangle3DFiniteElement.hpp>

#include <P0Triangle3DFiniteElement.hpp>
#include <P1Triangle3DFiniteElement.hpp>
#include <P2Triangle3DFiniteElement.hpp>

template <typename CellType,
	  DiscretizationType::Type>
struct FiniteElementTraits {};

template <>
struct FiniteElementTraits<Hexahedron,
			   DiscretizationType::lagrangianFEM0>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM0
  };

  typedef Q0HexahedronFiniteElement Type;

  typedef ConformTransformationQ1Hexahedron
  Transformation;

  typedef ConformTransformationQ1HexahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Hexahedron,
			   DiscretizationType::lagrangianFEM1>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM1
  };

  typedef Q1HexahedronFiniteElement Type;

  typedef ConformTransformationQ1Hexahedron
  Transformation;

  typedef ConformTransformationQ1HexahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Hexahedron,
			  DiscretizationType::lagrangianFEM2>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM2
  };

  typedef Q2HexahedronFiniteElement Type;

  typedef ConformTransformationQ1Hexahedron
  Transformation;

  typedef ConformTransformationQ1HexahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<CartesianHexahedron,
			  DiscretizationType::lagrangianFEM0>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM0
  };

  typedef Q0HexahedronFiniteElement Type;

  typedef ConformTransformationQ1CartesianHexahedron
  Transformation;

  typedef ConformTransformationQ1CartesianHexahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<CartesianHexahedron,
			  DiscretizationType::lagrangianFEM1>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM1
  };

  typedef Q1HexahedronFiniteElement Type;

  typedef ConformTransformationQ1CartesianHexahedron
  Transformation;

  typedef ConformTransformationQ1CartesianHexahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<CartesianHexahedron,
			  DiscretizationType::lagrangianFEM2>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM2
  };

  typedef Q2HexahedronFiniteElement Type;

  typedef ConformTransformationQ1CartesianHexahedron
  Transformation;

  typedef ConformTransformationQ1CartesianHexahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Tetrahedron,
			   DiscretizationType::lagrangianFEM0>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM0
  };

  typedef P0TetrahedronFiniteElement Type;

  typedef ConformTransformationP1Tetrahedron
  Transformation;

  typedef ConformTransformationP1TetrahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Tetrahedron,
			   DiscretizationType::lagrangianFEM1>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM1
  };

  typedef P1TetrahedronFiniteElement Type;

  typedef ConformTransformationP1Tetrahedron
  Transformation;

  typedef ConformTransformationP1TetrahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Tetrahedron,
			   DiscretizationType::lagrangianFEM2>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM2
  };

  typedef P2TetrahedronFiniteElement Type;

  typedef ConformTransformationP1Tetrahedron
  Transformation;

  typedef ConformTransformationP1TetrahedronJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Triangle,
			   DiscretizationType::lagrangianFEM0>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM0
  };


  typedef P0Triangle3DFiniteElement Type;

  typedef ConformTransformationP1Triangle
  Transformation;

  typedef ConformTransformationP1TriangleJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Triangle,
			   DiscretizationType::lagrangianFEM1>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM1
  };


  typedef P1Triangle3DFiniteElement Type;

  typedef ConformTransformationP1Triangle
  Transformation;

  typedef ConformTransformationP1TriangleJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Triangle,
			   DiscretizationType::lagrangianFEM2>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM2
  };

  typedef P2Triangle3DFiniteElement Type;

  typedef ConformTransformationP1Triangle
  Transformation;

  typedef ConformTransformationP1TriangleJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Quadrangle,
			   DiscretizationType::lagrangianFEM0>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM0
  };

  typedef Q0Quadrangle3DFiniteElement Type;

  typedef ConformTransformationQ1Quadrangle
  Transformation;

  typedef ConformTransformationQ1QuadrangleJacobian
  JacobianTransformation;
};

template <>
struct FiniteElementTraits<Quadrangle,
			   DiscretizationType::lagrangianFEM1>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM1
  };

  typedef Q1Quadrangle3DFiniteElement Type;

  typedef ConformTransformationQ1Quadrangle
  Transformation;

  typedef ConformTransformationQ1QuadrangleJacobian
  JacobianTransformation;
};


template <>
struct FiniteElementTraits<Quadrangle,
			   DiscretizationType::lagrangianFEM2>
{
  enum {
    DiscretizationType = DiscretizationType::lagrangianFEM2
  };

  typedef Q2Quadrangle3DFiniteElement Type;

  typedef ConformTransformationQ1Quadrangle
  Transformation;

  typedef ConformTransformationQ1QuadrangleJacobian
  JacobianTransformation;
};

#endif // FINITE_ELEMENT_TRAITS_HPP
