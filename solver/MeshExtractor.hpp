//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshExtractor.hpp,v 1.12 2007/02/07 23:54:57 delpinux Exp $

#ifndef MESH_EXTRACTOR_HPP
#define MESH_EXTRACTOR_HPP

#include <ReferenceCounting.hpp>
#include <CellDuplicator.hpp>

#include <ErrorHandler.hpp>

#include <sstream>
#include <set>

/**
 * @file   MeshExtractor.hpp
 * @author Stephane Del Pino
 * @date   Sat Sep 13 16:55:56 2003
 * 
 * @brief Extracts sub-meshes from a given one using elements
 * reference
 * 
 */

template <typename MeshType>
struct CopyBackgroundMesh
{
  static void copy(const MeshType& originalMesh,
		   MeshType* m)
  {
    ;
  }
};

template <>
struct CopyBackgroundMesh<SurfaceMeshOfTriangles>
{
  static void copy(const SurfaceMeshOfTriangles& originalMesh,
		   SurfaceMeshOfTriangles* m)
  {
    m->setBackgroundMesh(originalMesh.backgroundMesh());
  }
};

template <>
struct CopyBackgroundMesh<SurfaceMeshOfQuadrangles>
{
  static void copy(const SurfaceMeshOfQuadrangles& originalMesh,
		   SurfaceMeshOfQuadrangles* m)
  {
    m->setBackgroundMesh(originalMesh.backgroundMesh());
  }
};


template <typename MeshType>
class MeshExtractor
{
private:
  ConstReferenceCounting<MeshType> __originalMesh; /**< The original mesh */

public:
  /** 
   * Returns a sub-mesh composed of given references
   * 
   * @param referencesSet the set of references of elements to extract
   * 
   * @return the sub-mesh
   */
  MeshType*
  operator()(const std::set<size_t>& referencesSet) const
  {
    typedef typename MeshType::CellType CellType;

    std::set<size_t> keptCells;

    for (typename MeshType::const_iterator icell(*__originalMesh);
	 not(icell.end()); ++icell) {
      const CellType& cell = *icell;
      if (referencesSet.find(cell.reference()) != referencesSet.end()) {
	keptCells.insert(__originalMesh->cellNumber(cell));
      }
    }

    if (keptCells.size() == 0) {
      std::stringstream errorMsg;
      errorMsg << "no cell has reference that matches (";
      for (std::set<size_t>::const_iterator i = referencesSet.begin();
	   i != referencesSet.end(); ++i) {
	errorMsg << *i << ',';
      }
      errorMsg << "\b)!" << std::ends;
      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::normal);
    }

    ReferenceCounting<Vector<CellType> > pCellList
      = new Vector<CellType>(keptCells.size());

    Vector<CellType>& cellList = *pCellList;

    size_t n=0;
    for (std::set<size_t>::const_iterator i = keptCells.begin();
	 i != keptCells.end(); ++i, ++n) {
      const CellType& cell = __originalMesh->cell(*i);
      cellList[n] = cell;
    }

    MeshType* m = new MeshType(__originalMesh->__verticesSet,
			       __originalMesh->__verticesCorrespondance,
			       pCellList);

    CopyBackgroundMesh<MeshType>::copy(*__originalMesh, m);

    return m;
  }

  /** 
   * Creates a MeshExtrator using a given \a m mesh
   * 
   * @param m the given mesh
   */
  MeshExtractor(ConstReferenceCounting<MeshType> m)
    : __originalMesh(m)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param m a given MeshExtractor
   */
  MeshExtractor(const MeshExtractor& m)
    : __originalMesh(m.__originalMesh)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~MeshExtractor()
  {
    ;
  }
};

#endif // MESH_EXTRACTOR_HPP
