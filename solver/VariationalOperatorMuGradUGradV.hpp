//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalOperatorMuGradUGradV.hpp,v 1.6 2007/06/10 15:03:31 delpinux Exp $

#ifndef VARIATIONAL_OPERATOR_MU_GRADU_GRADV_HPP
#define VARIATIONAL_OPERATOR_MU_GRADU_GRADV_HPP

#include <VariationalBilinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>
/**
 * @file   VariationalOperatorMuGradUGradV.hpp
 * @author Stephane Del Pino
 * @date   Thu May 30 22:14:40 2002
 * 
 * @brief  @f$ a(u,v) = \int\mu\nabla u\cdot\nabla v@f$
 * 
 */
class VariationalMuGradUGradVOperator
  : public VariationalBilinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __mu;				/**< viscosity: @f$\mu@f$  */

public:

  /** 
   * 
   * Access to @f$\mu@f$
   * 
   * @return __mu
   */
  ConstReferenceCounting<ScalarFunctionBase> mu() const
  {
    return __mu;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int c\mu \nabla u\cdot \nabla v@f$
   */
  ReferenceCounting<VariationalBilinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VariationalMuGradUGradVOperator* newOperator
      = new VariationalMuGradUGradVOperator(*this);

    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__mu);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);

    newOperator->__mu = functionBuilder.getBuiltFunction();

    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param unknownNumber unknown number
   * @param unknownProperty unknown property
   * @param testFunctionNumber  test function number
   * @param testFunctionProperty  test function property
   * @param mu viscosity @f$\mu@f$
   */
  VariationalMuGradUGradVOperator(const size_t& unknownNumber ,
				  const VariationalOperator::Property& unknownProperty,
				  const size_t& testFunctionNumber,
				  const VariationalOperator::Property& testFunctionProperty,
				  ConstReferenceCounting<ScalarFunctionBase> mu)
    : VariationalBilinearOperator(VariationalBilinearOperator::muGradUGradV,
				  unknownNumber, unknownProperty,
				  testFunctionNumber, testFunctionProperty),
      __mu(mu)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V given VariationalMuGradUGradVOperator
   */
  VariationalMuGradUGradVOperator(const VariationalMuGradUGradVOperator& V)
    : VariationalBilinearOperator(V),
      __mu(V.__mu)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalMuGradUGradVOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_MU_GRADU_GRADV_HPP
