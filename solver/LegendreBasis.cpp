//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: LegendreBasis.cpp,v 1.2 2007/05/03 18:42:10 yakoubix Exp $

#include <LegendreBasis.hpp>
#include <iostream>
#include <Types.hpp>
#include <vector>

LegendreBasis::
LegendreBasis(const size_t& degree)
  : __degree(degree)
{
  ;
}

LegendreBasis::
~LegendreBasis()
{
  ;
}

void LegendreBasis::
getValues(const real_t& x,
	  Vector<real_t>& values) const
{
  values[0] = 1;
  if (__degree == 0) return ;

    values[1] = x;

  for (size_t n=1; n<values.size()-1; ++n) {
    values[n+1] = ((2*n+1)*x*values[n]-n*values[n-1]) / (n+1);
  }
}

void  LegendreBasis::
getDerivativeValues(const real_t& x,
		    Vector<real_t>& derivateValues) const
{
  derivateValues[0]= 0;
  if (__degree == 0) return ;

  derivateValues[1]= 1;
  if (__degree == 1) return ;

  Vector<real_t> values(derivateValues.size()-1);
  getValues(x,values);

  for (size_t n=1; n<derivateValues.size()-1; ++n) {
    derivateValues[n+1]
      = ((2*n+1)*(x*derivateValues[n]+values[n])
	 - n*derivateValues[n-1])
      / (n+1);
  }
 }

real_t LegendreBasis::
getValue(const real_t& x, 
	 const size_t& i) const
{
  Vector<real_t> values(__degree+1);
  this->getValues(x,values);
  return values[i];
}

real_t LegendreBasis::
getDerivativeValue(const real_t& x, 
		   const size_t& i) const
{
  Vector<real_t> derivateValues(__degree+1);
  this->getDerivativeValues(x, derivateValues);

  return derivateValues[i];
}
