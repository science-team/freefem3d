//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionReaderVTK.cpp,v 1.3 2007/06/10 15:03:07 delpinux Exp $

#include <ScalarFunctionReaderVTK.hpp>
#include <ScalarFunctionBase.hpp>

#include <ErrorHandler.hpp>
#include <Mesh.hpp>

#include <Structured3DMesh.hpp>

#include <DiscretizationType.hpp>
#include <Vector.hpp>
#include <FEMFunctionBase.hpp>

#include <FEMFunctionBuilder.hpp>

#include <XMLFileReader.hpp>
#include <XMLTree.hpp>

#include <XMLContentPosition.hpp>

#include <fstream>


ConstReferenceCounting<ScalarFunctionBase>
ScalarFunctionReaderVTK::
getFunction() const
{
  ffout(3) << "Reading file '" << __filename << "'\n";
  std::ifstream fin(__filename.c_str());

  if (not(fin)) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "error: cannot open file '"+__filename+"'",
		       ErrorHandler::normal);
  }

  XMLFileReader::instance().read(__filename);

  const XMLTree& xmlTree = * XMLFileReader::instance().xmlTree();
  xmlTree.check();

  // getting file type
  ReferenceCounting<XMLTag> vtkfile = xmlTree.findTag(">VTKFile");
  ReferenceCounting<XMLAttribute> filetype = vtkfile->findAttribute("type");
  ffout(3) << " - file type is '" << filetype->value() << "'\n";

  Vector<real_t> v;
  size_t numberOfCells = 0;
  size_t numberOfPoints = 0;
  size_t numberOfComponents = 0;

  if (filetype->value() == "UnstructuredGrid") {
    ReferenceCounting<XMLTag> piece = xmlTree.findTag(">VTKFile>UnstructuredGrid>Piece");

    // getting number of cells
    {
      ReferenceCounting<XMLAttribute> attribute = piece->findAttribute("NumberOfCells");
      std::istringstream is(attribute->value());
      is >> numberOfCells;
    }

    {
      ReferenceCounting<XMLAttribute> attribute = piece->findAttribute("NumberOfPoints");
      std::istringstream is(attribute->value());
      is >> numberOfPoints;
    }
  } else if (filetype->value() == "ImageData") {
    ReferenceCounting<XMLTag> piece = xmlTree.findTag(">VTKFile>ImageData>Piece");

    // getting number of cells
    {
      ReferenceCounting<XMLAttribute> attribute = piece->findAttribute("Extent");
      std::istringstream is(attribute->value());
      TinyVector<3,size_t> extent0;
      TinyVector<3,size_t> extent1;
      is >> extent0[0] >> extent1[0] >> extent0[1] >> extent1[1] >> extent0[2] >> extent1[2];
      numberOfCells  = (extent1[0]-extent0[0])*(extent1[1]-extent0[1])*(extent1[2]-extent0[2]);
      numberOfPoints = (1+extent1[0]-extent0[0])*(1+extent1[1]-extent0[1])*(1+extent1[2]-extent0[2]);
    }
  } else {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot read filetype '"+filetype->value()+"'",
		       ErrorHandler::unexpected);
  }


  if ((numberOfPoints != __mesh->numberOfVertices()) or
      (numberOfCells != __mesh->numberOfCells())) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "mesh in file '"+__filename+"' is different from 'read' argument",
		       ErrorHandler::normal);      
  }

    ReferenceCounting<XMLTag> dataArray = 0;

    // looking for point data field
    if (xmlTree.hasTag(">VTKFile>"+filetype->value()+">Piece>PointData>DataArray")) {
      XMLTree::const_range pointDataRange = xmlTree.findTagRange(">VTKFile>"+filetype->value()+">Piece>PointData>DataArray");
      for (XMLTree::TagList::const_iterator iDataArray = pointDataRange.first;
	   iDataArray != pointDataRange.second; ++iDataArray) {
	ReferenceCounting<XMLAttribute> name = iDataArray->second->findAttribute("Name");
	if (name->value() == __functionName) {
	  dataArray = iDataArray->second;
	  ReferenceCounting<XMLAttribute> components = iDataArray->second->findAttribute("NumberOfComponents");
	  std::istringstream is(components->value());
	  is >> numberOfComponents;
	  break;
	}
      }
    }

    if (dataArray == 0) { // looking for array data field
      if (xmlTree.hasTag(">VTKFile>"+filetype->value()+">Piece>CellData>DataArray")) {
	XMLTree::const_range cellDataRange = xmlTree.findTagRange(">VTKFile>"+filetype->value()+">Piece>CellData>DataArray");
	for (XMLTree::TagList::const_iterator iDataArray = cellDataRange.first;
	     iDataArray != cellDataRange.second; ++iDataArray) {
	  ReferenceCounting<XMLAttribute> name = iDataArray->second->findAttribute("Name");
	  if (name->value() == __functionName) {
	    dataArray = iDataArray->second;
	    ReferenceCounting<XMLAttribute> components = iDataArray->second->findAttribute("NumberOfComponents");
	    std::istringstream is(components->value());
	    is >> numberOfComponents;
	    break;
	  }
	}
      }
    }

    if (dataArray == 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot find field '"+__functionName+"' in '"+__filename+"'",
			 ErrorHandler::normal);
    }

    ConstReferenceCounting<XMLContentBase> content = dataArray->content();
    if (content == 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot find content for '"+__functionName+"' in '"+__filename+"'",
			 ErrorHandler::normal);
    }

    if (__componentNumber>=numberOfComponents) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "The function '"+__functionName+"' of the file '"+__filename
			 +"' does only contain "+stringify(__componentNumber)+" components.\n"
			 +"Cannot read component number "+stringify(__componentNumber),
			 ErrorHandler::normal);
    }


    switch(content->type()) {
    case XMLContentBase::filePosition: {
      const XMLContentPosition& position = dynamic_cast<const XMLContentPosition&>(*content);

      v.resize(numberOfPoints);
      fin.seekg(position.position(),std::ios_base::beg);
      fin.unget();
      switch (__mesh->type()) {
      case Mesh::cartesianHexahedraMesh: {
	const Structured3DMesh& mesh = dynamic_cast<const Structured3DMesh&>(*__mesh);
	Vector<real_t> componentsValue(numberOfComponents);

	for (size_t k=0; k<mesh.shape().nz(); ++k) {
	  for (size_t j=0; j<mesh.shape().ny(); ++j) {
	    for (size_t i=0; i<mesh.shape().nx(); ++i) {
	      for (size_t l=0; l<componentsValue.size(); ++l) {
		fin >> componentsValue[l];
	      }
	      v[mesh.shape()(i,j,k)] = componentsValue[__componentNumber];
	    }
	  }
	}
	break;
      }
      default: {
	Vector<real_t> componentsValue(numberOfComponents);
	for (size_t i=0; i<numberOfPoints; ++i) {
	  for (size_t j=0; j<componentsValue.size(); ++j) {
	    fin >> componentsValue[j];
	  }
	  v[i] = componentsValue[__componentNumber];
	}
      }
      }
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Bad content for '"+__functionName+"' in '"+__filename+"'",
			 ErrorHandler::normal);

    } 
    }


  FEMFunctionBuilder builder;
  DiscretizationType d(DiscretizationType::lagrangianFEM1);
  builder.build(d, __mesh, v);
  return builder.getBuiltScalarFunction();
}

ScalarFunctionReaderVTK::
ScalarFunctionReaderVTK(const std::string& filename,
			ConstReferenceCounting<Mesh> mesh,
			const std::string& functionName,
			const int& componentNumber)
  : ScalarFunctionReaderBase(filename, mesh, functionName, componentNumber)
{
  ;
}

ScalarFunctionReaderVTK::
ScalarFunctionReaderVTK(const ScalarFunctionReaderVTK& f)
  : ScalarFunctionReaderBase(f)
{
  ;
}

ScalarFunctionReaderVTK::
~ScalarFunctionReaderVTK()
{
  ;
}
