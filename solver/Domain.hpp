//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Domain.hpp,v 1.11 2007/06/09 10:37:07 delpinux Exp $


#ifndef DOMAIN_HPP
#define DOMAIN_HPP

class Scene;

#include <ReferenceCounting.hpp>
#include <Object.hpp>
#include <TinyVector.hpp>

#include <ErrorHandler.hpp>

#include <sstream>
#include <map>

/**
 * @file   Domain.hpp
 * @author Stephane Del Pino
 * @date   Fri Nov 15 18:32:48 2002
 * 
 * @brief This class describes a domain of \f$ R^3\f$ which is the
 * defined by the union of the interior and the exterior of sets of
 * Objects.
 * 
 */

class Domain
{
private:
  /** This boolean describes the Domain this way:
   * \li false: the domain is a subset of \f$ R^3\f$.
   * \li true:  the domain is \f$ R^3\f$
   */
  bool __isR3;

  ReferenceCounting<Object>
  __objects;			/**< The shape of the domain */

  ConstReferenceCounting<Scene>
  __scene;			/**< The POV-Ray scene */

  std::map<TinyVector<3, real_t>, size_t>
  __povToReference;		/**< The POV-Ray to internal
				   references association*/

  void __buildReferenceAssociation(const Object& o);

  bool __inShape(const TinyVector<3>& X) const
  {
    return (*__objects).inside(X);
  }

public:
  /** 
   * Returns true if the considered domain is \f$\mathbf{R}^3\f$
   * 
   * @return true if \f$\Omega=\mathbf{R}^3\f$
   */
  const bool& isR3() const
  {
    return __isR3;
  }

  size_t reference(const TinyVector<3, real_t>& ref) const
  {
    std::map<TinyVector<3, real_t>, size_t>::const_iterator i
      = __povToReference.find(ref);
    if (i == __povToReference.end()) {
      std::stringstream errorMsg;
      errorMsg << "You provided the reference " << ref
	       << " that is not used to define\n"
	       << "your computational domain. The possible references are\n";
      for (std::map<TinyVector<3, real_t>, size_t>::const_iterator j
	     = __povToReference.begin();
	   j != __povToReference.end(); ++j) {
	errorMsg << '\t' << j->first << std::ends;
      }
      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::normal);
    }
    return (*i).second;
  }

  /**
   * Outputs the Domain \a D in the \p std::ostream \a os.
   */
  friend std::ostream& operator<<(std::ostream& os,
				  const Domain& D)
  {
    os << *D.__objects;
    return os;
  }

  /** 
   * Returns true if X is inside the domain
   * 
   * @param X 
   * 
   * @return 
   */
  bool inside(const TinyVector<3>& X) const
  {
    return (__isR3 || __inShape(X));
  }

  /** 
   * read-only access to the scene
   * 
   * @return __scene
   */
  ConstReferenceCounting<Scene> scene() const;

  /** 
   * Returns the shape if defined...
   * 
   * @return *__objects
   */
  const Object& object() const
  {
    if (__objects == 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "no object list available when no domain is defined ",
			 ErrorHandler::unexpected);
    }

    return (*__objects);
  }

  /** 
   * Sets the shape of the domain
   * 
   * @param objects 
   */
  void setObjects(ReferenceCounting<Object> objects)
  {
    __isR3 = false;
    __objects = objects;
    ffout(2) << "\tReference translation (POV-Ray -> FEM)\n";
    this->__buildReferenceAssociation(*__objects);
    ffout(2) << "\tDone\n";
  }

  /** 
   * Constructor
   * 
   * @param scene domain scene
   */
  Domain(ConstReferenceCounting<Scene> scene);

  /** 
   * Copy constructor
   * 
   * @param D given domain
   */
  Domain(const Domain& D);

  /** 
   * Destructor
   * 
   */
  ~Domain();
};

#endif // DOMAIN_HPP

