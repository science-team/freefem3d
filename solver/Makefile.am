# -*- Mode: makefile  -*-

#  This file is part of ff3d - http://www.freefem.org/ff3d
#  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2, or (at your option)
#  any later version.

#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id: Makefile.am,v 1.48 2007/06/19 00:03:18 delpinux Exp $

if USES_PETSC
AM_CXXFLAGS=@MPI_CXXFLAGS@ @PETSC_CXXFLAGS@
endif # USES_PETSC

noinst_HEADERS	=				       \
	AnalyticShape.hpp			       \
	BaseFEMDiscretization.hpp		       \
	BinaryOperation.hpp			       \
	Boundary.hpp				       \
	BoundaryList.hpp			       \
	BoundaryCondition.hpp			       \
	BoundaryConditionCommonFEMDiscretization.hpp   \
	BoundaryConditionDiscretization.hpp	       \
	BoundaryConditionDiscretizationElimination.hpp \
	BoundaryConditionDiscretizationFEM.hpp	       \
	BoundaryConditionDiscretizationPenalty.hpp     \
	BoundaryConditionDiscretizationSpectral.hpp    \
	BoundaryConditionFDMDiscretization.hpp	       \
	BoundaryConditionSet.hpp		       \
	BoundaryConditionSurfaceMeshAssociation.hpp    \
	BoundaryMeshAssociation.hpp		       \
	BoundaryPOVRay.hpp			       \
	BoundaryReferences.hpp			       \
	BoundarySurfaceMesh.hpp			       \
	CartesianHexahedron.hpp			       \
	Cell.hpp				       \
	CellDuplicator.hpp			       \
	ComponentBoundaryConditions.hpp		       \
	ConformTransformation.hpp		       \
	Connectivity.hpp			       \
	ConnectivityBuilder.hpp			       \
	Convection.hpp				       \
	DegreeOfFreedomPositionsSet.hpp		       \
	DegreeOfFreedomSet.hpp			       \
	DegreeOfFreedomSetBuilder.hpp		       \
	DegreeOfFreedomSetManager.hpp		       \
	Dirichlet.hpp				       \
	Discretization.hpp			       \
	DiscretizationType.hpp			       \
	DiscretizedOperators.hpp		       \
	DivMuGrad.hpp				       \
	Domain.hpp				       \
	Edge.hpp				       \
	EdgesBuilder.hpp			       \
	EdgesSet.hpp				       \
	ElementaryMatrixSet.hpp			       \
	EliminatedFictitiousDomain.hpp		       \
	EliminatedFictitiousDomainOptions.hpp	       \
	FacesBuilder.hpp			       \
	FacesSet.hpp				       \
	FatBoundary.hpp				       \
	FatBoundaryOptions.hpp			       \
	FEMDiscretization.hpp			       \
	FEMFunction.hpp				       \
	FEMFunctionBase.hpp			       \
	FEMFunctionBuilder.hpp			       \
	FEMSolution.hpp				       \
	FieldOfScalarFunction.hpp		       \
	FictitiousDomainMethod.hpp		       \
	FileDescriptor.hpp			       \
	FiniteElementMethod.hpp			       \
	FiniteElementTraits.hpp			       \
	FirstOrderOperator.hpp			       \
	Fourrier.hpp				       \
	GaussLobatto.hpp			       \
	GmshFormatReader.hpp			       \
	Hexahedron.hpp				       \
	Index.hpp				       \
	Interval.hpp				       \
	KrylovSolver.hpp			       \
	KrylovSolverOptionalInformations.hpp	       \
	KrylovSolverOptions.hpp			       \
	LagrangianFiniteElement.hpp		       \
	LegendreBasis.hpp			       \
	LegendreSolution.hpp			       \
	MassOperator.hpp			       \
	MatrixManagement.hpp			       \
	MemoryManagerOptions.hpp		       \
	Mesh.hpp				       \
	MeshDomainTetrahedrizor.hpp		       \
	MeshExtractor.hpp			       \
	MeshFormatReader.hpp			       \
	MeshGenerator.hpp			       \
	MeshOfHexahedra.hpp			       \
	MeshOfTetrahedra.hpp			       \
	MeshOfTriangles.hpp			       \
	MeshPeriodizer.hpp			       \
	MeshReader.hpp				       \
	MeshReaderAM_FMTFormat.hpp		       \
	MeshSimplifier.hpp			       \
	MeshTetrahedrizor.hpp			       \
	MeshTransformer.hpp			       \
	Method.hpp				       \
	Neumann.hpp				       \
	NormalManager.hpp			       \
	P0TetrahedronFiniteElement.hpp		       \
	P0Triangle3DFiniteElement.hpp		       \
	P1TetrahedronFiniteElement.hpp		       \
	P1Triangle3DFiniteElement.hpp		       \
	P2TetrahedronFiniteElement.hpp		       \
	P2Triangle3DFiniteElement.hpp		       \
	PDE.hpp					       \
	PDECondition.hpp			       \
	PDEOperator.hpp				       \
	PDEOperatorSum.hpp			       \
	PDEProblem.hpp				       \
	PDESolution.hpp				       \
	PDESolver.hpp				       \
	PDESystem.hpp				       \
	POVRayReferences.hpp			       \
	PenalizedFictitousDomain.hpp		       \
	PenalizedFictitousDomainOptions.hpp	       \
	Problem.hpp				       \
	Q0HexahedronFiniteElement.hpp		       \
	Q0Quadrangle3DFiniteElement.hpp		       \
	Q1HexahedronFiniteElement.hpp		       \
	Q1Quadrangle3DFiniteElement.hpp		       \
	Q2HexahedronFiniteElement.hpp		       \
	Q2Quadrangle3DFiniteElement.hpp		       \
	Quadrangle.hpp				       \
	QuadratureFormula.hpp			       \
	Reference.hpp				       \
	ScalarFunctionAnd.hpp			       \
	ScalarFunctionBase.hpp			       \
	ScalarFunctionBuilder.hpp		       \
	ScalarFunctionCFunction.hpp		       \
	ScalarFunctionComposed.hpp		       \
	ScalarFunctionConstant.hpp		       \
	ScalarFunctionDerivative.hpp		       \
	ScalarFunctionDifference.hpp		       \
	ScalarFunctionDivision.hpp		       \
	ScalarFunctionDomainCharacteristic.hpp	       \
	ScalarFunctionEqual.hpp			       \
	ScalarFunctionGreaterEqual.hpp		       \
	ScalarFunctionGreaterThan.hpp		       \
	ScalarFunctionIntegrate.hpp		       \
	ScalarFunctionLinearBasis.hpp		       \
	ScalarFunctionLowerEqual.hpp		       \
	ScalarFunctionLowerThan.hpp		       \
	ScalarFunctionMax.hpp			       \
	ScalarFunctionMaxComputer.hpp		       \
	ScalarFunctionMin.hpp			       \
	ScalarFunctionMinComputer.hpp		       \
	ScalarFunctionMeshCharacteristic.hpp	       \
	ScalarFunctionMeshElementsReferences.hpp       \
	ScalarFunctionModulo.hpp		       \
	ScalarFunctionNormal.hpp		       \
	ScalarFunctionNot.hpp			       \
	ScalarFunctionNotEqual.hpp		       \
	ScalarFunctionObjectCharacteristic.hpp	       \
	ScalarFunctionOr.hpp			       \
	ScalarFunctionPower.hpp			       \
	ScalarFunctionProduct.hpp		       \
	ScalarFunctionReaderBase.hpp		       \
	ScalarFunctionReaderBuilder.hpp		       \
	ScalarFunctionReaderMedit.hpp		       \
	ScalarFunctionReaderRaw.hpp		       \
	ScalarFunctionReaderVTK.hpp		       \
	ScalarFunctionSum.hpp			       \
	ScalarFunctionUnaryMinus.hpp		       \
	ScalarFunctionXor.hpp			       \
	SecondOrderOperator.hpp			       \
	Solution.hpp				       \
	Solver.hpp				       \
	SolverDriver.hpp			       \
	SolverDriverOptions.hpp			       \
	SpectralConformTransformation.hpp	       \
	SpectralDiscretization.hpp		       \
	SpectralFunction.hpp			       \
	SpectralMesh.hpp			       \
	SpectralMethod.hpp			       \
	Structured3DMesh.hpp			       \
	Structured3DMeshShape.hpp		       \
	SurfElem.hpp				       \
	SurfaceMesh.hpp				       \
	SurfaceMeshGenerator.hpp		       \
	SurfaceMeshOfQuadrangles.hpp		       \
	SurfaceMeshOfTriangles.hpp		       \
	Tetrahedron.hpp				       \
	Triangle.hpp				       \
	VariationalBilinearBorderOperator.hpp	       \
	VariationalBilinearOperator.hpp		       \
	VariationalBorderOperator.hpp		       \
	VariationalBorderOperatorAlphaUV.hpp	       \
	VariationalBorderOperatorFV.hpp		       \
	VariationalLinearBorderOperator.hpp	       \
	VariationalLinearOperator.hpp		       \
	VariationalOperator.hpp			       \
	VariationalOperatorAlphaDxUDxV.hpp	       \
	VariationalOperatorAlphaUV.hpp		       \
	VariationalOperatorFdxGV.hpp		       \
	VariationalOperatorFdxV.hpp		       \
	VariationalOperatorFgradGgradV.hpp	       \
	VariationalOperatorFV.hpp		       \
	VariationalOperatorMuGradUGradV.hpp	       \
	VariationalOperatorNuDxUV.hpp		       \
	VariationalOperatorNuUdxV.hpp		       \
	VariationalProblem.hpp			       \
	VectorialPDEOperator.hpp		       \
	Vertex.hpp				       \
	VerticesCorrespondance.hpp		       \
	VerticesSet.hpp				       \
	connected_triangle.hpp			       \
	triangulation.hpp			       \
	WorkingMesh.hpp				       \
	WriterBase.hpp				       \
	WriterMedit.hpp				       \
	WriterRaw.hpp				       \
	WriterVTK.hpp

noinst_LIBRARIES = libffsolve.a

libffsolve_a_SOURCES =					\
	AnalyticShape.cpp				\
	BoundarySurfaceMesh.cpp				\
	BoundaryConditionDiscretizationElimination.cpp	\
	BoundaryConditionDiscretizationPenalty.cpp	\
	BoundaryConditionDiscretizationSpectral.cpp	\
	Cell.cpp					\
	CartesianHexahedron.cpp				\
	ConformTransformation.cpp			\
	ConnectivityBuilder.cpp				\
	Convection.cpp					\
	DegreeOfFreedomPositionsSet.cpp			\
	DegreeOfFreedomSetBuilder.cpp			\
	DegreeOfFreedomSetManager.cpp			\
	DiscretizedOperators.cpp			\
	Domain.cpp					\
	Edge.cpp					\
	ElementaryMatrixSet.cpp				\
	EliminatedFictitiousDomain.cpp			\
	FatBoundary.cpp					\
	FieldOfScalarFunction.cpp                       \
	FictitiousDomainMethod.cpp			\
	FEMFunctionBuilder.cpp				\
	FEMSolution.cpp					\
	FiniteElementMethod.cpp				\
	GaussLobatto.cpp				\
	GmshFormatReader.cpp				\
	Hexahedron.cpp					\
	Interval.cpp					\
	KrylovSolver.cpp				\
	LegendreBasis.cpp				\
	LegendreSolution.cpp				\
	MatrixManagement.cpp				\
	MeshDomainTetrahedrizor.cpp			\
	MeshFormatReader.cpp				\
	MeshOfHexahedra.cpp				\
	MeshOfTetrahedra.cpp				\
	MeshOfTriangles.cpp				\
	MeshPeriodizer.cpp				\
	MeshReader.cpp					\
	MeshReaderAM_FMTFormat.cpp			\
	MeshSimplifier.cpp				\
	MeshTetrahedrizor.cpp				\
	MeshTransformer.cpp				\
	Method.cpp					\
	P0TetrahedronFiniteElement.cpp			\
	P0Triangle3DFiniteElement.cpp			\
	P1TetrahedronFiniteElement.cpp			\
	P1Triangle3DFiniteElement.cpp			\
	P2TetrahedronFiniteElement.cpp			\
	P2Triangle3DFiniteElement.cpp			\
	PDESolution.cpp					\
	PDESolver.cpp					\
	PenalizedFictitousDomain.cpp			\
	Q0HexahedronFiniteElement.cpp			\
	Q0Quadrangle3DFiniteElement.cpp			\
	Q1HexahedronFiniteElement.cpp			\
	Q2HexahedronFiniteElement.cpp			\
	Q1Quadrangle3DFiniteElement.cpp			\
	Q2Quadrangle3DFiniteElement.cpp			\
	Quadrangle.cpp					\
	QuadratureFormula.cpp				\
	ScalarFunctionBuilder.cpp			\
	ScalarFunctionComposed.cpp			\
	ScalarFunctionDerivative.cpp			\
	ScalarFunctionDomainCharacteristic.cpp		\
	ScalarFunctionIntegrate.cpp			\
	ScalarFunctionMaxComputer.cpp			\
	ScalarFunctionMinComputer.cpp			\
	ScalarFunctionMeshCharacteristic.cpp		\
	ScalarFunctionMeshElementsReferences.cpp	\
	ScalarFunctionNormal.cpp			\
	ScalarFunctionObjectCharacteristic.cpp		\
	ScalarFunctionReaderBase.cpp			\
	ScalarFunctionReaderBuilder.cpp			\
	ScalarFunctionReaderMedit.cpp			\
	ScalarFunctionReaderRaw.cpp			\
	ScalarFunctionReaderVTK.cpp			\
	SolverDriver.cpp				\
	SpectralConformTransformation.cpp		\
	SpectralMesh.cpp				\
	SpectralMethod.cpp				\
	Structured3DMesh.cpp				\
	SurfaceMeshGenerator.cpp			\
	Tetrahedron.cpp					\
	Triangle.cpp					\
	triangulation.cpp				\
	Vertex.cpp					\
	WriterBase.cpp					\
	WriterMedit.cpp					\
	WriterRaw.cpp					\
	WriterVTK.cpp

SUFFIXES = .cpp .hpp .c .h .f .F .o .moc
