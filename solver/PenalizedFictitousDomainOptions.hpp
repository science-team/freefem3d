//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PenalizedFictitousDomainOptions.hpp,v 1.3 2007/06/09 10:37:07 delpinux Exp $

#ifndef _PENALIZED_FICTITOUS_DOMAIN_OPTIONS_HPP_
#define _PENALIZED_FICTITOUS_DOMAIN_OPTIONS_HPP_

#include <ParametrizableObject.hpp>

class PenalizedFictitousDomainOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:

  real_t epsilon()
  {
    real_t eps = 0;
    get("epsilon",eps);
    return eps;
  }

  static const char* identifier()
  {
    // autodoc: "sets penalty parameters"
    return "penalty";
  }

  explicit PenalizedFictitousDomainOptions()
  {
    // autodoc: "$\epsilon$'s value. ($\epsilon$ coming from $\frac{1}{\epsilon}\int_{\Gamma}{(u-g)v}$)"
    add(new DoubleParameter (1E-3,"epsilon"));
  }

  ~PenalizedFictitousDomainOptions()
  {
    ;
  }
};

#endif // _PENALIZED_FICTITOUS_DOMAIN_OPTIONS_HPP_

