//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshReader.hpp,v 1.17 2005/01/29 20:20:33 delpinux Exp $

#ifndef MESH_READER_HPP
#define MESH_READER_HPP

#include <MeshGenerator.hpp>

#include <ReferenceCounting.hpp>
#include <Vector.hpp>

#include <VerticesSet.hpp>

#include <Vertex.hpp>
#include <Hexahedron.hpp>
#include <Tetrahedron.hpp>
#include <Triangle.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

#include <string>
#include <cstdio>

/**
 * @file   MeshReader.hpp
 * @author Pascal Hav�
 * @date   Sun Nov 28 15 17:32:00 2004
 * 
 * @brief  Main class for all *FormatReader
 * 
 */

class MeshReader
  : public MeshGenerator
{
protected:
  FILE * __ifh;                 /**< File handle of open file */
  std::string __fileName;	/**< The file name if appropriate */

  /**
   * List of vertices.
   * 
   */
  ReferenceCounting<VerticesSet> __vertices;

  /**
   * List of hexahedra.
   * 
   */
  ReferenceCounting<Vector<Hexahedron> > __hexahedra;

  /**
   * List of quadrilaterals
   * 
   */
  ReferenceCounting<Vector<Quadrangle> > __quadrilaterals;

  /**
   * List of tetrahedra.
   * 
   */
  ReferenceCounting<Vector<Tetrahedron> > __tetrahedra;

  /**
   * List of triangles.
   * 
   */
  ReferenceCounting<Vector<Triangle> > __triangles;

  /** 
   * Copy constructor is forbidden
   * 
   * @param M 
   */
  MeshReader(const MeshReader& M);

  /** 
   * Tries to read next real
   * 
   * @return next real
   */
  inline real_t __getReal()
  {
    real_t r;
    int retval = fscanf(__ifh,"%le",&r);
    if (retval <= 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "reading mesh '"+__fileName+"': expected real not found",
			 ErrorHandler::normal);
      return 0;
    }
    return r;
  }

  /** 
   * Tries to read next integer
   * 
   * @return next integer
   */
  inline int __getInteger()
  {
    int i;
    int retval = fscanf(__ifh,"%d",&i);
    if (retval <= 0) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "reading mesh '"+__fileName+"': expected integer not found",
			 ErrorHandler::normal);
      return 0;
    }
    return i;
  }

  /** 
   * This function is called when lists are filled to build __mesh
   * 
   */
  virtual void __createMesh();

public:

  class Error
  { 
  private:
    const std::string __message;
  public:
    Error(const std::string & message) : __message(message) { }
    const std::string & message() const { return __message; }
  };

  /** 
   * Access to the read mesh
   * 
   * 
   * @return __mesh
   */
  ReferenceCounting<Mesh> mesh()
  {
    return __mesh;
  }

  /** 
   * Constructor
   * 
   * @param s the filename
   */
  MeshReader(const std::string & s);

  /** 
   * Destructor
   * 
   */
  virtual ~MeshReader();
};

#endif // MESH_READER_HPP
