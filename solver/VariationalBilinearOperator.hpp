//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalBilinearOperator.hpp,v 1.6 2007/06/10 15:03:32 delpinux Exp $

#ifndef VARIATIONAL_BILINEAR_OPERATOR_HPP
#define VARIATIONAL_BILINEAR_OPERATOR_HPP

#include <VariationalOperator.hpp>

/**
 * @file   VariationalBilinearOperator.hpp
 * @author Stephane Del Pino
 * @date   Thu May 30 18:25:38 2002
 * 
 * @brief  describe bilinear operators
 * 
 * Bilinear operators, ie: left hand side of the equation
 */
class VariationalBilinearOperator
  : public VariationalOperator
{
public:
  enum Type {
    muGradUGradV,
    alphaDxUDxV,
    nuUdxV,
    nuDxUV,
    alphaUV
  };

private:
  const Type __type;		/**< bilinear operator type */
  const size_t __unknownNumber;	/**< number of the considered unknown */

  const VariationalOperator::Property
  __unknownProperty;		/**< unknown operator property */
public:

  /** 
   * Returns the type of the operator
   * 
   * @return __type
   */
  const VariationalBilinearOperator::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Returns the number of the considered unknown
   * 
   * @return __unknownNumber
   */
  const size_t& unknownNumber() const
  {
    return __unknownNumber;
  }

  /** 
   * Returns the property of the unknown operator
   * 
   * @return __unknownProperty
   */
  const VariationalOperator::Property& unknownProperty() const
  {
    return __unknownProperty;
  }

  /** 
   * "Multiplies" the bilinear operator by some function
   * 
   * @param u the given function
   * 
   * @return the new VariationalBilinearOperator
   */
  virtual ReferenceCounting<VariationalBilinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& u) const = 0;

  /** 
   * Constructor
   * 
   * @param t the type of the variational bilinear border operator
   * @param unknownNumber the unknown number
   * @param unknownProperty the unknown operator property
   * @param testFunctionNumber the test function number
   * @param testFunctionProperty the test function operator property
   */
  VariationalBilinearOperator(const VariationalBilinearOperator::Type& t,
			      const size_t& unknownNumber,
			      const VariationalOperator::Property& unknownProperty,
			      const size_t& testFunctionNumber,
			      const VariationalOperator::Property& testFunctionProperty)
    : VariationalOperator(testFunctionNumber,
			  testFunctionProperty),
      __type(t),
      __unknownNumber(unknownNumber),
      __unknownProperty(unknownProperty)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V a VariationalBilinearOperator
   */
  VariationalBilinearOperator(const VariationalBilinearOperator& V)
    : VariationalOperator(V),
      __type(V.__type),
      __unknownNumber(V.__unknownNumber),
      __unknownProperty(V.__unknownProperty)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalBilinearOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_BILINEAR_OPERATOR_HPP

