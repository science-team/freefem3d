//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshReader.cpp,v 1.21 2007/05/11 09:45:44 delpinux Exp $

#include <MeshReader.hpp>

#include <SurfaceMeshOfTriangles.hpp>
#include <SurfaceMeshOfQuadrangles.hpp>

#include <MeshOfHexahedra.hpp>
#include <MeshOfTetrahedra.hpp>
#include <MeshOfTriangles.hpp>

MeshReader::MeshReader(const std::string & s)
  : MeshGenerator(),
    __ifh(NULL),
    __fileName(s),
    __vertices(0),
    __hexahedra(0),
    __quadrilaterals(0),
    __tetrahedra(0),
    __triangles(0)
{
  __ifh = fopen(__fileName.c_str(),"r");
  if (__ifh == NULL) {
    throw MeshReader::Error("cannot open file '"+__fileName+"'"); 
  }
}

MeshReader::~MeshReader() 
{
  if (__ifh != NULL) {
    fclose(__ifh);
  }
}

void
MeshReader::__createMesh()
{
  ReferenceCounting<VerticesCorrespondance> correspondance
    = new VerticesCorrespondance((*__vertices).numberOfVertices());

  if (__hexahedra != 0) {
    SurfaceMeshOfQuadrangles* quadrilaterals = 0;
    if (__quadrilaterals != 0) {
      quadrilaterals = new SurfaceMeshOfQuadrangles(__vertices,
						    correspondance,
						    __quadrilaterals);
    }
    MeshOfHexahedra* m
      = new MeshOfHexahedra(__vertices,
			    correspondance,
			    __hexahedra,
			    quadrilaterals);
    __mesh = m;
    ffout(3) << "- Number of Hexahedra: " << (*__hexahedra).size() << '\n';
    return;
  }


  if (__tetrahedra != 0) {
    SurfaceMeshOfTriangles* triangles = 0;
    if (__triangles != 0) {
      triangles = new SurfaceMeshOfTriangles(__vertices,
					     correspondance,
					     __triangles);
    }
    MeshOfTetrahedra* m
      = new MeshOfTetrahedra(__vertices,
			     correspondance,
			     __tetrahedra,
			     triangles);
    __mesh = m;
    ffout(3) << "- Number of Tetrahedra: " << (*__tetrahedra).size() << '\n';
    return;
  }

  if (__triangles != 0) {
    for (size_t i=0; i<__vertices->numberOfVertices();++i) {
      const Vertex& v = (*__vertices)[i];
      if (v[2] != 0) {
	SurfaceMeshOfTriangles* m
	  = new SurfaceMeshOfTriangles(__vertices,
				       correspondance,
				       __triangles);
	__mesh = m;
	ffout(3) << "- Number of Triangles: " << __triangles->size() << '\n';
	return;
      }
    }
    // If we get there: the mesh is a planar mesh
    MeshOfTriangles* m
      = new MeshOfTriangles(__vertices,
			    correspondance,
			    __triangles);
    __mesh = m;
    ffout(3) << "- Number of Triangles: " << __triangles->size() << '\n';
    return;
  }

  if (__quadrilaterals != 0) {
    SurfaceMeshOfQuadrangles* m
      = new SurfaceMeshOfQuadrangles(__vertices,
				     correspondance,
				     __quadrilaterals);
    __mesh = m;
    ffout(3) << "- Number of Quadrilaterals: " << (*__quadrilaterals).size() << '\n';
    return;
  }
}
