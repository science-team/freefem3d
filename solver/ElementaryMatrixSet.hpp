//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ElementaryMatrixSet.hpp,v 1.3 2007/05/20 23:02:47 delpinux Exp $

#ifndef ELEMENTARYMATRIXSET_HPP
#define ELEMENTARYMATRIXSET_HPP

#include <TinyMatrix.hpp>
#include <TinyVector.hpp>
#include <PDEOperator.hpp>

#include <ErrorHandler.hpp>

class Problem;
template <typename ElementaryMatrixType>
class ElementaryMatrixSet
{
private:
  bool __divMuGrad;
  bool __massOperator;
  bool __secondOrderOperator;
  bool __firstOrderOperator;

  TinyMatrix<3,3, bool> __secondOrderOperatorList;
  TinyVector<3, bool> __firstOrderUdxVList;
  TinyVector<3, bool> __firstOrderDxUVList;

  ElementaryMatrixType __divMuGradMatrix;
  ElementaryMatrixType __massOperatorMatrix;
  TinyVector<3,ElementaryMatrixType > __firstOrderUdxVMatrix;
  TinyVector<3,ElementaryMatrixType > __firstOrderDxUVMatrix;
  TinyMatrix<3,3, ElementaryMatrixType > __secondOrderOperatorMatrix;

public:

  const ElementaryMatrixType& operator()(const PDEOperator::Type& type) const
  {
    switch (type) {
    case (PDEOperator::massop): {
      return __massOperatorMatrix;
    }
    case (PDEOperator::divmugrad): {
      return __divMuGradMatrix;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unknown PDE operator",
			 ErrorHandler::unexpected);
    }
    }
  }

  const bool& isDivMuGrad() const
  {
    return __divMuGrad;
  }

  const ElementaryMatrixType& divMuGrad() const
  {
    ASSERT(__divMuGrad);
    return __divMuGradMatrix;
  }

  ElementaryMatrixType& divMuGrad()
  {
    ASSERT(__divMuGrad);
    return __divMuGradMatrix;
  }

  const bool& isMassOperator() const
  {
    return __massOperator;
  }

  const ElementaryMatrixType& massOperator() const
  {
    ASSERT(__massOperator);
    return __massOperatorMatrix;
  }

  ElementaryMatrixType& massOperator()
  {
    ASSERT(__massOperator);
    return __massOperatorMatrix;
  }

  const bool& isSecondOrderOperator() const
  {
    return __secondOrderOperator;
  }

  const bool& isSecondOrderOperator(const size_t i, const size_t j) const
  {
    return __secondOrderOperatorList(i,j);
  }

  const ElementaryMatrixType& secondOrderOperator(const size_t i, const size_t j) const
  {
    ASSERT(__secondOrderOperatorList(i,j));
    return __secondOrderOperatorMatrix(i,j);
  }

  ElementaryMatrixType& secondOrderOperator(const size_t i, const size_t j)
  {
    ASSERT(__secondOrderOperatorList(i,j));
    return __secondOrderOperatorMatrix(i,j);
  }

  const bool& isFirstOrderOperator() const
  {
    return __firstOrderOperator;
  }

  const bool& isFirstOrderUdxV(const size_t i) const
  {
    return __firstOrderUdxVList[i];
  }

  const ElementaryMatrixType& firstOrderOperatorUdxV(const size_t i) const
  {
    ASSERT(__firstOrderUdxVList[i]);
    return __firstOrderUdxVMatrix[i];
  }

  ElementaryMatrixType& firstOrderOperatorUdxV(const size_t i)
  {
    ASSERT(__firstOrderUdxVList[i]);
    return __firstOrderUdxVMatrix[i];
  }

  const bool& isFirstOrderDxUV(const size_t i) const
  {
    return __firstOrderDxUVList[i];
  }

  const ElementaryMatrixType& firstOrderOperatorDxUV(const size_t i) const
  {
    ASSERT(__firstOrderDxUVList[i]);
    return __firstOrderDxUVMatrix[i];
  }

  ElementaryMatrixType& firstOrderOperatorDxUV(const size_t i)
  {
    ASSERT(__firstOrderDxUVList[i]);
    return __firstOrderDxUVMatrix[i];
  }

  /*!
    Determines the number of needed elementary matrices for a given
    PDE System at construction
  */
  ElementaryMatrixSet(const Problem& problem);
};

#endif

