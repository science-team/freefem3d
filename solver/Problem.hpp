//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Problem.hpp,v 1.6 2006/07/20 19:08:54 delpinux Exp $

#ifndef PROBLEM_HPP
#define PROBLEM_HPP

#include <ReferenceCounting.hpp>
#include <Domain.hpp>

class ScalarFunctionBase;
class BoundaryConditionSet;

/**
 * @file   Problem.hpp
 * @author Stephane Del Pino
 * @date   Thu May 30 12:17:49 2002
 * 
 * @brief  describes a problem
 * 
 */
class Problem
{
public:
  enum Type {
    pdeProblem,
    variationalProblem
  };
  
private:
  ConstReferenceCounting<Domain>
  __omega;			/**< @f$\Omega@f$ domain of computation */

  const Problem::Type
  __type;			/**< type of the problem */
  
public:
  /** 
   * Sets the domain of computation
   * 
   * @param omega @f$\Omega@f$
   */
  void setDomain(ReferenceCounting<Domain> omega)
  {
    __omega = omega;
  }

  /** 
   * Access to the problem type
   * 
   * @return __type
   */
  const Problem::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Access to the domain of computation
   * 
   * @return __omega 
   */
  ConstReferenceCounting<Domain>
  domain() const
  {
    return __omega;
  }

  /** 
   * Access to the number of unknown
   * 
   * @return number of unknown
   */
  virtual size_t numberOfUnknown() const = 0;

  /** 
   * "multiplies" the problem definition by a function
   * 
   * @param c the given coefficient
   * 
   * @return the now modified problem
   */
  virtual ReferenceCounting<Problem>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const=0;

  /** 
   * Access to the boundary condition set associated to the ith unknown
   * 
   * @param i ith unknown number
   * 
   * @return boundary condition set associated to the ith unknown
   */
  virtual const BoundaryConditionSet&
  boundaryConditionSet(const size_t& i) const = 0;

  /** 
   * Constructor
   * 
   * @param t the type of the problem
   * @param omega a pointer to the domain where the problem is posed
   */
  Problem(const Problem::Type& t,
	  ConstReferenceCounting<Domain> omega)
    : __omega(omega),
      __type(t)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param P the copied problem
   */
  Problem(const Problem& P)
    : __omega(P.__omega),
      __type(P.__type)
  {
    ;
  }

  /** 
   * The destructor
   * 
   */
  virtual ~Problem()
  {
    ;
  }
};

#endif // PROBLEM_HPP

