//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionBase.hpp,v 1.9 2007/06/13 23:11:22 delpinux Exp $

#ifndef SCALAR_FUNCTION_BASE_HPP
#define SCALAR_FUNCTION_BASE_HPP

#include <Types.hpp>
#include <TinyVector.hpp>

#include <iostream>
#include <ErrorHandler.hpp>

/**
 * @file   ScalarFunctionBase.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:29:36 2006
 * 
 * @brief base class for scalar functions
 * 
 */
class ScalarFunctionBase
{
public:
  enum Type {
    cfunction,
    constant,
    convection,
    femfunction,
    linearBasis,
    spectral,
    unaryMinus,

    modulo,
    sum,
    difference,
    product,
    division,
    power,

    min,
    max,

    gt,
    ge,
    lt,
    le,
    ne,
    eq,
    and_,
    or_,
    xor_,

    not_,

    derivate,
    integrate,

    normal,

    domainCharacteristic,
    meshCharacteristic,
    objectCharacteristic,

    composed,
    references,

    FEM0,

    undefined
  };

protected:
  const Type __type;		/**< type of the function */

  std::string __name;		/**< name of the function */

  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return the modified stream
   */
  virtual std::ostream& __put(std::ostream& os) const = 0;

public:
  /** 
   * Sets the name of the function
   * 
   * @param name name to give to this function
   */
  void setName(const std::string& name)
  {
    __name = name;
  }

  /** 
   * Gets the name of the function
   * 
   * @return __name
   */
  const std::string& name() const
  {
    return __name;
  }

  /** 
   * Read-only access to the type of the function
   * 
   * @return __type
   */
  inline const Type& type() const
  {
    return __type;
  }

  /** 
   * Writes the function @a scalarFunction to the stream @a os
   * 
   * @param os the output stream
   * @param scalarFunction the function to write
   * 
   * @return os
   */
  friend std::ostream& operator<<(std::ostream& os,
				  const ScalarFunctionBase& scalarFunction)
  {
    if (scalarFunction.__name.size()>0) {
      os << scalarFunction.__name;
      return os;
    } else {
      return scalarFunction.__put(os);
    }
  }

  /** 
   * Evaluates the function at point @f$ (x,y,z) @f$
   * 
   * @param x @f$ x @f$
   * @param y @f$ y @f$
   * @param z @f$ z @f$
   * 
   * @return @f$ f(x,y,z) @f$
   */
  inline real_t operator()(const real_t& x,
			   const real_t& y,
			   const real_t& z) const
  {
    return this->operator()(TinyVector<3, real_t>(x,y,z));    
  }

  /** 
   * Evaluates the gunction at point @f$ X @f$
   * 
   * @param X point of evaluation of the function
   * 
   * @return @f$ f(X) @f$
   */
  virtual real_t operator()(const TinyVector<3, real_t>& X) const = 0;

  /** 
   * Evaluates first derivative of the function
   * 
   * @param x position of evaluation
   * 
   * @return @f$ \partial_x f @f$ at position @f$ x @f$
   */
  virtual real_t dx(const TinyVector<3, real_t>& V) const
  {
    std::stringstream errorMsg;

    errorMsg << "cannot compute derivative of non discrete functions :-(\n";
    errorMsg << "the function " << (*this) << " is not of that kind"
	     << std::ends;

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
    return 0;
  }

  /** 
   * Evaluates second derivative of the function
   * 
   * @param x position of evaluation
   * 
   * @return @f$ \partial_y f @f$ at position @f$ x @f$
   */
  virtual real_t dy(const TinyVector<3, real_t>& V) const
  {
    std::stringstream errorMsg;

    errorMsg << "cannot compute derivative of non discrete functions :-(\n";
    errorMsg << "the function " << (*this) << " is not of that kind"
	     << std::ends;

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
    return 0;
  }

  /** 
   * Evaluates third derivative of the function
   * 
   * @param x position of evaluation
   * 
   * @return @f$ \partial_z f @f$ at position @f$ x @f$
   */
  virtual real_t dz(const TinyVector<3, real_t>& V) const
  {
    std::stringstream errorMsg;

    errorMsg << "cannot compute derivative of non discrete functions :-(\n";
    errorMsg << "the function " << (*this) << " is not of that kind"
	     << std::ends;

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
    return 0;
  }

  /** 
   * Checks if the function can be simplified: it means that composed
   * functions of using this function can be partialy
   * pre-evaluated. For instance a fem-function multiplied by a
   * constant can be evaluate when operation is declared.
   * 
   * @return true if the function can be simplified
   */
  virtual bool canBeSimplified() const = 0;

  /** 
   * Copy constructor
   * 
   * @param f given function
   */
  ScalarFunctionBase(const ScalarFunctionBase& f)
    : __type(f.__type),
      __name(f.__name)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param type the type of the function 
   */
  ScalarFunctionBase(const Type& type)
    : __type(type),
      __name("")
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~ScalarFunctionBase()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_BASE_HPP
