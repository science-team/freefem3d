//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDECondition.hpp,v 1.2 2006/07/20 19:08:54 delpinux Exp $

#ifndef PDE_CONDITION_HPP
#define PDE_CONDITION_HPP

#include <string>

#include <StreamCenter.hpp>

/**
 * @file   PDECondition.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 23:32:15 2006
 * 
 * @brief  defines boundary condition type
 * 
 */
class PDECondition
{
public:
  //! allowed types.
  enum Type {
    dirichlet,
    neumann,
    fourrier
  };

private:
  const PDECondition::Type __type; /**< type of condition */

  const size_t __unknownNumber;	/**< number of the associated unknown */
  
  /** 
   * gets type name of a boundary condition
   * 
   * @return a string containing the name
   */
  virtual std::string
  typeName() const = 0;
public:

  /** 
   * Read-only access to the unknown number
   * 
   * @return __unknownNumber
   */
  const size_t& unknownNumber() const
  {
    return __unknownNumber;
  }
  
  /** 
   * Read-only access to the type of condition
   * 
   * @return __type
   */
  const PDECondition::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Constructor
   * 
   * @param type type of condition
   * @param unknownNumber unknown number
   */
  PDECondition(const PDECondition::Type& type,
	       const size_t& unknownNumber)
    : __type(type),
      __unknownNumber(unknownNumber)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param C given condition
   */
  PDECondition(const PDECondition& C)
    : __type(C.__type),
      __unknownNumber(C.__unknownNumber)
  {
    ;
  }

  /** 
   * Writes the condition
   * 
   * @param os output stream
   * @param bc the condition to write
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os, const PDECondition& bc)
  {
    os << bc.typeName();
    return os;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~PDECondition()
  {
    ;
  }
};

#endif // PDE_CONDITION_HPP
