//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionFDMDiscretization.hpp,v 1.9 2006/09/28 20:08:51 delpinux Exp $


#ifndef BOUNDARY_CONDITION_FDM_DISCRETIZATION_HPP
#define BOUNDARY_CONDITION_FDM_DISCRETIZATION_HPP

#include <BoundaryConditionCommonFEMDiscretization.hpp>

/**
 * @file   BoundaryConditionFDMDiscretization.hpp
 * @author Stephane Del Pino
 * @date   Sun Nov 24 17:37:59 2002
 * 
 * @brief Fictitious domain method discretization of boundary
 * conditions. The only difference with the mother class is the
 * call of the mesh generator in the constructor.
 */

template <typename MeshType,
	  DiscretizationType::Type TypeOfDiscretization>
class BoundaryConditionFDMDiscretization
  : public BoundaryConditionCommonFEMDiscretization<MeshType,
						    TypeOfDiscretization>
{
private:
  void __markFictitiousCells(const BoundaryMeshAssociation& bma)
  {
    for (BoundaryMeshAssociation::const_iterator i = bma.begin();
	 i != bma.end(); ++i) {
      (*i->second).computesFictitiousCells();
    }
  }

public:
  void associatesMeshesToBoundaryConditions()
  {
    BoundaryMeshAssociation bma(this->__problem, this->__mesh);

    if (bma.hasPOVReferences()) {
      const Domain& omega = *(this->__problem).domain();
      ReferenceCounting<SurfaceMeshOfTriangles>
	omegaMesh = new SurfaceMeshOfTriangles();

      ffout(3) << "Generating Surface Meshes\n";
      
      SurfaceMeshGenerator S;
      S.generateSurfacicMesh(omega, this->__mesh, *omegaMesh);
      bma.setPOVMeshes(omega, omegaMesh);
    }
    this->__checkBoundaryMeshAssociation(bma);
    this->__markFictitiousCells(bma);
    this->__associatesDefinedMeshToBoundaryConditions(bma);
  }

  BoundaryConditionFDMDiscretization(const Problem& problem,
				     MeshType& givenMesh,
				     const DegreeOfFreedomSet& dof)
    : BoundaryConditionCommonFEMDiscretization<MeshType,
					       TypeOfDiscretization>(problem,
								     givenMesh,
								     dof)
  {
    ;
  }

  virtual ~BoundaryConditionFDMDiscretization()
  {
    ;
  }
};

#endif // BOUNDARY_CONDITION_FDM_DISCRETIZATION_HPP

