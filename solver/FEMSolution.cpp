//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FEMSolution.cpp,v 1.1 2007/05/20 23:26:37 delpinux Exp $

#include <FEMSolution.hpp>
#include <FEMFunctionBase.hpp>

void
FEMSolution::
setUserFunction (const std::vector<ReferenceCounting<FEMFunctionBase> >& U)
{
  Vector<real_t>& v = *__values;
  for(size_t j=0; j<__degreeOfFreedomSet.numberOfDOFPositions(); ++j) {
    if (__degreeOfFreedomSet.isDOFVertex(j)) {
      for (size_t i=0; i<__degreeOfFreedomSet.numberOfVariables(); ++i) {
	const FEMFunctionBase& fem = *U[i];
	v[__degreeOfFreedomSet(i,j)]=fem[j];
      }
    }
  }
}

void
FEMSolution::
getUserFunction(std::vector<ReferenceCounting<FEMFunctionBase> >& U) const
{
  const Vector<real_t>& v = *__values;
  for(size_t j=0; j<__degreeOfFreedomSet.numberOfDOFPositions(); ++j) {
    if (__degreeOfFreedomSet.isDOFVertex(j)) {
      for (size_t i=0; i<__degreeOfFreedomSet.numberOfVariables(); ++i) {
	FEMFunctionBase& fem = *U[i];
	fem[j] =  v[__degreeOfFreedomSet(i,j)];
      }
    } else {
      for (size_t i=0; i<__degreeOfFreedomSet.numberOfVariables(); ++i) {
	FEMFunctionBase& fem = *U[i];
	fem[j] =  0;
      }
    }
  }
}
