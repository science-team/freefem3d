//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001-2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DegreeOfFreedomPositionsSet.hpp,v 1.6 2007/05/20 23:02:47 delpinux Exp $

#ifndef DEGREE_OF_FREEDOM_POSITIONS_SET_HPP
#define DEGREE_OF_FREEDOM_POSITIONS_SET_HPP

#include <DiscretizationType.hpp>
#include <Mesh.hpp>
#include <Connectivity.hpp>

/**
 * @file   DegreeOfFreedomPositionsSet.hpp
 * @author Stephane Del Pino
 * @date   Sat Nov 11 16:11:45 2006
 * 
 * @brief  Set of degrees of freedom positions
 * 
 * This class contains the information for degrees of freedom position
 * given a mesh and a discretization type. The mesh might contain
 * periodicity information.
 */
class DegreeOfFreedomPositionsSet
{
private:
  Vector<TinyVector<3,real_t> >
  __positions;			/**< position list */

  Vector<size_t> __dofNumber;	/**< degrees of freedom numbers
				   (stored by cell) */

  size_t __numberOfDOFPerCell;	/**< number of degrees of freedom
				   carried by a cell */

  class Builder;		/**< Degrees of freedom set builder */
  friend class Builder;

  /** 
   * Constructs the degrees of freedom position set for a given mesh
   * and a given discretization type
   * 
   * @param discretizationType given discretization type
   * @param mesh given mesh
   */
  void __build(const DiscretizationType& discretizationType,
	       const Mesh& mesh);

public:
  /** 
   * Access to a degree of freedom position
   * 
   * @param i degree of freedom number
   * 
   * @return ith degree of freedom position
   */
  const TinyVector<3,real_t>&
  vertex(const size_t& i) const
  {
    return __positions[i];
  }

  /** 
   * Read-only access to the global number of a local degree of
   * freedom in a cell
   * 
   * @return the global number of a cell degree of freedom
   */
  const size_t& operator()(const size_t& cellNumber,
			   const size_t& localDOFNumber) const
  {
    return __dofNumber[cellNumber*__numberOfDOFPerCell + localDOFNumber];
  }

  /** 
   * Read-only access to the total number of degrees of freedom
   * 
   * @return the total number of degrees of freedom
   */
  const size_t& number() const
  {
    return __positions.size();
  }

  /** 
   * Constructor
   * 
   * @param discretizationType given discretization type
   * @param mesh given mesh
   */
  DegreeOfFreedomPositionsSet(const DiscretizationType& discretizationType,
			      const Mesh& mesh)
  {
    this->__build(discretizationType, mesh);
  }

  /** 
   * Destructor
   * 
   */
  ~DegreeOfFreedomPositionsSet()
  {
    ;
  }
};

#endif // DEGREE_OF_FREEDOM_POSITIONS_SET_HPP
