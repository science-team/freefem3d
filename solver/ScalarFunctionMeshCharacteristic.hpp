//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionMeshCharacteristic.hpp,v 1.3 2006/10/28 12:26:54 delpinux Exp $

#ifndef SCALAR_FUNCTION_MESH_CHARACTERISTIC_HPP
#define SCALAR_FUNCTION_MESH_CHARACTERISTIC_HPP

#include <ScalarFunctionBase.hpp>
#include <ReferenceCounting.hpp>

class Mesh;

/**
 * @file   ScalarFunctionMeshCharacteristic.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 01:56:49 2006
 * 
 * @brief  characteristic function of a mesh
 * 
 */
class ScalarFunctionMeshCharacteristic
  : public ScalarFunctionBase
{
private:
  //! The Mesh on which lives the function.
  ConstReferenceCounting<Mesh> __mesh;

  /** 
   * Writes the function on a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "one({mesh})";
    return os; 
  }

public:
  /** 
   * checks if some point is inside the mesh
   * 
   * @param X evaluation point
   * 
   * @return 1 if the point is inside the mesh, 0 else
   */
  real_t operator()(const TinyVector<3>& V) const;

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param mesh given mesh
   */
  ScalarFunctionMeshCharacteristic(ConstReferenceCounting<Mesh> mesh);

  /** 
   * Copy constructor
   * 
   * @param f original function
   */
  ScalarFunctionMeshCharacteristic(const ScalarFunctionMeshCharacteristic& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionMeshCharacteristic();
};

#endif // SCALAR_FUNCTION_MESH_CHARACTERISTIC_HPP
