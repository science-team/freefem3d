//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SpectralConformTransformation.cpp,v 1.1 2007/04/29 19:20:35 yakoubix Exp $

#include <SpectralConformTransformation.hpp>
#include <Interval.hpp>

SpectralConformTransformation::
SpectralConformTransformation(const Interval& interval)
  : __interval(interval)
{
  ;
}

SpectralConformTransformation::~
SpectralConformTransformation()
{
  ;
}


real_t SpectralConformTransformation::
operator()(const real_t& x) const
{
  return  (__interval.b()-__interval.a())/2 *x + (__interval.b()+__interval.a())/2;
}

real_t SpectralConformTransformation::
inverse(const real_t& x) const
{
  return 2./(__interval.b()-__interval.a())*x - (__interval.b()+__interval.a())/(__interval.b()-__interval.a());
}

real_t SpectralConformTransformation::
inverseDeterminant() const
{
  return 2./(__interval.b() -__interval.a());
}
