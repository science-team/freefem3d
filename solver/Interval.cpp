//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Interval.cpp,v 1.1 2007/04/29 19:20:35 yakoubix Exp $

#include <Interval.hpp>
#include <algorithm>

Interval::Interval(const Interval& i)
  : __a(i.__a),
    __b(i.__b)
{
  ;
}

Interval::
Interval(const real_t& a,
	 const real_t& b)
  : __a(std::min(a,b)),
    __b(std::max(a,b))
{
  ;
}

Interval::
~Interval()
{
  ;
}

const real_t&
Interval::a() const
{
  return __a;
}

const real_t&
Interval::b() const
{
  return __b; 
}

