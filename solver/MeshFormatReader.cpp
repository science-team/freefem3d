//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshFormatReader.cpp,v 1.6 2007/05/20 23:09:00 delpinux Exp $

#include <MeshFormatReader.hpp>

#include <Stringify.hpp>

#include <set>
#include <sstream>

void MeshFormatReader::__skipComments() {
  char checkComment[2];
  while (fscanf(__ifh,"%1s",checkComment) > 0) {
    if (checkComment[0] == '#') {
      char c;
      do {
	c = fgetc(__ifh);
      } while(c != '\n');
    } else {
      ungetc(checkComment[0],__ifh);
    }
    return;
  }
}

MeshFormatReader::Keyword MeshFormatReader::__nextKeyword()
{
  MeshFormatReader::Keyword kw;

  __skipComments();
  char pKeyword[256];
  int retval = fscanf(__ifh,"%255s",pKeyword);
  const std::string aKeyword = pKeyword;

  if (retval < 0)  {
    kw.second = EndOfFile;
    return kw;
  } else if (retval == 0) {
    kw.second = Unknown;
    return kw;
  }

  KeywordList::iterator i = __keywordList.find(aKeyword.c_str());

  if (i != __keywordList.end()) {
    kw.first = (*i).first;
    kw.second = (*i).second;
    return kw;
  }
  throw ErrorHandler(__FILE__,__LINE__,
		     "reading file '"+__fileName
		     +"': keyword '"+aKeyword+"' unknown",
		     ErrorHandler::normal);

  kw.first = aKeyword;
  kw.second = Unknown;
  return kw;
}

void MeshFormatReader::__checkVertex(const size_t& n)
{
  if ((n == 0) or (n > (*__vertices).numberOfVertices())) {
    throw MeshFormatReader::VertexError();
  }
}

size_t MeshFormatReader::__getVertexNumber()
{
  size_t n = this->__getInteger();
  this->__checkVertex(n);

  return n;
}


void MeshFormatReader::
__writeReferences(const std::set<size_t>& references,
		  std::string objectName)
{
  ffout(3) << "  " << objectName << " references: ";
  for (std::set<size_t>::const_iterator i = references.begin();
       i != references.end(); ++i) {
    if (i != references.begin()) ffout(3) << ',';
    ffout(3) << *i;
  }
  ffout(3) << '\n';
}

void MeshFormatReader::__readVertices()
{
  std::set<size_t> references;
  VerticesSet& V = *__vertices;
  for (size_t i=0; i<V.numberOfVertices(); ++i) {
    const double x   = __getReal();
    const double y   = __getReal();
    const double z   = __getReal();
    const size_t ref = __getInteger();
    references.insert(ref);
    V[i] = Vertex(x,y,z, ref);
  }

  this->__writeReferences(references, "Vertices");
}

void MeshFormatReader::__readElements()
{
  std::string elementType;
  Keyword kw = __nextKeyword();
  while (kw.second != EndOfFile) {
    switch (kw.second) {
    case Vertices: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "reading file '"+__fileName
			 +"': vertices defined twice",
			 ErrorHandler::normal);
      break;
    }
    case Hexahedra: {
      __skipComments();
      size_t numberOfHexahedra = __getInteger();
      __skipComments();

      __hexahedra = new Vector<Hexahedron>(numberOfHexahedra);

      ffout(3) << "- Number of hexahedra: " << numberOfHexahedra << '\n';
      this->__readHexahedra();

      break;
    }
    case Tetrahedra: {
      __skipComments();
      size_t numberOfTetrahedra = __getInteger();
      __skipComments();

      __tetrahedra = new Vector<Tetrahedron>(numberOfTetrahedra);

      ffout(3) << "- Number of tetrahedra: " << numberOfTetrahedra << '\n';
      this->__readTetrahedra();

      break;
    }
    case Triangles: {
      __skipComments();
      size_t numberOfTriangles = __getInteger();
      __skipComments();

      __triangles = new Vector<Triangle>(numberOfTriangles);

      ffout(3) << "- Number of triangles: " << numberOfTriangles << '\n';
      this->__readTriangles();

      break;
    }
    case Quadrilaterals: {
      __skipComments();
      size_t numberOfQuadrilaterals = __getInteger();
      __skipComments();

      __quadrilaterals = new Vector<Quadrangle>(numberOfQuadrilaterals);

      ffout(3) << "- Number of quadrilaterals: "
	       << numberOfQuadrilaterals << '\n';
      this->__readQuadrilaterals();

      break;
    }
    case Corners: {
      __skipComments();
      size_t numberOfCorners = __getInteger();
      __skipComments();
      for (size_t j=0; j<numberOfCorners; ++j) {
	__getInteger();
      }

      fferr(3) << "  warning: while reading \"" << __fileName
	       << "\": skipping Corners\n";
      break;
    }
    case Edges: {
      __skipComments();
      size_t numberOfEdges = __getInteger();
      __skipComments();
      for (size_t j=0; j<numberOfEdges; ++j) {
	__getInteger();
	__getInteger();
	__getInteger();
      }

      fferr(3) << "  warning: while reading \"" << __fileName
	       << "\": skipping Edges\n";
      break;
    }
    case Ridges: {
      __skipComments();
      size_t numberOfRidges = __getInteger();
      __skipComments();
      for (size_t j=0; j<numberOfRidges; ++j) {
	__getInteger();
      }

      fferr(3) << "  warning: while reading \"" << __fileName
	       << "\": skipping Ridges\n";
      break;
    }
    default: {
      std::stringstream errorMsg;
      errorMsg << "reading file '" << __fileName
	       << "' unknown keyword: " << kw.first << std::ends;
      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::normal);

    }
    }
    kw = __nextKeyword();
  }
}


void MeshFormatReader::__readHexahedra()
{
  VerticesSet& V = *__vertices;
  Vector<Hexahedron>& H = *__hexahedra;
  std::set<size_t> references;

  size_t i=0;
  try {
    for (; i<H.size(); ++i) {
      const size_t a = __getVertexNumber() - 1;
      const size_t b = __getVertexNumber() - 1;
      const size_t c = __getVertexNumber() - 1;
      const size_t d = __getVertexNumber() - 1;
      const size_t e = __getVertexNumber() - 1;
      const size_t f = __getVertexNumber() - 1;
      const size_t g = __getVertexNumber() - 1;
      const size_t h = __getVertexNumber() - 1;

      const size_t ref   = __getInteger();
      references.insert(ref);

      H[i] = Hexahedron(V[a],V[b],V[c],V[d],V[e],V[f],V[g],V[h], ref);
    }
  }
  catch (MeshFormatReader::VertexError e) {
    throw MeshReader::Error("invalid vertex number reading hexahedron "+stringify(i));
  }

  this->__writeReferences(references, "Hexahedra");  
}

void MeshFormatReader::__readQuadrilaterals()
{
  VerticesSet& V = *__vertices;
  Vector<Quadrangle>& Q = *__quadrilaterals;

  std::set<size_t> references;
  size_t i;
  try {
    for (i=0; i < Q.size(); ++i) {
      const size_t a   = __getVertexNumber() - 1;
      const size_t b   = __getVertexNumber() - 1;
      const size_t c   = __getVertexNumber() - 1;
      const size_t d   = __getVertexNumber() - 1;

      const size_t ref   = __getInteger();
      references.insert(ref);

      Q[i] = Quadrangle(V[a], V[b], V[c], V[d], ref);
    }
  }
  catch (MeshFormatReader::VertexError e) {
    throw MeshReader::Error("invalid vertex number reading quadrilateral "+stringify(i));
  }

  this->__writeReferences(references, "Quadrilaterals");
}


void MeshFormatReader::__readTetrahedra()
{
  VerticesSet& V = *__vertices;
  Vector<Tetrahedron>& T = *__tetrahedra;

  std::set<size_t> references;
  size_t i;
  try {
    for (i=0; i<T.size(); ++i) {
      const size_t a   = __getVertexNumber() - 1;
      const size_t b   = __getVertexNumber() - 1;
      const size_t c   = __getVertexNumber() - 1;
      const size_t d   = __getVertexNumber() - 1;

      const size_t ref   = __getInteger();
      references.insert(ref);

      T[i] = Tetrahedron(V[a],V[b],V[c],V[d], ref);
    }
  }
  catch (MeshFormatReader::VertexError e) {
    throw MeshReader::Error("invalid vertex number reading tetrahedron "+stringify(i));
  }

  this->__writeReferences(references, "Tetrahedra");
}

void MeshFormatReader::__readTriangles()
{
  VerticesSet& V = *__vertices;
  Vector<Triangle>& T = *__triangles;

  std::set<size_t> references;
  size_t i;
  try {
    for (i=0; i<T.size(); ++i) {
      const size_t a = __getVertexNumber() - 1;
      const size_t b = __getVertexNumber() - 1;
      const size_t c = __getVertexNumber() - 1;

      const size_t ref   = __getInteger();
      references.insert(ref);
    
      T[i] = Triangle(V[a],V[b],V[c], ref);
    }
  }
  catch (MeshFormatReader::VertexError e) {
    throw MeshReader::Error("invalid vertex number reading triangle "+stringify(i));
  }

  this->__writeReferences(references, "Triangles");
}

MeshFormatReader::MeshFormatReader(const std::string & s)
  : MeshReader(s)
{
  __keywordList["MeshVersionFormatted"]=MeshVersionFormatted;
  __keywordList["Dimension"]=MeshDimension;
  __keywordList["Vertices"]=Vertices;
  __keywordList["Triangles"]=Triangles;
  __keywordList["Quadrilaterals"]=Quadrilaterals;
  __keywordList["Tetrahedra"]=Tetrahedra;
  __keywordList["Hexahedra"]=Hexahedra;
  __keywordList["Normals"]=Normals;
  __keywordList["Tangents"]=Tangents;
  __keywordList["Corners"]=Corners;
  __keywordList["Edges"]=Edges;
  __keywordList["Ridges"]=Ridges;
  __keywordList["End"]=EndOfFile;

  size_t integer;

  ffout(3) << "\n";
  ffout(3) << "Reading file " << s << '\n';
  MeshFormatReader::Keyword kw;

  kw = this->__nextKeyword();
  if (kw.second != MeshVersionFormatted) {
    std::stringstream errorMsg;
    errorMsg << "reading file '" << __fileName
	     << "' expecting 'MeshVersionFormatted'" << std::ends;
    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
  }
  __skipComments();
  integer = __getInteger();

  ffout(3) << "- MeshVersionFormatted: " << integer << '\n';

  kw = this->__nextKeyword();
  if (kw.second != MeshDimension) {
    std::stringstream errorMsg;
    errorMsg << "reading file '" << __fileName
	     << "' expecting 'Dimension'" << std::ends;
    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
  }
  __skipComments();
  integer = __getInteger();  

  ffout(3) << "- Dimension: " << integer << '\n';

  if (integer != 3) {
    std::stringstream errorMsg;
    errorMsg << "reading file '" << __fileName
	     << "' only 3D meshes can be read" << std::ends;
    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
  }

  kw = this->__nextKeyword();
  if (kw.second != Vertices) {
    std::stringstream errorMsg;
    errorMsg << "reading file '" << __fileName
	     << "' expecting vertices list" << std::ends;
    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
  }
  __skipComments();
  integer = __getInteger();  
  __skipComments();

  ffout(3) << "- Number of vertices: " << integer << '\n';

  __vertices = new VerticesSet(integer);

  this->__readVertices();

  this->__readElements();

  this->__createMesh();
}
