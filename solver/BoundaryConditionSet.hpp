//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionSet.hpp,v 1.4 2007/05/20 23:02:47 delpinux Exp $

#ifndef BOUNDARYCONDITIONSET_HPP
#define BOUNDARYCONDITIONSET_HPP

#include <vector>
#include <BoundaryCondition.hpp>

/**
 * @file   BoundaryConditionSet.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 14:56:53 2006
 * 
 * @brief This class is used to describe sets of boundary conditions.
 * 
 * 
 */
class BoundaryConditionSet
{
private:
  std::vector<ConstReferenceCounting<BoundaryCondition> >
  __boundaryConditionSet;	/**< list of boundary conditions */

public:
  /** 
   *  Add a boundary condition to the list.
   * 
   * @param bc a boundary condition
   */
  void addBoundaryCondition(ConstReferenceCounting<BoundaryCondition> bc)
  {
    __boundaryConditionSet.push_back(bc);
  }

  /** 
   * read only access to the number of boundary condition.
   * 
   * @return 
   */
  size_t nbBoundaryCondition() const
  {
    return __boundaryConditionSet.size();
  }

  /** 
   * Access to the ith boundary condition
   * 
   * @param i the number of the boundary condition to access
   * 
   * @return __boundaryConditionSet[i]
   */
  const BoundaryCondition& operator[] (const size_t i) const
  {
    ASSERT(i<__boundaryConditionSet.size());
    return *(__boundaryConditionSet[i]);
  }

  /** 
   * writes the ComponentBoundaryConditions.
   * 
   * @param os output stream
   * @param bcSet boundary condition ste
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os,
	       const BoundaryConditionSet& bcSet)
  {
    for(size_t i=0; i<bcSet.__boundaryConditionSet.size(); ++i)
      os << *(bcSet.__boundaryConditionSet[i]) << '\n';
    return os;
  }

  /** 
   * Copy constructor
   * 
   * @param bcSet a given boundary condition
   */
  BoundaryConditionSet(const BoundaryConditionSet& bcSet)
    : __boundaryConditionSet(bcSet.__boundaryConditionSet)
  {
    ;
  }

  /** 
   * Default constructor
   * 
   */
  BoundaryConditionSet()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~BoundaryConditionSet()
  {
    ;
  }
};

#endif // BOUNDARYCONDITIONSET_HPP

