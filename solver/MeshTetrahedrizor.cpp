//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshTetrahedrizor.cpp,v 1.17 2007/05/20 23:09:00 delpinux Exp $

#include <MeshTetrahedrizor.hpp>

#include <MeshOfTetrahedra.hpp>
#include <Structured3DMesh.hpp>

#include <set>

template <>
void MeshTetrahedrizor::
__build<Structured3DMesh>(const bool& translateSurface)
{
  // In this function hexahedra are divide into 5 tetrahedra
  const Structured3DMesh& M
    = static_cast<const Structured3DMesh&>(*__input);

  // copies the set of vertices
  VerticesSet* pVerticesSet = new VerticesSet(*M.verticesSet());
  VerticesSet& verticesSet = *pVerticesSet;

  // copies vertices correspondances
  VerticesCorrespondance* pCorrespondance
    = new VerticesCorrespondance(*M.verticesCorrespondance());

  // reserves the space to store the tetrahedra
  Vector<Tetrahedron>* pTetrahedra = new Vector<Tetrahedron>(5*M.numberOfCells());
  Vector<Tetrahedron>& tetrahedra = *pTetrahedra;

  size_t cellNumber=0;
  for(size_t i=0; i<M.shape().nx()-1; ++i)
    for(size_t j=0; j<M.shape().ny()-1; ++j)
      for(size_t k=0; k<M.shape().nz()-1; ++k) {
	const CartesianHexahedron& H = M.cell(i,j,k);

	if ( (i+j+k) % 2 == 0) {
	  Tetrahedron T0(verticesSet[M.vertexNumber(H(4))],
			 verticesSet[M.vertexNumber(H(0))],
			 verticesSet[M.vertexNumber(H(7))],
			 verticesSet[M.vertexNumber(H(5))],
			 H.reference());

	  Tetrahedron T1(verticesSet[M.vertexNumber(H(1))],
			 verticesSet[M.vertexNumber(H(2))],
			 verticesSet[M.vertexNumber(H(0))],
			 verticesSet[M.vertexNumber(H(5))],
			 H.reference());

	  Tetrahedron T2(verticesSet[M.vertexNumber(H(0))],
			 verticesSet[M.vertexNumber(H(2))],
			 verticesSet[M.vertexNumber(H(7))],
			 verticesSet[M.vertexNumber(H(5))],
			 H.reference());

	  Tetrahedron T3(verticesSet[M.vertexNumber(H(6))],
			 verticesSet[M.vertexNumber(H(2))],
			 verticesSet[M.vertexNumber(H(5))],
			 verticesSet[M.vertexNumber(H(7))],
			 H.reference());

	  Tetrahedron T4(verticesSet[M.vertexNumber(H(3))],
			 verticesSet[M.vertexNumber(H(0))],
			 verticesSet[M.vertexNumber(H(2))],
			 verticesSet[M.vertexNumber(H(7))],
			 H.reference());

	  tetrahedra[5*cellNumber  ] = T0;
	  tetrahedra[5*cellNumber+1] = T1;
	  tetrahedra[5*cellNumber+2] = T2;
	  tetrahedra[5*cellNumber+3] = T3;
	  tetrahedra[5*cellNumber+4] = T4;
	} else {
	  Tetrahedron T0(verticesSet[M.vertexNumber(H(0))],
			 verticesSet[M.vertexNumber(H(1))],
			 verticesSet[M.vertexNumber(H(3))],
			 verticesSet[M.vertexNumber(H(4))],
			 H.reference());

	  Tetrahedron T1(verticesSet[M.vertexNumber(H(5))],
			 verticesSet[M.vertexNumber(H(1))],
			 verticesSet[M.vertexNumber(H(4))],
			 verticesSet[M.vertexNumber(H(6))],
			 H.reference());

	  Tetrahedron T2(verticesSet[M.vertexNumber(H(1))],
			 verticesSet[M.vertexNumber(H(4))],
			 verticesSet[M.vertexNumber(H(6))],
			 verticesSet[M.vertexNumber(H(3))],
			 H.reference());

	  Tetrahedron T3(verticesSet[M.vertexNumber(H(2))],
			 verticesSet[M.vertexNumber(H(1))],
			 verticesSet[M.vertexNumber(H(6))],
			 verticesSet[M.vertexNumber(H(3))],
			 H.reference());

	  Tetrahedron T4(verticesSet[M.vertexNumber(H(7))],
			 verticesSet[M.vertexNumber(H(3))],
			 verticesSet[M.vertexNumber(H(6))],
			 verticesSet[M.vertexNumber(H(4))],
			 H.reference());

	  tetrahedra[5*cellNumber  ] = T0;
	  tetrahedra[5*cellNumber+1] = T1;
	  tetrahedra[5*cellNumber+2] = T2;
	  tetrahedra[5*cellNumber+3] = T3;
	  tetrahedra[5*cellNumber+4] = T4;
	}
	cellNumber++;
      }

  if (translateSurface) {

    // Computes faces of the mesh [needed to build the surface mesh]
    // @note cannot use FacesBuilder, since the mesh is not built ...
    typedef TinyVector<Triangle::NumberOfVertices, size_t> FaceVertices;
    std::map<FaceVertices, FaceVertices> facesIdSet;
    for (size_t i = 0; i<tetrahedra.size(); ++i) {
      const Tetrahedron& T = tetrahedra[i];
      for (size_t j=0; j<Tetrahedron::NumberOfFaces; ++j) {
	std::set<size_t> vertices;
	FaceVertices face;
	for (size_t k=0; k<Triangle::NumberOfVertices ;++k) {
	  face[k] = verticesSet.number(T(Tetrahedron::faces[j][k]));
	  vertices.insert(face[k]);
	}
	ASSERT(vertices.size() == Triangle::NumberOfVertices);
	FaceVertices faceId;
	size_t k=0;
	for (std::set<size_t>::const_iterator iv = vertices.begin();
	     iv != vertices.end(); ++iv,++k) {
	  faceId[k] = *iv;
	}

	if (facesIdSet.find(faceId) == facesIdSet.end()) {
	  facesIdSet.insert(facesIdSet.end(),
			    std::make_pair(faceId, face));
	}
      }
    }

    // Treating border elements
    const SurfaceMeshOfQuadrangles& sq = *M.surfaceMesh();

    ReferenceCounting<Vector<Triangle> > pBorderTriangles
      = new Vector<Triangle>(2 * sq.numberOfCells());
    Vector<Triangle>& borderTriangles = *pBorderTriangles;
    size_t quadrangleNumber = 0;
    for (SurfaceMeshOfQuadrangles::const_iterator j=(sq);
	 not(j.end()); ++j) {

      const Quadrangle& Q = *j;

      std::set<size_t> vertices;
      vertices.insert(M.vertexNumber(Q(0)));
      vertices.insert(M.vertexNumber(Q(1)));
      vertices.insert(M.vertexNumber(Q(2)));

      FaceVertices faceId;
      size_t k=0;
      for (std::set<size_t>::const_iterator iv = vertices.begin();
	   iv != vertices.end(); ++iv,++k) {
	faceId[k] = *iv;
      }

      if (facesIdSet.find(faceId) != facesIdSet.end()) {
	Triangle T1(verticesSet[M.vertexNumber(Q(0))],
		    verticesSet[M.vertexNumber(Q(1))],
		    verticesSet[M.vertexNumber(Q(2))],
		    Q.reference());
	Triangle T2(verticesSet[M.vertexNumber(Q(2))],
		    verticesSet[M.vertexNumber(Q(3))],
		    verticesSet[M.vertexNumber(Q(0))],
		    Q.reference());

	borderTriangles[quadrangleNumber*2]     = T1;
	borderTriangles[quadrangleNumber*2 + 1] = T2;
      } else {
	Triangle T1(verticesSet[M.vertexNumber(Q(0))],
		    verticesSet[M.vertexNumber(Q(1))],
		    verticesSet[M.vertexNumber(Q(3))],
		    Q.reference());
	Triangle T2(verticesSet[M.vertexNumber(Q(2))],
		    verticesSet[M.vertexNumber(Q(3))],
		    verticesSet[M.vertexNumber(Q(1))],
		    Q.reference());

	borderTriangles[quadrangleNumber*2]     = T1;
	borderTriangles[quadrangleNumber*2 + 1] = T2;
      }
      quadrangleNumber++;
    }

    MeshOfTetrahedra* meshOfTetrahedra
      = new MeshOfTetrahedra(pVerticesSet,
			     pCorrespondance,
			     pTetrahedra,
			     new SurfaceMeshOfTriangles(pVerticesSet,
							pCorrespondance,
							pBorderTriangles));

    __mesh = meshOfTetrahedra;
  } else {
    __mesh = new MeshOfTetrahedra(pVerticesSet,
				  pCorrespondance,
				  pTetrahedra);
  }
}


template<>
ReferenceCounting<MeshTetrahedrizor::CellMapping>
MeshTetrahedrizor::__motherCells<Structured3DMesh>()
{
  const Structured3DMesh& M
    = static_cast<const Structured3DMesh&>(*__input);

  // In this function hexahedra supposed to be divided into 5
  // tetrahedra
  MeshTetrahedrizor::CellMapping* pCellMapping
    = new MeshTetrahedrizor::CellMapping(5*M.numberOfCells());
  MeshTetrahedrizor::CellMapping& cellMapping = *pCellMapping;

  size_t tetrahedronNumber = 0;
  for(size_t i=0; i<M.shape().nx()-1; ++i)
    for(size_t j=0; j<M.shape().ny()-1; ++j)
      for(size_t k=0; k<M.shape().nz()-1; ++k) {
	const CartesianHexahedron& H = M.cell(i,j,k);
	for (size_t l = 0; l<5; ++l) {
	  cellMapping[tetrahedronNumber] = &H;
	  tetrahedronNumber++;
	}
      }

  return pCellMapping;
}



ReferenceCounting<MeshTetrahedrizor::CellMapping>
MeshTetrahedrizor::motherCells()
{
  switch ((*__input).type()) {
  case Mesh::cartesianHexahedraMesh: {
    return __motherCells<Structured3DMesh>();
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented: unexpected mesh type",
		       ErrorHandler::normal);
    return 0;
  }
  }
}

void
MeshTetrahedrizor::run(const bool& builtLocalizationTools)
{
  switch ((*__input).type()) {
  case Mesh::cartesianHexahedraMesh: {
    this->__build<Structured3DMesh>(builtLocalizationTools);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented: unexpected mesh type",
		       ErrorHandler::normal);
  }
  }
}
