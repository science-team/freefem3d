//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Q2Quadrangle3DFiniteElement.cpp,v 1.2 2006/04/08 10:43:20 delpinux Exp $

#include <Q2Quadrangle3DFiniteElement.hpp>

TinyVector<3, real_t> Q2Quadrangle3DFiniteElement::__massCenter(0.5, 0.5, 0.0);

real_t
Q2Quadrangle3DFiniteElement::W(const size_t& i, const TinyVector<3, real_t>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return __w1(x)*__w1(y);
  }
  case 1: {
    return __w3(x)*__w1(y);
  }
  case 2: {
    return __w3(x)*__w3(y);
  }
  case 3: {
    return __w1(x)*__w3(y);
  }
    // Edges basis functions
  case 4: {
    return __w2(x)*__w1(y);
  }
  case 5: {
    return __w3(x)*__w2(y);
  }
  case 6: {
    return __w2(x)*__w3(y);
  }
  case 7: {
    return __w1(x)*__w2(y);
  }
  case 8: {
    return __w1(x)*__w1(y);
  }
    // Faces basis functions
  case 9: {
    return __w2(x)*__w2(y);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
Q2Quadrangle3DFiniteElement::dxW(const size_t& i, const TinyVector<3, real_t>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return __dw1(x)*__w1(y);
  }
  case 1: {
    return __dw3(x)*__w1(y);
  }
  case 2: {
    return __dw3(x)*__w3(y);
  }
  case 3: {
    return __dw1(x)*__w3(y);
  }
    // Edges basis functions
  case 4: {
    return __dw2(x)*__w1(y);
  }
  case 5: {
    return __dw3(x)*__w2(y);
  }
  case 6: {
    return __dw2(x)*__w3(y);
  }
  case 7: {
    return __dw1(x)*__w2(y);
  }
    // Faces basis functions
  case 8: {
    return __dw2(x)*__w2(y);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
Q2Quadrangle3DFiniteElement::dyW(const size_t& i, const TinyVector<3, real_t>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return __w1(x)*__dw1(y);
  }
  case 1: {
    return __w3(x)*__dw1(y);
  }
  case 2: {
    return __w3(x)*__dw3(y);
  }
  case 3: {
    return __w1(x)*__dw3(y);
  }
    // Edges basis functions
  case 4: {
    return __w2(x)*__dw1(y);
  }
  case 5: {
    return __w3(x)*__dw2(y);
  }
  case 6: {
    return __w2(x)*__dw3(y);
  }
  case 7: {
    return __w1(x)*__dw2(y);
  }
    // Faces basis functions
  case 8: {
    return __w2(x)*__dw2(y);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t 
Q2Quadrangle3DFiniteElement::dzW(const size_t& i, const TinyVector<3, real_t>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return __w1(x)*__w1(y);
  }
  case 1: {
    return __w3(x)*__w1(y);
  }
  case 2: {
    return __w3(x)*__w3(y);
  }
  case 3: {
    return __w1(x)*__w3(y);
  }
    // Edges basis functions
  case 4: {
    return __w2(x)*__w1(y);
  }
  case 5: {
    return __w3(x)*__w2(y);
  }
  case 6: {
    return __w2(x)*__w3(y);
  }
  case 7: {
    return __w1(x)*__w2(y);
  }
    // Faces basis functions
  case 8: {
    return __w2(x)*__w2(y);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}
