//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FatBoundary.cpp,v 1.7 2005/09/25 14:16:05 delpinux Exp $


#include <FatBoundary.hpp>
#include <PDESystem.hpp>

#include <Structured3DMesh.hpp>
#include <FEMDiscretization.hpp>

#include <MatrixManagement.hpp>

void FatBoundary::Discretize (ConstReferenceCounting<Problem> Pb)
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "FBM is not implemented yet ",
		     ErrorHandler::unexpected);

//   //! Sets the problem.
//   __problem = Pb;

//   MemoryManager MM;
//   bool performAssembling
//     = MM.ReserveMatrix(__A,
// 		       problem().numberOfUnknown(),
// 		       mesh().numberOfVertices());

//   MM.ReserveVector(__b, 
// 		   problem().numberOfUnknown(),
// 		   mesh().numberOfVertices());

//   ffout(2) << "Discretizing the PDE Problem ... ";

//   DegreeOfFreedomSet __degreeOfFreedomSet(0,0,0,0);
//   FEMDiscretization<Structured3DMesh>
//     FEM(problem(), mesh(), *__A, *__b, __degreeOfFreedomSet);

//   if (performAssembling) {
//     FEM.assembleMatrix();
//   } else {
//     ffout(2) << "keeping previous operator discretization\n";
//   }
//   FEM.assembleSecondMember();
}

void FatBoundary::Compute ( Solution& u )
{
  ;
}


