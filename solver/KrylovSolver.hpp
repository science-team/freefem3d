//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: KrylovSolver.hpp,v 1.2 2006/07/20 19:08:54 delpinux Exp $

#ifndef KRYLOV_SOLVER_HPP
#define KRYLOV_SOLVER_HPP

#include <ReferenceCounting.hpp>

#include <KrylovSolverOptions.hpp>
#include <GetParameter.hpp>

#include <Structured3DMeshShape.hpp>

#include <Vector.hpp>

class BaseMatrix;
class BaseVector;

class DegreeOfFreedomSet;
class Problem;

/**
 * @file   KrylovSolver.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 18:58:02 2006
 * 
 * @brief  This object class drives the linear system resolution
 * 
 */
class KrylovSolver
{
private:
  const BaseMatrix& __A;	/**< the matrix  */
  const BaseVector& __b;	/**< the second member */

  const DegreeOfFreedomSet&
  __degreeOfFreedomSet;		/**< The degree of freedom set */

  KrylovSolverOptions::Type
  __type;			/**< the type of the solver */

  KrylovSolverOptions::PreconditionerType
  __pType;			/**< the preconditioner type */

  ReferenceCounting<Structured3DMeshShape>
  __meshShape;			/**< the mesh shape */

  GetParameter<KrylovSolverOptions>
  __options;			/**< the solver options */

public:
  /** 
   * Constructor
   * 
   * @param A given matric
   * @param b given second member
   * @param degreeOfFreedomSet degree of freedom set
   * @param meshShape mesh shape
   */
  KrylovSolver(const BaseMatrix& A,
	       const BaseVector& b,
	       const DegreeOfFreedomSet& degreeOfFreedomSet,
	       ReferenceCounting<Structured3DMeshShape> meshShape = 0)
    : __A(A),
      __b(b),
      __degreeOfFreedomSet(degreeOfFreedomSet),
      __meshShape(meshShape)
  {
    __type = __options.value().type();
    __pType = __options.value().precond();
  }

  /** 
   * Solves the problem
   * 
   * @param problem given problem
   * @param u unknowns vector
   */
  void solve(const Problem& problem, ReferenceCounting<Vector<real_t> > u);
};

#endif // KRYLOVSOLVER_HPP
