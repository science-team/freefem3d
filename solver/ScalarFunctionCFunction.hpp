//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionCFunction.hpp,v 1.4 2007/02/26 01:12:21 delpinux Exp $

#ifndef SCALAR_FUNCTION_CFUNCTION_HPP
#define SCALAR_FUNCTION_CFUNCTION_HPP

#include <ScalarFunctionBase.hpp>
#include <iostream>
#include <string>

/**
 * @file   ScalarFunctionCFunction.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:43:18 2006
 * 
 * @brief  C-function interface
 * 
 */
class ScalarFunctionCFunction
  : public ScalarFunctionBase
{
private:
  const std::string __cFunctionName; /**< name of a C-function */
  ConstReferenceCounting<ScalarFunctionBase>
  __function;			/**< argument to the C-function */

  real_t (*__cfunction)(real_t x); /**< function-pointer to the C-function */

  /** 
   * Writes the function
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << __cFunctionName << '(' << *__function << ')';
    return os;
  }

public:
  /** 
   * Evaluates the C-function at the @f$ X @f$ position
   * 
   * @return (C-function)(__function(X))
   */
  real_t operator()(const TinyVector<3, real_t>& X) const
  {
    const ScalarFunctionBase& function = *__function;
    return __cfunction(function(X));
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param cfunction the C-function name
   * @param function the argument function
   */
  ScalarFunctionCFunction(const std::string& cfunction,
			  ConstReferenceCounting<ScalarFunctionBase> function)
    : ScalarFunctionBase(ScalarFunctionBase::cfunction),
      __cFunctionName(cfunction),
      __function(function)
  {
    if (__cFunctionName == "abs") {
      __cfunction = &std::abs;
      return;
    }
    if (__cFunctionName == "sin") {
      __cfunction = &std::sin;
      return;
    }
    if (__cFunctionName == "cos") {
      __cfunction = &std::cos;
      return;
    }
    if (__cFunctionName == "tan") {
      __cfunction = &std::tan;
      return;
    }
    if (__cFunctionName == "asin") {
      __cfunction = &std::asin;
      return;
    }
    if (__cFunctionName == "acos") {
      __cfunction = &std::acos;
      return;
    }
    if (__cFunctionName == "atan") {
      __cfunction = &std::atan;
      return;
    }
    if (__cFunctionName == "sqrt") {
      __cfunction = &std::sqrt;
      return;
    }
    if (__cFunctionName == "exp") {
      __cfunction = &std::exp;
      return;
    }
    if (__cFunctionName == "log") {
      __cfunction = &std::log;
      return;
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }

  /** 
   * Copy constructor
   * 
   * @param f given C-function
   */
  ScalarFunctionCFunction(const ScalarFunctionCFunction& f)
    : ScalarFunctionBase(f),
      __cFunctionName(f.__cFunctionName),
      __function(f.__function),
      __cfunction(f.__cfunction)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionCFunction()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_CFUNCTION_HPP
