//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Index.hpp,v 1.2 2007/05/20 23:02:47 delpinux Exp $

// This class allow to Manipulate Indices.

#ifndef _INDEX_HPP_
#define _INDEX_HPP_

/*! \class Index
  This class is used to deal with (i,j,k) indices.

  \remark Could provide a template version, but interest is actualy limited.

  \author St�phane Del Pino.
 */
class Index {
private:
  //! The number of values of the Index. This could be a template argument.
  static const size_t __size = 3;

  //! The values of the Index, they are unsigned integers.
  size_t __index[__size];

  //! Good tells if the index is indexing something
  bool __good;
public:

  //! This method is used to change the boolean attribute.
  inline bool& isGood()
  {
    return __good;
  }

  //! Returns true if the index is good (ie: pointing to something)
  inline const bool& isGood() const
  {
    return __good;
  }

  //! Access to the element \a i if the Index.
  size_t& operator[] (const size_t i)
  {
    ASSERT ((i>=0) && (i<__size));
    return __index[i];
  }

  //! Read-only access to the element \a i if the Index.
  const size_t& operator[] (const size_t i) const
  {
    ASSERT ((i>=0) && (i<__size));
    return __index[i];
  }

  //! Affects an Index
  const Index& operator= (const Index& I)
  {
    for (size_t i=0; i<__size; i++)
      __index[i] = I.__index[i];
    return *this;
  }

  /*! Adds an Index
    \remark difference between indices may not be always defined since the
    data are unsigned integers.
   */
  const Index& operator+= (const Index& I)
  {
    for (size_t i=0; i<__size; i++)
      __index[i] += I.__index[i];
    return *this;
  }

  /*! Converts an interger of [0,7] in its decomposition using 2^{index[0..2]}
    basis. So, 3 will become (0,1,1).
  */
  const Index& operator= (const size_t& i)
  {
    ASSERT ((i>=0)&&(i<8));
    __index[2] = ( i >> 2);
    __index[1] = ((i - (__index[2] << 2)) >> 1);
    __index[0] = ( i - (__index[1] << 1) - (__index[2] << 2));

    return *this;
  }

  //! Constructs the Index (\a i,\a j,\a k).
  Index(const size_t i, const size_t j, const size_t k)
    : __good(true)
  {
    ASSERT ((i>=0) && (j>=0) && (k>=0));

    __index[0] = i;
    __index[1] = j;
    __index[2] = k;
  }

  /*!
    Constructor based on the decomposition of an interger of [0,7] using
    binary description.
    \par So, 3 will become (0,1,1).
  */
  explicit Index(const size_t& i)
    : __good(true)
  {
    ASSERT ((i>=0)&&(i<8));
    __index[2] = ( i >> 2);
    __index[1] = ((i - (__index[2] << 2)) >> 1);
    __index[0] = ( i - (__index[1] << 1) - (__index[2] << 2));
  }

  // Default Constructor only useful for CodeWarrior STL
  Index()
    : __good(true)
  {
    ;
  }

  //! Copy constructor.
  Index(const Index& I)
    : __good(I.__good)
  {
    for (size_t i=0; i<__size; i++)
      __index[i] = I.__index[i];
  }

  //! Writes the Index \a I in the stream \a os.
  friend std::ostream& operator << (std::ostream& os,
				    const Index& I)
  {
    os << '(' << I[0] << ',' << I[1] << ',' << I[2] << ')';
    return os;
  }
};

#endif // _INDEX_HPP_

