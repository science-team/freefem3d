//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalOperator.hpp,v 1.4 2007/06/10 15:03:32 delpinux Exp $

#ifndef VARIATIONAL_OPERATOR_HPP
#define VARIATIONAL_OPERATOR_HPP

/**
 * @file   VariationalOperator.hpp
 * @author Stephane Del Pino
 * @date   Thu May 30 18:19:22 2002
 * 
 * @brief  Variational Operator
 */
class VariationalOperator
{
public:
  enum Property {
    normal,
    jump,
    mean
  };
private:
  const size_t
  __testFunctionNumber;		/**< test function number */

protected:
  const VariationalOperator::Property
  __testFunctionProperty;	/**< stores test function properties */

public:
  /** 
   * Returns the test function number
   * 
   * @return __testFunctionNumber
   */
  const size_t& testFunctionNumber() const
  {
    return __testFunctionNumber;
  }

  /** 
   * Returns the test function property
   * 
   * @return __testFunctionProperty
   */
  const VariationalOperator::Property& testFunctionProperty() const
  {
    return __testFunctionProperty;
  }

  /** 
   * Copy constructor
   * 
   * @param number number of the test function
   */
  VariationalOperator(const size_t& number,
		      const VariationalOperator::Property& property)
    : __testFunctionNumber(number),
      __testFunctionProperty(property)
  {
    ;
  }

  /** 
   * Variational Operator
   * 
   * @param vo VariationalOperator
   */
  VariationalOperator(const VariationalOperator& vo)
    : __testFunctionNumber(vo.__testFunctionNumber),
      __testFunctionProperty(vo.__testFunctionProperty)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_HPP
