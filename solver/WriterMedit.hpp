//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: WriterMedit.hpp,v 1.2 2007/02/27 00:30:13 delpinux Exp $

#ifndef WRITER_MEDIT_HPP
#define WRITER_MEDIT_HPP

#include <WriterBase.hpp>

/**
 * @file   WriterMedit.hpp
 * @author St�phane Del Pino
 * @date   Fri Feb 23 16:26:22 2007
 * 
 * @brief  Writer for medit mesh format
 */
class WriterMedit
  : public WriterBase
{
private:
  /** 
   * Copy constructor is forbidden
   * 
   */
  WriterMedit(const WriterMedit&);

  /** 
   * Fills crossed component
   * 
   * @param field the field that will have its component crossed
   * @param values values vector
   */
  void __fillCrossedComponent(const FieldOfScalarFunction& field,
			      Vector<real_t>& values) const;

  /** 
   * Write mesh elements to a stream
   * 
   * @param os given stream
   * @param m mesh
   */
  template <typename MeshType>
  void __saveElements(std::ostream& os,
		      const MeshType& mesh) const;

  /** 
   * Writes mesh file
   * 
   */
  void __proceedMesh() const;

  /** 
   * Write data file
   * 
   */
  void __proceedData() const;
public:
  /** 
   * Write the Medit file
   * 
   */
  void proceed() const;

  /** 
   * Constructor
   * 
   * @param mesh given mesh
   * @param fileName basename of the file to create
   * @param CR type of cardriage return (OS specific)
   */
  WriterMedit(ConstReferenceCounting<Mesh> mesh,
	      const std::string& fileName,
	      const std::string& CR);

  /** 
   * Destructor
   * 
   */
  ~WriterMedit();
};

#endif // WRITER_MEDIT_HPP
