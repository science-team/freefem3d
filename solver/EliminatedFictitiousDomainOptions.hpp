//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: EliminatedFictitiousDomainOptions.hpp,v 1.1 2003/09/23 19:03:23 delpinux Exp $

#ifndef ELIMINATED_FICTITIOUS_DOMAIN_OPTIONS_HPP
#define ELIMINATED_FICTITIOUS_DOMAIN_OPTIONS_HPP

#include <ParametrizableObject.hpp>

class EliminatedFictitiousDomainOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  static const char* identifier()
  {
    // autodoc: "Options for elimination method (by now none)"
    return "eliminate";
  }
};

#endif // ELIMINATED_FICTITIOUS_DOMAIN_OPTIONS_HPP

