//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionDerivative.cpp,v 1.3 2007/05/01 13:15:32 delpinux Exp $

#include <ScalarFunctionDerivative.hpp>
#include <ErrorHandler.hpp>

std::ostream& 
ScalarFunctionDerivative::
__put(std::ostream& os) const
{
  switch(__direction) {
  case ScalarFunctionDerivative::x: {
    os << "dx(";
    break;
  }
  case ScalarFunctionDerivative::y: {
    os << "dy(";
    break;
  }
  case ScalarFunctionDerivative::z: {
    os << "dz(";
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown derivative direction",
		       ErrorHandler::unexpected);
  }
  }
  os << *__functionToDerive << ')';
  return os;
}


real_t
ScalarFunctionDerivative::
operator()(const TinyVector<3, real_t>& X) const
{
  switch(__direction) {
  case ScalarFunctionDerivative::x: {
    return __functionToDerive->dx(X);
  }
  case ScalarFunctionDerivative::y: {
    return __functionToDerive->dy(X);
    break;
  }
  case ScalarFunctionDerivative::z: {
    return __functionToDerive->dz(X);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown derivative direction",
		       ErrorHandler::unexpected);
  }
  }
  return 0;
}

ScalarFunctionDerivative::
ScalarFunctionDerivative(const ScalarFunctionDerivative::Direction& direction,
			 ConstReferenceCounting<ScalarFunctionBase> functionToDerive)
  : ScalarFunctionBase(ScalarFunctionBase::derivate),
    __direction(direction)
{
  switch(functionToDerive->type()) {
  case ScalarFunctionBase::femfunction:
  case ScalarFunctionBase::spectral: {
    const ScalarFunctionBase* f = functionToDerive;
    __functionToDerive = f;
    break;
  }
  default:
  if (functionToDerive->type() != ScalarFunctionBase::femfunction) {
    std::stringstream errorMsg;

    errorMsg << "cannot compute derivative of non discrete functions :-(\n";
    errorMsg << "the function " << (*functionToDerive) << " is not of that kind"
	     << std::ends;

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);

  }
  }
}

ScalarFunctionDerivative::
ScalarFunctionDerivative(const ScalarFunctionDerivative& f)
  : ScalarFunctionBase(f),
    __direction(f.__direction),
    __functionToDerive(f.__functionToDerive)
{
  ;
}
    
ScalarFunctionDerivative::
~ScalarFunctionDerivative()
{
  ;
}
