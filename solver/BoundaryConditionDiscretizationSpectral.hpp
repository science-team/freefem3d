//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionDiscretizationSpectral.hpp,v 1.2 2007/06/01 11:25:19 yakoubix Exp $

#ifndef BOUNDARY_CONDITION_DISCRETIZATION_SPECTRAL_HPP
#define BOUNDARY_CONDITION_DISCRETIZATION_SPECTRAL_HPP

#include <BoundaryConditionDiscretization.hpp>
#include <SpectralMesh.hpp>
#include <Interval.hpp>
#include <GaussLobatto.hpp>
#include <LegendreBasis.hpp>
#include <SpectralConformTransformation.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

class BoundaryConditionDiscretizationSpectral
  : public BoundaryConditionDiscretization
{
private:
  const SpectralMesh& __mesh;

  TinyVector<3,ConstReferenceCounting<Interval> > __interval;
  TinyVector<3,ConstReferenceCounting<SpectralConformTransformation> >__transform;
  TinyVector<3,ConstReferenceCounting<GaussLobatto> >__gaussLobatto;
  TinyVector<3,ConstReferenceCounting<LegendreBasis> >__basis;
public:
  void getDiagonal(BaseVector& X) const;

  void setMatrix(ReferenceCounting<BaseMatrix> A,
		 ReferenceCounting<BaseVector> b) const;

  void setSecondMember(ReferenceCounting<BaseMatrix> A,
		       ReferenceCounting<BaseVector> b) const;

  void timesX(const BaseVector& X, BaseVector& Z) const;

  void transposedTimesX(const BaseVector& X, BaseVector& Z) const;


  BoundaryConditionDiscretizationSpectral(const Problem& problem,
					  const SpectralMesh& mesh,
					  const DegreeOfFreedomSet& dof)
    : BoundaryConditionDiscretization(problem,dof),
      __mesh(mesh),
      __interval(new Interval(__mesh.shape().a()[0],__mesh.shape().b()[0]),
		 new Interval(__mesh.shape().a()[1],__mesh.shape().b()[1]),
		 new Interval(__mesh.shape().a()[2],__mesh.shape().b()[2])),
      __transform(new SpectralConformTransformation(*__interval[0]),
		  new SpectralConformTransformation(*__interval[1]),
		  new SpectralConformTransformation(*__interval[2])),
      __gaussLobatto(new GaussLobatto(__mesh.degree(0)+1),
		     new GaussLobatto(__mesh.degree(1)+1),
		     new GaussLobatto(__mesh.degree(2)+1)),
      __basis(new LegendreBasis(__mesh.degree(0)),
	      new LegendreBasis(__mesh.degree(1)),
	      new LegendreBasis(__mesh.degree(2)))
  {
    ;
  }
  
  ~BoundaryConditionDiscretizationSpectral()
  {
    ;
  }
};

#endif // BOUNDARY_CONDITION_DISCRETIZATION_SPECTRAL_HPP
