//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionDiscretizationSpectral.cpp,v 1.6 2007/06/20 19:08:11 delpinux Exp $

#include <BoundaryConditionDiscretizationSpectral.hpp>
#include <ErrorHandler.hpp>

#include <VariationalProblem.hpp>
#include <VariationalBorderOperatorAlphaUV.hpp>
#include<VariationalBorderOperatorFV.hpp>
#include <VariationalLinearOperator.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <BoundaryReferences.hpp>

#include <UnAssembledMatrix.hpp>

#include <DoubleHashedMatrix.hpp>


void BoundaryConditionDiscretizationSpectral::
getDiagonal(BaseVector& X) const
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "not implemented",
		     ErrorHandler::unexpected);
}

void BoundaryConditionDiscretizationSpectral::
setMatrix(ReferenceCounting<BaseMatrix> givenA,
	  ReferenceCounting<BaseVector> b) const
{
  switch((*givenA).type()) {
  case BaseMatrix::doubleHashedMatrix: {

    DoubleHashedMatrix& A = dynamic_cast<DoubleHashedMatrix&>(*givenA);

    switch(this->problem().type()) {
    case Problem::variationalProblem: {
      const VariationalProblem& variationalProblem
	=  dynamic_cast<const VariationalProblem&>(this->problem());

      for (VariationalProblem::bilinearBorderOperatorConst_iterator
	     iBorderOperator = variationalProblem.beginBilinearBorderOperator();
	   iBorderOperator != variationalProblem.endBilinearBorderOperator();
	   ++iBorderOperator) {

	const VariationalBilinearBorderOperator& borderOperator = **iBorderOperator;
	switch (borderOperator.type()) {
	case VariationalBilinearBorderOperator::alphaUV: {
	  const VariationalBorderOperatorAlphaUV& operatorAlphaUV
	    = dynamic_cast<const VariationalBorderOperatorAlphaUV&>(borderOperator);
	  ConstReferenceCounting<Boundary> boundary = operatorAlphaUV.boundary();

	  switch(boundary->type()) {
	  case Boundary::references: {
	    const BoundaryReferences& boundaryReference = dynamic_cast<const BoundaryReferences&>(*boundary);
	    for (BoundaryReferences::ReferencesSet::const_iterator
		   iBoundary = boundaryReference.references().begin();
		 iBoundary != boundaryReference.references().end(); ++iBoundary) {

	      const ScalarFunctionBase& alpha = operatorAlphaUV.alpha();
	      const size_t boundaryNumber = *iBoundary;  

	      TinyVector<3, size_t> direction;
	      for (size_t i=0; i<3; ++i) {
		direction[i] = (boundaryNumber/2+i+1) % 3;
	      }

	      // constructs the quadrature vertices
	      TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
	      for (size_t i=0; i<2; ++i) {
		quadratureBaseValues[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  real_t xj= (*__gaussLobatto[direction[i]])(j);
		  quadratureBaseValues[i][j].resize((*__basis[direction[i]]).dimension());
		  (*__basis[direction[i]]).getValues(xj,quadratureBaseValues[i][j]);
		}
	      }
	      quadratureBaseValues[2].resize(2);
	      for (size_t j=0; j<2 ; ++j) {
		real_t xj= -1.+2.*j;
		quadratureBaseValues[2][j].resize((*__basis[direction[2]]).dimension());
		(*__basis[direction[2]]).getValues(xj,quadratureBaseValues[2][j]);
		std::cout << "quadratureBaseValues[2][j]=\n" <<quadratureBaseValues[2][j]<<'\n';
	      }

	      //construction des nodes
	      TinyVector<2, Vector<real_t> >nodes;
	      for (size_t i=0; i<2; ++i) {
		nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
		  std::cout << "nodes["<<i<<"]["<<j<<"]=" << nodes[i][j] << '\n';
		}
	      }

	      const real_t jacobianDet = 1./((*__transform[direction[0]]).inverseDeterminant()*
					      (*__transform[direction[1]]).inverseDeterminant());

	      for (size_t i=0; i < (*__gaussLobatto[direction[0]]).numberOfPoints(); ++i) {
		const real_t x = nodes[0][i];
		const real_t wi = (*__gaussLobatto[direction[0]]).weight(i);
	    
		for (size_t j=0; j < (*__gaussLobatto[direction[1]]).numberOfPoints(); ++j) {
		  const real_t y = nodes[1][j];
		  const real_t wij = (*__gaussLobatto[direction[1]]).weight(j)*wi;

		  real_t alphaValue = wij * jacobianDet;
		  switch(boundaryNumber) {
		  case 0: {
		    alphaValue *= alpha(__mesh.shape().a(0), x, y);
		    break;
		  }
		  case 1: {
		    alphaValue *= alpha(__mesh.shape().b(0), x, y);
		    break;
		  }
		  case 2: {
		    alphaValue *= alpha(y, __mesh.shape().a(1), x);
		    break;
		  }
		  case 3: {
		    alphaValue *= alpha(y, __mesh.shape().b(1), x);
		    break;
		  }
		  case 4: {
		    alphaValue *= alpha(x, y, __mesh.shape().a(2));
		    break;
		  }
		  case 5: {
		    alphaValue *= alpha(x, y, __mesh.shape().b(2));
		    break;
		  }
		  }

		  TinyVector<3,size_t> J;
		  for (size_t ml=0; ml<(*__basis[direction[0]]).dimension(); ++ml){
		    J[direction[0]] = ml;
		    for (size_t pl=0; pl<(*__basis[direction[1]]).dimension(); ++pl){
		      J[direction[1]] = pl;
		      for (size_t ql=0; ql<(*__basis[direction[2]]).dimension(); ++ql){
			J[direction[2]] = ql;
			const size_t l=__mesh.dofNumber(J[0],J[1],J[2]);

			TinyVector<3,size_t> I;
			for (size_t mm=0; mm<(*__basis[direction[0]]).dimension(); ++mm){
			  I[direction[0]] = mm;
			  for (size_t pm=0; pm<(*__basis[direction[1]]).dimension(); ++pm){
			    I[direction[1]] = pm;
			    for (size_t qm=0; qm<(*__basis[direction[2]]).dimension(); ++qm){
			      I[direction[2]] = qm;

			      const size_t m = __mesh.dofNumber(I[0],I[1],I[2]);
			      A(__degreeOfFreedomSet(operatorAlphaUV.testFunctionNumber(),m),
				__degreeOfFreedomSet(operatorAlphaUV.unknownNumber(),l))
				+=quadratureBaseValues[0][i][ml]
				* quadratureBaseValues[0][i][mm]

				* quadratureBaseValues[1][j][pl]
				* quadratureBaseValues[1][j][pm]

				* quadratureBaseValues[2][boundaryNumber%2][qm]
				* quadratureBaseValues[2][boundaryNumber%2][ql]

				* alphaValue;

			    }
			  }
			}
		      }
		    }
		  }
		}
	      }
	    }
	  
	    break;
	  }
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);	
	  }
	  }
	
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);	
	}
	}
      }
      break;
    }

    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented",
			 ErrorHandler::unexpected);
    }
    }
    break;
  }

  case BaseMatrix::unAssembled: {
    UnAssembledMatrix& A = dynamic_cast<UnAssembledMatrix&>(*givenA);
    A.setBoundaryConditions(this);
    break;
  }

  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexected matrix type",
		       ErrorHandler::unexpected);
  }
  }
}


void BoundaryConditionDiscretizationSpectral::
setSecondMember(ReferenceCounting<BaseMatrix> givenA,
		ReferenceCounting<BaseVector> givenb) const
{
 
  //return;
  //  Timer t;
  //t.star();
  ffout(2)<<"- assembling second membre-boundary\n";
  Vector<real_t>& b= (static_cast<Vector<real_t>&>(*givenb));

  //  switch((*givenA).type()) {
  // case BaseMatrix::doubleHashedMatrix: {
      
  //   DoubleHashedMatrix& A = dynamic_cast<DoubleHashedMatrix&>(*givenA);
      
      switch(this->problem().type()) {
      case Problem::variationalProblem: {
	const VariationalProblem& variationalProblem
	  =  dynamic_cast<const VariationalProblem&>(this->problem());
	
	for (VariationalProblem::linearBorderOperatorConst_iterator
	       iLinearBorderOperator = variationalProblem.beginLinearBorderOperator();
	     iLinearBorderOperator != variationalProblem.endLinearBorderOperator();
	     ++iLinearBorderOperator) {

	const VariationalLinearBorderOperator& linearBorderOperator = **iLinearBorderOperator;
	switch (linearBorderOperator.type()) {
	case VariationalLinearBorderOperator::FV: {
	  const VariationalBorderOperatorFV& operatorFV
	    = dynamic_cast<const VariationalBorderOperatorFV&>(linearBorderOperator);
	  ConstReferenceCounting<Boundary> boundary = operatorFV.boundary();

	  switch(boundary->type()) {
	  case Boundary::references: {
	    const BoundaryReferences& boundaryReference = 
	      dynamic_cast<const BoundaryReferences&>(*boundary);
	    
	    for (BoundaryReferences::ReferencesSet::const_iterator
		   iBoundary = boundaryReference.references().begin();
		 iBoundary != boundaryReference.references().end(); ++iBoundary) {

	      const ScalarFunctionBase& g = operatorFV.f();
	      const size_t boundaryNumber = *iBoundary;  
	      
	      TinyVector<3, size_t> direction;
	      for (size_t i=0; i<3; ++i) {
		direction[i] = (boundaryNumber/2+i+1) % 3;
	      }
	      
	      // constructs the quadrature vertices
	      TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
	      for (size_t i=0; i<2; ++i) {
		quadratureBaseValues[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  real_t xj= (*__gaussLobatto[direction[i]])(j);
		  quadratureBaseValues[i][j].resize((*__basis[direction[i]]).dimension());
		  (*__basis[direction[i]]).getValues(xj,quadratureBaseValues[i][j]);
		}
	      }
	      quadratureBaseValues[2].resize(2);
	      for (size_t j=0; j<2 ; ++j) {
		real_t xj= -1.+2.*j;
		quadratureBaseValues[2][j].resize((*__basis[direction[2]]).dimension());
		(*__basis[direction[2]]).getValues(xj,quadratureBaseValues[2][j]);
	      }
	      
	      //construction des nodes
	      TinyVector<2, Vector<real_t> >nodes;
	      for (size_t i=0; i<2; ++i) {
		nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
		for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		  nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
		}
	      }
	      
	      const real_t jacobianDet = 1. /((*__transform[direction[0]]).inverseDeterminant()*
					      (*__transform[direction[1]]).inverseDeterminant());
	      
	      for (size_t i=0; i < (*__gaussLobatto[direction[0]]).numberOfPoints(); ++i) {
		const real_t& x = nodes[0][i];
		const real_t& wi = (*__gaussLobatto[direction[0]]).weight(i);
		
		for (size_t j=0; j < (*__gaussLobatto[direction[1]]).numberOfPoints(); ++j) {
		  const real_t& y = nodes[1][j];
		  const real_t& wij = (*__gaussLobatto[direction[1]]).weight(j)*wi;
		  
		  real_t gValue = wij * jacobianDet;
		  switch(boundaryNumber) {
		  case 0: {
		    gValue *= g(__mesh.shape().a(0), x, y);
		    break;
		  }
		  case 1: {
		    gValue *= g(__mesh.shape().b(0), x, y);
		    break;
		  }
		  case 2: {
		    gValue *= g(y, __mesh.shape().a(1), x);
		    break;
		  }
		  case 3: {
		    gValue *= g(y, __mesh.shape().b(1), x);
		    break;
		  }
		  case 4: {
		    gValue *= g(x, y, __mesh.shape().a(2));
		    break;
		  }
		  case 5: {
		    gValue *= g(x, y, __mesh.shape().b(2));
		    break;
		  }
		  default: {
		    throw ErrorHandler(__FILE__,__LINE__,
  				       "not implemented",
				       ErrorHandler::unexpected);
		  }
		  }


		  TinyVector<3,size_t> J;
		  for (size_t ml=0; ml<(*__basis[direction[0]]).dimension(); ++ml){
		    J[direction[0]] = ml;
		    for (size_t pl=0; pl<(*__basis[direction[1]]).dimension(); ++pl){
		      J[direction[1]] = pl;
		      for (size_t ql=0; ql<(*__basis[direction[2]]).dimension(); ++ql){
			J[direction[2]] = ql;
			const size_t l=__mesh.dofNumber(J[0],J[1],J[2]);
			
			b[__degreeOfFreedomSet(operatorFV.testFunctionNumber(),l)] 
			  
			  +=quadratureBaseValues[0][i][ml]
			  * quadratureBaseValues[1][j][pl]
			  * quadratureBaseValues[2][boundaryNumber%2][ql]
			  *gValue
			  ;		
		      }
		    }
		  }
		}
	      }
	    }
	    break;
	  }
	  default: {
	    throw ErrorHandler(__FILE__,__LINE__,
			       "not implemented",
			       ErrorHandler::unexpected);
	  }
	    
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);
	}
	}
	}
	
	break;
      }
	
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);
      }
      }
}


void BoundaryConditionDiscretizationSpectral::
timesX(const BaseVector& X, BaseVector& Z) const
{
  
  const Vector<real_t>& u = dynamic_cast<const Vector<real_t>&>(X);

  Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(Z);
  
  
  switch(this->problem().type()) {
  case Problem::variationalProblem: {
    const VariationalProblem& variationalProblem
      =  dynamic_cast<const VariationalProblem&>(this->problem());
    
    for (VariationalProblem::bilinearBorderOperatorConst_iterator
	   iBorderOperator = variationalProblem.beginBilinearBorderOperator();
	 iBorderOperator != variationalProblem.endBilinearBorderOperator();
	 ++iBorderOperator) {
      
      const VariationalBilinearBorderOperator& borderOperator = **iBorderOperator;
      switch (borderOperator.type()) {
      case VariationalBilinearBorderOperator::alphaUV: {

	const VariationalBorderOperatorAlphaUV& operatorAlphaUV
	  = dynamic_cast<const VariationalBorderOperatorAlphaUV&>(borderOperator);
	ConstReferenceCounting<Boundary> boundary = operatorAlphaUV.boundary();

	switch(boundary->type()) {
	case Boundary::references: {
	  const BoundaryReferences& boundaryReference = dynamic_cast<const BoundaryReferences&>(*boundary);
	  for (BoundaryReferences::ReferencesSet::const_iterator
		 iBoundary = boundaryReference.references().begin();
	       iBoundary != boundaryReference.references().end(); ++iBoundary) {
	    
	    const ScalarFunctionBase& alpha = operatorAlphaUV.alpha();
	    const size_t boundaryNumber = *iBoundary;  
	    
	    TinyVector<3, size_t> direction;
	    for (size_t i=0; i<3; ++i) {
	      direction[i] = (boundaryNumber/2+i+1) % 3;
	    }

	    // constructs the quadrature vertices

	    TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
	    for (size_t i=0; i<2; ++i) {
	      quadratureBaseValues[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		real_t xj= (*__gaussLobatto[direction[i]])(j);
		quadratureBaseValues[i][j].resize((*__basis[direction[i]]).dimension());
		(*__basis[direction[i]]).getValues(xj,quadratureBaseValues[i][j]);
	      }
	    }
	    quadratureBaseValues[2].resize(2);
	    for (size_t j=0; j<2 ; ++j) {
	      real_t xj= -1.+2.*j;
	      quadratureBaseValues[2][j].resize((*__basis[direction[2]]).dimension());
	      (*__basis[direction[2]]).getValues(xj,quadratureBaseValues[2][j]);
	    }

	    //construction des nodes
	    TinyVector<2, Vector<real_t> >nodes;
	    for (size_t i=0; i<2; ++i) {
	      nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
	      }
	    }
  
	    Vector<Vector<real_t> > u_jk(__basis[direction[0]]->dimension());
	    for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
	      u_jk[j].resize(__basis[direction[1]]->dimension());
	      for(size_t k=0; k<__basis[direction[1]]->dimension(); ++k){
		 u_jk[j][k]=0;
	       }
	    }

	    {
	      TinyVector<3,size_t> J;
	      for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
		J[direction[0]]=j;
		for(size_t k=0; k<__basis[direction[1]]->dimension(); ++k){
		  J[direction[1]]=k;
		  real_t& u_jk_value = u_jk[j][k];
		  for(size_t i=0; i<__basis[direction[2]]->dimension(); ++i){
		    J[direction[2]]=i;
		    const size_t dofNumberU = __mesh.dofNumber(J[0],J[1],J[2]);
		    u_jk_value
		      += quadratureBaseValues[2][boundaryNumber%2][i]
		      *  u[__degreeOfFreedomSet(operatorAlphaUV.unknownNumber(),dofNumberU)];
		  }
		}
	      }
	    }
	    
	    Vector<Vector<real_t> >  u_js(__basis[direction[0]]->dimension());
	    for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
	      u_js[j].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	       for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		 u_js[j][s]=0;
	       }
	    }

	    for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_js_value = u_js[j][s];
		for(size_t k=0; k<__basis[direction[1]]->dimension(); ++k){
		  u_js_value
		    += quadratureBaseValues[1][s][k]* u_jk[j][k];
		}
	      }
	    }

	    Vector<Vector<real_t> >  u_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      u_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	       for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		 u_rs[r][s]=0;
	       }
	    }

	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_rs_value = u_rs[r][s];
		for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
		  u_rs_value
		    += quadratureBaseValues[0][r][j]*u_js[j][s];
		}
	      }
	    }

	    const real_t jacobianDet = 1. /(__transform[direction[0]]->inverseDeterminant()*
					    __transform[direction[1]]->inverseDeterminant());
	    
	    Vector<Vector<real_t> > alpha_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      alpha_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		alpha_rs[r][s]=0;
	       }
	    }
	    
	    for (size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      const real_t& x = nodes[0][r];
	      for (size_t s=0; s <__gaussLobatto[direction[1]]->numberOfPoints(); ++s) {
		const real_t& y = nodes[1][s];
		
		switch(boundaryNumber) {
		case 0: {
		  alpha_rs[r][s] = alpha(__mesh.shape().a(0), x, y);
		  break;
		}
		case 1: {
		  alpha_rs[r][s] = alpha(__mesh.shape().b(0), x, y);
		  break;
		}
		case 2: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().a(1), x);
		  break;
		}
		case 3: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().b(1), x);
		  break;
		}
		case 4: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().a(2));
		  break;
		}
		case 5: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().b(2));
		  break;
		}
		}
	      }
	    }
	    Vector<Vector<real_t> >  v_rp(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      v_rp[r].resize(__basis[direction[1]]->dimension());
	      for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		v_rp[r][p]=0;
	      }
	    }
		
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		real_t& v_rp_value = v_rp[r][p];
		for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		  v_rp_value
		    += quadratureBaseValues[1][s][p]*u_rs[r][s]*
		    __gaussLobatto[direction[1]]->weight(s)* alpha_rs[r][s]*jacobianDet;
		}
	      }
	    }

	    Vector<Vector<real_t> >  v_np(__basis[direction[0]]->dimension());
	    for(size_t n=0; n<__basis[direction[0]]->dimension(); ++n){
	      v_np[n].resize(__basis[direction[1]]->dimension());
	       for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		 v_np[n][p]=0;
	       }
	    }

	    for(size_t n=0; n<__basis[direction[0]]->dimension(); ++n){
	      for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		real_t& v_np_value = v_np[n][p];
		for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
		  v_np_value
		    += quadratureBaseValues[0][r][n]*v_rp[r][p]*
		    __gaussLobatto[direction[0]]->weight(r);
		}
	      }
	    }
	    
	    TinyVector<3,size_t> I;	    
	    for(size_t m=0; m<__basis[direction[2]]->dimension(); ++m){
	      I[direction[2]]=m;
	      for(size_t n=0; n<__basis[direction[0]]->dimension(); ++n){
		I[direction[0]]=n;
		for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		  I[direction[1]]=p;
		  const size_t dofNumberV =  __mesh.dofNumber(I[0],I[1],I[2]);
		  v[__degreeOfFreedomSet(operatorAlphaUV.testFunctionNumber(),dofNumberV)]
				
		    += quadratureBaseValues[2][boundaryNumber%2][m]*v_np[n][p];
		}
	      }
	    }
	  
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);	
	}
	}
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);	
      }
      }
    }
    break;
  }
  case Problem::pdeProblem: {
    break; 
 }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}


void BoundaryConditionDiscretizationSpectral::
transposedTimesX(const BaseVector& X, BaseVector& Z) const
{
  const Vector<real_t>& u = dynamic_cast<const Vector<real_t>&>(X);
  Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(Z);
  
  switch(this->problem().type()) {
  case Problem::variationalProblem: {
    const VariationalProblem& variationalProblem
      =  dynamic_cast<const VariationalProblem&>(this->problem());
    
    for (VariationalProblem::bilinearBorderOperatorConst_iterator
	   iBorderOperator = variationalProblem.beginBilinearBorderOperator();
	 iBorderOperator != variationalProblem.endBilinearBorderOperator();
	 ++iBorderOperator) {
      
      const VariationalBilinearBorderOperator& borderOperator = **iBorderOperator;
      switch (borderOperator.type()) {
      case VariationalBilinearBorderOperator::alphaUV: {
	
	const VariationalBorderOperatorAlphaUV& operatorAlphaUV
	  = dynamic_cast<const VariationalBorderOperatorAlphaUV&>(borderOperator);
	ConstReferenceCounting<Boundary> boundary = operatorAlphaUV.boundary();
	
	switch(boundary->type()) {
	case Boundary::references: {
	  const BoundaryReferences& boundaryReference = dynamic_cast<const BoundaryReferences&>(*boundary);
	  for (BoundaryReferences::ReferencesSet::const_iterator
		 iBoundary = boundaryReference.references().begin();
	       iBoundary != boundaryReference.references().end(); ++iBoundary) {
	    
	    const ScalarFunctionBase& alpha = operatorAlphaUV.alpha();
	    const size_t boundaryNumber = *iBoundary;  
	    
	    TinyVector<3, size_t> direction;
	    for (size_t i=0; i<3; ++i) {
	      direction[i] = (boundaryNumber/2+i+1) % 3;
	    }
	    
	    // constructs the quadrature vertices

	    TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
	    for (size_t i=0; i<2; ++i) {
	      quadratureBaseValues[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j < (*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		real_t xj= (*__gaussLobatto[direction[i]])(j);
		quadratureBaseValues[i][j].resize((*__basis[direction[i]]).dimension());
		(*__basis[direction[i]]).getValues(xj,quadratureBaseValues[i][j]);
	      }
	    }
	    quadratureBaseValues[2].resize(2);
	    for (size_t j=0; j<2 ; ++j) {
	      real_t xj= -1.+2.*j;
	      quadratureBaseValues[2][j].resize((*__basis[direction[2]]).dimension());
	      (*__basis[direction[2]]).getValues(xj,quadratureBaseValues[2][j]);
	    }
	    
	    //construction des nodes
	    TinyVector<2, Vector<real_t> >nodes;
	    for (size_t i=0; i<2; ++i) {
	      nodes[i].resize((*__gaussLobatto[direction[i]]).numberOfPoints());
	      for (size_t j=0; j<(*__gaussLobatto[direction[i]]).numberOfPoints(); ++j) {
		nodes[i][j] =(*__transform[direction[i]])((*__gaussLobatto[direction[i]])(j));
	      }
	    }
	    
	    Vector<Vector<real_t> > u_jk(__basis[direction[0]]->dimension());
	    for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
	      u_jk[j].resize(__basis[direction[1]]->dimension());
	      for(size_t k=0; k<__basis[direction[1]]->dimension(); ++k){
		 u_jk[j][k]=0;
	       }
	    }

	    {
	      TinyVector<3,size_t> J;
	      for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
		J[direction[0]]=j;
		for(size_t k=0; k<__basis[direction[1]]->dimension(); ++k){
		  J[direction[1]]=k;
		  real_t& u_jk_value = u_jk[j][k];
		  for(size_t i=0; i<__basis[direction[2]]->dimension(); ++i){
		    J[direction[2]]=i;
		    const size_t dofNumberU = __mesh.dofNumber(J[0],J[1],J[2]);
		    u_jk_value
		      += quadratureBaseValues[2][boundaryNumber%2][i]
		      *  u[__degreeOfFreedomSet(operatorAlphaUV.testFunctionNumber(),dofNumberU)];
		  }
		}
	      }
	    }
	    
	    Vector<Vector<real_t> >  u_js(__basis[direction[0]]->dimension());
	    for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
	      u_js[j].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	       for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		 u_js[j][s]=0;
	       }
	    }

	    for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_js_value = u_js[j][s];
		for(size_t k=0; k<__basis[direction[1]]->dimension(); ++k){
		  u_js_value
		    += quadratureBaseValues[1][s][k]* u_jk[j][k];
		}
	      }
	    }

	    Vector<Vector<real_t> >  u_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      u_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	       for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		 u_rs[r][s]=0;
	       }
	    }

	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		real_t& u_rs_value = u_rs[r][s];
		for(size_t j=0; j<__basis[direction[0]]->dimension(); ++j){
		  u_rs_value
		    += quadratureBaseValues[0][r][j]*u_js[j][s];
		}
	      }
	    }

	    const real_t jacobianDet = 1. /(__transform[direction[0]]->inverseDeterminant()*
					    __transform[direction[1]]->inverseDeterminant());
	    
	    Vector<Vector<real_t> > alpha_rs(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      alpha_rs[r].resize(__gaussLobatto[direction[1]]->numberOfPoints());
	      for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		alpha_rs[r][s]=0;
	       }
	    }
	    
	    for (size_t r=0; r < __gaussLobatto[direction[0]]->numberOfPoints(); ++r) {
	      const real_t& x = nodes[0][r];
	      for (size_t s=0; s <__gaussLobatto[direction[1]]->numberOfPoints(); ++s) {
		const real_t& y = nodes[1][s];
		
		switch(boundaryNumber) {
		case 0: {
		  alpha_rs[r][s] = alpha(__mesh.shape().a(0), x, y);
		  break;
		}
		case 1: {
		  alpha_rs[r][s] = alpha(__mesh.shape().b(0), x, y);
		  break;
		}
		case 2: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().a(1), x);
		  break;
		}
		case 3: {
		  alpha_rs[r][s] = alpha(y, __mesh.shape().b(1), x);
		  break;
		}
		case 4: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().a(2));
		  break;
		}
		case 5: {
		  alpha_rs[r][s] = alpha(x, y, __mesh.shape().b(2));
		  break;
		}
		}
	      }
	    }
	    Vector<Vector<real_t> >  v_rp(__gaussLobatto[direction[0]]->numberOfPoints());
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      v_rp[r].resize(__basis[direction[1]]->dimension());
	      for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		v_rp[r][p]=0;
	      }
	    }
	    
	    for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
	      for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		real_t& v_rp_value = v_rp[r][p];
		for(size_t s=0; s<__gaussLobatto[direction[1]]->numberOfPoints(); ++s){
		  v_rp_value
		    += quadratureBaseValues[1][s][p]*u_rs[r][s]*
		    __gaussLobatto[direction[1]]->weight(s)* alpha_rs[r][s]*jacobianDet;
		}
	      }
	    }

	    Vector<Vector<real_t> >  v_np(__basis[direction[0]]->dimension());
	    for(size_t n=0; n<__basis[direction[0]]->dimension(); ++n){
	      v_np[n].resize(__basis[direction[1]]->dimension());
	       for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		 v_np[n][p]=0;
	       }
	    }

	    for(size_t n=0; n<__basis[direction[0]]->dimension(); ++n){
	      for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		real_t& v_np_value = v_np[n][p];
		for(size_t r=0; r<__gaussLobatto[direction[0]]->numberOfPoints(); ++r){
		  v_np_value
		    += quadratureBaseValues[0][r][n]*v_rp[r][p]*
		    __gaussLobatto[direction[0]]->weight(r);
		}
	      }
	    }
	    
	    TinyVector<3,size_t> I;	    
	    for(size_t m=0; m<__basis[direction[2]]->dimension(); ++m){
	      I[direction[2]]=m;
	      for(size_t n=0; n<__basis[direction[0]]->dimension(); ++n){
		I[direction[0]]=n;
		for(size_t p=0; p<__basis[direction[1]]->dimension(); ++p){
		  I[direction[1]]=p;
		  const size_t dofNumberV =  __mesh.dofNumber(I[0],I[1],I[2]);
		  v[__degreeOfFreedomSet(operatorAlphaUV.unknownNumber(),dofNumberV)]
		    += quadratureBaseValues[2][boundaryNumber%2][m]*v_np[n][p];
		}
	      }
	    }
	  
	  }
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "not implemented",
			     ErrorHandler::unexpected);	
	}
	}
	break;
      }
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "not implemented",
			   ErrorHandler::unexpected);	
      }
      }
    }
    break;
  }
  case Problem::pdeProblem: {
    break; 
 }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented",
		       ErrorHandler::unexpected);
  }
  }
}






