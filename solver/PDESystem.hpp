//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDESystem.hpp,v 1.5 2007/05/20 23:02:47 delpinux Exp $

#ifndef PDE_SYSTEM_HPP
#define PDE_SYSTEM_HPP

#include <vector>

#include <PDEProblem.hpp>
#include <ReferenceCounting.hpp>

#include <Problem.hpp>

/**
 * @file   PDESystem.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:09:14 2006
 * 
 * @brief  describes PDE System
 * 
 * @note the implementation can be confusing. It is in fact a
 * collection of coupled PDE problems.
 */
class PDESystem
  : public Problem
{
private:
  std::vector<ConstReferenceCounting<PDEProblem> >
  __pdeProblems;		/**< list of PDE Problems */

public:
  /** 
   * Adds a PDEProblem to the system
   * 
   * @param pdeProblem PDEProblem to add
   */
  void add(ReferenceCounting<PDEProblem> pdeProblem)
  {
    __pdeProblems.push_back(pdeProblem);
  }

  /** 
   * read-only access to the ith equation of the problem
   * 
   * @param i number of the equation
   * 
   * @return __pdeProblems[i]
   */
  const PDEProblem&
  operator[](const size_t& i) const
  {
    ASSERT(i<__pdeProblems.size());
    return *(__pdeProblems[i]);
  }

  /** 
   * Read-only access to the ith component of the second member
   * 
   * @param i number of the component
   * 
   * @return the second member ith PDE
   */
  const ScalarFunctionBase&
  secondMember(const size_t& i) const
  {
    return *__pdeProblems[i]->pde().secondMember();
  }

  /** 
   * Read-only access to the ith boundary condition set
   * 
   * @param i the number of the unknown 
   * 
   * @return the ith PDEProblem's boundary condition set
   */
  const BoundaryConditionSet&
  boundaryConditionSet(const size_t& i) const
  {
    return __pdeProblems[i]->boundaryConditionSet();
  }

  /** 
   * "multiplies" a PDESystem by a coefficient
   * 
   * @param c the coefficient
   * 
   * @return the new PDESystem
   */
  ReferenceCounting<Problem>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    PDESystem* newPDESystem = new PDESystem(*this);

    for (size_t i=0; i<this->numberOfEquations(); ++i) {
      (*newPDESystem).__pdeProblems[i] = (*__pdeProblems[i]) * c;
    }
    return newPDESystem;
  }

  /** 
   * Read-only access to the number of unknown
   * 
   * @return number of unknown
   */
  size_t numberOfUnknown() const
  {
    return __pdeProblems.size();
  }

  /** 
   * Read-only access to the number of equations
   * 
   * @return the number of equations
   */
  size_t numberOfEquations() const
  {
    return __pdeProblems.size();
  }

  /** 
   * Writes a PDESystem
   * 
   * @param os output stream
   * @param pdeSystem a PDE system
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os, const PDESystem& pdeSystem)
  {
    for(size_t i=0; i<pdeSystem.numberOfEquations(); ++i) {
      os << *(pdeSystem.__pdeProblems[i]) << '\n';
    }
    return os;
  }

  /** 
   * Constructor
   * 
   */
  PDESystem ()
    : Problem(Problem::pdeProblem, 0)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param dimension dimension of the system
   */
  PDESystem (const size_t& dimension)
    : Problem(Problem::pdeProblem, 0)
  {
    __pdeProblems.reserve(dimension);
  }

  /** 
   * Copy constructor
   * 
   * @param p a given problem
   */
  PDESystem(const PDESystem& p)
    : Problem(p),
      __pdeProblems(p.__pdeProblems)
  {
  }

  /** 
   * Destructor
   * 
   */
  ~PDESystem ()
  {
    ;
  }
};

#endif // PDE_SYSTEM_HPP
