//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: WriterVTK.cpp,v 1.6 2007/07/03 19:37:59 delpinux Exp $

#include <WriterVTK.hpp>
#include <Mesh.hpp>

#include <FieldOfScalarFunction.hpp>

#include <ScalarFunctionBase.hpp>

#include <FEMFunctionBase.hpp>

#include <Mesh.hpp>

#include <Structured3DMesh.hpp>

#include <MeshOfTetrahedra.hpp>
#include <MeshOfHexahedra.hpp>

#include <SpectralMesh.hpp>

#include <SurfaceMeshOfTriangles.hpp>
#include <SurfaceMeshOfQuadrangles.hpp>

#include <XMLWriter.hpp>

#include <fstream>

template <typename CellType>
struct WriterVTK::Traits {};

template <>
struct WriterVTK::Traits<Triangle>
{
  enum {
    VTKType = 5
  };
};

template <>
struct WriterVTK::Traits<Quadrangle>
{
  enum {
    VTKType = 9
  };
};

template <>
struct WriterVTK::Traits<Tetrahedron>
{
  enum {
    VTKType = 10
  };
};

template <>
struct WriterVTK::Traits<Hexahedron>
{
  enum {
    VTKType = 12
  };
};

template <>
struct WriterVTK::Traits<CartesianHexahedron>
{
  enum {
    VTKType = 12
  };
};

void WriterVTK::
__fillCrossedComponent(const FieldOfScalarFunction& field,
		       Vector<real_t>& values) const
{
  const size_t numberOfComponents = field.numberOfComponents();
  ASSERT(values.size() == __mesh->numberOfVertices()*numberOfComponents);

  switch (__mesh->type()) {
  case Mesh::cartesianHexahedraMesh: {
    const Structured3DMesh& mesh = dynamic_cast<const Structured3DMesh&>(*__mesh);
    for (size_t l = 0; l<numberOfComponents; ++l) {
      const ScalarFunctionBase& function = *field.function(l);

      switch (function.type()) {
      case ScalarFunctionBase::femfunction: {
	const FEMFunctionBase& fem
	  = static_cast<const FEMFunctionBase&>(function);

	if ((fem.discretizationType() == DiscretizationType::lagrangianFEM1)
	    and (fem.baseMesh() == __mesh)) {
	  size_t position = 0;
	  for (size_t k=0; k<mesh.shape().nz(); ++k)
	    for (size_t j=0; j<mesh.shape().ny(); ++j)
	      for (size_t i=0; i<mesh.shape().nx(); ++i) {
		const size_t pointNumber = mesh.shape()(i,j,k);
		values[position*numberOfComponents+l] = fem[pointNumber];
		position++;
	      }
	  break;
	}
      }
      default: {
	size_t position = 0;
	for (size_t k=0; k<mesh.shape().nz(); ++k)
	  for (size_t j=0; j<mesh.shape().ny(); ++j)
	    for (size_t i=0; i<mesh.shape().nx(); ++i) {
	      const Vertex& x = mesh.vertex(i,j,k);
	      values[position*numberOfComponents+l] = function(x);
	      position++;
	    }
      }
      }
    }
    break;
  }
  default: {
    for (size_t i = 0; i<numberOfComponents; ++i) {
      const ScalarFunctionBase& function = *field.function(i);

      switch (function.type()) {
      case ScalarFunctionBase::femfunction: {
	const FEMFunctionBase& fem
	  = static_cast<const FEMFunctionBase&>(function);
	if ((fem.baseMesh() == __mesh) and
	    ((fem.discretizationType() == DiscretizationType::lagrangianFEM1))) {
	  for (size_t j=0; j<fem.values().size(); ++j) {
	    values[numberOfComponents*j+i] = fem[j];
	  }
	  break;
	} // if not continues the standard method
      }
      default: {
	for (size_t j=0; j<__mesh->numberOfVertices(); ++j) {
	  const TinyVector<3,real_t>& x = __mesh->vertex(j);
	  values[numberOfComponents*j+i] = function(x);
	}
      }
      }
    }
  }
  }
}

template <typename MeshType>
void WriterVTK::
__proceed() const
{
  typedef typename MeshType::CellType CellType;

  std::string filename = __fileName;
  filename += ".vtu";

  const MeshType& mesh
    = static_cast<const MeshType&>(*__mesh);

  std::ofstream file(filename.c_str());

  XMLWriter xmlWriter(file);

  xmlWriter.writeHeader();
  {
    XMLTag vtkFile("VTKFile");
    ReferenceCounting<XMLAttribute> attribute
      = new XMLAttribute("type","UnstructuredGrid");
    vtkFile.add(attribute);
    xmlWriter.add(vtkFile);
  }

  {
    XMLTag unstructuredGrid("UnstructuredGrid");
    xmlWriter.add(unstructuredGrid);
  }

  {
    XMLTag piece("Piece");
    ReferenceCounting<XMLAttribute>
      nbPoints = new XMLAttribute("NumberOfPoints",
				  stringify(mesh.numberOfVertices()));
    piece.add(nbPoints);
    ReferenceCounting<XMLAttribute>
      nbCells = new XMLAttribute("NumberOfCells",
				 stringify(mesh.numberOfCells()));
    piece.add(nbCells);
    xmlWriter.add(piece);

    {
      XMLTag points("Points");
      xmlWriter.add(points);
   
      {  
	XMLTag dataarray("DataArray");
	ReferenceCounting<XMLAttribute>
	  type = new XMLAttribute("type","Float64");
	dataarray.add(type);
	ReferenceCounting<XMLAttribute>
	  name = new XMLAttribute("Name","Position");
	dataarray.add(name);
	ReferenceCounting<XMLAttribute>
	  nbComponent = new XMLAttribute("NumberOfComponents","3");
	dataarray.add(nbComponent);
	ReferenceCounting<XMLAttribute>
	  format = new XMLAttribute("format","ascii");
	dataarray.add(format);
    
	xmlWriter.add(dataarray);

	for (size_t i=0; i<mesh.numberOfVertices(); ++i) {
	  const Vertex& x = mesh.vertex(i); 
	  xmlWriter.insert(x[0]);
	  xmlWriter.insert(x[1]);
	  xmlWriter.insert(x[2]);
	}
	xmlWriter.insertNewLine();

	xmlWriter.closeTag();
      }
      xmlWriter.closeTag();
    }

    {
      XMLTag cells("Cells");
      xmlWriter.add(cells);

      {
	XMLTag dataarray("DataArray");
	ReferenceCounting<XMLAttribute>
	  type = new XMLAttribute("type","Int32");
	dataarray.add(type);
	ReferenceCounting<XMLAttribute>
	  name = new XMLAttribute("Name","connectivity");
	dataarray.add(name);
	ReferenceCounting<XMLAttribute>
	  nbComponent = new XMLAttribute("NumberOfComponents","1");
	dataarray.add(nbComponent);
	ReferenceCounting<XMLAttribute>
	  format = new XMLAttribute("format","ascii");
	dataarray.add(format);
	xmlWriter.add(dataarray);

	for (size_t i=0;i<mesh.numberOfCells(); ++i) {
	  const CellType& cell = mesh.cell(i);
	  for (size_t j=0; j<CellType::NumberOfVertices; ++j) {
	    xmlWriter.insert(mesh.vertexNumber(cell(j)));
	  }
	}
	xmlWriter.insertNewLine();

	xmlWriter.closeTag();      
      }

      {
	XMLTag dataarray("DataArray");
	ReferenceCounting<XMLAttribute>
	  type = new XMLAttribute("type","Int32");
	dataarray.add(type);
	ReferenceCounting<XMLAttribute>
	  name = new XMLAttribute("Name","offsets");
	dataarray.add(name);
	ReferenceCounting<XMLAttribute>
	  nbComponent = new XMLAttribute("NumberOfComponents","1");
	dataarray.add(nbComponent);
	ReferenceCounting<XMLAttribute>
	  format = new XMLAttribute("format","ascii");
	dataarray.add(format);
	xmlWriter.add(dataarray);
     
	for (size_t i=1; i<=mesh.numberOfCells(); ++i) {
	  xmlWriter.insert(i*CellType::NumberOfVertices);
	}
	xmlWriter.insertNewLine();

	xmlWriter.closeTag();      
      }

      {
	XMLTag dataarray("DataArray");
	ReferenceCounting<XMLAttribute>
	  type = new XMLAttribute("type","UInt8");
	dataarray.add(type);
	ReferenceCounting<XMLAttribute>
	  name = new XMLAttribute("Name","types");
	dataarray.add(name);
	ReferenceCounting<XMLAttribute>
	  nbComponent = new XMLAttribute("NumberOfComponents","1");
	dataarray.add(nbComponent);
	ReferenceCounting<XMLAttribute>
	  format = new XMLAttribute("format","ascii");
	dataarray.add(format);
	xmlWriter.add(dataarray);
     
	for (size_t i=0; i<mesh.numberOfCells(); ++i) {
	  xmlWriter.insert(Traits<CellType>::VTKType);
	}
	xmlWriter.insertNewLine();

	xmlWriter.closeTag();      
      }
    }
    xmlWriter.closeTag();
  }
  {
    XMLTag pointdata("PointData");
    xmlWriter.add(pointdata);

    for (size_t i=0; i<__scalarFunctionList.size(); ++i) {

      const ScalarFunctionBase& function = *__scalarFunctionList[i];

      XMLTag dataarray("DataArray");
      ReferenceCounting<XMLAttribute>
	type = new XMLAttribute("type","Float64");
      dataarray.add(type);
      ReferenceCounting<XMLAttribute>
	name = new XMLAttribute("Name",stringify(function));
      dataarray.add(name);
      ReferenceCounting<XMLAttribute>
	nbComponent = new XMLAttribute("NumberOfComponents","1");
      dataarray.add(nbComponent);
      ReferenceCounting<XMLAttribute>
	format = new XMLAttribute("format","ascii");
      dataarray.add(format);
    
      xmlWriter.add(dataarray);

      switch (function.type()) {
      case ScalarFunctionBase::femfunction: {
	const FEMFunctionBase& fem
	  = static_cast<const FEMFunctionBase&>(function);

	if ((fem.discretizationType() == DiscretizationType::lagrangianFEM1)
	    and (fem.baseMesh() == __mesh)) {
	  for (size_t i=0; i<mesh.numberOfVertices(); ++i) {
	    xmlWriter.insert(fem[i]);
	  }
	  break;
	}
      }
      default: {
	for (size_t i=0; i<mesh.numberOfVertices(); ++i) {
	  const Vertex& x = mesh.vertex(i);
	  xmlWriter.insert(function(x));
	}
      }
      }

      xmlWriter.insertNewLine();

      xmlWriter.closeTag();
    }

    for (size_t i=0; i<__fieldList.size(); ++i) {

      const FieldOfScalarFunction& field = *__fieldList[i];

      XMLTag dataArray("DataArray");
      ReferenceCounting<XMLAttribute> type
	= new XMLAttribute("type","Float64");
      dataArray.add(type);
      ReferenceCounting<XMLAttribute> name
	= new XMLAttribute("Name",stringify(field));
      dataArray.add(name);
      ReferenceCounting<XMLAttribute>
	nbComponent = new XMLAttribute("NumberOfComponents",
				       stringify(field.numberOfComponents()));
      dataArray.add(nbComponent);
      ReferenceCounting<XMLAttribute> format
	= new XMLAttribute("format","ascii");
      dataArray.add(format);
      xmlWriter.add(dataArray);

      Vector<real_t> values(field.numberOfComponents()*__mesh->numberOfVertices());

      this->__fillCrossedComponent(field,values);

      for (size_t i=0; i<values.size(); ++i) {
	xmlWriter.insert(values[i]);
      }

      xmlWriter.insertNewLine();

      xmlWriter.closeTag();
    }

    xmlWriter.closeTag();
  }
}

template <>
void WriterVTK::
__proceed<Structured3DMesh>() const
{
  std::string filename = __fileName;
  filename += ".vti";

  const Structured3DMesh& mesh
    = static_cast<const Structured3DMesh&>(*__mesh);

  std::ofstream file(filename.c_str());

  XMLWriter xmlWriter(file);

  xmlWriter.writeHeader();
  {
    XMLTag vtkFile("VTKFile");
    ReferenceCounting<XMLAttribute> attribute
      = new XMLAttribute("type","ImageData");
    vtkFile.add(attribute);
    xmlWriter.add(vtkFile);
  }

  {
    XMLTag rectilinearGrid("ImageData");
    ReferenceCounting<XMLAttribute> wholeExtent
      = new XMLAttribute("WholeExtent",
			 "0 "+stringify(mesh.shape().nx()-1)+
			 " 0 "+stringify(mesh.shape().ny()-1)+
			 " 0 "+stringify(mesh.shape().nz()-1));
    rectilinearGrid.add(wholeExtent);
    ReferenceCounting<XMLAttribute> origin
      = new XMLAttribute("Origin",
			 stringify(mesh.shape().a(0))+" "+
			 stringify(mesh.shape().a(1))+" "+
			 stringify(mesh.shape().a(2)));
    rectilinearGrid.add(origin);
    ReferenceCounting<XMLAttribute> spacing
      = new XMLAttribute("Spacing",
			 stringify(mesh.shape().hx())+" "+
			 stringify(mesh.shape().hy())+" "+
			 stringify(mesh.shape().hz()));
    rectilinearGrid.add(spacing);
    xmlWriter.add(rectilinearGrid);
  }

  {
    XMLTag piece("Piece");
    ReferenceCounting<XMLAttribute> extent
      = new XMLAttribute("Extent",
			 "0 "+stringify(mesh.shape().nx()-1)
			 +" 0 "+stringify(mesh.shape().ny()-1)
			 +" 0 "+stringify(mesh.shape().nz()-1));
    piece.add(extent);
    xmlWriter.add(piece);
  }

  {
    XMLTag pointData("PointData");
    XMLAttribute scalars("Scalars",__fileName);
    xmlWriter.add(pointData);

    for (size_t i=0; i<__scalarFunctionList.size(); ++i) {

      const ScalarFunctionBase& function = *__scalarFunctionList[i];

      XMLTag dataArray("DataArray");
      ReferenceCounting<XMLAttribute> type
	= new XMLAttribute("type","Float64");
      dataArray.add(type);
      ReferenceCounting<XMLAttribute> name
	= new XMLAttribute("Name",stringify(function));
      dataArray.add(name);
      ReferenceCounting<XMLAttribute> format
	= new XMLAttribute("format","ascii");
      dataArray.add(format);
      xmlWriter.add(dataArray);

      switch (function.type()) {
      case ScalarFunctionBase::femfunction: {
	const FEMFunctionBase& fem
	  = static_cast<const FEMFunctionBase&>(function);

	if ((fem.discretizationType() == DiscretizationType::lagrangianFEM1)
	    and (fem.baseMesh() == __mesh)) {
	  for (size_t k=0; k<mesh.shape().nz(); ++k)
	    for (size_t j=0; j<mesh.shape().ny(); ++j)
	      for (size_t i=0; i<mesh.shape().nx(); ++i) {
		const size_t pointNumber = mesh.shape()(i,j,k);
		xmlWriter.insert(fem[pointNumber]);
	      }
	  break;
	}
      }
      default: {
	for (size_t k=0; k<mesh.shape().nz(); ++k)
	  for (size_t j=0; j<mesh.shape().ny(); ++j)
	    for (size_t i=0; i<mesh.shape().nx(); ++i) {
	      const Vertex& x = mesh.vertex(i,j,k);
	      xmlWriter.insert(function(x));
	    }
      }
      }

      xmlWriter.insertNewLine();

      xmlWriter.closeTag();
    }

    for (size_t i=0; i<__fieldList.size(); ++i) {

      const FieldOfScalarFunction& field = *__fieldList[i];

      XMLTag dataArray("DataArray");
      ReferenceCounting<XMLAttribute> type
	= new XMLAttribute("type","Float64");
      dataArray.add(type);
      ReferenceCounting<XMLAttribute> name
	= new XMLAttribute("Name",stringify(field));
      dataArray.add(name);
      ReferenceCounting<XMLAttribute>
	nbComponent = new XMLAttribute("NumberOfComponents",
				       stringify(field.numberOfComponents()));
      dataArray.add(nbComponent);
      ReferenceCounting<XMLAttribute> format
	= new XMLAttribute("format","ascii");
      dataArray.add(format);
      xmlWriter.add(dataArray);

      Vector<real_t> values(field.numberOfComponents()*__mesh->numberOfVertices());

      this->__fillCrossedComponent(field,values);

      for (size_t i=0; i<values.size(); ++i) {
	xmlWriter.insert(values[i]);
      }

      xmlWriter.insertNewLine();

      xmlWriter.closeTag();
    }

    xmlWriter.closeTag();
  }
}

void WriterVTK::
proceed() const
{
  switch (__mesh->type()) {
  case Mesh::cartesianHexahedraMesh: {
    return this->__proceed<Structured3DMesh>();
  }
  case Mesh::hexahedraMesh: {
    return this->__proceed<MeshOfHexahedra>();
  }
  case Mesh::tetrahedraMesh: {
    return this->__proceed<MeshOfTetrahedra>();
  }
  case Mesh::spectralMesh: {
    return this->__proceed<SpectralMesh>();
  }
  case Mesh::surfaceMeshTriangles: {
    return this->__proceed<SurfaceMeshOfTriangles>();
  }
  case Mesh::surfaceMeshQuadrangles: {
    return this->__proceed<SurfaceMeshOfQuadrangles>();
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot save this mesh type to VTK format",
		       ErrorHandler::normal);
  }
  }
}

WriterVTK::
WriterVTK(ConstReferenceCounting<Mesh> mesh,
	  const std::string& fileName,
	  const std::string& CR)
  : WriterBase(mesh,fileName,CR)
{
  ;
}

WriterVTK::
~WriterVTK()
{
  ;
}
