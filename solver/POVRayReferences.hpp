//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: POVRayReferences.hpp,v 1.3 2003/08/27 16:48:45 delpinux Exp $

#ifndef POVRAY_REFERENCES_HPP
#define POVRAY_REFERENCES_HPP

/**
 * @file   POVRayReferences.hpp
 * @author Stephane Del Pino
 * @date   Wed Feb  5 17:42:09 2003
 * 
 * @brief  This class is used to manage POVRay references lists
 * 
 */

#include <map>
#include <TinyVector.hpp>

class POVRayReferences
{
public:
  typedef std::map<TinyVector<3,real_t>,size_t> Builtins;

private:
  POVRayReferences::Builtins __povreferences;

public:
  typedef Builtins::iterator iterator;

  typedef Builtins::const_iterator const_iterator;

  inline iterator begin()
  {
    return __povreferences.begin();
  }

  inline iterator end()
  {
    return __povreferences.end();
  }

  inline const_iterator begin() const
  {
    return __povreferences.begin();
  }

  inline const_iterator end() const
  {
    return __povreferences.end();
  }

  inline Builtins::mapped_type&
  operator[](const Builtins::key_type& k)
  {
    return __povreferences[k];
  }

  POVRayReferences()
  {
    ;
  }

  ~POVRayReferences()
  {
    ;
  }
};
 
#endif // POVRAY_REFERENCES_HPP
