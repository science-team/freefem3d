//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Dirichlet.hpp,v 1.2 2006/07/20 19:08:54 delpinux Exp $

// this class allows to define Dirichlet Boundary Conditions

#ifndef DIRICHLET_HPP
#define DIRICHLET_HPP

#include <PDECondition.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   Dirichlet.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 15:16:37 2006
 * 
 * @brief  Dirichlet condition
 * 
 */
class Dirichlet
  : public PDECondition
{
private:
  //! The function to impose as a Dirichlet Boundary Condition.
  ConstReferenceCounting<ScalarFunctionBase> __g;

public:
  /** 
   * evaluates __g at the position @a x
   * 
   * @param x evaluation point
   * 
   * @return \f$ g(x) \f$
   */
  real_t g(const TinyVector<3>& X) const
  {
    return (*__g)(X);
  }

  /** 
   * returns the name of the condition
   * 
   * @return "Dirichlet"
   */
  std::string typeName() const
  {
    return "Dirichlet";
  }

  /** 
   * Constructor
   * 
   * @param g the dirichlet given function
   * @param unknownNumber the unknown function number
   */
  Dirichlet(ConstReferenceCounting<ScalarFunctionBase> g,
	    const size_t& unknownNumber)
    : PDECondition(PDECondition::dirichlet,
		   unknownNumber),
      __g(g)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d a given dirichlet condition
   * 
   * @return 
   */
  Dirichlet(const Dirichlet& d)
    : PDECondition(d),
      __g(d.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Dirichlet()
  {
    ;
  }
};

#endif // DIRICHLET_HPP
