//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FileDescriptor.hpp,v 1.6 2007/06/09 10:37:07 delpinux Exp $

#ifndef FILE_DESCRIPTOR_HPP
#define FILE_DESCRIPTOR_HPP

#include <ErrorHandler.hpp>
#include <StreamCenter.hpp>

#include <cstring>

/**
 * @file   FileDescriptor.hpp
 * @author Stephane Del Pino
 * @date   Fri Dec 31 11:04:10 2004
 * 
 * @brief  defines file descriptor
 */
class FileDescriptor
{
public:
  enum FormatType {
    am_fmt,
    gmsh,
    mesh,
    medit,
    raw,
    vtk
  };

  enum FileType {
    Unix,
    Dos,
    Mac,
    Binary
  };

  /** 
   * Gets the format type according to its name
   * 
   * @param c name of the format
   * 
   * @return the type of the format
   */
  static FileDescriptor::FormatType
  formatType(const char* c)
  {
    if (std::strcmp(c,"gmsh") == 0) {
      return FileDescriptor::gmsh;
    }
    if (std::strcmp(c,"mesh") == 0) {
      return FileDescriptor::mesh;
    }
    if (std::strcmp(c,"medit") == 0) {
      return FileDescriptor::medit;
    }
    if (std::strcmp(c,"opendx") == 0) {
      fferr(1) << "warning: \"opendx\" is deprecated, use \"raw\" keyword instead\n";
      return FileDescriptor::raw;
    }
    if (std::strcmp(c,"raw") == 0) {
      return FileDescriptor::raw;
    }
    if (std::strcmp(c,"vtk") == 0) {
      return FileDescriptor::vtk;
    }
    if (std::strcmp(c,"am_fmt") == 0) {
      return FileDescriptor::am_fmt;
    }

    throw ErrorHandler(__FILE__,__LINE__,
		       "Unknown File Type: '"+std::string(c)+"'",
		       ErrorHandler::unexpected);
    return FileDescriptor::raw;
  }

  /** 
   * Gets the file type according to its name
   * 
   * @param c file type name
   * 
   * @return the file type
   */
  static FileDescriptor::FileType
  fileType(const char* c)
  {
    if (std::strcmp(c,"unix") == 0) {
      return FileDescriptor::Unix;
    }
    if (std::strcmp(c,"dos") == 0) {
      return FileDescriptor::Dos;
    }
    if (std::strcmp(c,"mac") == 0) {
      return FileDescriptor::Mac;
    }
    if (std::strcmp(c,"binary") == 0) {
      return FileDescriptor::Binary;
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown file type",
		       ErrorHandler::unexpected);
    return FileDescriptor::Unix;
  }

private:
  const FileDescriptor::FormatType
  __formatType;			/**< Format type */
  const FileDescriptor::FileType
  __fileType;			/**< File type */

public:
  /** 
   * Read-only access to the format
   * 
   * @return __formatType
   */
  const FileDescriptor::FormatType&
  format() const
  {
    return __formatType;
  }

  /** 
   * Read-only access to the file type
   * 
   * @return __fileType
   */
  const FileDescriptor::FileType&
  type() const
  {
    return __fileType;
  }

  /** 
   * Access to the valid cardriage return string
   * 
   * @return CR
   */
  const std::string
  cr() const
  {
    std::string CR;

    switch (__fileType) {
    case (FileDescriptor::Unix): {
      CR = '\n';
      break;
    }
    case (FileDescriptor::Dos): {
#ifndef __MINGW32__
      CR = "\r\n";
#else // __MINGW32__
      CR = "\n";
#endif // __MINGW32__
      break;
    }
    case (FileDescriptor::Mac): {
      CR = '\r';
      break;
    }
    case (FileDescriptor::Binary): {
      CR = '\r';
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unknown file type",
			 ErrorHandler::unexpected);
    }
    }
    return CR;
  }
  /** 
   * Converts format type to a string
   * 
   * @return the format name
   */
  std::string toString() const 
  {
    switch (__formatType) {
    case am_fmt:
      return "am_fmt";
    case gmsh:
      return "gmsh";
    case mesh:
      return "mesh";
    case medit:
      return "medit";
    case raw:
      return "raw";
    case vtk:
      return "vtk";
    default:
      ASSERT(false); // never occurs except if a keyword has been forgot
      return "error";
    }
  }

  /** 
   * Constructor
   * 
   * @param format given file format
   * @param type given file type
   */
  FileDescriptor(const FileDescriptor::FormatType& format,
		 const FileDescriptor::FileType& type)
    : __formatType(format),
      __fileType(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param f given file descriptor
   */
  FileDescriptor(const FileDescriptor& f)
    : __formatType(f.__formatType),
      __fileType(f.__fileType)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~FileDescriptor()
  {
    ;
  }
};

#endif // FILE_DESCRIPTOR_HPP
