//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: P0Triangle3DFiniteElement.cpp,v 1.1 2006/11/11 17:37:40 delpinux Exp $

#include <P0Triangle3DFiniteElement.hpp>

TinyVector<3, real_t> P0Triangle3DFiniteElement::__massCenter(1./3., 1./3., 0);

real_t
P0Triangle3DFiniteElement::W(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 1;
}

real_t
P0Triangle3DFiniteElement::dxW(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 0;
}

real_t
P0Triangle3DFiniteElement::dyW(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 0;
}

real_t 
P0Triangle3DFiniteElement::dzW(const size_t& i, const TinyVector<3>& X) const
{
  if (i>0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
  }

  return 0;
}
