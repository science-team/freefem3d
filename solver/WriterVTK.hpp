//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: WriterVTK.hpp,v 1.1 2007/02/26 01:09:27 delpinux Exp $

#ifndef WRITER_VTK_HPP
#define WRITER_VTK_HPP

#include <WriterBase.hpp>

/**
 * @file   WriterVTK.hpp
 * @author St�phane Del Pino
 * @date   Fri Feb 23 16:26:22 2007
 * 
 * @brief  Writer for VTK format
 */
class WriterVTK
  : public WriterBase
{
private:
  /** 
   * Type conversion for VTK
   * 
   */
  template <typename CellType>
  struct Traits;

  /** 
   * Save data on a specific mesh type
   * 
   */
  template <typename MeshType>
  void __proceed() const;

  /** 
   * Copy constructor is forbidden
   * 
   */
  WriterVTK(const WriterVTK&);

  /** 
   * Fills crossed component
   * 
   * @param field the field that will have its component crossed
   * @param values values vector
   */
  void __fillCrossedComponent(const FieldOfScalarFunction& field,
			      Vector<real_t>& values) const;

public:
  /** 
   * Save the data to the file
   * 
   */
  void proceed() const;

  /** 
   * Constructor
   * 
   * @param mesh given mesh
   * @param fileName basename of the VTK file
   * @param CR cardriage return type (OS Specific)
   */
  WriterVTK(ConstReferenceCounting<Mesh> mesh,
	    const std::string& fileName,
	    const std::string& CR);

  /** 
   * Destructor
   * 
   */
  ~WriterVTK();
};

#endif // WRITER_VTK_HPP
