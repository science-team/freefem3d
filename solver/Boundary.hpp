//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Boundary.hpp,v 1.4 2004/08/28 08:28:03 delpinux Exp $

#ifndef BOUNDARY_HPP
#define BOUNDARY_HPP

#include <StreamCenter.hpp>

/**
 * @file   Boundary.hpp
 * @author Stephane Del Pino
 * @date   Fri Aug 27 23:50:24 2004
 * 
 * @brief  This describes boundaries.
 * 
 */
class Boundary
{
public:
  enum Type {
    povRay,
    structured3DMesh,
    surfaceMesh,
    references,
    list
  };
private:
  //! The boundary type
  const Boundary::Type __type;

  //! ostream overload.
  virtual void put(std::ostream& os) const = 0;

public:
  //! Returns the type of the boundary.
  const Boundary::Type& type() const
  {
    return __type;
  }

  //! Writes the boundary conditions.
  friend std::ostream& operator << (std::ostream& os,
				    const Boundary& B)
  {
    B.put(os);
    return os;
  }

  //! copy constructor.
  Boundary(const Boundary& B)
    : __type(B.__type)
  {
    ;
  }

  //! constructs the boundary.
  Boundary(const Boundary::Type aType)
    : __type(aType)
  {
    ;
  }

  virtual ~Boundary()
  {
    ;
  }
};

#endif // BOUNDARY_HPP
