//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SpectralMethod.hpp,v 1.1 2007/05/20 23:30:08 delpinux Exp $

#ifndef SPECTRAL_METHOD_HPP
#define SPECTRAL_METHOD_HPP

#include <DegreeOfFreedomSet.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <Method.hpp>

#include <Problem.hpp>
#include <Solution.hpp>

#include <Mesh.hpp>

/**
 * @file   SpectralFictitious.hpp
 * @author St�phane Del Pino
 * @date   Sun May 20 23:34:02 2007
 * 
 * @brief  this implementes the spectral method
 */
class SpectralMethod
  : public Method
{
private:
  ReferenceCounting<BaseMatrix> __A;
  ReferenceCounting<BaseVector> __b;

  ReferenceCounting<Mesh> __mesh;

  ConstReferenceCounting<Problem> __problem;

  const DegreeOfFreedomSet& __degreeOfFreedomSet;

  template <typename MeshType,
	    DiscretizationType::Type TypeOfDiscretization>
  void __discretizeOnMesh();

  template <DiscretizationType::Type TypeOfDiscretization>
  void __discretize();

public:

  void Discretize(ConstReferenceCounting<Problem> Pb);

  void Compute(Solution& u);

  const Problem& problem() const
  {
    return *__problem;
  }

  Mesh& mesh()
  {
    return *__mesh;
  }

  SpectralMethod(const DiscretizationType& discretizationType,
		 ReferenceCounting<Mesh> M,
		 const DegreeOfFreedomSet& dOfFreedom)
    : Method(discretizationType),
      __mesh(M),
      __degreeOfFreedomSet(dOfFreedom)
  {
    ;
  }

  ~SpectralMethod()
  {
    ;
  }
};

#endif // SPECTRAL_METHOD_HPP
