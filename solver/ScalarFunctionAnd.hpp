//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionAnd.hpp,v 1.3 2006/10/28 12:26:54 delpinux Exp $

#ifndef SCALAR_FUNCTION_AND_HPP
#define SCALAR_FUNCTION_AND_HPP

#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionAnd.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:26:08 2006
 * 
 * @brief  boolean operation "and" between functions
 * 
 */
class ScalarFunctionAnd
  : public ScalarFunctionBase
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< first operand */
  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< second operand */

  /** 
   * Writes the function to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << '(' << *__f << " and " << *__g << ')';
    return os;
  }

public:
  /** 
   * evaluates the function
   * 
   * @return @f$ f(x) \mbox{and} g(x)@f$
   */
  real_t operator()(const TinyVector<3,real_t>& x) const
  {
    return (*__f)(x) and (*__g)(x);
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param f first operand
   * @param g second operand
   */
  ScalarFunctionAnd(ConstReferenceCounting<ScalarFunctionBase> f,
		    ConstReferenceCounting<ScalarFunctionBase> g)
    : ScalarFunctionBase(ScalarFunctionBase::and_),
      __f(f),
      __g(g)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param f given ScalarFunctionAnd
   */
  ScalarFunctionAnd(const ScalarFunctionAnd& f)
    : ScalarFunctionBase(f),
      __f(f.__f),
      __g(f.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionAnd()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_AND_HPP
