//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDEProblem.hpp,v 1.3 2006/07/20 19:08:54 delpinux Exp $

#ifndef PDE_PROBLEM_HPP
#define PDE_PROBLEM_HPP

#include <PDE.hpp>
#include <BoundaryConditionSet.hpp>

#include <StreamCenter.hpp>

/**
 * @file   PDEProblem.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:00:39 2006
 * 
 * @brief  this class defines PDE problems
 * 
 */
class PDEProblem
{
private:
  ConstReferenceCounting<PDE> __pde; /**< a given PDE */

  ReferenceCounting<BoundaryConditionSet>
  __boundaryConditionSet;	/**< a given boundary condition set */

public:
  /** 
   * Read only access to the PDE
   * 
   * @return *__pde
   */
  const PDE& pde() const
  {
    return *__pde;
  }

  /** 
   * Read-only access to the boundary condition set
   * 
   * @return *__boundaryConditionSet
   */
  const BoundaryConditionSet&
  boundaryConditionSet() const
  {
    return *__boundaryConditionSet;
  }

  /** 
   * "multiplies" a PDE problem by a coefficient @a c
   * 
   * @param c the coefficient
   * 
   * @return the new PDEProblem
   */
  ReferenceCounting<PDEProblem>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    PDEProblem* newPDEProblem = new PDEProblem(*this);
    newPDEProblem->__pde = (*__pde) * c;
    return newPDEProblem;
  }

  /** 
   * Writes the PDE problem to a stream
   * 
   * @param os given stream
   * @param P the problem
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os, const PDEProblem& P)
  {
    os << *P.__pde << '\n';
    os << *P.__boundaryConditionSet;
    return os;
  }

  /** 
   * Constructor
   * 
   * @param pde given PDE
   * @param boundaryConditionSet a boundary condition set
   */
  PDEProblem(ReferenceCounting<PDE> pde,
	     ReferenceCounting<BoundaryConditionSet> boundaryConditionSet)
    : __pde(pde),
      __boundaryConditionSet(boundaryConditionSet)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param pdeProblem a given PDE problem
   */
  PDEProblem(const PDEProblem& pdeProblem)
    : __pde(pdeProblem.__pde),
      __boundaryConditionSet(pdeProblem.__boundaryConditionSet)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~PDEProblem()
  {
    ;
  }
};

#endif // PDE_PROBLEM_HPP
