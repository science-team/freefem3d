//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FirstOrderOperator.hpp,v 1.6 2007/02/14 21:53:19 delpinux Exp $


#ifndef FIRST_ORDER_OPERATOR_HPP
#define FIRST_ORDER_OPERATOR_HPP

#include <PDEOperator.hpp>

/**
 * @file   FirstOrderOperator.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 18:41:41 2006
 * 
 * @brief First order operator
 * 
 * This class describes the first order partial differencial
 * operators: @f$ \nu\cdot\nabla @f$ where @f$ \nu @f$ is a dimension
 * 3 function vector.
 * 
 * @par example: if @f$ \nu = (1,1,1) @f$, the operator is
 * @f$\partial_x+\partial_y+\partial_z@f$.
 * 
 */
class FirstOrderOperator
  : public PDEOperator
{
public:
  typedef TinyVector<3,ConstReferenceCounting<ScalarFunctionBase> > Vector;

private:
  ConstReferenceCounting<FirstOrderOperator::Vector>
  __nu;				/**< coefficients vector */

public:
  /** 
   * Gets operator name
   * 
   * @return "FirstOrderOperator"
   */
  std::string
  typeName() const
  {
    return std::string("FirstOrderOperator");
  }

  /** 
   * Checks if component @a i of @f$ \nu @f$ is set
   * 
   * @param i number of the component
   * 
   * @return @a true if @f$ \nu_i @f$ has been set
   */
  bool isSet(const int&i) const
  {
    return ((*__nu)[i] != 0);
  }

  /** 
   * Read-only access to operator coefficient @a i
   * 
   * @param i number of the component
   * 
   * @return @f$ \nu_i @f$ 
   */
  ConstReferenceCounting<ScalarFunctionBase>
  nu(const size_t& i) const
  {
    return (*__nu)[i];
  }

  /** 
   * Read-only access to operator coefficients
   * 
   * @return @f$ \nu @f$ 
   */
  ConstReferenceCounting<FirstOrderOperator::Vector>
  nu() const
  {
    return __nu;
  }

  /** 
   * Returns a pointer on the "multiplied" by u FirstOrderOperator
   * 
   * @param c the multiplicative coefficient
   * 
   * @return @f$ (c\nu)\cdot\nabla @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    ReferenceCounting<FirstOrderOperator::Vector> nu2
      = new FirstOrderOperator::Vector;

    for (size_t i=0; i<3; ++i) {
      if (this->isSet(i)) {
	ScalarFunctionBuilder functionBuilder;
	functionBuilder.setFunction((*__nu)[i]);
	functionBuilder.setBinaryOperation(BinaryOperation::product,c);
	(*nu2)[i] = functionBuilder.getBuiltFunction();
      }
    }

    return new FirstOrderOperator(nu2);
  }

  /** 
   * Returns a pointer on the opposed FirstOrderOperator operator.
   * 
   * @return @f$ -\nu\cdot\nabla @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator-() const
  {
    ReferenceCounting<FirstOrderOperator::Vector> nu2
      = new FirstOrderOperator::Vector;

    for (size_t i=0; i<3; ++i) {
      if (this->isSet(i)) {
	ScalarFunctionBuilder functionBuilder;
	functionBuilder.setFunction((*__nu)[i]);
	functionBuilder.setUnaryMinus();
	(*nu2)[i] = functionBuilder.getBuiltFunction();
      }
    }
    return new FirstOrderOperator(nu2);
  }

  /** 
   * Constructor
   * 
   * @param nu given @f$ nu @f$
   */
  FirstOrderOperator(ReferenceCounting<FirstOrderOperator::Vector> nu)
    : PDEOperator(PDEOperator::firstorderop,
		  3),
      __nu(nu)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param o given first order operator
   */
  FirstOrderOperator(const FirstOrderOperator& o)
    : PDEOperator(o),
      __nu(o.__nu)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~FirstOrderOperator()
  {
    ;
  }
};

#endif // FIRST_ORDER_OPERATOR_HPP
