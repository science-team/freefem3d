//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Tetrahedron.cpp,v 1.6 2005/04/20 22:09:41 delpinux Exp $

#include <Tetrahedron.hpp>

Tetrahedron::Tetrahedron(Vertex& x0,
			 Vertex& x1,
			 Vertex& x2,
			 Vertex& x3,
			 const size_t& ref)
  : Cell(Tetrahedron::NumberOfVertices, ref)
{
  __vertices[0] = &x0;
  __vertices[1] = &x1;
  __vertices[2] = &x2;
  __vertices[3] = &x3;

  TinyVector<3> A = x1 - x0;
  TinyVector<3> B = x2 - x0;
  TinyVector<3> C = x3 - x0;

  __volume = 1./6.*((A^B)*C);
  if (__volume < 0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "negative tetrahedron volume",
		       ErrorHandler::normal);
  }
}

const size_t
Tetrahedron::
faces[Tetrahedron::NumberOfFaces][Tetrahedron::FaceType::NumberOfVertices]
={{1,2,3},{3,2,0},{0,1,3},{2,1,0}};

const size_t
Tetrahedron::
edges[Tetrahedron::NumberOfEdges][Edge::NumberOfVertices]
= {{0,1},{0,2},{0,3},{1,2},{3,1},{2,3}};
