//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DegreeOfFreedomSetBuilder.hpp,v 1.8 2006/11/11 18:07:55 delpinux Exp $

#ifndef DEGREE_OF_FREEDOM_SET_BUILDER_HPP
#define DEGREE_OF_FREEDOM_SET_BUILDER_HPP

#include <Mesh.hpp>
#include <Domain.hpp>

#include <DegreeOfFreedomSet.hpp>
#include <DegreeOfFreedomSetManager.hpp>

#include <DiscretizationType.hpp>
#include <ReferenceCounting.hpp>

/**
 * @file   DegreeOfFreedomSetBuilder.hpp
 * @author Stephane Del Pino
 * @date   Sun Jan 16 23:39:29 2005
 * 
 * @brief Builds degrees of freefom set according to the mesh, the
 * periodicity, the number of variables and eventually the
 * computational domain
 * 
 */

class DegreeOfFreedomSetBuilder
{
private:
  /// the degree of freedom set
  ReferenceCounting<DegreeOfFreedomSet> __degreeOfFreedomSet;

  /// the degree of freedom set
  const DegreeOfFreedomPositionsSet& __dofPositionSet;

  template <size_t ItemDOFNumber>
  class ItemTraits;

  /** 
   * Builds the degrees of freedom correspondance vector according to
   * the mesh type and the discretization for a particular FEM type
   * 
   * @param mesh the non-abstract mesh
   * @param correspondance correspondance table
   * 
   * @return the number of used degrees of freedom
   * 
   * @bug This uses the degrees of vertices periodic correspondance
   * which does not work in every case. This is wrong for instance
   * when there are not enought cells in the mesh (consecutive edges
   * can be considered as one which is wrong). It first occures for
   * Q2 finite element on less than 3*3*3 periodic meshes...
   */
  template <typename MeshType,
	    typename FiniteElementType>
  size_t __buildFEMCorrespondance(const MeshType& mesh,
				  DegreeOfFreedomSet::Correspondance& correspondance);

  /** 
   * Builds the degrees of freedom correspondance vector according to
   * the mesh type and the discretization
   * 
   * @param mesh the non-abstract mesh
   * @param discretization the discretization type
   * @param correspondance correspondance table
   * 
   * @return the number of used degrees of freedom
   */
  template <typename MeshType>
  size_t __buildCorrespondance(const MeshType& mesh,
			       const DiscretizationType& discretization,
			       DegreeOfFreedomSet::Correspondance& correspondance);

  /** 
   * Builds the degrees of freedom set for non abstract mesh classes
   * in the case of fictitious domain methods for a particular FEM
   * 
   * @param numberOfVariables the number of variables
   * @param mesh the non-abstract mesh
   * @param domain the computational domain
   */
  template <typename MeshType,
	    typename FiniteElementType>
  void __buildFEMFictitious(const size_t& numberOfVariables,
			    const MeshType& mesh,
			    const Domain& domain);

  /** 
   * Builds the degrees of freedom set for non abstract mesh classes
   * in the case of fictitious domain methods
   * 
   * @param numberOfVariables the number of variables
   * @param discretization the discretization type
   * @param mesh the non-abstract mesh
   * @param domain the computational domain
   */
  template <typename MeshType>
  void __buildFictitious(const size_t& numberOfVariables,
			 const DiscretizationType& discretization,
			 const MeshType& mesh,
			 const Domain& domain);
public:
  /** 
   * Read only access to the degree of freedom set
   * 
   * @return the degree of freedom set
   */
  const DegreeOfFreedomSet& degreeOfFreedomSet() const
  {
    return *__degreeOfFreedomSet;
  }

  /** 
   * Access to the dof position set
   * 
   * @return __dofPositionSet
   */
  const DegreeOfFreedomPositionsSet& positionsSet()
  {
    return __dofPositionSet;
  }

  /** 
   * Builds the DegreeOfFreedomSet according to the number of
   * variable, the periodicity and the mesh
   * 
   * @param numberOfVariables the number of variables
   * @param discretizationType the type of discretization
   * @param mesh the mesh
   */
  DegreeOfFreedomSetBuilder(const size_t& numberOfVariables,
			    const DiscretizationType& discretizationType,
			    const Mesh& mesh);

  /** 
   * Builds the DegreeOfFreedomSet according to the number of
   * variables, the periodicity, the mesh and the domain. This
   * constructor is called in the case of fictitious domain methods
   * 
   * @param numberOfVariables the number of variables
   * @param discretizationType the type of discretization
   * @param mesh the mesh
   * @param domain the domain
   */
  DegreeOfFreedomSetBuilder(const size_t& numberOfVariables,
			    const DiscretizationType& discretizationType,
			    const Mesh& mesh,
			    const Domain& domain);

  /** 
   * Destructor
   * 
   */
  ~DegreeOfFreedomSetBuilder();
};

#endif // DEGREE_OF_FREEDOM_SET_BUILDER_HPP
