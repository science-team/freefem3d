//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalOperatorFgradGgradV.hpp,v 1.6 2007/06/10 15:03:32 delpinux Exp $

#ifndef VARIATIONAL_OPERATOR_F_GRAD_G_GRAD_V_HPP
#define VARIATIONAL_OPERATOR_F_GRAD_G_GRAD_V_HPP

#include <VariationalLinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorFgradGgradV.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun  2 21:55:49 2002
 * 
 * @brief  @f$ l(v) = \int f\nabla g\cdot \nabla v @f$
 * 
 */
class VariationalOperatorFgradGgradV
  : public VariationalLinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< the @f$ f@f$ function */
  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< the @f$ g@f$ function */

public:
  /** 
   * Access to @f$ f@f$
   * 
   * @return *__f
   */
  const ScalarFunctionBase& f() const
  {
    return *__f;
  }

  /** 
   * Access to @f$ g@f$
   * 
   * @return *__g
   */
  const ScalarFunctionBase& g() const
  {
    return *__g;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int cf\nabla g \cdot\nabla v @f$
   */
  ReferenceCounting<VariationalLinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& u) const
  {
    VariationalOperatorFgradGgradV* newOperator
      = new VariationalOperatorFgradGgradV(*this);
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__f);
    functionBuilder.setBinaryOperation(BinaryOperation::product,u);

    (*newOperator).__f = functionBuilder.getBuiltFunction();
    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param testFunctionNumber test function number
   * @param testFunctionProperty  test function property
   * @param f @f$ f @f$
   * @param g @f$ g @f$
   */
  VariationalOperatorFgradGgradV(const size_t& testFunctionNumber,
				 const VariationalOperator::Property& testFunctionProperty,
				 ConstReferenceCounting<ScalarFunctionBase> f,
				 ConstReferenceCounting<ScalarFunctionBase> g)
    : VariationalLinearOperator(VariationalLinearOperator::FgradGgradV,
				testFunctionNumber, testFunctionProperty),
      __f(f),
      __g(g)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param fv given VariationalOperatorFgradGgradV
   */
  VariationalOperatorFgradGgradV(const VariationalOperatorFgradGgradV& fv)
    : VariationalLinearOperator(fv),
      __f(fv.__f),
      __g(fv.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalOperatorFgradGgradV()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_F_GRAD_G_GRAD_V_HPP
