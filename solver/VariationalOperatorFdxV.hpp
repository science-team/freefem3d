//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalOperatorFdxV.hpp,v 1.2 2007/06/10 15:03:32 delpinux Exp $

#ifndef VARIATIONAL_OPERATOR_F_DX_V_HPP
#define VARIATIONAL_OPERATOR_F_DX_V_HPP

#include <VariationalLinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorFdxV.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 12:35:07 2006
 * 
 * @brief  describes the term @f$ \int f\partial_{x_i}v @f$
 * 
 */
class VariationalOperatorFdxV
  : public VariationalLinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< the @f$ f@f$ function */
  const size_t __i;		/**< The @f$ i @f$ in @f$\partial_{x_i}@f$  */
public:
  /** 
   * Access to @f$ f@f$
   * 
   * @return *__f
   */
  const ScalarFunctionBase& f() const
  {
    return *__f;
  }

  /** 
   * gets the derivation direction of @f$ v @f$
   * 
   * @return __i
   */
  const size_t& number() const
  {
    return __i;
  }

  /** 
   * "multiplies" the operator by a coeficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int cf\partial_{x_i}v @f$
   */
  ReferenceCounting<VariationalLinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& u) const
  {
    VariationalOperatorFdxV* newOperator
      = new VariationalOperatorFdxV(*this);
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__f);
    functionBuilder.setBinaryOperation(BinaryOperation::product,u);

    (*newOperator).__f = functionBuilder.getBuiltFunction();
    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param testFunctionNumber the test function number
   * @param testFunctionProperty  test function property
   * @param f the given @f$ f @f$ function
   * @param i the index in @f$ \int f \partial_{x_i} g @f$
   */
  VariationalOperatorFdxV(const size_t& testFunctionNumber,
			  const VariationalOperator::Property& testFunctionProperty,
			  ConstReferenceCounting<ScalarFunctionBase> f,
			  const size_t& i)
    : VariationalLinearOperator(VariationalLinearOperator::FdxV,
				testFunctionNumber, testFunctionProperty),
      __f(f),
      __i(i)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V another VariationalOperatorFdxV
   */
  VariationalOperatorFdxV(const VariationalOperatorFdxV& V)
    : VariationalLinearOperator(V),
      __f(V.__f),
      __i(V.__i)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalOperatorFdxV()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_F_DX_V_HPP
