//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 Pascal Have

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

//  $Id: connected_triangle.hpp,v 1.4 2007/05/20 23:02:46 delpinux Exp $

#ifndef CONNECTED_TRIANGLE_HPP
#define CONNECTED_TRIANGLE_HPP

#include <Triangle.hpp>

class ConnectedTriangle; // prédéclaration

typedef Vertex*  PointIndex;
typedef ConnectedTriangle * TriangleIndex;

class ConnectedTriangle : public Triangle {
private:
  TriangleIndex __neighs[3];

  inline void __initNeighs() {
    __neighs[0] = NULL;
    __neighs[1] = NULL;
    __neighs[2] = NULL;
  }

public:
  int tag;
  ConnectedTriangle() { __initNeighs(); tag=0; }

  ConnectedTriangle(const PointIndex & s1,const PointIndex & s2,const PointIndex & s3) : Triangle(*s1,*s2,*s3) { 
    __initNeighs(); 
    tag=0; 
  }

  ConnectedTriangle(const PointIndex & s1,const PointIndex & s2,const PointIndex & s3,
		    const TriangleIndex & v1,const TriangleIndex & v2,const TriangleIndex & v3) : Triangle(*s1,*s2,*s3) {
    __neighs[0] = v1;
    __neighs[1] = v2;
    __neighs[2] = v3;
    tag = 0;
  }

  ConnectedTriangle(const ConnectedTriangle & t) : Triangle(t) {
    __neighs[0] = t.__neighs[0];
    __neighs[1] = t.__neighs[1];
    __neighs[2] = t.__neighs[2];
    tag = t.tag;
  }
  
  /*! Pas de désallocation car sinon perte du trgle pointé */
  ~ConnectedTriangle() { }

  //base
  Vertex**  base()  {
      return __vertices;
  }

  const TriangleIndex & neigh(const unsigned k) const { ASSERT(k < 3); return __neighs[k]; }
  TriangleIndex & neigh(const unsigned k) { ASSERT(k < 3); return __neighs[k]; }

  //! Modification globale des sommets.
  void setVertices(const PointIndex &s1,const PointIndex &s2,const PointIndex &s3) {
    ASSERT(s1 != s2 & s2 != s3 & s3 !=s1); // A remettre pour un bon debug de insert_point.    
    __vertices[0]=s1; __vertices[1]=s2; __vertices[2]=s3; 
  }
  
  //! Modification globale des voisins.
  void setNeighs(const TriangleIndex &v1,const TriangleIndex &v2,const TriangleIndex &v3) {
    __neighs[0]=v1; __neighs[1]=v2; __neighs[2]=v3;
  }
  
  //! determine le numero associe au sommet P
  unsigned localizeVertex(const PointIndex & P)  {
    unsigned local = 0;
    while(base()[local] != P) {
      ++local;
      ASSERT(local < 3);
    }
    return local;
  }

  //! determine le numero associe au voisin N
  unsigned localizeNeigh(const ConnectedTriangle * const N) const {
    unsigned local = 0;
    while(__neighs[local] != N) {
      ++local; 
      ASSERT(local < 3); 
    }
    return local;
  }
};

#endif /* CONNECTED_TRIANGLE_HPP */
