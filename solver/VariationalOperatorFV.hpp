//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalOperatorFV.hpp,v 1.5 2007/06/10 15:03:32 delpinux Exp $

#ifndef VARIATIONAL_OPERATOR_FV_HPP
#define VARIATIONAL_OPERATOR_FV_HPP

#include <VariationalLinearOperator.hpp>
#include <ScalarFunctionBuilder.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   VariationalOperatorFV.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun  2 21:55:49 2002
 * 
 * @brief  @f$ l(v) = \int fv @f$
 * 
 */
class VariationalOperatorFV
  : public VariationalLinearOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase> __f; /**< the @f$ f@f$ function */

public:
  /** 
   * Access to @f$ f@f$
   * 
   * @return *__f
   */
  const ScalarFunctionBase& f() const
  {
    return *__f;
  }

  /** 
   * "multiplies" the operator by a coefficient
   * 
   * @param c the given coefficient
   * 
   * @return @f$ \int cfv @f$
   */
  ReferenceCounting<VariationalLinearOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    VariationalOperatorFV* newOperator
      = new VariationalOperatorFV(*this);
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__f);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);

    newOperator->__f = functionBuilder.getBuiltFunction();
    return newOperator;
  }

  /** 
   * Constructor
   * 
   * @param testFunctionNumber tes function number
   * @param testFunctionProperty  test function property
   * @param f @f$ f @f$
   */
  VariationalOperatorFV(const size_t& testFunctionNumber,
			const VariationalOperator::Property& testFunctionProperty,
			ConstReferenceCounting<ScalarFunctionBase> f)
    : VariationalLinearOperator(VariationalLinearOperator::FV,
				testFunctionNumber, testFunctionProperty),
      __f(f)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param fv given VariationalOperatorFV
   */
  VariationalOperatorFV(const VariationalOperatorFV& fv)
    : VariationalLinearOperator(fv),
      __f(fv.__f)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalOperatorFV()
  {
    ;
  }
};

#endif // VARIATIONAL_OPERATOR_FV_HPP
