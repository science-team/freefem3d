//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryPOVRay.hpp,v 1.4 2007/05/20 23:02:47 delpinux Exp $

#ifndef BOUNDARY_POVRAY_HPP
#define BOUNDARY_POVRAY_HPP

#include <Boundary.hpp>
#include <TinyVector.hpp>
#include <vector>

/**
 * @file   BoundaryPOVRay.hpp
 * @author Stephane Del Pino
 * @date   Fri Aug 27 23:49:48 2004
 * 
 * @brief  POVRay boundaries description
 */
class BoundaryPOVRay
  : public Boundary
{
private:
  std::vector<TinyVector<3,real_t> >
  __povReferences;		/**< list of POV-Ray like boundary
				   references (colors). */

  /** 
   * Writes the BoundaryPOVRay to a stream
   * 
   * @param os output stream
   */
  void put(std::ostream& os) const {
    if (__povReferences.size() > 0) {
      os << " POVref:";
      for (size_t i=0; i<__povReferences.size(); ++i) {
	os << " <" << __povReferences[i][0]
	   <<  ',' << __povReferences[i][1]
	   <<  ',' << __povReferences[i][2] << '>';
      }
    }
  }

public:
  /** 
   * Read-only access to the number of POVRay references
   * 
   * @return __povReferences.size()
   */
  size_t numberOfPOVReferences() const
  {
    return __povReferences.size();
  }

  /** 
   * Adds a POVRay reference.
   * 
   * @param povReference reference to add
   */
  void addPOVReference(const TinyVector<3,real_t>& povReference)
  {
    for(size_t i=0; i<__povReferences.size(); ++i) {
      if (__povReferences[i] == povReference)
	return;
    }
    __povReferences.push_back(povReference);
  }

  /** 
   * Read-only access to the ith POVRay reference.
   * 
   * @param i number of the reference
   * 
   * @return ith POVRay reference
   */
  const TinyVector<3,real_t>&
  povReference(const size_t& i) const
  {
    ASSERT(i<__povReferences.size());
    return __povReferences[i];
  }

  /** 
   * Copy constructor
   * 
   * @param B given BoundaryPOVRay
   */
  BoundaryPOVRay(const BoundaryPOVRay& B)
    : Boundary(B),
      __povReferences(B.__povReferences)
  {
    ;
  }

  /** 
   * Constructor
   * 
   */
  BoundaryPOVRay()
    : Boundary(Boundary::povRay)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~BoundaryPOVRay()
  {
    ;
  }
};

#endif // BOUNDARY_POVRAY_HPP
