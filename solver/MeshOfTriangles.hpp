//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshOfTriangles.hpp,v 1.2 2007/05/20 23:08:08 delpinux Exp $

#ifndef MESH_OF_TRIANGLES_HPP
#define MESH_OF_TRIANGLES_HPP

#include <Vector.hpp>
#include <Triangle.hpp>

#include <Mesh.hpp>

#include <Octree.hpp>
#include <Connectivity.hpp>

#include <ErrorHandler.hpp>
#include <EdgesBuilder.hpp>

/**
 * @file   SurfaceMeshOfTriangles.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun  9 14:31:05 2002
 * 
 * @brief This class describes 2d meshes made of triangles
 * 
 * This class describes 2d meshes made of triangles
 */

class MeshOfTriangles
  : public Mesh
{
public:
  enum {
    family = Mesh::plane
  };

  typedef Triangle CellType;
  typedef Edge FaceType;

  typedef struct {} BorderMeshType; /**< BorderMeshType is not defined  */

  typedef Mesh::T_iterator<MeshOfTriangles, Triangle> iterator;
  typedef Mesh::T_iterator<const MeshOfTriangles, const Triangle> const_iterator;

private:
  ReferenceCounting<Vector<Triangle> > __cells;	/**< The set of cells */

  ReferenceCounting<Octree<size_t, 2> > __octree; /**< Octree refering to the cells */
  TinyVector<2,real_t> __a;	/**< bounding box lower  corner */
  TinyVector<2,real_t> __b;	/**< bounding box higher corner */

  Connectivity<MeshOfTriangles> __connectivity; /**< Connectivity */

public:
  std::string typeName() const
  {
    return "mesh of triangles";
  }

  void buildLocalizationTools();

  /** 
   * Faces and edges are treated as the same for surfaces meshes
   * 
   */
  void buildFaces()
  {
    this->buildEdges();
  }

  void buildEdges()
  {
    EdgesBuilder<MeshOfTriangles> edgesBuilder(*this);
    __edgesSet = edgesBuilder.edgesSet();
  }

  /** 
   * Read only access to the mesh connectivity
   * 
   * @return the connectivity
   */
  const Connectivity<MeshOfTriangles>& connectivity() const
  {
    return __connectivity;
  }

  /** 
   * Access to the mesh connectivity
   * 
   * @return the connectivity
   */
  Connectivity<MeshOfTriangles>& connectivity()
  {
    return __connectivity;
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const real_t& x, const real_t& y, const real_t& z) const
  {
    return not(this->find(x, y, z).end());
  }

  //! Returns \p true if the point \a p is inside the mesh.
  inline bool inside(const TinyVector<3>& p) const
  {
    return this->inside(p[0], p[1], p[2]);
  }

  /** 
   * Find the element which contains a point \f$ P \f$
   * 
   * @param P \f$ P\f$
   * 
   * @return an iterator on the element if found else returns the end
   * iterator
   * 
   */
  MeshOfTriangles::const_iterator
  find(const TinyVector<3, real_t>& P) const
  {
    return find(P[0], P[1], P[2]);
  }

  /** 
   * Find the element which contains a point \f$ P \f$
   * 
   * @param x \f$ P_1\f$
   * @param y \f$ P_2\f$ 
   * @param z  \f$ P_3\f$
   * 
   * @return an iterator on the element if found else returns the end
   * iterator
   * 
   * @bug Criteria uses an epsilon that should be relative to the
   * element size
   */
  MeshOfTriangles::const_iterator
  find(const double& x,
       const double& y,
       const double& z) const;


//   /** 
//    * Computes fictitious cells of the mesh
//    * 
//    */
//   void computesFictitiousCells() const
//   {
//     this->__computesFictitiousCells<Triangle>(*__cells);
//   }

  /** 
   * Access to the ith cell
   * 
   * @param i number if the cell
   * 
   * @return the ith cell
   */
  Triangle& cell(const size_t& i)
  {
    return (*__cells)[i];
  }

  /** 
   * RO-Access to the ith cell
   * 
   * @param i number of the cell
   * 
   * @return the ith cell
   */
  const Triangle& cell(const size_t& i) const
  {
    return (*__cells)[i];
  }

  /** 
   * Reserves storage for \a n surface elements.
   * @note the content is lost!
   * @param n number of elements
   */
  inline void setNumberOfCells(const int n)
  {
    __cells = new Vector<Triangle>(n);
  }

  /** 
   * 
   * Returns the number of cells
   * 
   * @return number of cells
   */
  inline const size_t& numberOfCells() const
  {
    return (*__cells).size();
  }

  size_t cellNumber(const Triangle& t) const
  {
    return (*__cells).number(t);
  }

  /** 
   * Returns true if the faces set has been built
   * 
   * @return true if __edgesSet is not NULL
   */
  inline bool hasFaces() const
  {
    return __edgesSet != 0;
  }

  /** 
   * 
   * Returns the number of faces
   * 
   * @return number of faces
   */
  inline const size_t& numberOfFaces() const
  {
    return __edgesSet->numberOfEdges();
  }

  /** 
   * Read-only access to a face
   * 
   * @param i the face number
   * 
   * @return the \a i th face
   */
  const FaceType& face(const size_t& i) const
  {
    return (*__edgesSet)[i];
  }

  /** 
   * Access to the number of the face
   * 
   * @param f the face
   * 
   * @return @f number
   */
  size_t faceNumber(const FaceType& f) const
  {
    return __edgesSet->number(f);
  }

  MeshOfTriangles(const size_t& theNumberOfCells)
    : Mesh(Mesh::trianglesMesh,
	   Mesh::plane,
	   0),
      __cells(new Vector<Triangle>(theNumberOfCells)),
      __connectivity(*this)
  {
    ;
  }

  MeshOfTriangles(ReferenceCounting<VerticesSet> vertices,
		  ReferenceCounting<VerticesCorrespondance> correspondances,
		  ReferenceCounting<Vector<Triangle> > triangles);

  /** 
   * Default constructor
   * 
   */
  MeshOfTriangles()
    : Mesh(Mesh::trianglesMesh,
	   Mesh::plane,
	   0),
      __connectivity(*this)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~MeshOfTriangles()
  {
    ;
  }
};

#endif // MESH_OF_TRIANGLES_HPP
