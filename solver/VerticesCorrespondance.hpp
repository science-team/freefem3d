//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003, 2004, 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VerticesCorrespondance.hpp,v 1.1 2005/01/29 20:20:33 delpinux Exp $

#ifndef VERTICES_CORRESPONDANCE_HPP
#define VERTICES_CORRESPONDANCE_HPP

#include <ReferenceCounting.hpp>
#include <Vector.hpp>

/**
 * @file   VerticesCorrespondance.hpp
 * @author Stephane Del Pino
 * @date   Wed Jan 26 20:04:36 2005
 * 
 * @brief This class describes vertices correspondances. It is used
 * for instance to indicate vertices associations in the case of
 * periodic domains
 * 
 */

class VerticesCorrespondance
{
private:
  /// maximum number of correspondances. It represents the number of
  /// real vertices (periodic copies of vertices are not counted). It
  /// is usefull for instance to compute the number of degrees of
  /// freedom.
  const size_t __numberOfCorrespondances;

  /// This table contains correspondance to "real" vertices 
  ReferenceCounting <Vector<size_t> > __pVerticesNumbers;

  /// This is used to avoid *__pVerticesNumbers
  Vector<size_t>& __verticesNumbers;

public:
  /** 
   * Read only access to the number of correspondance
   * 
   * @return the __numberOfCorrespondances
   */
  const size_t& numberOfCorrespondances() const
  {
    return __numberOfCorrespondances;
  }

  /** 
   * Read-only access to the vertices "real" numbers
   * 
   * @param i the number of the vertex
   * 
   * @return the number of the \a i th vertex
   */
  inline const size_t& operator[] (const size_t& i) const
  {
    return __verticesNumbers[i];
  }

  /** 
   * Access to the vertices "real" numbers
   * 
   * @param i the number of the vertex
   * 
   * @return the number of the \a i th vertex
   */
  inline size_t& operator[] (const size_t& i)
  {
    return __verticesNumbers[i];
  }

  /** 
   * Constructor. This constructor considers that vertices are all
   * non-periodic vertices.
   * 
   * @param nbVertices number of vertices
   */
  VerticesCorrespondance(const size_t& nbVertices)
    : __numberOfCorrespondances(nbVertices),
      __pVerticesNumbers(new Vector<size_t>(nbVertices)),
      __verticesNumbers(*__pVerticesNumbers)
  {
    for (size_t i=0; i<nbVertices; ++i) {
      __verticesNumbers[i] = i;
    }
  }

  /** 
   * Constructor.
   * 
   * @param nbCorrespondances the number of real vertices
   * @param verticesNumbers the vertices numbers
   */
  VerticesCorrespondance(const size_t& nbCorrespondances,
			 ReferenceCounting<Vector<size_t> > verticesNumbers)
    : __numberOfCorrespondances(nbCorrespondances),
      __pVerticesNumbers(verticesNumbers),
      __verticesNumbers(*__pVerticesNumbers)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param v a VerticesCorrespondance to copy
   */
  VerticesCorrespondance(const VerticesCorrespondance& v)
    : __numberOfCorrespondances(v.__numberOfCorrespondances),
      __pVerticesNumbers(new Vector<size_t>(v.__verticesNumbers)),
      __verticesNumbers(*__pVerticesNumbers)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VerticesCorrespondance()
  {
    ;
  }
};

#endif // VERTICES_CORRESPONDANCE_HPP
