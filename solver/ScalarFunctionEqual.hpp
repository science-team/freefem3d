//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionEqual.hpp,v 1.3 2006/10/28 12:26:54 delpinux Exp $

#ifndef SCALAR_FUNCTION_EQUAL_HPP
#define SCALAR_FUNCTION_EQUAL_HPP

#include <ScalarFunctionBase.hpp>

/**
 * @file   ScalarFunctionEqual.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 01:14:45 2006
 * 
 * @brief true if the the two functions are equal at points
 * 
 */
class ScalarFunctionEqual
  : public ScalarFunctionBase
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< first operand */
  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< second operand */

  /** 
   * Writes the operation to a stream
   * 
   * @param os output stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << '(' << *__f << "==" << *__g << ')';
    return os;
  }

public:
  /** 
   * Evaluates the function
   * 
   * @param X point of evaluation
   * 
   * @return if @f$ f(X) = g(X) @f$ returns 1 else 0
   */
  real_t operator()(const TinyVector<3,real_t>& X) const
  {
    return (*__f)(X)==(*__g)(X);
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return false
   */
  bool canBeSimplified() const
  {
    return false;
  }

  /** 
   * Constructor
   * 
   * @param f first operand
   * @param g second operand
   */
  ScalarFunctionEqual(ConstReferenceCounting<ScalarFunctionBase> f,
		      ConstReferenceCounting<ScalarFunctionBase> g)
    : ScalarFunctionBase(ScalarFunctionBase::eq),
      __f(f),
      __g(g)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param f given function
   */
  ScalarFunctionEqual(const ScalarFunctionEqual& f)
    : ScalarFunctionBase(f),
      __f(f.__f),
      __g(f.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionEqual()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_EQUAL_HPP
