//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003 Pascal Hav�
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id: BoundaryList.hpp,v 1.2 2006/11/02 15:41:25 delpinux Exp $

#ifndef BOUNDARY_LIST_HPP
#define BOUNDARY_LIST_HPP

#include <Boundary.hpp>
#include <list>

/**
 * @file   BoundaryList.hpp
 * @author Stephane Del Pino
 * @date   Fri Apr 16 16:45:01 2004
 * 
 * @brief  list of boundaries of various type
 * 
 * 
 */
class BoundaryList
  : public Boundary
{
public:
  typedef std::list<ReferenceCounting<Boundary> > List;

private:
  std::list<ReferenceCounting<Boundary> > __list;

  void put(std::ostream& os) const
  {
    for (List::const_iterator i = __list.begin();
	 i != __list.end(); ++i) {
      os << **i << ',';
    }
    os << "\b ";
  }

public:
  const List& list() const
  {
    return __list;
  }

  void add(ReferenceCounting<Boundary> b)
  {
    __list.push_back(b);
  }

  BoundaryList(const BoundaryList& B)
    : Boundary(B),
      __list(B.__list)
  {
    ;
  }

  BoundaryList()
    : Boundary(Boundary::list)
  {
    ;
  }

  ~BoundaryList()
  {
    ;
  }
};

#endif // BOUNDARY_LIST_HPP
