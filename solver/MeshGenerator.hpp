//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshGenerator.hpp,v 1.3 2006/11/13 00:31:13 delpinux Exp $

#ifndef MESHGENERATOR_HPP
#define MESHGENERATOR_HPP

#include <ReferenceCounting.hpp>
#include <Mesh.hpp>
/**
 * @file   MeshGenerator.hpp
 * @author St�phane Del Pino
 * @date   Sun Dec 15 17:38:59 2002
 * 
 * @brief  Base class for mesh generators
 */
class MeshGenerator
{
protected:
  ReferenceCounting<Mesh> __mesh; /**< The generated mesh */

public:
  /** 
   * Access to the generated mesh
   * 
   * @return __mesh
   */
  ReferenceCounting<Mesh> mesh() const
  {
    return __mesh;
  }

  /** 
   * Default constructor
   * 
   */
  MeshGenerator()
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param m 
   */
  MeshGenerator(const MeshGenerator& m)
    : __mesh(m.__mesh)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~MeshGenerator()
  {
    ;
  }
};

#endif // MESHGENERATOR_HPP
