//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: GaussLobatto.cpp,v 1.2 2007/06/09 10:37:07 delpinux Exp $

#include <cmath>
#include <LegendreBasis.hpp>
#include <Interval.hpp>
#include <GaussLobatto.hpp>

void GaussLobatto::
__bisection(const Interval& interval,
	    real_t& root)
{
  LegendreBasis Ln(__degree);
  const real_t epsilon = 1e-10;

  real_t a = interval.a();
  real_t b = interval.b();

  do {
    root= 0.5*(a+b);
 
    if (Ln.getDerivativeValue(root,__degree)== 0) return;
    if (Ln.getDerivativeValue(root,__degree)*Ln.getDerivativeValue(a,__degree) <0) {
      b=root;
    } else {
      a=root;
    }
  } while (std:: abs(b-a) > epsilon );
}

GaussLobatto::
GaussLobatto(const size_t& degree,
	     const bool computeWeight)
  : __degree(degree),
    __vertices(degree+1),
    __weights(degree+1)
{   
  __vertices[0] = -1; 
  __vertices[degree] = 1;
  
  if (__degree!=1) {
    if (__degree==2) {
      Interval intervalRef(-1.,1.);
      __bisection(intervalRef,__vertices[1]);
    } else {
      Interval intervalRef(-1.,1.);
      GaussLobatto gaussN_1(__degree-1,false);   
          
      for (size_t i=1; i<__degree; ++i) {
	Interval interval( gaussN_1(i-1),gaussN_1(i));	
	__bisection(interval,__vertices[i]);
      }
    }
  }

  if (computeWeight) {

    const real_t w0= 2./(__degree*(__degree+1));
    __weights[0]= w0;
    __weights[__degree]=w0;

    LegendreBasis Ln(__degree);
    for (size_t i=1;i<__degree; ++i) {
      __weights[i]= w0 / (Ln.getValue(__vertices[i],__degree) * Ln.getValue(__vertices[i],__degree));
    }
  }
}
