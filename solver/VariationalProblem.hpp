//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalProblem.hpp,v 1.7 2007/06/10 15:03:31 delpinux Exp $

#ifndef VARIATIONAL_PROBLEM_HPP
#define VARIATIONAL_PROBLEM_HPP

#include <list>

#include <Problem.hpp>

#include <VariationalBilinearBorderOperator.hpp>
#include <VariationalLinearBorderOperator.hpp>

#include <VariationalBilinearOperator.hpp>
#include <VariationalLinearOperator.hpp>

#include <BoundaryConditionSet.hpp>

/**
 * @file   VariationalProblem.hpp
 * @author Stephane Del Pino
 * @date   Fri May 31 00:48:36 2002
 * 
 * @brief  Describes variational problems
 * 
 * this class describes problems of the form: find @f$ u @f$ such that
 * @f$ a(u,v)=l(v)\forall v@f$
 */
class VariationalProblem
  : public Problem
{
private:
  typedef std::list <ConstReferenceCounting<VariationalBilinearOperator> >
  BilinearOperatorListType;

  BilinearOperatorListType
  __bilinearOperatorList;	/**< list of bilinear operators */

  typedef std::list <ConstReferenceCounting<VariationalLinearOperator> >
  LinearOperatorListType;

  LinearOperatorListType
  __linearOperatorList;		/**< list of linear operators */


  typedef std::list <ConstReferenceCounting<VariationalBilinearBorderOperator> >
  BilinearBorderOperatorListType;

  BilinearBorderOperatorListType
  __bilinearBorderOperatorList; /**< list of bilinear border
				   operators, ie operators living on
				   the border for natural BC as an
				   example */

  typedef std::list <ConstReferenceCounting<VariationalLinearBorderOperator> >
  LinearBorderOperatorListType;

  LinearBorderOperatorListType
  __linearBorderOperatorList; /**< list of bilinear border operators,
				 ie operators living on the border for
				 natural BC for example */
  
  std::vector<ConstReferenceCounting<BoundaryConditionSet> >
  __bc;				/**< list of dirichlet conditions */

public:
  typedef BilinearOperatorListType::iterator bilinearOperatorIterator;
  typedef BilinearOperatorListType::const_iterator bilinearOperatorConst_iterator;

  typedef LinearOperatorListType::iterator linearOperatorIterator;
  typedef LinearOperatorListType::const_iterator linearOperatorConst_iterator;

  typedef BilinearBorderOperatorListType::iterator bilinearBorderOperatorIterator;
  typedef BilinearBorderOperatorListType::const_iterator bilinearBorderOperatorConst_iterator;

  typedef LinearBorderOperatorListType::iterator linearBorderOperatorIterator;
  typedef LinearBorderOperatorListType::const_iterator linearBorderOperatorConst_iterator;

  /** 
   * Bilinear Operator iterator begin
   * 
   * @return iterator on the first bilinear operator
   */
  bilinearOperatorIterator beginBilinearOperator()
  {
    return __bilinearOperatorList.begin();
  }

  /** 
   * Bilinear Operator const iterator begin
   * 
   * @return iterator on the first bilinear operator
   */
  bilinearOperatorConst_iterator beginBilinearOperator() const
  {
    return __bilinearOperatorList.begin();
  }

  /** 
   * Bilinear Operator const iterator end
   * 
   * @return iterator at the end of the bilinear operator list
   */
  bilinearOperatorConst_iterator endBilinearOperator() const
  {
    return __bilinearOperatorList.end();
  }

  /** 
   * Linear Operator iterator begin
   * 
   * @return iterator on the first bilinear operator
   */
  linearOperatorIterator beginLinearOperator()
  {
    return __linearOperatorList.begin();
  }

  /** 
   * Linear Operator iterator begin
   * 
   * @return iterator on the first bilinear operator
   */
  linearOperatorConst_iterator beginLinearOperator() const
  {
    return __linearOperatorList.begin();
  }

  /** 
   * Linear Operator iterator end
   * 
   * @return iterator at the end of the linear opearator list
   */
  linearOperatorConst_iterator endLinearOperator() const
  {
    return __linearOperatorList.end();
  }

  /** 
   * Bilinear Border Operator iterator begin
   * 
   * @return iterator on the first bilinear border operator
   */
  bilinearBorderOperatorIterator beginBilinearBorderOperator()
  {
    return __bilinearBorderOperatorList.begin();
  }

  /** 
   * Bilinear Border Operator const iterator begin
   * 
   * @return iterator on the first bilinear border operator
   */
  bilinearBorderOperatorConst_iterator beginBilinearBorderOperator() const
  {
    return __bilinearBorderOperatorList.begin();
  }

  /** 
   * Bilinear Border Operator const iterator end
   * 
   * @return iterator at the end of the bilinear border operator list
   */
  bilinearBorderOperatorConst_iterator endBilinearBorderOperator() const
  {
    return __bilinearBorderOperatorList.end();
  }

  /** 
   * Linear Border Operator iterator begin
   * 
   * @return iterator on the first bilinear border operator
   */
  linearBorderOperatorIterator beginLinearBorderOperator()
  {
    return __linearBorderOperatorList.begin();
  }

  /** 
   * Linear Border Operator iterator begin
   * 
   * @return iterator on the first bilinear border operator
   */
  linearBorderOperatorConst_iterator beginLinearBorderOperator() const
  {
    return __linearBorderOperatorList.begin();
  }

  /** 
   * Linear Border Operator iterator end
   * 
   * @return iterator at the end of the linear border opearator list
   */
  linearBorderOperatorConst_iterator endLinearBorderOperator() const
  {
    return __linearBorderOperatorList.end();
  }

  /** 
   * Returns the problem given by the "product" of a function u in the
   * variational forms
   * 
   * @param u the given function
   * 
   * @return the modified problem
   */
  ReferenceCounting<Problem>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& u) const
  {
    VariationalProblem* newVariationalProblem = new VariationalProblem(*this);

    newVariationalProblem->__bilinearOperatorList.clear();
    for (BilinearOperatorListType::const_iterator i = __bilinearOperatorList.begin(); 
	 i != __bilinearOperatorList.end(); ++i) {
      newVariationalProblem->__bilinearOperatorList.push_back((**i) * u);
    }

    newVariationalProblem->__linearOperatorList.clear();
    for (LinearOperatorListType::const_iterator i = __linearOperatorList.begin(); 
	 i != __linearOperatorList.end(); ++i) {
      newVariationalProblem->__linearOperatorList.push_back((**i) * u);
    }

    return newVariationalProblem;
  }

  /** 
   * Adds a variational bilinear operator to its list
   * 
   * @param v a variational bilinear operator
   */
  void add(ReferenceCounting<VariationalBilinearOperator> v)
  {
    __bilinearOperatorList.push_back(v);
  }


  /** 
   * Adds a variational "border" bilinear operator to its list
   * 
   * @param v a variational "border" bilinear operator
   */
  void add(ReferenceCounting<VariationalBilinearBorderOperator> v)
  {
    __bilinearBorderOperatorList.push_back(v);
  }

  /** 
   * Adds a variational linear operator to its list
   * 
   * @param v a variational linear operator
   */
  void add(ReferenceCounting<VariationalLinearOperator> v)
  {
    __linearOperatorList.push_back(v);
  }

  /** 
   * Adds a variational "border" linear operator to its list
   * 
   * @param v a variational "border" linear operator
   */
  void add(ReferenceCounting<VariationalLinearBorderOperator> v)
  {
    __linearBorderOperatorList.push_back(v);
  }

  /** 
   * Returns the number of unknowns
   * 
   * 
   * @return __numberOfUnknow
   */
  size_t numberOfUnknown() const
  {
    return __bc.size();
  }

  /** 
   * Returns the boundary condition set associated to the ith unknown
   * 
   * @param i 
   * 
   * @return 
   */
  const BoundaryConditionSet& boundaryConditionSet(const size_t& i) const
  {
    return *(__bc[i]);
  }

  bool hasJumpOrMean() const
  {
    for (BilinearOperatorListType::const_iterator i=__bilinearOperatorList.begin();
	 i != __bilinearOperatorList.end(); ++i) {
      if (((*i)->testFunctionProperty() != VariationalOperator::normal) or
	  ((*i)->unknownProperty() != VariationalOperator::normal)) {
	return true;
      }
    }

    for (LinearOperatorListType::const_iterator i=__linearOperatorList.begin();
	 i != __linearOperatorList.end(); ++i) {
      if ((*i)->testFunctionProperty() != VariationalOperator::normal) {
	return true;
      }
    }

    return false;
  }

  /** 
   * Constructor
   * 
   * @param bc Boundary condition sets
   */
  VariationalProblem(std::vector<ConstReferenceCounting<BoundaryConditionSet> > bc)
    : Problem(Problem::variationalProblem, 0),
      __bc(bc)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V original variational problem
   */
  VariationalProblem(const VariationalProblem& V)
    : Problem(V),
      __bilinearOperatorList(V.__bilinearOperatorList),
      __linearOperatorList(V.__linearOperatorList),
      __bilinearBorderOperatorList(V.__bilinearBorderOperatorList),
      __linearBorderOperatorList(V.__linearBorderOperatorList),
      __bc(V.__bc)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalProblem()
  {
    ;
  }
};

#endif // VARIATIONAL_PROBLEM_HPP
