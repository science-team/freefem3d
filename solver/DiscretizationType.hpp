//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: DiscretizationType.hpp,v 1.5 2007/05/20 23:15:36 delpinux Exp $

#ifndef DISCRETIZATION_TYPE_HPP
#define DISCRETIZATION_TYPE_HPP

#include <ErrorHandler.hpp>
#include <string>

/**
 * @file   DiscretizationType.hpp
 * @author Stephane Del Pino
 * @date   Mon May 30 23:28:37 2005
 * 
 * @brief  This class describes type of Discretization
 * 
 */
class DiscretizationType
{
public:
  enum Type {
    undefined       =-1,	/**< not defined */
    functionLike    = 0,	/**< keeps function type if possible */

    // finite element-like discretizations
    lagrangianFEM0  =10,	/**< Lagrangian finite element of degree 0 */
    lagrangianFEM1  =11,	/**< Lagrangian finite element of degree 1 */
    lagrangianFEM2  =12,	/**< Lagrangian finite element of degree 2 */

    // Spectral-like discretizations
    spectralLegendre=20		/**< Lagrange spectral method */
  };

private:
  Type __type;		/**< Type of discretization */

public:
  static Type getDefault(const Type& type)
  {
    return (type==undefined)?lagrangianFEM1:type;
  }

  const DiscretizationType& operator=(const DiscretizationType::Type& type)
  {
    __type=type;
    return *this;
  }

  /** 
   * convertes type in its name
   * 
   * @param type type of discretization
   * 
   * @return name of the type 
   */
  static std::string name(const DiscretizationType::Type& type)
  {
    switch (type) {
    case lagrangianFEM0:  return "P0";
    case lagrangianFEM1:  return "P1";
    case lagrangianFEM2:  return "P2";
    case spectralLegendre:return "Legendre";
    case undefined:       return "undefined";
    case functionLike:    return "function-like";
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown Discretization type",
		       ErrorHandler::unexpected);
  }

  /** 
   * convertes type in its name
   * 
   * @param type type of discretization
   * 
   * @return name of the type 
   */
  static std::string name(const DiscretizationType& discretization)
  {
    return DiscretizationType::name(discretization.type());
  }

  /** 
   * Read-only access to the type
   * 
   * @return __type
   */
  const Type& type() const
  {
    return __type;
  }

  /** 
   * Constructor
   * 
   * @param type type of discretization
   */
  DiscretizationType(const DiscretizationType::Type& type = undefined)
    : __type(type)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d originale discretization type
   */
  DiscretizationType(const DiscretizationType& d)
    : __type(d.__type)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~DiscretizationType()
  {
    ;
  }
};

#endif // DISCRETIZATION_TYPE_HPP
