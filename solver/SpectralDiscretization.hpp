//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SpectralDiscretization.hpp,v 1.12 2007/06/23 15:16:33 delpinux Exp $

#ifndef SPECTRAL_DISCRETIZATION_HPP
#define SPECTRAL_DISCRETIZATION_HPP

#include <Problem.hpp>
#include <DegreeOfFreedomSet.hpp>

#include <SpectralMesh.hpp>

#include <SpectralFunction.hpp>
#include <ScalarFunctionBase.hpp>

#include <Interval.hpp>
#include <LegendreBasis.hpp>
#include <GaussLobatto.hpp>

#include <SpectralConformTransformation.hpp>

#include <Timer.hpp>

#include <DoubleHashedMatrix.hpp>
#include <UnAssembledMatrix.hpp>

#include <Discretization.hpp>


#include <PDE.hpp>
#include <PDEProblem.hpp>
#include <MassOperator.hpp>
#include <FirstOrderOperator.hpp>
#include <DivMuGrad.hpp>
#include <SecondOrderOperator.hpp>

#include <VariationalProblem.hpp>

#include <VariationalOperatorFV.hpp>
#include <VariationalOperatorFdxV.hpp>
#include <VariationalOperatorFdxGV.hpp>
#include <VariationalOperatorFgradGgradV.hpp>

#include <VariationalOperatorAlphaUV.hpp>
#include <VariationalOperatorMuGradUGradV.hpp>
#include <VariationalOperatorAlphaDxUDxV.hpp>
#include <VariationalOperatorNuUdxV.hpp>
#include <VariationalOperatorNuDxUV.hpp>

#include <Mesh.hpp>

#include <ErrorHandler.hpp>

template <DiscretizationType::Type discretization>
class SpectralDiscretization
  : public Discretization
{
private:
  // Set of degrees of freedom
  const DegreeOfFreedomSet& __degreeOfFreedomSet;

  const SpectralMesh& __mesh;


  TinyVector<3, ConstReferenceCounting<Interval> > __interval;
  TinyVector<3, ConstReferenceCounting<SpectralConformTransformation> > __transform;
  TinyVector<3, ConstReferenceCounting<GaussLobatto> > __gaussLobatto;
  TinyVector<3, ConstReferenceCounting<LegendreBasis> > __basis;

  typedef Vector<Vector<Vector<real_t> > > TensorVector;

  void _getAu(const TensorVector& coef,
	      const real_t& detX, const real_t& detY, const real_t& detZ,
	      const Vector<Vector<real_t> >& basisOrDerivativeValueU0,
	      const Vector<Vector<real_t> >& basisOrDerivativeValueU1,
	      const Vector<Vector<real_t> >& basisOrDerivativeValueU2,
	      const Vector<Vector<real_t> >& basisOrDerivativeValueV0,
	      const Vector<Vector<real_t> >& basisOrDerivativeValueV1,
	      const Vector<Vector<real_t> >& basisOrDerivativeValueV2,
	      const size_t& unknownNumber, const size_t& testFunctionNumber,
	      const Vector<real_t>& u,
	      Vector<real_t>& v) const
  {
    Vector<Vector<Vector<real_t> > > u_ijt(__basis[0]->dimension());
    for (size_t i=0; i<__basis[0]->dimension(); ++i) {
      u_ijt[i].resize(__basis[1]->dimension());
      for (size_t j=0; j<__basis[1]->dimension(); ++j) {
	u_ijt[i][j].resize(__gaussLobatto[2]->numberOfPoints());
	u_ijt[i][j] = 0;
      }
    }

    {
      size_t l=0;
      for (size_t i=0; i<__basis[0]->dimension(); ++i) {
	for (size_t j=0; j<__basis[1]->dimension(); ++j) {
	  for (size_t k=0; k<__basis[2]->dimension(); ++k) {
	    const real_t& u_ijk = u[__degreeOfFreedomSet(unknownNumber,l)];
	    for (size_t t=0; t<__gaussLobatto[2]->numberOfPoints(); ++t) {
	      u_ijt[i][j][t] += u_ijk * basisOrDerivativeValueU2[t][k];
	    }
	    l++;
	  }
	}
      }
    }

    Vector<Vector<Vector<real_t> > > u_ist(__basis[0]->dimension());
    for (size_t i=0; i<__basis[0]->dimension(); ++i) {
      u_ist[i].resize(__gaussLobatto[1]->numberOfPoints());
      for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	u_ist[i][s].resize(__gaussLobatto[2]->numberOfPoints());
	u_ist[i][s] = 0;
      }
    }

    for (size_t i=0; i<__basis[0]->dimension(); ++i) {
      for (size_t j=0; j<__basis[1]->dimension(); ++j) {
	for (size_t t=0; t<__gaussLobatto[2]->numberOfPoints(); ++t) {
	  const real_t& u_ijt_value = u_ijt[i][j][t];
	  for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	    u_ist[i][s][t] += u_ijt_value*basisOrDerivativeValueU1[s][j];
	  }
	}
      }
    }
	    
    Vector<Vector<Vector<real_t> > > u_rst(__gaussLobatto[0]->numberOfPoints());
    for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
      u_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
      for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	u_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	u_rst[r][s] = 0;
      }
    }

    for (size_t i=0; i<__basis[0]->dimension(); ++i) {
      for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	for (size_t t=0; t<__gaussLobatto[2]->numberOfPoints(); ++t) {
	  const real_t& u_ist_value = u_ist[i][s][t];
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    u_rst[r][s][t] += u_ist_value*basisOrDerivativeValueU0[r][i];
	  }
	}
      }
    }


    Vector<Vector<Vector<real_t> > > v_rsk(__gaussLobatto[0]->numberOfPoints());
    for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
      v_rsk[r].resize( __gaussLobatto[1]->numberOfPoints());
      for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	v_rsk[r][s].resize( __basis[2]->dimension());
	v_rsk[r][s] = 0;
      }
    }

    {
      for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	  for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
	    const real_t& wt = __gaussLobatto[2]->weight(t);
	    const real_t value_rst = coef[r][s][t]*u_rst[r][s][t]*detZ*wt;
	    for (size_t k=0; k<__basis[2]->dimension(); ++k) {
	      v_rsk[r][s][k] += value_rst * basisOrDerivativeValueV2[t][k];
	    }
	  }
	}
      }
    }

    Vector<Vector<Vector<real_t> > > v_rjk(__gaussLobatto[0]->numberOfPoints());
    for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
      v_rjk[r].resize( __basis[1]->dimension());
      for (size_t j=0; j<__basis[1]->dimension(); ++j) {
	v_rjk[r][j].resize( __basis[2]->dimension());
	v_rjk[r][j] = 0;
      }
    }

    {
      for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	  const real_t& ws = __gaussLobatto[1]->weight(s);
	  for (size_t k=0; k < __basis[2]->dimension(); ++k) {
	    const real_t value_rsk = v_rsk[r][s][k]*detY*ws;
	    for (size_t j=0; j<__basis[1]->dimension(); ++j) {
	      v_rjk[r][j][k] += value_rsk * basisOrDerivativeValueV1[s][j];
	    }
	  }
	}
      }
    }

    {
      for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	const real_t& wr = __gaussLobatto[0]->weight(r);
	for (size_t j=0; j < __basis[1]->dimension(); ++j) {
	  for (size_t k=0; k < __basis[2]->dimension(); ++k) {
	    const real_t value_rjk = v_rjk[r][j][k]*detX*wr;
	    for (size_t i=0; i<__basis[0]->dimension(); ++i) {
	      v[__degreeOfFreedomSet(testFunctionNumber, __mesh.dofNumber(i,j,k))] +=
		value_rjk * basisOrDerivativeValueV0[r][i];
	    }
	  }
	}
      }
    }
  }


public:  
  /** 
   * Assembles the matrix associated to the PDE operators of the PDE
   * problem.
   * 
   */
  void assembleMatrix()
  {
    switch ((this->__A).type()) {
    case BaseMatrix::doubleHashedMatrix: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "Spectral Method cannot be used with assembled matrices",
			 ErrorHandler::unexpected);
    }
    case BaseMatrix::unAssembled: {
      UnAssembledMatrix& A = dynamic_cast<UnAssembledMatrix&>(this->__A);
      A.setDiscretization(this);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected matrix type",
		       ErrorHandler::unexpected);
    }
    }
  }

  /** 
   *  Applies directly the operator discretization to the vector X.
   * 
   * @param U input vector
   * @param V \f$ v=Au \f$
   */
  void timesX(const BaseVector& U, BaseVector& V) const
  {
    const Vector<real_t>& u = dynamic_cast<const Vector<real_t>&>(U);

    Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(V);
    v=0;


    TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
    quadratureBaseValues[0].resize((*__gaussLobatto[0]).numberOfPoints());
    quadratureBaseValues[1].resize((*__gaussLobatto[1]).numberOfPoints());
    quadratureBaseValues[2].resize((*__gaussLobatto[2]).numberOfPoints());
	
    TinyVector<3,Vector<Vector<real_t> > > quadratureBaseDerivativeValues;
    quadratureBaseDerivativeValues[0].resize((*__gaussLobatto[0]).numberOfPoints());
    quadratureBaseDerivativeValues[1].resize((*__gaussLobatto[1]).numberOfPoints());
    quadratureBaseDerivativeValues[2].resize((*__gaussLobatto[2]).numberOfPoints());
	
    for (size_t i=0; i < (*__gaussLobatto[0]).numberOfPoints(); ++i) {
      real_t xi= (*__gaussLobatto[0])(i);
      quadratureBaseValues[0][i].resize((*__basis[0]).dimension());
      quadratureBaseDerivativeValues[0][i].resize((*__basis[0]).dimension());
	  
      (*__basis[0]).getValues(xi,quadratureBaseValues[0][i]);
      (*__basis[0]).getDerivativeValues(xi,quadratureBaseDerivativeValues[0][i]);
    }
	
    for (size_t j=0; j < (*__gaussLobatto[1]).numberOfPoints(); ++j) {
      real_t yj= (*__gaussLobatto[1])(j);
      quadratureBaseValues[1][j].resize((*__basis[1]).dimension());
      quadratureBaseDerivativeValues[1][j].resize((*__basis[1]).dimension());
      (*__basis[1]).getValues(yj,quadratureBaseValues[1][j]);
      (*__basis[1]).getDerivativeValues(yj,quadratureBaseDerivativeValues[1][j]);
    }
	  
    for (size_t k=0; k < (*__gaussLobatto[2]).numberOfPoints(); ++k) {
      real_t zk= (*__gaussLobatto[2])(k);
      quadratureBaseValues[2][k].resize((*__basis[2]).dimension());
      quadratureBaseDerivativeValues[2][k].resize((*__basis[2]).dimension());
      (*__basis[2]).getValues(zk,quadratureBaseValues[2][k]);
      (*__basis[2]).getDerivativeValues(zk,quadratureBaseDerivativeValues[2][k]);
    }
	  
    //construction des nodes
    Vector<real_t> nodesX((*__gaussLobatto[0]).numberOfPoints());
    for (size_t i=0; i<(*__gaussLobatto[0]).numberOfPoints(); ++i) {
      nodesX[i] =(*__transform[0])((*__gaussLobatto[0])(i));
    }
    Vector<real_t> nodesY((*__gaussLobatto[1]).numberOfPoints());
    for (size_t i=0; i<(*__gaussLobatto[1]).numberOfPoints(); ++i) {
      nodesY[i] =(*__transform[1])((*__gaussLobatto[1])(i));
    }
    Vector<real_t> nodesZ((*__gaussLobatto[2]).numberOfPoints());
    for (size_t i=0; i<(*__gaussLobatto[2]).numberOfPoints(); ++i) {
      nodesZ[i] = (*__transform[2])((*__gaussLobatto[2])(i));
    }


    switch (this->problem().type()) {
    case Problem::pdeProblem: {
      const PDESystem& pdeSystem = dynamic_cast<const PDESystem&>(this->problem());
  
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented yet!",
			 ErrorHandler::unexpected);
      break;
    }
    case Problem::variationalProblem: {
      const VariationalProblem& P
	= dynamic_cast<const VariationalProblem&>(this->problem());

      for (VariationalProblem::bilinearOperatorConst_iterator
	     iOperator = P.beginBilinearOperator();
	   iOperator != P.endBilinearOperator(); ++iOperator) {

	switch ((**iOperator).type()) {	 
	case VariationalBilinearOperator::alphaUV: {
	  const VariationalAlphaUVOperator& alphaUV
	    = dynamic_cast<const VariationalAlphaUVOperator&>(**iOperator);

	  const ScalarFunctionBase& alpha = *alphaUV.alpha();

	  Vector<Vector<Vector<real_t> > > alpha_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    alpha_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      alpha_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      alpha_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		alpha_rst[r][s][t] = alpha(x,y,z);
	      }
	    }
	  }
	  _getAu(alpha_rst,
		 1./__transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 alphaUV.unknownNumber(), alphaUV.testFunctionNumber(),
		 u,v);
	  break;
	}
	
	case VariationalBilinearOperator::muGradUGradV: {
	  const VariationalMuGradUGradVOperator& muGradUgradV
	    = dynamic_cast<const VariationalMuGradUGradVOperator&>(**iOperator);
	  const ScalarFunctionBase& mu = *muGradUgradV.mu();

	  Vector<Vector<Vector<real_t> > > mu_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    mu_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      mu_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      mu_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		mu_rst[r][s][t] = mu(x,y,z);
	      }
	    }
	  }
	  _getAu(mu_rst,
		 __transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseDerivativeValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 quadratureBaseDerivativeValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 muGradUgradV.unknownNumber(), muGradUgradV.testFunctionNumber(),
		 u,v);
	  _getAu(mu_rst,
		 1./__transform[0]->inverseDeterminant(),
		 __transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseDerivativeValues[1],quadratureBaseValues[2],
		 quadratureBaseValues[0],quadratureBaseDerivativeValues[1],quadratureBaseValues[2],
		 muGradUgradV.unknownNumber(), muGradUgradV.testFunctionNumber(),
		 u,v);
	  _getAu(mu_rst,
		 1./__transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 __transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseDerivativeValues[2],
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseDerivativeValues[2],
		 muGradUgradV.unknownNumber(), muGradUgradV.testFunctionNumber(),
		 u,v);
	  break;
	}
	case VariationalBilinearOperator::alphaDxUDxV: {
	  const VariationalAlphaDxUDxVOperator& alphaDxUDxV
	    = dynamic_cast<const VariationalAlphaDxUDxVOperator&>(**iOperator);
	  const ScalarFunctionBase& alpha = *alphaDxUDxV.alpha();

	  Vector<Vector<Vector<real_t> > > alpha_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    alpha_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      alpha_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      alpha_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		alpha_rst[r][s][t] = alpha(x,y,z);
	      }
	    }
	  }

	  // sets default determinant values
	  TinyVector<3, real_t> dets;
	  dets[0] = 1./ __transform[0]->inverseDeterminant();
	  dets[1] = 1./ __transform[1]->inverseDeterminant();
	  dets[2] = 1./ __transform[2]->inverseDeterminant();

	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureUValues(0,0,0);
	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureVValues(0,0,0);
	  // Set default legendre base quadrature values
	  for (size_t i=0; i<3;++i) {
	    pQuadratureUValues[i] = &quadratureBaseValues[i];
	    pQuadratureVValues[i] = &quadratureBaseValues[i];
	  }

	  dets[alphaDxUDxV.j()] *= __transform[alphaDxUDxV.j()]->inverseDeterminant();
	  dets[alphaDxUDxV.i()] *= __transform[alphaDxUDxV.i()]->inverseDeterminant();

	  pQuadratureUValues[alphaDxUDxV.j()] = &quadratureBaseDerivativeValues[alphaDxUDxV.j()];
	  pQuadratureVValues[alphaDxUDxV.i()] = &quadratureBaseDerivativeValues[alphaDxUDxV.i()];

	  _getAu(alpha_rst,
		 dets[0], dets[1], dets[2],
		 *pQuadratureUValues[0],*pQuadratureUValues[1],*pQuadratureUValues[2],
		 *pQuadratureVValues[0],*pQuadratureVValues[1],*pQuadratureVValues[2],
		 alphaDxUDxV.unknownNumber(), alphaDxUDxV.testFunctionNumber(),
		 u,v);
	  break;
	}
// 	default: {
// 	  throw ErrorHandler(__FILE__,__LINE__,
// 			     "unexpected operator type",
// 			     ErrorHandler::unexpected);
// 	}


	case VariationalBilinearOperator::nuUdxV: {
	  const VariationalNuUdxVOperator& nuUdxV
	    = dynamic_cast<const VariationalNuUdxVOperator&>(**iOperator);

	  const ScalarFunctionBase& nu = *nuUdxV.nu();

	  Vector<Vector<Vector<real_t> > > nu_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    nu_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      nu_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      nu_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		nu_rst[r][s][t] = nu(x,y,z);
	      }
	    }
	  }
	  // sets default determinant values
	  
	  TinyVector<3, real_t> dets;
	  dets[0] = 1./ __transform[0]->inverseDeterminant();
	  dets[1] = 1./ __transform[1]->inverseDeterminant();
	  dets[2] = 1./ __transform[2]->inverseDeterminant();

	  //	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureUValues(0,0,0);
	  
	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureVValues(0,0,0);
	  
	  // Set default legendre base quadrature values
	  
	  for (size_t i=0; i<3;++i) {
	    // pQuadratureUValues[i] = &quadratureBaseValues[i];
	    pQuadratureVValues[i] = &quadratureBaseValues[i];
	  }

	  dets[nuUdxV.i()] *= __transform[nuUdxV.i()]->inverseDeterminant();
	  // dets[nuUDxV.i()] *= __transform[nuUDxV.i()]->inverseDeterminant();

	  //	  pQuadratureUValues[nuUDxV.j()] = &quadratureBaseDerivativeValues[nuUDxV.j()];
	  pQuadratureVValues[nuUdxV.i()] = &quadratureBaseDerivativeValues[nuUdxV.i()];

	  _getAu(nu_rst,
		 dets[0], dets[1], dets[2],
		 quadratureBaseValues[0], quadratureBaseValues[1], quadratureBaseValues[2],
		 *pQuadratureVValues[0],*pQuadratureVValues[1],*pQuadratureVValues[2],
		 nuUdxV.unknownNumber(), nuUdxV.testFunctionNumber(),
		 u,v);
	  break;
	}


	case VariationalBilinearOperator::nuDxUV: {
	  const VariationalNuDxUVOperator& nuDxUV
	    = dynamic_cast<const VariationalNuDxUVOperator&>(**iOperator);

	  const ScalarFunctionBase& nu = *nuDxUV.nu();

	  Vector<Vector<Vector<real_t> > > nu_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    nu_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      nu_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      nu_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		nu_rst[r][s][t] = nu(x,y,z);
	      }
	    }
	  }
	  // sets default determinant values
	  
	  TinyVector<3, real_t> dets;
	  dets[0] = 1./ __transform[0]->inverseDeterminant();
	  dets[1] = 1./ __transform[1]->inverseDeterminant();
	  dets[2] = 1./ __transform[2]->inverseDeterminant();

	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureUValues(0,0,0);
	  
	  //TinyVector<3,Vector<Vector<real_t> >*> pQuadratureVValues(0,0,0);
	  
	  // Set default legendre base quadrature values
	  
	  for (size_t i=0; i<3;++i) {
	    pQuadratureUValues[i] = &quadratureBaseValues[i];
	    //pQuadratureVValues[i] = &quadratureBaseValues[i];
	  }

	  // dets[nuUdxV.i()] *= __transform[nuUdxV.i()]->inverseDeterminant();
	  dets[nuDxUV.i()] *= __transform[nuDxUV.i()]->inverseDeterminant();

	  pQuadratureUValues[nuDxUV.i()] = &quadratureBaseDerivativeValues[nuDxUV.i()];
	  // pQuadratureVValues[nuUdxV.i()] = &quadratureBaseDerivativeValues[nuUdxV.i()];

	  _getAu(nu_rst,
		 dets[0], dets[1], dets[2],
		 *pQuadratureUValues[0],*pQuadratureUValues[1],*pQuadratureUValues[2],
		 quadratureBaseValues[0], quadratureBaseValues[1], quadratureBaseValues[2],
		 nuDxUV.unknownNumber(), nuDxUV.testFunctionNumber(),
		 u,v);
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "unexpected operator type",
			     ErrorHandler::unexpected);
	}
      }
      }
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected problem type",
			 ErrorHandler::unexpected);
    }
    }
  }
    
  /** 
   *  Applies directly the operator discretization to the vector X.
   * 
   * @param X input vector
   * @param Z \f$ z=A^T x \f$
   */
  void transposedTimesX(const BaseVector& X, BaseVector& Z) const
  {
    const Vector<real_t>& x = dynamic_cast<const Vector<real_t>&>(X);
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);
    z=0;


    //  Vector<real_t>& v = dynamic_cast<Vector<real_t>&>(V);
    // v=0;


    TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
    quadratureBaseValues[0].resize((*__gaussLobatto[0]).numberOfPoints());
    quadratureBaseValues[1].resize((*__gaussLobatto[1]).numberOfPoints());
    quadratureBaseValues[2].resize((*__gaussLobatto[2]).numberOfPoints());
	
    TinyVector<3,Vector<Vector<real_t> > > quadratureBaseDerivativeValues;
    quadratureBaseDerivativeValues[0].resize((*__gaussLobatto[0]).numberOfPoints());
    quadratureBaseDerivativeValues[1].resize((*__gaussLobatto[1]).numberOfPoints());
    quadratureBaseDerivativeValues[2].resize((*__gaussLobatto[2]).numberOfPoints());
	
    for (size_t i=0; i < (*__gaussLobatto[0]).numberOfPoints(); ++i) {
      real_t xi= (*__gaussLobatto[0])(i);
      quadratureBaseValues[0][i].resize((*__basis[0]).dimension());
      quadratureBaseDerivativeValues[0][i].resize((*__basis[0]).dimension());
	  
      (*__basis[0]).getValues(xi,quadratureBaseValues[0][i]);
      (*__basis[0]).getDerivativeValues(xi,quadratureBaseDerivativeValues[0][i]);
    }
	
    for (size_t j=0; j < (*__gaussLobatto[1]).numberOfPoints(); ++j) {
      real_t yj= (*__gaussLobatto[1])(j);
      quadratureBaseValues[1][j].resize((*__basis[1]).dimension());
      quadratureBaseDerivativeValues[1][j].resize((*__basis[1]).dimension());
      (*__basis[1]).getValues(yj,quadratureBaseValues[1][j]);
      (*__basis[1]).getDerivativeValues(yj,quadratureBaseDerivativeValues[1][j]);
    }
	  
    for (size_t k=0; k < (*__gaussLobatto[2]).numberOfPoints(); ++k) {
      real_t zk= (*__gaussLobatto[2])(k);
      quadratureBaseValues[2][k].resize((*__basis[2]).dimension());
      quadratureBaseDerivativeValues[2][k].resize((*__basis[2]).dimension());
      (*__basis[2]).getValues(zk,quadratureBaseValues[2][k]);
      (*__basis[2]).getDerivativeValues(zk,quadratureBaseDerivativeValues[2][k]);
    }
	  
    //construction des nodes
    Vector<real_t> nodesX((*__gaussLobatto[0]).numberOfPoints());
    for (size_t i=0; i<(*__gaussLobatto[0]).numberOfPoints(); ++i) {
      nodesX[i] =(*__transform[0])((*__gaussLobatto[0])(i));
    }
    Vector<real_t> nodesY((*__gaussLobatto[1]).numberOfPoints());
    for (size_t i=0; i<(*__gaussLobatto[1]).numberOfPoints(); ++i) {
      nodesY[i] =(*__transform[1])((*__gaussLobatto[1])(i));
    }
    Vector<real_t> nodesZ((*__gaussLobatto[2]).numberOfPoints());
    for (size_t i=0; i<(*__gaussLobatto[2]).numberOfPoints(); ++i) {
      nodesZ[i] = (*__transform[2])((*__gaussLobatto[2])(i));
    }


    switch (this->problem().type()) {
    case Problem::pdeProblem: {
      const PDESystem& pdeSystem = dynamic_cast<const PDESystem&>(this->problem());
  
      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented yet!",
			 ErrorHandler::unexpected);
      break;
    }
    case Problem::variationalProblem: {
      const VariationalProblem& P
	= dynamic_cast<const VariationalProblem&>(this->problem());

      for (VariationalProblem::bilinearOperatorConst_iterator
	     iOperator = P.beginBilinearOperator();
	   iOperator != P.endBilinearOperator(); ++iOperator) {

	switch ((**iOperator).type()) {	 
	case VariationalBilinearOperator::alphaUV: {
	  const VariationalAlphaUVOperator& alphaUV
	    = dynamic_cast<const VariationalAlphaUVOperator&>(**iOperator);

	  const ScalarFunctionBase& alpha = *alphaUV.alpha();

	  Vector<Vector<Vector<real_t> > > alpha_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    alpha_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      alpha_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      alpha_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		alpha_rst[r][s][t] = alpha(x,y,z);
	      }
	    }
	  }
	  _getAu(alpha_rst,
		 1./__transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 alphaUV.testFunctionNumber(), alphaUV.unknownNumber(),
		 x,z);
	  break;
	}
	
	case VariationalBilinearOperator::muGradUGradV: {
	  const VariationalMuGradUGradVOperator& muGradUgradV
	    = dynamic_cast<const VariationalMuGradUGradVOperator&>(**iOperator);
	  const ScalarFunctionBase& mu = *muGradUgradV.mu();

	  Vector<Vector<Vector<real_t> > > mu_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    mu_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      mu_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      mu_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		mu_rst[r][s][t] = mu(x,y,z);
	      }
	    }
	  }
	  _getAu(mu_rst,
		 __transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseDerivativeValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 quadratureBaseDerivativeValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 muGradUgradV.testFunctionNumber(), muGradUgradV.unknownNumber(),
		 x,z);
	  _getAu(mu_rst,
		 1./__transform[0]->inverseDeterminant(),
		 __transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseDerivativeValues[1],quadratureBaseValues[2],
		 quadratureBaseValues[0],quadratureBaseDerivativeValues[1],quadratureBaseValues[2],
		 muGradUgradV.testFunctionNumber(), muGradUgradV.unknownNumber(),
		 x,z);
	  _getAu(mu_rst,
		 1./__transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 __transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseDerivativeValues[2],
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseDerivativeValues[2],
		 muGradUgradV.testFunctionNumber(), muGradUgradV.unknownNumber(),
		 x,z);
	  break;
	}
	case VariationalBilinearOperator::alphaDxUDxV: {
	  const VariationalAlphaDxUDxVOperator& alphaDxUDxV
	    = dynamic_cast<const VariationalAlphaDxUDxVOperator&>(**iOperator);
	  const ScalarFunctionBase& alpha = *alphaDxUDxV.alpha();

	  Vector<Vector<Vector<real_t> > > alpha_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    alpha_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      alpha_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      alpha_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		alpha_rst[r][s][t] = alpha(x,y,z);
	      }
	    }
	  }

	  // sets default determinant values
	  TinyVector<3, real_t> dets;
	  dets[0] = 1./ __transform[0]->inverseDeterminant();
	  dets[1] = 1./ __transform[1]->inverseDeterminant();
	  dets[2] = 1./ __transform[2]->inverseDeterminant();

	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureUValues(0,0,0);
	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureVValues(0,0,0);
	  // Set default legendre base quadrature values
	  for (size_t i=0; i<3;++i) {
	    pQuadratureUValues[i] = &quadratureBaseValues[i];
	    pQuadratureVValues[i] = &quadratureBaseValues[i];
	  }

	  dets[alphaDxUDxV.j()] *= __transform[alphaDxUDxV.j()]->inverseDeterminant();
	  dets[alphaDxUDxV.i()] *= __transform[alphaDxUDxV.i()]->inverseDeterminant();

	  pQuadratureUValues[alphaDxUDxV.j()] = &quadratureBaseDerivativeValues[alphaDxUDxV.j()];
	  pQuadratureVValues[alphaDxUDxV.i()] = &quadratureBaseDerivativeValues[alphaDxUDxV.i()];

	  _getAu(alpha_rst,
		 dets[0], dets[1], dets[2],
		 *pQuadratureUValues[0],*pQuadratureUValues[1],*pQuadratureUValues[2],
		 *pQuadratureVValues[0],*pQuadratureVValues[1],*pQuadratureVValues[2],
		 alphaDxUDxV.testFunctionNumber(), alphaDxUDxV.unknownNumber(),
		 x,z);
	  break;
	}

	case VariationalBilinearOperator::nuUdxV: {
	  const VariationalNuUdxVOperator& nuUdxV
	    = dynamic_cast<const VariationalNuUdxVOperator&>(**iOperator);

	  const ScalarFunctionBase& nu = *nuUdxV.nu();

	  Vector<Vector<Vector<real_t> > > nu_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    nu_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      nu_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      nu_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		nu_rst[r][s][t] = nu(x,y,z);
	      }
	    }
	  }
		  
	  TinyVector<3, real_t> dets;
	  dets[0] = 1./ __transform[0]->inverseDeterminant();
	  dets[1] = 1./ __transform[1]->inverseDeterminant();
	  dets[2] = 1./ __transform[2]->inverseDeterminant();

	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureVValues(0,0,0);
	  	  
	  for (size_t i=0; i<3;++i) {
	  
	    pQuadratureVValues[i] = &quadratureBaseValues[i];
	  }

	  dets[nuUdxV.i()] *= __transform[nuUdxV.i()]->inverseDeterminant();
	  // dets[nuUDxV.i()] *= __transform[nuUDxV.i()]->inverseDeterminant();

	  //	  pQuadratureUValues[nuUDxV.j()] = &quadratureBaseDerivativeValues[nuUDxV.j()];
	  pQuadratureVValues[nuUdxV.i()] = &quadratureBaseDerivativeValues[nuUdxV.i()];

	  _getAu(nu_rst,
		 dets[0], dets[1], dets[2],
		 *pQuadratureVValues[0],  *pQuadratureVValues[1],  *pQuadratureVValues[2],
		 quadratureBaseValues[0], quadratureBaseValues[1], quadratureBaseValues[2],
		 nuUdxV.testFunctionNumber(), nuUdxV.unknownNumber(),
		 x,z);
	  break;
	}
	case VariationalBilinearOperator::nuDxUV: {
	  const VariationalNuDxUVOperator& nuDxUV
	    = dynamic_cast<const VariationalNuDxUVOperator&>(**iOperator);

	  const ScalarFunctionBase& nu = *nuDxUV.nu();

	  Vector<Vector<Vector<real_t> > > nu_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    nu_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      nu_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      nu_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		nu_rst[r][s][t] = nu(x,y,z);
	      }
	    }
	  }
	  // sets default determinant values
	  
	  TinyVector<3, real_t> dets;
	  dets[0] = 1./ __transform[0]->inverseDeterminant();
	  dets[1] = 1./ __transform[1]->inverseDeterminant();
	  dets[2] = 1./ __transform[2]->inverseDeterminant();

	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureUValues(0,0,0);
	  
	
	  
	  for (size_t i=0; i<3;++i) {
	    pQuadratureUValues[i] = &quadratureBaseValues[i];
	
	  }
	
	  dets[nuDxUV.i()] *= __transform[nuDxUV.i()]->inverseDeterminant();

	  pQuadratureUValues[nuDxUV.i()] = &quadratureBaseDerivativeValues[nuDxUV.i()];

	  _getAu(nu_rst,
		 dets[0], dets[1], dets[2],
		 quadratureBaseValues[0], quadratureBaseValues[1], quadratureBaseValues[2],
		 *pQuadratureUValues[0],*pQuadratureUValues[1],*pQuadratureUValues[2],
		 nuDxUV.testFunctionNumber(),nuDxUV.unknownNumber(),
		 x,z);
	  break;
	}
	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "unexpected operator type",
			     ErrorHandler::unexpected);
	}
      }
      }
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexpected problem type",
			 ErrorHandler::unexpected);
    }
    }
  }
  

  /** 
   * Computes diagonal of the operator
   * 
   * @param Z diagonal of the operator
   */
  void getDiagonal(BaseVector& Z) const
  {
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);

    throw ErrorHandler(__FILE__,__LINE__,
		       "not implemented yet!",
		       ErrorHandler::unexpected);
  }

  /** 
   * Second member assembling
   * 
   */
  void assembleSecondMember()
  {
    Timer t;
    t.start();

    ffout(2) << "- assembling second member\n";

    //! The elementary vector
    Vector<real_t>& b = (static_cast<Vector<real_t>&>(this->__b));
    b = 0;
    const DegreeOfFreedomPositionsSet& dofPositions
      = this->__degreeOfFreedomSet.positionsSet();

	  TinyVector<3,Vector<Vector<real_t> > > quadratureBaseValues;
	  quadratureBaseValues[0].resize((*__gaussLobatto[0]).numberOfPoints());
	  quadratureBaseValues[1].resize((*__gaussLobatto[1]).numberOfPoints());
	  quadratureBaseValues[2].resize((*__gaussLobatto[2]).numberOfPoints());
	  
	  TinyVector<3,Vector<Vector<real_t> > > quadratureBaseDerivativeValues;
	  quadratureBaseDerivativeValues[0].resize((*__gaussLobatto[0]).numberOfPoints());
	  quadratureBaseDerivativeValues[1].resize((*__gaussLobatto[1]).numberOfPoints());
	  quadratureBaseDerivativeValues[2].resize((*__gaussLobatto[2]).numberOfPoints());
	  
 	  for (size_t i=0; i < (*__gaussLobatto[0]).numberOfPoints(); ++i) {
	    real_t xi= (*__gaussLobatto[0])(i);
	    quadratureBaseValues[0][i].resize((*__basis[0]).dimension());
	    (*__basis[0]).getValues(xi,quadratureBaseValues[0][i]);
	    quadratureBaseDerivativeValues[0][i].resize((*__basis[0]).dimension());
	    (*__basis[0]).getDerivativeValues(xi,quadratureBaseDerivativeValues[0][i]);
	  }

	  for (size_t j=0; j < (*__gaussLobatto[1]).numberOfPoints(); ++j) {
	    real_t yj= (*__gaussLobatto[1])(j);
	    quadratureBaseValues[1][j].resize((*__basis[1]).dimension());
	    (*__basis[1]).getValues(yj,quadratureBaseValues[1][j]);
	    quadratureBaseDerivativeValues[1][j].resize((*__basis[1]).dimension());
	    (*__basis[1]).getDerivativeValues(yj,quadratureBaseDerivativeValues[1][j]);
	  }

	  for (size_t k=0; k < (*__gaussLobatto[2]).numberOfPoints(); ++k) {
	    real_t zk= (*__gaussLobatto[2])(k);
	    quadratureBaseValues[2][k].resize((*__basis[2]).dimension());
	    (*__basis[2]).getValues(zk,quadratureBaseValues[2][k]);
	    quadratureBaseDerivativeValues[2][k].resize((*__basis[2]).dimension());
	    (*__basis[2]).getDerivativeValues(zk,quadratureBaseDerivativeValues[2][k]);
	  }
	  
	  //construction des nodes
	  Vector<real_t> nodesX((*__gaussLobatto[0]).numberOfPoints());
	  for (size_t i=0; i<(*__gaussLobatto[0]).numberOfPoints(); ++i) {
	    nodesX[i] = (*__transform[0])((*__gaussLobatto[0])(i));
	  }
	  Vector<real_t> nodesY((*__gaussLobatto[1]).numberOfPoints());
	  for (size_t i=0; i<(*__gaussLobatto[1]).numberOfPoints(); ++i) {
	    nodesY[i] = (*__transform[1])((*__gaussLobatto[1])(i));
	  }
	  Vector<real_t> nodesZ((*__gaussLobatto[2]).numberOfPoints());
	  for (size_t i=0; i<(*__gaussLobatto[2]).numberOfPoints(); ++i) {
	    nodesZ[i] = (*__transform[2])((*__gaussLobatto[2])(i));
	  }

  
    switch(this->problem().type()) {
    case Problem::pdeProblem: {
      const PDESystem& pdeSystem
	= static_cast<const PDESystem&>(this->problem());

      throw ErrorHandler(__FILE__,__LINE__,
			 "not implemented yet!",
			 ErrorHandler::unexpected);
      break;
    }
    case Problem::variationalProblem: {
      const VariationalProblem& variationalProblem
	=  dynamic_cast<const VariationalProblem&>(this->problem());

      for (VariationalProblem::linearOperatorConst_iterator i
	     = variationalProblem.beginLinearOperator();
	   i != variationalProblem.endLinearOperator(); ++i) {
	switch ((*(*i)).type()) {
	case VariationalLinearOperator::FV: {
	  const VariationalOperatorFV& fv
	    = dynamic_cast<const VariationalOperatorFV&>(*(*i));
	  //  ffout(0) << "\tassemble int(f*v)\n";

	  const ScalarFunctionBase& f = fv.f();
	  const size_t testFunctionNumber = fv.testFunctionNumber();
	  
	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    const real_t& wr = __gaussLobatto[0]->weight(r);
	    
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      const real_t& wrs = __gaussLobatto[1]->weight(s)*wr;
	      
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {	  
		const real_t& z = nodesZ[t];
		const real_t& wrst = __gaussLobatto[2]->weight(t)*wrs;
		
		const real_t fweight = f(x,y,z)* wrst *
		  1./(*__transform[0]).inverseDeterminant()*  
		  1./(*__transform[1]).inverseDeterminant()*
		  1./(*__transform[2]).inverseDeterminant();
		
		size_t l=0;
		for (size_t m=0; m<__basis[0]->dimension(); ++m) {
		  const real_t p1 = fweight*quadratureBaseValues[0][r][m];
		  for (size_t p=0; p<__basis[1]->dimension(); ++p) {
		    const real_t p2= quadratureBaseValues[1][s][p] *p1;
		    for (size_t q=0; q<(*__basis[2]).dimension(); ++q) {
		      const real_t p3= quadratureBaseValues[2][t][q]*p2;
		      b[__degreeOfFreedomSet(fv.testFunctionNumber(),l)] +=p3;
		      l++;
		    }
		  }
		}
	      }
	    }
	  }
	  break;
	}
	  
	case VariationalLinearOperator::FdxV: {
	  const VariationalOperatorFdxV& fdxV
	    = dynamic_cast<const VariationalOperatorFdxV&>(*(*i));
	  //  ffout(0) << "\tassemble int(f*v)\n";
	  
	  const ScalarFunctionBase& f = fdxV.f();
	  const size_t testFunctionNumber = fdxV.testFunctionNumber();

	  TinyVector<3,real_t> dets;
	  dets[0] =1./__transform[0]->inverseDeterminant();
	  dets[1] =1./__transform[1]->inverseDeterminant();
	  dets[2] =1./__transform[2]->inverseDeterminant();
	  
	  TinyVector<3,Vector<Vector<real_t> >*>pQuadratureVValues(0,0,0);
	  for (size_t i=0;i<3;i++){
	    pQuadratureVValues[i] = &quadratureBaseValues[i];
	  }
	  dets[fdxV.number()] *= __transform[fdxV.number()]->inverseDeterminant();
	  pQuadratureVValues[fdxV.number()] = &quadratureBaseDerivativeValues[fdxV.number()];
	  

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    const real_t& wr = __gaussLobatto[0]->weight(r);
	    
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      const real_t& wrs = __gaussLobatto[1]->weight(s)*wr;
	      
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {	  
		const real_t& z = nodesZ[t];
		const real_t& wrst = __gaussLobatto[2]->weight(t)*wrs;
		
		const real_t fweight = f(x,y,z)* wrst *dets[0]*dets[1]*dets[2];
		
		size_t l=0;
		for (size_t m=0; m<__basis[0]->dimension(); ++m) {
		  const real_t p1 = fweight*(*pQuadratureVValues[0])[r][m];
		  for (size_t p=0; p<__basis[1]->dimension(); ++p) {
		    const real_t p2=(*pQuadratureVValues[1])[s][p] *p1;
		    for (size_t q=0; q<(*__basis[2]).dimension(); ++q) {
		      const real_t p3= (*pQuadratureVValues[2])[t][q]*p2;
		      b[__degreeOfFreedomSet(fdxV.testFunctionNumber(),l)] +=p3;
		      l++;
		    }
		  }
		}
	      }
	    }
	  }
	  break;	
	}
	case VariationalLinearOperator::FdxGV: {
	  const VariationalOperatorFdxGV& fdxGV
	    = dynamic_cast<const VariationalOperatorFdxGV&>(*(*i));
	 	  
	  const ScalarFunctionBase& f = fdxGV.f();
	  const ScalarFunctionBase& g = fdxGV.g();

	  Vector<Vector<Vector<real_t> > > f_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    f_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      f_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      f_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		f_rst[r][s][t] = f(x,y,z);
	      }
	    }
	  }

	  SpectralFunction gProjection(&__mesh, g);

	  TinyVector<3,real_t> dets;
	  dets[0] =1./__transform[0]->inverseDeterminant();
	  dets[1] =1./__transform[1]->inverseDeterminant();
	  dets[2] =1./__transform[2]->inverseDeterminant();
	  
	  dets[fdxGV.number()] *= __transform[fdxGV.number()]->inverseDeterminant();

	  TinyVector<3,Vector<Vector<real_t> >*> pQuadratureGValues(0,0,0);
	  for (size_t i=0; i<3; ++i) {
	    pQuadratureGValues[i] = &quadratureBaseValues[i];
	  }
	  pQuadratureGValues[fdxGV.number()] = &quadratureBaseDerivativeValues[fdxGV.number()];

	  _getAu(f_rst,
		 dets[0], dets[1], dets[2],
		 *pQuadratureGValues[0],*pQuadratureGValues[1],*pQuadratureGValues[2],
		 quadratureBaseValues[0], quadratureBaseValues[1], quadratureBaseValues[2],
		 fdxGV.testFunctionNumber(),fdxGV.testFunctionNumber(),
		 gProjection.values(),b);
	  
	  break;
	}
	case VariationalLinearOperator::FgradGgradV: {
	  const VariationalOperatorFgradGgradV& fgradGgradV
	    = dynamic_cast<const VariationalOperatorFgradGgradV&>(*(*i));
	 	  
	  const ScalarFunctionBase& f = fgradGgradV.f();
	  const ScalarFunctionBase& g = fgradGgradV.g();

	  Vector<Vector<Vector<real_t> > > f_rst(__gaussLobatto[0]->numberOfPoints());
	  for (size_t r=0; r<__gaussLobatto[0]->numberOfPoints(); ++r) {
	    f_rst[r].resize(__gaussLobatto[1]->numberOfPoints());
	    for (size_t s=0; s<__gaussLobatto[1]->numberOfPoints(); ++s) {
	      f_rst[r][s].resize(__gaussLobatto[2]->numberOfPoints());
	      f_rst[r][s] = 0;
	    }
	  }

	  for (size_t r=0; r < __gaussLobatto[0]->numberOfPoints(); ++r) {
	    const real_t& x = nodesX[r];
	    for (size_t s=0; s < __gaussLobatto[1]->numberOfPoints(); ++s) {
	      const real_t& y = nodesY[s];
	      for (size_t t=0; t < __gaussLobatto[2]->numberOfPoints(); ++t) {
		const real_t& z = nodesZ[t];
		f_rst[r][s][t] = f(x,y,z);
	      }
	    }
	  }

	  SpectralFunction gProjection(&__mesh, g);

	  _getAu(f_rst,
		 __transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseDerivativeValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 quadratureBaseDerivativeValues[0],quadratureBaseValues[1],quadratureBaseValues[2],
		 fgradGgradV.testFunctionNumber(), fgradGgradV.testFunctionNumber(),
		 gProjection.values(),b);
	  _getAu(f_rst,
		 1./__transform[0]->inverseDeterminant(),
		 __transform[1]->inverseDeterminant(),
		 1./__transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseDerivativeValues[1],quadratureBaseValues[2],
		 quadratureBaseValues[0],quadratureBaseDerivativeValues[1],quadratureBaseValues[2],
		 fgradGgradV.testFunctionNumber(), fgradGgradV.testFunctionNumber(),
		 gProjection.values(),b);
	  _getAu(f_rst,
		 1./__transform[0]->inverseDeterminant(),
		 1./__transform[1]->inverseDeterminant(),
		 __transform[2]->inverseDeterminant(),
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseDerivativeValues[2],
		 quadratureBaseValues[0],quadratureBaseValues[1],quadratureBaseDerivativeValues[2],
		 fgradGgradV.testFunctionNumber(), fgradGgradV.testFunctionNumber(),
		 gProjection.values(),b);

	  break;
	}

	default: {
	  throw ErrorHandler(__FILE__,__LINE__,
			     "unknown variational operator",
			     ErrorHandler::unexpected);
	}
	}
      }
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unknown problem type",
			 ErrorHandler::unexpected);
    }
    }
    ffout(2) << "- assembling second member: done";
    t.stop();
    ffout(3) << " [cost: " << t << ']';
    ffout(2) << '\n';
  }

public:

  /** 
   * Constructor of the discretization
   * 
   * @param p the problem
   * @param m the mesh used for discretization
   * @param a matrix storing discretization
   * @param bb vector that stores second member discretization
   * @param dof degrees of freedom set
   * 
   */
  SpectralDiscretization(const Problem& p,
			 SpectralMesh& m,
			 BaseMatrix& a,
			 BaseVector& bb,
			 const DegreeOfFreedomSet& dof)
    : Discretization(Discretization::spectral,
		     p, a, bb),
      __degreeOfFreedomSet(dof),
      __mesh(m),
      __interval(new Interval(__mesh.shape().a()[0],__mesh.shape().b()[0]),
		 new Interval(__mesh.shape().a()[1],__mesh.shape().b()[1]),
		 new Interval(__mesh.shape().a()[2],__mesh.shape().b()[2])),
      __transform(new SpectralConformTransformation(*__interval[0]),
		  new SpectralConformTransformation(*__interval[1]),
		  new SpectralConformTransformation(*__interval[2])),
      __gaussLobatto(new GaussLobatto(__mesh.degree(0)+1),
		     new GaussLobatto(__mesh.degree(1)+1),
		     new GaussLobatto(__mesh.degree(2)+1)),
      __basis(new LegendreBasis(__mesh.degree(0)),
	      new LegendreBasis(__mesh.degree(1)),
	      new LegendreBasis(__mesh.degree(2)))
  {
    ;
  }

  /** 
   * Virtual destructor
   * 
   */
  ~SpectralDiscretization()
  {
    ;
  }
};

    
#endif // FEM_DISCRETIZATION_HPP
