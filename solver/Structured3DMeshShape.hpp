//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Structured3DMeshShape.hpp,v 1.2 2004/08/23 10:00:45 delpinux Exp $


#ifndef STUCTURED_3D_MESH_SHAPE_HPP
#define STUCTURED_3D_MESH_SHAPE_HPP

#include <TinyVector.hpp>
#include <Array3DShape.hpp>

#include <StreamCenter.hpp>

/*!
  \class Structured3DMeshShape

  This class defines the shape of a 3d structured mesh, vector, ...

  \author St�phane Del Pino.
 */

class Structured3DMeshShape
{
private:
  //! The shape.
  Array3DShape __shape;
  TinyVector<3,real_t> __stepSize;

  TinyVector<3> __a;
  TinyVector<3> __b;

  TinyVector<3> __dimensions;
public:

  inline size_t operator()(const size_t& i, const size_t& j, const size_t& k) const
  {
    return __shape(i,j,k);
  }

  inline const Array3DShape& shape() const 
  {
    return __shape;
  }

  inline const TinyVector<3>& a() const
  {
    return __a;
  }

  inline const real_t& a(const size_t& i) const
  {
    return __a[i];
  }

  const TinyVector<3>& b() const
  {
    return __b;
  }

  inline const real_t& b(const size_t& i) const
  {
    return __b[i];
  }

  inline const real_t& dimension(const size_t& i) const
  {
    return __dimensions[i];
  }

  const real_t& hx() const
  {
    return __stepSize[0];
  }

  const real_t& hy() const
  {
    return __stepSize[1];
  }
  const real_t& hz() const
  {
    return __stepSize[2];
  }

  bool operator==(const Structured3DMeshShape& s) const
  {
    return ((__shape==s.__shape)&&
	    (__a==s.__a)&&
	    (__b==s.__b));
  }

  //! Access to the first component of the shape.
  inline size_t nx() const
  {
    return __shape.nx();
  }

  //! Access to the second component of the shape.
  inline size_t ny() const
  {
    return __shape.ny();
  }

  //! Access to the third component of the shape.
  inline size_t nz() const
  {
    return __shape.nz();
  }

  //! Returns the product of shape components (ie: the number of elements when tensorized).
  size_t numberOfVertices() const
  {
    return __shape.nx()*__shape.ny()*__shape.nz();
  }

  //!  Computes the number of edges
  size_t numberOfEdges() const
  {
    return ((__shape.nx()-1)*__shape.ny()*__shape.nz()
	    +__shape.nx()*(__shape.ny()-1)*__shape.nz()
	    +__shape.nx()*__shape.ny()*(__shape.nz()-1));
  }

  //! Returns the product of shape components (ie: the number of elements when tensorized).
  size_t numberOfCells() const
  {
    return (__shape.nx()-1)*(__shape.ny()-1)*(__shape.nz()-1);
  }

  //! Copy constructor
  const Structured3DMeshShape& operator=(const Structured3DMeshShape& s)
  {
    __shape=s.__shape;
    __stepSize=s.__stepSize;
    __a=s.__a;
    __b=s.__b;
    __dimensions=s.__dimensions;

    return *this;
  }

  /*! Default constructor.
    The generated Structured3DMeshShape is not valid!
   */
  Structured3DMeshShape()
  {
    ;
  }

  //! Constructs a Structured3DMeshShape.
  Structured3DMeshShape(const size_t& i, const size_t& j, const size_t& k,
			const TinyVector<3>& v0, const TinyVector<3>& v1)
    : __shape(i,j,k),
      __a( TinyVector<3>( (v0[0]<v1[0]) ? v0[0] : v1[0],
			  (v0[1]<v1[1]) ? v0[1] : v1[1],
			  (v0[2]<v1[2]) ? v0[2] : v1[2] )),

      __b( TinyVector<3>( (v0[0]>v1[0]) ? v0[0] : v1[0],
			  (v0[1]>v1[1]) ? v0[1] : v1[1],
			  (v0[2]>v1[2]) ? v0[2] : v1[2] )),

      __dimensions(std::abs(v0[0]-v1[0]),
		   std::abs(v0[1]-v1[1]),
		   std::abs(v0[2]-v1[2]))
  {
    for (size_t n=0; n<3; ++n)
      __stepSize[n] = (__b[n]-__a[n])/(__shape[n]-1.);
  }

  //! Copy constructor
  Structured3DMeshShape(const Structured3DMeshShape& s)
    : __shape(s.__shape),
      __stepSize(s.__stepSize),
      __a(s.__a),
      __b(s.__b),
      __dimensions(s.__dimensions)
  {
    if ((nx()<=1)||(ny()<=1)||nz()<=1)
      fferr(2) << "\nthe mesh shape is not valid: "
	       << __shape << '\n';
  }

  ~Structured3DMeshShape()
  {
    ;
  }
};

#endif // STUCTURED_3D_MESH_SHAPE_HPP
