//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SolverDriver.cpp,v 1.11 2007/05/20 23:35:53 delpinux Exp $


#include <SolverDriver.hpp>

#include <FiniteElementMethod.hpp>
#include <PenalizedFictitousDomain.hpp>
#include <EliminatedFictitiousDomain.hpp>

#include <FatBoundary.hpp>

#include <SpectralMethod.hpp>

#include <PDESolution.hpp>
#include <PDESolver.hpp>

void SolverDriver::run()
{
  PDESolution& u = static_cast<PDESolution&>(__u);
  ReferenceCounting<Method> M = NULL;

  switch (__methodClass) {
  case SolverDriver::fictitiousFEM: {
    ffout(2) << "Using FEM fictitious domain discretization:\n";
    ffout(2) << "Method:  ";
    switch (__methodType) {
    case (SolverDriverOptions::penalty): {
      M = new PenalizedFictitousDomain(__discretizationType,
				       __mesh,
				       __degreeOfFreedomSet);
      ffout(2) << "Penalty\n";
      break;
    }
    case (SolverDriverOptions::eliminate): {
      M = new EliminatedFictitiousDomain(__discretizationType,
					 __mesh,
					 __degreeOfFreedomSet);
      ffout(2) << "Elimination\n";
      break;
    }
    case (SolverDriverOptions::fatBoundary): {
      M = new FatBoundary(__discretizationType,
			  __mesh);
      break;
    }
    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "not defined method",
			 ErrorHandler::unexpected);
      break;
    }
    }
    break;
  }
  case SolverDriver::fem: {
    ffout(2) << "Using FEM discretization:\n";
    switch(__methodType) {
    case SolverDriverOptions::penalty: {
      fferr(2) << 
	"WARNING: penalty method is not an option for standard FEM\n"
	"WARNING: using elimination for Dirichlet conditions\n";
    }
    default: {
    }
    }
    M = new FiniteElementMethod(__discretizationType,
				__mesh,
				__degreeOfFreedomSet);
    break;
  }
  case SolverDriver::spectral: {
    ffout(2) << "Using spectral discretization:\n";
    M = new SpectralMethod(__discretizationType,
			   __mesh,
			   __degreeOfFreedomSet);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "method type is not implemented",
		       ErrorHandler::unexpected);
  }
  }

  PDESolver solver(__p, M, u);

  solver.Solve();
}

