//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Neumann.hpp,v 1.4 2006/07/20 19:08:54 delpinux Exp $

// this class allows to define Neumann Boundary Conditions

#ifndef NEUMANN_HPP
#define NEUMANN_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <PDECondition.hpp>
#include <ScalarFunctionBase.hpp>

/**
 * @file   Neumann.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 19:21:48 2006
 * 
 * @brief Neumann boundary conditions @f$ A \nabla u\cdot n = g @f$ on
 * @f$ \gamma @f$
 * 
 * @f$ A @f$ is the matrix associated to the operator
 * @f$\nabla\cdot A\nabla @f$ and @f$ n @f$ the normal
 * to @f$ \gamma @f$.
 * 
 * So @f$ g @f$ is the @b conormal derivative of @f$ u @f$.
 */
class Neumann
  : public PDECondition
{
private:
  //! The function to impose as a Neumann boundary condition.
  ConstReferenceCounting<ScalarFunctionBase>
  __g;				/**< @f$ g @f$ */

public:
  /** 
   * Read-only access to the second member
   * 
   * @return @f$ g @f$
   */
  ConstReferenceCounting<ScalarFunctionBase>
  g() const
  {
    return __g;
  }

  /** 
   * Gets the condition name
   * 
   * @return "Neumann"
   */
  std::string typeName() const
  {
    return "Neumann";
  }

  /** 
   * Constructor
   * 
   * @param g the conormal derivative
   * @param unknownNumber unknown number
   */
  Neumann(ConstReferenceCounting<ScalarFunctionBase> g,
	  const size_t unknownNumber)
    : PDECondition(PDECondition::neumann, unknownNumber),
      __g(g)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param N given Neumann
   */
  Neumann(const Neumann& N)
    : PDECondition(N),
      __g(N.__g)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Neumann()
  {
    ;
  }
};

#endif // NEUMANN_HPP
