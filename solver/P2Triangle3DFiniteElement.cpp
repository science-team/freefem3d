//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: P2Triangle3DFiniteElement.cpp,v 1.1 2005/09/25 14:04:32 delpinux Exp $

#include <P2Triangle3DFiniteElement.hpp>

TinyVector<3, real_t> P2Triangle3DFiniteElement::__massCenter(1./3., 1./3., 0);

real_t
P2Triangle3DFiniteElement::W(const size_t& i,
			     const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return (1-x-y)*(1-x-y);
  }
  case 1: {
    return x*x;
  }
  case 2: {
    return y*y;
  }
    // Edges basis functions
  case 3: {
    return 4*x*y;
  }
  case 4: {
    return 4*(1-x-y)*x;
  }
  case 5: {
    return 4*(1-x-y)*y;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
P2Triangle3DFiniteElement::dxW(const size_t& i, const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return -2*(1-x-y);
  }
  case 1: {
    return 2*x;
  }
  case 2: {
    return 0;
  }
    // Edges basis functions
  case 3: {
    return 4*y;
  }
  case 4: {
    return 4*(1-2*x-y);
  }
  case 5: {
    return -4*y;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t
P2Triangle3DFiniteElement::dyW(const size_t& i,
			       const TinyVector<3>& X) const
{
  const real_t& x = X[0];
  const real_t& y = X[1];

  switch (i) {
    // Vertices basis functions 
  case 0: {
    return -2*(1-x-y);
  }
  case 1: {
    return 0;
  }
  case 2: {
    return 2*y;
  }
    // Edges basis functions
  case 3: {
    return 4*x;
  }
  case 4: {
    return -4*x;
  }
  case 5: {
    return 4*(1-x-2*y);
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}

real_t 
P2Triangle3DFiniteElement::dzW(const size_t& i,
			       const TinyVector<3>& X) const
{
  switch (i) {
    // Vertices basis functions 
  case 0:
  case 1:
  case 2:
    // Edges basis functions
  case 3:
  case 4:
  case 5: {
    return 0;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unexpected basis function number",
		       ErrorHandler::unexpected);
    return 0.;
  }
  }
}
