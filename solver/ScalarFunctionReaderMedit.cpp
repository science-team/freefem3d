//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionReaderMedit.cpp,v 1.3 2007/06/10 15:03:07 delpinux Exp $

#include <ScalarFunctionReaderMedit.hpp>
#include <ScalarFunctionBase.hpp>

#include <ErrorHandler.hpp>
#include <Mesh.hpp>

#include <DiscretizationType.hpp>
#include <Vector.hpp>
#include <FEMFunctionBase.hpp>

#include <FEMFunctionBuilder.hpp>


#include <fstream>

ConstReferenceCounting<ScalarFunctionBase>
ScalarFunctionReaderMedit::
getFunction() const
{
  std::ifstream fin(__filename.c_str());
  if (not(fin)) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "error: cannot open file '"+__filename+"'",
		       ErrorHandler::normal);
  }

  size_t a, numberOfComponents, c, numberOfVertices;
  fin >> a >> numberOfComponents >> numberOfVertices >> c;

  if (__componentNumber>=numberOfComponents) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "The file '"+__filename+"' does only contain "
		       +stringify(__componentNumber)+" components.\n"
		       +"Cannot read component number "+stringify(__componentNumber),
		       ErrorHandler::normal);

  }
  const Mesh& mesh = (*__mesh);

  if ((a!=3)or(c!=2)) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Unknown file descriptor while reading file '"+__filename+"'",
		       ErrorHandler::normal);
  }

  if (numberOfVertices != mesh.numberOfVertices()) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Number of vertices does not match mesh while reading '"
		       +__filename+"'",
		       ErrorHandler::normal);
  }

  Vector<real_t> v(numberOfVertices);
  Vector<real_t> componentValue(numberOfComponents);
  for (size_t i=0; i<numberOfVertices; ++i) {
    for (size_t j=0; j<numberOfComponents; ++j) {
      fin >> componentValue[j];
    }
    v[i] = componentValue[__componentNumber];
  }

  if (fin.bad()) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot read all datas in file '"+__filename+"'",
		       ErrorHandler::normal);
  }

  FEMFunctionBuilder builder;
  DiscretizationType d(DiscretizationType::lagrangianFEM1);
  builder.build(d, __mesh, v);
  return builder.getBuiltScalarFunction();
}

ScalarFunctionReaderMedit::
ScalarFunctionReaderMedit(const std::string& filename,
			  ConstReferenceCounting<Mesh> mesh,
			  const std::string& functionName,
			  const int& componentNumber)
  : ScalarFunctionReaderBase(filename, mesh, functionName, componentNumber)
{
  ;
}

ScalarFunctionReaderMedit::
ScalarFunctionReaderMedit(const ScalarFunctionReaderMedit& f)
  : ScalarFunctionReaderBase(f)
{
  ;
}

ScalarFunctionReaderMedit::
~ScalarFunctionReaderMedit()
{
  ;
}
