//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SpectralMethod.cpp,v 1.4 2007/06/09 14:25:58 delpinux Exp $

#include <SpectralMethod.hpp>

#include <SpectralDiscretization.hpp>
#include <BoundaryConditionDiscretizationSpectral.hpp>

#include <PDESolution.hpp>

#include <PDEProblem.hpp>

#include <SpectralMesh.hpp>

#include <KrylovSolver.hpp>

#include <MatrixManagement.hpp>

#include <SparseMatrix.hpp>

#include <Timer.hpp>

#include <ErrorHandler.hpp>

template <typename MeshType,
	  DiscretizationType::Type TypeOfDiscretization>
void SpectralMethod::__discretizeOnMesh()
{
  // overwritting memory matrix
  ParameterCenter::instance().set("memory::matrix","none");

  MemoryManager MM;

  bool performAssembling =MM.ReserveMatrix(__A,
					   problem().numberOfUnknown(),
					   __degreeOfFreedomSet.size());

  MM.ReserveVector(__b,
		   problem().numberOfUnknown(),
		   __degreeOfFreedomSet.size());

  ffout(2) << "Spectral method: disretization...\n";

  ReferenceCounting<SpectralDiscretization<TypeOfDiscretization> > spectralMethod
    = new SpectralDiscretization<TypeOfDiscretization>(problem(),
						       dynamic_cast<MeshType&>(mesh()),
						       *__A,*__b, __degreeOfFreedomSet);

//   if (performAssembling) {
    spectralMethod->assembleMatrix();
//   } else {
//     ffout(2) << "- keeping previous operator discretization\n";
//   }

   spectralMethod->assembleSecondMember();

  ffout(2) << "- discretizing boundary conditions\n";

  BoundaryConditionDiscretizationSpectral* bcd
    = new BoundaryConditionDiscretizationSpectral(problem(),
						  dynamic_cast<MeshType&>(mesh()),
						  __degreeOfFreedomSet);
//   bcd->associatesMeshesToBoundaryConditions();
  ReferenceCounting<BoundaryConditionDiscretization> bcDiscretization = bcd;

//   // Set Dirichlet information to the matrix
//   FEM->setDirichletList(bcDiscretization->getDirichletList());

  ffout(2) << "- second member modification\n";
  bcDiscretization->setSecondMember(__A,__b);

  ffout(2) << "- matrix modification\n";
  bcDiscretization->setMatrix(__A,__b);

  ffout(2) << "Spectral method: disretization done\n";

  if (__A->type() == BaseMatrix::doubleHashedMatrix) {
    Timer t;
    t.start();

    SparseMatrix* aa
      = new SparseMatrix(static_cast<DoubleHashedMatrix&>(*__A));
    
    __A = aa; // now use sparse matrix

    t.stop();
    ffout(2) << "Matrix copy: " << t << '\n';
  }
}

template <DiscretizationType::Type TypeOfDiscretization>
void SpectralMethod::__discretize()
{
  switch (mesh().type()) {
  case Mesh::spectralMesh: {
    this->__discretizeOnMesh<SpectralMesh, TypeOfDiscretization>();
    break;
  }
  default: {
    throw ErrorHandler(__FILE__, __LINE__,
		       "Cannot use '"+mesh().typeName()+"' for spectral method computations",
		       ErrorHandler::normal);
  }
  }
}

void SpectralMethod::Discretize (ConstReferenceCounting<Problem> Pb)
{
  __problem = Pb;

  switch(__discretizationType.type()) {
  case DiscretizationType::spectralLegendre: {
    this->__discretize<DiscretizationType::spectralLegendre>();
    return;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "Discretization type not implemented",
		       ErrorHandler::normal);
  }
  }
}

void SpectralMethod::Compute (Solution& U)
{
  PDESolution& u = static_cast<PDESolution&>(U);
  KrylovSolver K(*__A, *__b, __degreeOfFreedomSet);
  K.solve(problem(), u.values());
}


