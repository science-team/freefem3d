//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionReaderBase.hpp,v 1.2 2007/06/10 15:03:07 delpinux Exp $

#ifndef SCALAR_FUNCTION_READER_BASE_HPP
#define SCALAR_FUNCTION_READER_BASE_HPP

#include <ReferenceCounting.hpp>

#include <string>

class ScalarFunctionBase;
class Mesh;

/**
 * @file   ScalarFunctionReaderBase.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 11:25:16 2006
 * 
 * @brief  base class for function file reader 
 * 
 */
class ScalarFunctionReaderBase
{
protected:
  const std::string __filename;	/**< file name */

  ConstReferenceCounting<Mesh>
  __mesh;			/**< mesh associated with the
				   function */

  const std::string
  __functionName;		/**< function name */

  const size_t __componentNumber; /**< component number */

public:
  /** 
   * Reads the function in the file file
   * 
   * @return read function
   */
  virtual ConstReferenceCounting<ScalarFunctionBase>
  getFunction() const=0;

  /** 
   * Constructor
   * 
   * @param filename file name
   * @param mesh given mesh
   * @param functionName function name
   * @param componentNumber component number
   */
  ScalarFunctionReaderBase(const std::string& filename,
			   ConstReferenceCounting<Mesh> mesh,
			   const std::string& functionName,
			   const size_t& componentNumber);

  /** 
   * Copy constructor
   * 
   * @param reader given function reader
   */
  ScalarFunctionReaderBase(const ScalarFunctionReaderBase& reader);

  /** 
   * Destructor
   * 
   */
  virtual ~ScalarFunctionReaderBase();
};

#endif // SCALAR_FUNCTION_READER_BASE_HPP
