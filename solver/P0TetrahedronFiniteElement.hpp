//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: P0TetrahedronFiniteElement.hpp,v 1.2 2006/11/11 18:25:21 delpinux Exp $

#ifndef P0_TETRAHEDRON_FINITE_ELEMENT_HPP
#define P0_TETRAHEDRON_FINITE_ELEMENT_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <QuadratureFormula.hpp>
#include <ConformTransformation.hpp>

#include <LagrangianFiniteElement.hpp>
#include <ThreadStaticBase.hpp>

class P0TetrahedronFiniteElement
  : public ThreadStaticBase<P0TetrahedronFiniteElement>,
    public LagrangianFiniteElement<1, P0TetrahedronFiniteElement,
				   QuadratureFormulaP0Tetrahedron>
{
private:
  static TinyVector<3, real_t> __massCenter; /**< mass center of the reference element */

public:
  enum {
    numberOfDegreesOfFreedom = 1,
    numberOfVertexDegreesOfFreedom = 0,
    numberOfEdgeDegreesOfFreedom = 0,
    numberOfFaceDegreesOfFreedom = 0,
    numberOfVolumeDegreesOfFreedom = 1,
    numberOfFaceLivingDegreesOfFreedom = 0 // degrees of freedom carried by a face
  };

  /**
   * Degrees of freedom living on faces
   * 
   * @note the 1 is here for template compatbility
   */
  static const size_t facesDOF[Tetrahedron::NumberOfFaces][1];

  /** 
   * returns the mass center of the reference element
   * 
   * @return __massCenter
   */
  static const TinyVector<3, real_t>& massCenter()
  {
    return __massCenter;
  }


  real_t W  (const size_t& i, const TinyVector<3>& X) const;
  real_t dxW(const size_t& i, const TinyVector<3>& X) const;
  real_t dyW(const size_t& i, const TinyVector<3>& X) const;
  real_t dzW(const size_t& i, const TinyVector<3>& X) const;

  inline const
  TinyVector<QuadratureType::numberOfQuadraturePoints,
	     TinyVector<3> >&
  integrationVertices() const
  {
    return QuadratureType::instance().vertices();
  }

  P0TetrahedronFiniteElement()
  {
    ;
  }

  ~P0TetrahedronFiniteElement()
  {
    ;
  }
};

#endif // P0_TETRAHEDRON_FINITE_ELEMENT_HPP
