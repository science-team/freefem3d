//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FiniteElementMethod.hpp,v 1.3 2005/12/04 16:54:51 delpinux Exp $

#ifndef FINITE_ELEMENT_METHOD_HPP
#define FINITE_ELEMENT_METHOD_HPP

#include <DegreeOfFreedomSet.hpp>

#include <BaseMatrix.hpp>
#include <BaseVector.hpp>

#include <Method.hpp>

#include <Problem.hpp>

#include <Solution.hpp>

#include <Mesh.hpp>

/**
 * @file   FiniteElementMethod.hpp
 * @author Stephane Del Pino
 * @date   Wed Jan  8 19:25:01 2003
 * 
 * @brief  This is the standard Finite element method
 * 
 */

class FiniteElementMethod
  : public Method
{
private:
  ReferenceCounting<BaseMatrix> __A;
  ReferenceCounting<BaseVector> __b;

  ReferenceCounting<Mesh> __mesh;

  ConstReferenceCounting<Problem> __problem;

  const DegreeOfFreedomSet& __degreeOfFreedomSet;

  template <typename MeshType,
	    DiscretizationType::Type TypeOfDiscretization>
  void __discretizeOnMesh();

  template <DiscretizationType::Type TypeOfDiscretization>
  void __discretize();

public:

  void Discretize (ConstReferenceCounting<Problem> Pb);

  void Compute ( Solution& u );

  const Problem& problem() const
  {
    return *__problem;
  }

  Mesh& mesh()
  {
    return *__mesh;
  }

  FiniteElementMethod(const DiscretizationType& discretizationType,
		      ReferenceCounting<Mesh> M,
		      const DegreeOfFreedomSet& dOfFreedom)
    : Method(discretizationType),
      __mesh(M),
      __degreeOfFreedomSet(dOfFreedom)
  {
    ;
  }

  ~FiniteElementMethod()
  {
    ;
  }
};


#endif // FINITE_ELEMENT_METHOD_HPP
