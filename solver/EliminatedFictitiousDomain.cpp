//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: EliminatedFictitiousDomain.cpp,v 1.9 2007/05/20 23:15:36 delpinux Exp $

#include <EliminatedFictitiousDomain.hpp>
#include <BoundaryConditionDiscretizationElimination.hpp>

#include <ScalarFunctionBase.hpp>
#include <Mesh.hpp>

template <DiscretizationType::Type TypeOfDiscretization,
	  typename MeshType>
ReferenceCounting<BoundaryConditionDiscretization>
EliminatedFictitiousDomain::__discretizeBoundaryConditionsOnMesh()
{
  BoundaryConditionDiscretizationElimination<MeshType,TypeOfDiscretization>* bcDiscretization
    = new BoundaryConditionDiscretizationElimination<MeshType,
                                                     TypeOfDiscretization>(problem(),
									   dynamic_cast<MeshType&>(mesh()),
									   __degreeOfFreedomSet);
  (*bcDiscretization).associatesMeshesToBoundaryConditions();
  return bcDiscretization;
}


template <DiscretizationType::Type TypeOfDiscretization>
ReferenceCounting<BoundaryConditionDiscretization>
EliminatedFictitiousDomain::__discretizeBoundaryConditions()
{
  switch (mesh().type()) {
  case Mesh::cartesianHexahedraMesh: {
    return this->__discretizeBoundaryConditionsOnMesh<TypeOfDiscretization, Structured3DMesh>();
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "this mesh type is not supported by FDM penalty",
		       ErrorHandler::normal);
    return 0;
  }
  }
}

ReferenceCounting<BoundaryConditionDiscretization>
EliminatedFictitiousDomain::discretizeBoundaryConditions()
{
  switch (this->__discretizationType.type()) {
  case DiscretizationType::lagrangianFEM1: {
    return this->__discretizeBoundaryConditions<DiscretizationType::lagrangianFEM1>();
    break;
  }
  case DiscretizationType::lagrangianFEM2: {
    return this->__discretizeBoundaryConditions<DiscretizationType::lagrangianFEM2>();
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "discretization not supported in Penalty",
		       ErrorHandler::unexpected);
    return 0;
  }
  }
}
