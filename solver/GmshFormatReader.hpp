//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: GmshFormatReader.hpp,v 1.5 2007/03/08 23:48:26 delpinux Exp $

#ifndef GMSH_FORMAT_READER_HPP
#define GMSH_FORMAT_READER_HPP

#include <MeshReader.hpp>
#include <string>

#include <ReferenceCounting.hpp>

#include <VerticesSet.hpp>

#include <Vector.hpp>
#include <Vertex.hpp>

#include <Hexahedron.hpp>
#include <Tetrahedron.hpp>
#include <Quadrangle.hpp>
#include <Triangle.hpp>

#include <set>
#include <vector>
#include <cstdio>

/**
 * @file   GmshFormatReader.hpp
 * @author Stephane Del Pino
 * @date   Tue Dec 21 23:40:21 2004
 * 
 * @brief  Reads mesh from file in the 'gmsh' format.
 * 
 */

class GmshFormatReader
  : public MeshReader
{
private:
  struct VertexError {};

  /** 
   * Gmsh format provides a numbered, none ordrered and none dense
   * vertices list, this stores the number of read vertices.
   */
  std::vector<int> __verticesNumbers;

  /** 
   * Gmsh format provides a numbered, none ordrered and none dense
   * vertices list, this provides vertices renumbering correspondance
   */
  std::vector<int> __verticesCorrepondance;

  /** 
   * elements types
   */
  std::vector<short> __elementType;

  /** 
   * References
   */
  std::vector<int> __references;

  /** 
   * References
   */
  std::vector<std::vector<int> > __elementVertices;

  /** 
   * Stores the number of nodes associated to each primitive
   * 
   */
  std::vector<int> __numberOfPrimitiveNodes;

  /** 
   * Array of boolean describing gmsh supported primitives by ff3d
   * 
   */
  TinyVector<15, bool> __supportedPrimitives;

  /** 
   * Primitives names according to there number
   * 
   */
  std::map<int, std::string> __primitivesNames;

  /** 
   * Adapts gmsh data to ff3d's structures 
   * 
   */
  void __proceedData();

  /** 
   * Reads data in format 1.0
   * 
   */
  void __readGmshFormat1();

  /** 
   * Reads data in format 2.0
   * 
   */
  void __readGmshFormat2();

  /**
   * List of allowed keyword in mesh file
   * 
   */
  enum KeywordType {
    // gmsh 1.0
    NOD,
    ENDNOD,
    ELM,
    ENDELM,

    // gmsh 2.0
    MESHFORMAT,
    ENDMESHFORMAT,
    NODES,
    ENDNODES,
    ELEMENTS,
    ENDELEMENTS,

    Unknown,
    EndOfFile
  };

  /**
   * List of known keywords
   * 
   */
  typedef std::map<std::string, int> KeywordList;

  KeywordList __keywordList;	/**< The keyword list */

  /**
   * Type for keyword
   * 
   */
  typedef std::pair<std::string, int> Keyword;

  /** 
   * Skips next comments if exists
   * 
   */
  void __skipComments();

  /** 
   * Reads the next keyword and returns its token
   * 
   * @return KeywordToken
   */
  GmshFormatReader::Keyword __nextKeyword();

  /** 
   * get list of vertices
   * 
   */
  void __getVertices();

  /**
   * Read list of vertices
   * 
   */
  void __readVertices();

  /**
   * Read all elements in format 1.0
   * 
   */
  void __readElements1();

  /**
   * Read all elements in format 2.0
   * 
   */
  void __readElements2();

  /** 
   * Copy constructor is forbidden
   * 
   * @param M a given GmshFormatReader
   */
  GmshFormatReader(const GmshFormatReader& M);

  /** 
   * Common interface for writing references
   * 
   * @param references the set of computed references
   * @param objectName the type of refernces
   */
  void __writeReferences(const std::set<size_t>& references,
			 std::string objectName);
public:

  /** 
   * Constructor
   * 
   * @param s the filename
   */
  GmshFormatReader(const std::string & s);

  /** 
   * Destructor
   * 
   */
  ~GmshFormatReader()
  {
    ;
  }
};

#endif // GMSH_FORMAT_READER_HPP
