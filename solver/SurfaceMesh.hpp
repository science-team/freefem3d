//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SurfaceMesh.hpp,v 1.11 2006/09/20 16:41:38 delpinux Exp $

#ifndef SURFACEMESH_HPP
#define SURFACEMESH_HPP

#include <Mesh.hpp>

/**
 * @file   SurfaceMesh.hpp
 * @author Stephane Del Pino
 * @date   Sat Dec 13 17:52:38 2003
 * 
 * @brief  mother class for surface meshes
 * 
 * 
 */
class SurfaceMesh
  : public Mesh
{
public:
  enum {
    family = Mesh::surface
  };

protected:
  const Mesh* __backgroundMesh;		/**< Sets which is background mesh */

  template <typename CellType>
  void __computesFictitiousCells(const Vector<CellType>& cells) const
  {
    for (size_t i=0; i<cells.size(); ++i) {
      cells[i].setFictitious(cells[i].mother().isFictitious());
    }
  }

  SurfaceMesh(const SurfaceMesh& s); /**< Forbids copy constructor */
public:

  /** 
   * Faces and edges are treated as the same for surfaces meshes
   * 
   */
  void buildFaces()
  {
    this->buildEdges();
  }

  /** 
   * Computes fictitious cells (ie: cells that will not be taken into
   * account for computation)
   * 
   */
  virtual void computesFictitiousCells() const = 0;

  /** 
   * tests if a given mesh is the mesh associated to the surface mesh
   * 
   * @param m a given mesh
   * 
   * @return true if m is the one!
   */
  bool isAssociatedTo(const Mesh& m) const
  {
    return &m == __backgroundMesh;
  }

  /** 
   * Sets the background mesh
   * 
   * @param m a pointer to a given background mesh
   */
  void setBackgroundMesh(const Mesh* m)
  {
    __backgroundMesh = m;
  }

  /** 
   * gets the pointer to the background mesh
   * 
   * @return __backgroundMesh
   */
  const Mesh* backgroundMesh() const
  {
    return __backgroundMesh;
  }

  /** 
   * Creates a surface mesh providing its type
   * 
   * @param t the surface mesh type
   * 
   */
  SurfaceMesh(const Mesh::Type t)
    : Mesh(t, Mesh::surface, 0),
      __backgroundMesh(0)
  {
    ;
  }

  /** 
   * Returns true if the faces set has been built
   * 
   * @return true if __edgesSet is not NULL
   */
  inline bool hasFaces() const
  {
    return __edgesSet != 0;
  }

  /** 
   * 
   * Returns the number of faces
   * 
   * @return number of faces
   */
  inline const size_t& numberOfFaces() const
  {
    return __edgesSet->numberOfEdges();
  }

  /** 
   * Creates a surface mesh providing its type and a set of vertices
   * 
   * @param t the surface mesh type
   * @param vertices a set of vertices
   * @param correspondance vertices association table
   * 
   */
  SurfaceMesh(const Mesh::Type t,
	      ReferenceCounting<VerticesSet> vertices,
	      ReferenceCounting<VerticesCorrespondance> correspondances)
    : Mesh(t,
	   Mesh::surface,
	   vertices,
	   correspondances),
      __backgroundMesh(0)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~SurfaceMesh()
  {
    ;
  }
};

#endif // SURFACEMESH_HPP

