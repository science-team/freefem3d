//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDEOperator.hpp,v 1.4 2006/07/20 19:08:54 delpinux Exp $


#ifndef PDE_OPERATOR_HPP
#define PDE_OPERATOR_HPP

#include <ReferenceCounting.hpp>
#include <ScalarFunctionBase.hpp>

#include <string>
/**
 * @file   PDEOperator.hpp
 * @author St�phane Del Pino
 * @date   Mon Jun  9 23:22:45 2003
 * 
 * @brief  base PDE operators class
 * 
 * The class PDEOperator is the base class for PDE operators
 */
class UserFunction;
class PDEOperator
{
public:
  enum Type {
    firstorderop,
    firstorderopTransposed,
    divmugrad,
    secondorderop,
    massop
  };

protected:
  const PDEOperator::Type __type; /**< type of the operator */

  size_t __numberOfSubOperators; /**< This is used for "composed"
				    operators to provied an efficient
				    way to know how many elementary
				    matrices are needed to discretize
				    it */
public:
  /** 
   * Read only access to the number of sub-operators
   * 
   * @return __numberOfSubOperators
   */
  const size_t& numberOfSubOperators() const
  {
    return __numberOfSubOperators;
  }

  /** 
   * Return the name of the PDEOperator
   * 
   * @return a string of the name of the operator
   */
  virtual std::string
  typeName() const = 0;

  /** 
   * Read-only access to the type of the operator
   * 
   * @return __type
   */
  const PDEOperator::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Each operator can be "multiplied" by a coefficient @a c
   * 
   * @param c the coefficient
   * 
   * @return the new operator
   */
  virtual ConstReferenceCounting<PDEOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const=0; 

  /** 
   * Each operator should know to compute its opposite
   * 
   * @return the opposite of the operator
   */
  virtual ConstReferenceCounting<PDEOperator>
  operator-() const=0; 

  /** 
   * Writes a PDEOperator to a stream
   * 
   * @param os the output stream
   * @param pdeOperator a PDEOperator
   * 
   * @return os
   */
  friend std::ostream&
  operator << (std::ostream& os, const PDEOperator& pdeOperator)
  {
    os << pdeOperator.typeName();
    return os;
  }

  /** 
   * Constructor
   * 
   * @param type the type of the operator
   * @param theNumberOfSubOperators the number of sub-operators
   */
  PDEOperator(const PDEOperator::Type& type,
	      const size_t& numberOfSubOperators)
    : __type(type),
      __numberOfSubOperators(numberOfSubOperators)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param P a given PDEOperator
   */
  PDEOperator(const PDEOperator& P)
    : __type(P.__type),
      __numberOfSubOperators(P.__numberOfSubOperators)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~PDEOperator()
  {
    ;
  }
};

#endif // PDE_OPERATOR_HPP
