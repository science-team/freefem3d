//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 Driss Yakoubi

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: GaussLobatto.hpp,v 1.2 2007/06/09 10:37:07 delpinux Exp $

#ifndef GAUSS_LOBATTO_HPP
#define GAUSS_LOBATTO_HPP

#include <Vector.hpp>
#include <Interval.hpp>
#include <Types.hpp>

/**
 * @file   GaussLobatto.hpp
 * @author Dris Yakoubi
 * @date   Sun Apr 29 20:35:40 2007
 * 
 * @brief  Manages Gauss-Lobatto quadratures on @f$ ]-1,1[ @f$
 * 
 * Formulae are exact for polynomials of degree @f$ 2d-1@f$
 */
class GaussLobatto
{
private:
  const size_t __degree;	/**< degree @f$ d @f$ */

  Vector<real_t> __vertices;	/**< nodes for the quadrature */
  Vector<real_t> __weights;	/**< weights for the quadrature  */

  /** 
   * Bisection to compute recursively qudrature vertices
   * 
   * @param interval interval of research
   * @param root root of the polynom
   */
  void __bisection(const Interval& interval,
		   real_t& root);

  /** 
   * Forbiddes the copy constructor
   * 
   */
  GaussLobatto(const GaussLobatto&);
public:
  /** 
   * Read-only access to the number of quadrature nodes
   * 
   * @return number of quadrature points
   */
  size_t numberOfPoints() const
  {
    return __vertices.size();
  }

  /** 
   * Read-only access to the @a i-th quadrature point
   * 
   * @param i the number of the quadrature point
   * 
   * @return the quadrature point @f$ x_i @f$
   */
  real_t operator() (const size_t& i) const
  {
    return __vertices[i];
  }

  /** 
   * Read-only access to the weight of the @a i-th quadrature point
   * 
   * @param i the number of the quadrature point
   * 
   * @return @f$ \rho_i @f$
   */
  real_t weight(const size_t& i) const
  {
    return __weights[i];
  }

  /** 
   * Constructor
   * 
   * @param d the degree @f$ d @f$
   * @param computeWeight specifies if weight should be calculated
   */
  GaussLobatto(const size_t& d,
	       const bool computeWeight = true);

  /** 
   * Destructor
   * 
   */
  ~GaussLobatto()
  {
    ;
  }
};

#endif // GAUSS_LOBATTO_HPP
