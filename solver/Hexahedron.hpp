//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Hexahedron.hpp,v 1.7 2005/04/20 22:09:41 delpinux Exp $

#ifndef HEXAHEDRON_HPP
#define HEXAHEDRON_HPP

/**
 * @file   Hexahedron.hpp
 * @author St�phane Del Pino
 * @date   Sat Jul 27 17:01:33 2002
 * 
 * @brief  describes Hexaheron (using 8 vertices)
 * 
 * @todo use numberOfVertices() in constructor instead of 8 when compiler allows it.
 */

#include <Cell.hpp>
#include <Quadrangle.hpp>

class Hexahedron
  : public Cell
{
public:
  enum {
    NumberOfVertices   = 8,	/**< number of vertices */
    NumberOfFaces      = 6,	/**< number of faces */
    NumberOfEdges      = 12	/**< number of edges */
  };

  typedef Quadrangle FaceType;

  static const size_t faces[NumberOfFaces][FaceType::NumberOfVertices];
  static const size_t edges[NumberOfEdges][Edge::NumberOfVertices];

  virtual Cell::Type type() const
  {
    return Cell::hexahedron;
  }

  //! Number of Vertices.
  size_t numberOfVertices() const
  {
    return NumberOfVertices;
  }

  //! Number of Edges.
  size_t numberOfEdges() const
  {
    return NumberOfEdges;
  }

  //! sets equal to a given tetrahedron
  inline const Hexahedron& operator=(const Hexahedron& H)
  {
    Cell::operator=(H);
    return *this;
  }

  //! Default constructor does nothing but reserving memory
  Hexahedron()
    : Cell(Hexahedron::NumberOfVertices)
  {
    ;
  }

  Hexahedron(Vertex& x0,
	     Vertex& x1,
	     Vertex& x2,
	     Vertex& x3,
	     Vertex& x4,
	     Vertex& x5,
	     Vertex& x6,
	     Vertex& x7,
	     const size_t& ref = 0);

  ~Hexahedron()
  {
    ;
  }
};

#endif // HEXAHEDRON_HPP

