//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: CartesianHexahedron.hpp,v 1.9 2005/04/20 22:09:41 delpinux Exp $

#ifndef CARTESIAN_HEXAHEDRON_HPP
#define CARTESIAN_HEXAHEDRON_HPP

/**
 * @file   CartesianHexahedron.hpp
 * @author Stephane Del Pino
 * @date   Wed Nov 27 13:12:18 2002
 * 
 * @brief  Rectangular Haxaheron
 * 
 * 
 */

#include <Cell.hpp>
#include <Quadrangle.hpp>

class CartesianHexahedron
  : public Cell
{
public:
  enum {
    NumberOfVertices   = 8,	/**< number of vertices */
    NumberOfFaces      = 6,	/**< number of faces */
    NumberOfEdges      = 12	/**< number of edges */
  };

  typedef Quadrangle FaceType;

  static const size_t faces[NumberOfFaces][FaceType::NumberOfVertices];
  static const size_t edges[NumberOfEdges][Edge::NumberOfVertices];

  virtual Cell::Type type() const
  {
    return Cell::cartesianHexahedron;
  }

  /** 
   * computes local coordinates in the current hexahedron related to a
   * given \a X position, in the basis of the reference element.
   * 
   * @param X Cartesian coordinates
   * @param lambda local coordinates
   *
   */
  void getLocalCoordinates(const TinyVector<3, real_t>& X,
			   TinyVector<3, real_t>& lambda) const
  {
    const TinyVector<3>& a = *__vertices[0];
    const TinyVector<3>& b = *__vertices[6];
    lambda = X-a;
    for (size_t i=0; i<3; ++i) {
      lambda[i] /= b[i]-a[i];
    }
  }

  //! Number of Vertices.
  size_t numberOfVertices() const
  {
    return NumberOfVertices;
  }

  //! Number of Edges.
  size_t numberOfEdges() const
  {
    return NumberOfEdges;
  }

  //! sets equal to a given CartesianHexahedrons.
  inline const CartesianHexahedron& operator=(const CartesianHexahedron& H)
  {
    Cell::operator=(H);
    return *this;
  }

  //! Default constructor does nothing but reserving memory
  CartesianHexahedron()
    : Cell(NumberOfVertices)
  {
    ;
  }

  CartesianHexahedron(Vertex& x0,
		      Vertex& x1,
		      Vertex& x2,
		      Vertex& x3,
		      Vertex& x4,
		      Vertex& x5,
		      Vertex& x6,
		      Vertex& x7)
    : Cell(NumberOfVertices)
  {
    __vertices[0] = &x0;
    __vertices[1] = &x1;
    __vertices[2] = &x2;
    __vertices[3] = &x3;
    __vertices[4] = &x4;
    __vertices[5] = &x5;
    __vertices[6] = &x6;
    __vertices[7] = &x7;

    const TinyVector<3>& a = *__vertices[0];
    const TinyVector<3>& b = *__vertices[6];

    const real_t hx = b[0] - a[0];
    const real_t hy = b[1] - a[1];
    const real_t hz = b[2] - a[2];

    __volume = hx*hy*hz;
  }

  ~CartesianHexahedron()
  {
    ;
  }
};

#endif // CARTESIAN_HEXAHEDRON_HPP

