//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Reference.hpp,v 1.5 2007/06/09 10:37:07 delpinux Exp $

#ifndef REFERENCE_HPP
#define REFERENCE_HPP

#include <vector>
/**
 * @file   Reference.hpp
 * @author Stephane Del Pino
 * @date   Wed Jun 19 18:32:59 2002
 * 
 * @brief  References
 * 
 * 
 */

class Reference
{
public:
  typedef std::vector<bool> Representation;

private:
  Reference::Representation __reference;

  bool __insideDomain;

public:

  const bool& insideDomain() const
  {
    return __insideDomain;
  }

  bool& insideDomain()
  {
    return __insideDomain;
  }

  void resize(const size_t i)
  {
    __reference.resize(i);
  }

  inline size_t size() const
  {
    return __reference.size();
  }

  void set(const Representation& r)
  {
    ASSERT(r.size() == __reference.size());
    __reference = r;
  }

  bool isSet() const
  {
    for(Representation::const_iterator i = __reference.begin();
	i != __reference.end(); ++i) {
      if (*i) {
	return true;
      }
    }
    return false;  
  }

  bool isSet(const Reference::Representation& r) const
  {
    ASSERT(r.size() == __reference.size());

    for (size_t i=0; i<r.size(); ++i) {
      if (r[i] && !__reference[i])
	return false;
    }
    return true;
  }

  bool operator == (const Reference& R) const
  {
    return (__reference == R.__reference);
  }

  bool operator != (const Reference& R) const
  {
    return (__reference != R.__reference);
  }

  const Reference& operator = (const size_t& i)
  {
    __reference[i] = true;
    return *this;
  }

  const Reference& operator = (const Reference& R)
  {
    __reference = R.__reference;
    return *this;
  }

  inline void set(const size_t& i, const bool& b)
  {
    ASSERT(i<__reference.size());
    __reference[i] = b;
  }


  inline bool get(const size_t& i) const
  {
    ASSERT(i<__reference.size());
    return __reference[i];
  }

  /**
     This function writes the reference of the object using a binary
     representation.
  */
  friend std::ostream& operator<<(std::ostream& os,
				  const Reference& R)
  {
    size_t ref=0;
    for (size_t i = R.__reference.size()-1; i<R.__reference.size(); --i) {
      ref <<= 1;
      ref |= (R.__reference[i])?1:0;
    }
    
    os << ref;
    return os;
  }


  Reference()
  {
    ;
  }

  Reference(const Reference& R)
    : __reference(R.__reference)
  {
    ;
  }

  Reference(const size_t size)
  {
    __reference.resize(size);
  }

  ~Reference()
  {
    ;
  }
};

#endif // REFERENCE_HPP

