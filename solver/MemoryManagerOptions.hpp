//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MemoryManagerOptions.hpp,v 1.3 2007/06/09 10:37:07 delpinux Exp $

#ifndef _MEMORY_MANAGER_OTIONS_HPP_
#define _MEMORY_MANAGER_OTIONS_HPP_

class MemoryManagerOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  enum MatrixType {
    none,
    sparse
  };

  static const char* identifier()
  {
    // autodoc: "sets memory management options"
    return "memory";
  }

  MatrixType matrixType()
  {
    MemoryManagerOptions::MatrixType __t = none;
    get("matrix", __t);
    return __t;
  }

  explicit MemoryManagerOptions()
  {
    // autodoc: "sets matrix type"
    EnumParameter<MatrixType>* E
      = new EnumParameter<MatrixType>(MemoryManagerOptions::sparse,
				      "matrix");

    // autodoc: "used for sparse matrices, cost is approximatly $27\times n_v\times {n_u}^2$, where $n_v$ is the number of vertices and $n_u$ the number of unknown (for a $Q_1$ discretization)"
    (*E).addSwitch("sparse",
		   MemoryManagerOptions::sparse);

    // autodoc: "do not store the matrix. Cost no memory, but is slower"
    (*E).addSwitch("none",
		   MemoryManagerOptions::none);
    add(E);
  }
};

#endif // _MEMORY_MANAGER_OTIONS_HPP_

