//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: BoundaryConditionDiscretizationFEM.hpp,v 1.7 2005/09/25 18:04:32 delpinux Exp $

#ifndef BOUNDARY_CONDITION_DISCRETIZATION_FEM_HPP
#define BOUNDARY_CONDITION_DISCRETIZATION_FEM_HPP

#include <BoundaryConditionDiscretization.hpp>

#include <BoundaryConditionCommonFEMDiscretization.hpp>
#include <SurfaceMesh.hpp>

#include <UnAssembledMatrix.hpp>

#include <ErrorHandler.hpp>

/**
 * @file   BoundaryConditionDiscretizationFEM.hpp
 * @author Stephane Del Pino
 * @date   Tue Nov 11 20:05:20 2003
 * 
 * @brief  Finite Element boundary conditions discretization
 * 
 */
template <typename MeshType,
	  DiscretizationType::Type TypeOfDiscretization>
class BoundaryConditionDiscretizationFEM
  : public BoundaryConditionCommonFEMDiscretization<MeshType,
						    TypeOfDiscretization>
{
public:
  void getDiagonal (BaseVector& Z) const
  {
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);

    // Natural Boundary conditions
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>
      ::__StandardGetDiagonalVariationalBorderBilinearOperator(*this, z);
    // Dirichlet on the mesh border ...
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>
      ::__StandardGetDiagonalDirichletBorderBilinearOperator(*this, z);
  }

  void setMatrix (ReferenceCounting<BaseMatrix> givenA,
		  ReferenceCounting<BaseVector> b) const
  {
    switch((*givenA).type()) {
    case BaseMatrix::doubleHashedMatrix: {

      DoubleHashedMatrix& A = dynamic_cast<DoubleHashedMatrix&>(*givenA);

      // Variational Problem's Natural BC
      BoundaryConditionCommonFEMDiscretization<MeshType,
                                               TypeOfDiscretization>
	::__StandardVariationalBorderBilinearOperator(*this, A);

      // Dirichlet on the mesh border ...
      BoundaryConditionCommonFEMDiscretization<MeshType,
                                               TypeOfDiscretization>
	::__StandardDirichletBorderBilinearOperator(*this, A);
      break;
    }

    case BaseMatrix::unAssembled: {
      UnAssembledMatrix& A = dynamic_cast<UnAssembledMatrix&>(*givenA);
      A.setBoundaryConditions(this);
      break;
    }

    default: {
      throw ErrorHandler(__FILE__,__LINE__,
			 "unexected matrix type",
			 ErrorHandler::unexpected);
    }
    }
  }

  void setSecondMember (ReferenceCounting<BaseMatrix> givenA,
			ReferenceCounting<BaseVector> givenB) const
  {
    Vector<real_t>& b = dynamic_cast<Vector<real_t>&>(*givenB);
    BaseMatrix& A = *givenA;

    //! Natural boundary conditions
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>::
      __StandardVariationalBorderLinearOperator(*this, b);

    //! Dirichlet Boundary conditions
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>::
      __StandardDirichletBorderLinearOperator(*this, A, b);
  }

  void timesX(const BaseVector& X, BaseVector& Z) const
  {
    const Vector<real_t>& x = dynamic_cast<const Vector<real_t>&>(X);
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);

    // Natural Boundary Conditions.
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>::
      __StandardVariationalBorderBilinearOperatorTimesX(*this,
							x, z);

    // Dirichlet on the mesh border ...
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>::
      __StandardDirichletBorderBilinearOperatorTimesX(*this,
						      x, z);
  }

  void transposedTimesX(const BaseVector& X, BaseVector& Z) const
  {
    const Vector<real_t>& x = dynamic_cast<const Vector<real_t>&>(X);
    Vector<real_t>& z = dynamic_cast<Vector<real_t>&>(Z);

    // Natural Boundary Conditions.
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>::
      __StandardVariationalBorderBilinearOperatorTransposedTimesX(*this,
								  x, z);

    // Dirichlet on the mesh border ...
    BoundaryConditionCommonFEMDiscretization<MeshType,
                                             TypeOfDiscretization>::
      __StandardDirichletBorderBilinearOperatorTransposedTimesX(*this,
								x, z);
  }

  BoundaryConditionDiscretizationFEM(const Problem& problem,
				     const MeshType& mesh,
				     const DegreeOfFreedomSet& dof)
    : BoundaryConditionCommonFEMDiscretization<MeshType,
					       TypeOfDiscretization>(problem, mesh, dof)
  {
    ;
  }

  BoundaryConditionDiscretizationFEM(const BoundaryConditionDiscretizationFEM<MeshType, TypeOfDiscretization>& b)
    : BoundaryConditionCommonFEMDiscretization<MeshType,
					       TypeOfDiscretization>(b)
  {
    ;
  }

  ~BoundaryConditionDiscretizationFEM()
  {
    ;
  }
};

#endif // BOUNDARY_CONDITION_DISCRETIZATION_FEM_HPP
