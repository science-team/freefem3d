//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MassOperator.hpp,v 1.7 2006/07/20 19:08:54 delpinux Exp $


#ifndef MASS_OPERATOR_HPP
#define MASS_OPERATOR_HPP

#include <PDEOperator.hpp>

/**
 * @file   MassOperator.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 19:05:51 2006
 * 
 * @brief describes partial differencial operators of order 0
 * 
 */
class MassOperator
  : public PDEOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __Alpha;			/**< order 0 coefficient */

public:
  /** 
   * gets operator name
   * 
   * @return "MassOperator"
   */
  std::string typeName() const
  {
    return "MassOperator";
  }

  /** 
   * Read only access to the coefficient
   * 
   * @return  @f$ \alpha @f$.
   */
  ConstReferenceCounting<ScalarFunctionBase>
  alpha() const
  {
    return __Alpha;
  }

  /** 
   * "multiplies" the mass MassOperator by a coefficient
   * 
   * @param c the coefficient
   * 
   * @return @f$ (c\alpha)\cdot @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator*(const ConstReferenceCounting<ScalarFunctionBase>& c) const
  {
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__Alpha);
    functionBuilder.setBinaryOperation(BinaryOperation::product,c);
    return new MassOperator(functionBuilder.getBuiltFunction());
  }

  /** 
   * gets the opposed MassOperator operator.
   * 
   * @return @f$ -\alpha\cdot @f$
   */
  ConstReferenceCounting<PDEOperator>
  operator-() const
  {
    ScalarFunctionBuilder functionBuilder;
    functionBuilder.setFunction(__Alpha);
    functionBuilder.setUnaryMinus();
    return new MassOperator(functionBuilder.getBuiltFunction());
  }

  /** 
   * Constructor
   * 
   * @param alpha given coefficient
   */
  MassOperator(ConstReferenceCounting<ScalarFunctionBase> alpha)
    : PDEOperator(PDEOperator::massop,
		  1),
      __Alpha(alpha)
  {
    ;
  }
  
  /** 
   * Copy constructor
   * 
   * @param M given mass operator
   */
  MassOperator(const MassOperator& M)
    : PDEOperator(M),
      __Alpha(M.__Alpha)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~MassOperator()
  {
    ;
  }
};

#endif // MASS_OPERATOR_HPP
