//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshReaderAM_FMTFormat.cpp,v 1.1 2006/09/15 06:55:11 delpinux Exp $

#include <MeshReaderAM_FMTFormat.hpp>

#include <Stringify.hpp>

#include <set>
#include <sstream>

void MeshReaderAM_FMTFormat::
__checkVertex(const size_t& n)
{
  if ((n == 0) or (n > (*__vertices).numberOfVertices())) {
    throw MeshReaderAM_FMTFormat::VertexError();
  }
}


size_t MeshReaderAM_FMTFormat::
__getVertexNumber()
{
  size_t n = this->__getInteger();
  this->__checkVertex(n);

  return n;
}

void MeshReaderAM_FMTFormat::
__readTriangles()
{
  ffout(4) << "  - reading triangles\n";
  VerticesSet& V = *__vertices;
  Vector<Triangle>& T = *__triangles;

  size_t i;
  try {
    for (i=0; i<T.size(); ++i) {
      const size_t a = __getVertexNumber() - 1;
      const size_t b = __getVertexNumber() - 1;
      const size_t c = __getVertexNumber() - 1;

      T[i] = Triangle(V[a],V[b],V[c], 0);
    }
  }
  catch (MeshReaderAM_FMTFormat::VertexError e) {
    throw MeshReader::Error("invalid vertex number reading triangle "+stringify(i));
  }
}

void MeshReaderAM_FMTFormat::
__readTrianglesReferences()
{
  ffout(4) << "  - reading triangles'references\n";
  Vector<Triangle>& T = *__triangles;
  for (size_t i=0; i<T.size(); ++i) {
    T[i].reference() = this->__getInteger();
  }
}

void MeshReaderAM_FMTFormat::
__readVertices()
{
  ffout(4) << "  - reading vertices\n";
  VerticesSet& V = *__vertices;
  for (size_t i=0; i<V.numberOfVertices(); ++i) {
    const double x   = __getReal();
    const double y   = __getReal();
    V[i] = Vertex(x,y,0);
  }
}

void MeshReaderAM_FMTFormat::
__readVerticesReferences()
{
  ffout(4) << "  - reading vertices'references\n";
  VerticesSet& V = *__vertices;
  for (size_t i=0; i<V.numberOfVertices(); ++i) {
    V[i].reference() = this->__getInteger();
  }
}

MeshReaderAM_FMTFormat::
MeshReaderAM_FMTFormat(const std::string& s)
  : MeshReader(s)
{

  ffout(3) << "\n";
  ffout(3) << "Reading file " << s << '\n';
  
  const size_t numberOfVertices  = this->__getInteger();
  const size_t numberOfTriangles = this->__getInteger();

  ffout(3) << "- Number of vertices: " << numberOfVertices << '\n';
  ffout(3) << "- Number of triangles: " << numberOfTriangles << '\n';

  __vertices = new VerticesSet(numberOfVertices);
  __triangles= new Vector<Triangle>(numberOfTriangles);

  this->__readTriangles();
  this->__readVertices();
  this->__readTrianglesReferences();
  this->__readVerticesReferences();

  this->__createMesh();
}
