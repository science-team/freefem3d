//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Q1HexahedronFiniteElement.hpp,v 1.2 2006/05/20 15:22:46 delpinux Exp $

#ifndef Q1_HEXAHEDRON_FINITE_ELEMENT_HPP
#define Q1_HEXAHEDRON_FINITE_ELEMENT_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <QuadratureFormula.hpp>
#include <ConformTransformation.hpp>

#include <LagrangianFiniteElement.hpp>
#include <ThreadStaticBase.hpp>

class Q1HexahedronFiniteElement
  : public ThreadStaticBase<Q1HexahedronFiniteElement>,
    public LagrangianFiniteElement<8, Q1HexahedronFiniteElement,
				   QuadratureFormulaQ1Hexahedron>
{
private:
  static TinyVector<3, real_t> __massCenter; /**< mass center of the reference element */

public:
  enum {
    numberOfDegreesOfFreedom = 8,
    numberOfVertexDegreesOfFreedom = 1,
    numberOfEdgeDegreesOfFreedom = 0,
    numberOfFaceDegreesOfFreedom = 0,
    numberOfVolumeDegreesOfFreedom = 0,
    numberOfFaceLivingDegreesOfFreedom = 4 // degrees of freedom carried by a face
  };

  /**
   * Degrees of freedom living on faces
   */
  static const size_t facesDOF[Hexahedron::NumberOfFaces][numberOfFaceLivingDegreesOfFreedom];

  /** 
   * returns the mass center of the reference element
   * 
   * @return __massCenter
   */
  static const TinyVector<3, real_t>& massCenter()
  {
    return __massCenter;
  }

  /** 
   * Computes a hat function at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ w_i(\mathbf{x}) \f$
   */
  real_t W  (const size_t& i, const TinyVector<3>& x) const;

  /** 
   * Computes a hat function derivative at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ \partial_x w_i(\mathbf{x}) \f$
   */
  real_t dxW(const size_t& i, const TinyVector<3>& x) const;

  /** 
   * Computes a hat function derivative at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ \partial_y w_i(\mathbf{x}) \f$
   */
  real_t dyW(const size_t& i, const TinyVector<3>& x) const;

  /** 
   * Computes a hat function derivative at a given point
   * 
   * @param i the hat function number
   * @param x the evaluation point
   * 
   * @return \f$ \partial_z w_i(\mathbf{x}) \f$
   */
  real_t dzW(const size_t& i, const TinyVector<3>& x) const;

  inline const
  TinyVector<QuadratureType::numberOfQuadraturePoints,
	     TinyVector<3> >&
  integrationVertices() const
  {
    return QuadratureType::instance().vertices();
  }

  Q1HexahedronFiniteElement()
  {
    ;
  }

  ~Q1HexahedronFiniteElement()
  {
    ;
  }
};

#endif // Q1_HEXAHEDRON_FINITE_ELEMENT_HPP
