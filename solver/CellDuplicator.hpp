//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: CellDuplicator.hpp,v 1.1 2003/09/14 17:18:38 delpinux Exp $

#ifndef CELL_DUPLICATOR_HPP
#define CELL_DUPLICATOR_HPP

#include <VerticesSet.hpp>
#include <map>

/**
 * @file   CellDuplicator.hpp
 * @author Stephane Del Pino
 * @date   Sat Sep 13 19:13:08 2003
 * 
 * @brief copying cells from one mesh to the other using generic
 * technic
 * 
 */
template <typename CellType>
class CellDuplicator
  : public CellType
{
public:
  typedef std::map<const Vertex*, size_t> Translation;

  /** 
   * The constructor
   * 
   * @param cell a given cell
   * @param translator the correspondance list
   * @param newVerticesSet the new set of vertices
   * 
   */
  CellDuplicator(const CellType& cell,
		 Translation& translator,
		 VerticesSet& newVerticesSet)
    : CellType(cell)
  {
    for (size_t i=0; i<CellType::NumberOfVertices; ++i) {
      this->__vertices[i] = &newVerticesSet[translator[&cell(i)]];
    }
  }

  /** 
   * Destructor
   * 
   */
  ~CellDuplicator()
  {
    ;
  }
};

#endif // CELL_DUPLICATOR_HPP
