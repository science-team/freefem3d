//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001-2005 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SpectralFunction.hpp,v 1.6 2007/05/28 18:27:02 yakoubix Exp $


#ifndef SPECTRAL_FUNCTION_HPP
#define SPECTRAL_FUNCTION_HPP

#include <ScalarFunctionBase.hpp>

#include <SpectralMesh.hpp>
#include <Vector.hpp>
#include <Interval.hpp>
#include <LegendreBasis.hpp>
#include <GaussLobatto.hpp>
#include <SpectralConformTransformation.hpp>

#include <ScalarFunctionConstant.hpp>

/**
 * @file   SpectralFunction.hpp
 * @author Stephane Del Pino & Driss Yakoubi
 * @date   Sun Apr  1 18:25:10 2007
 * 
 * @brief This class manages spectral functions 
 */
class SpectralFunction
  : public ScalarFunctionBase
{
private:
  Vector<real_t> __values;	/**< coefficients in the spectral
				   basis */
   
  ConstReferenceCounting<SpectralMesh>
  __mesh;			/**< the mesh where the function
				   leaves */

  const DiscretizationType::Type
  __discretizationType;		/**< type of discretization*/

  const Interval __intervalX;	/**< interval in the @f$ x @f$ direction */
  const Interval __intervalY;	/**< interval in the @f$ y @f$ direction */
  const Interval __intervalZ;	/**< interval in the @f$ z @f$ direction */

  const SpectralConformTransformation
  __transformX;			/**< geometric transformation of
				   @f$ (-1,1) @f$ to @f$ (a_x,b_x) @f$*/
  const SpectralConformTransformation
  __transformY;			/**< geometric transformation of
				   @f$ (-1,1) @f$ to @f$ (a_y,b_y) @f$*/
  const SpectralConformTransformation
  __transformZ;			/**< geometric transformation of
				   @f$ (-1,1) @f$ to @f$ (a_z,b_z) @f$*/

  const GaussLobatto
  __gaussLobattoX;		/**< Quadrature points for @f$ (a_x,b_x) @f$ */
  const GaussLobatto
  __gaussLobattoY;		/**< Quadrature points for @f$ (a_y,b_y) @f$ */
  const GaussLobatto
  __gaussLobattoZ;		/**< Quadrature points for @f$ (a_z,b_z) @f$ */

  const LegendreBasis __basisX;	/**< Basis in the @f$ x @f$ direction */
  const LegendreBasis __basisY;	/**< Basis in the @f$ y @f$ direction */
  const LegendreBasis __basisZ;	/**< Basis in the @f$ z @f$ direction */

  /** 
   * Copy constructor
   * 
   * @param f given function
   * @note forbidden to avoid wild copies
   */
  SpectralFunction(const SpectralFunction& f);

  /** 
   * Overloading of the __put function
   * 
   * @param os given stream
   * 
   * @return os
   */
  std::ostream& __put(std::ostream& os) const
  {
    os << "{sfunction}";
    return os;
  }

public:
  /** 
   * Read-only access to the set of values
   * 
   * @return __values
   */
  const Vector<real_t>& values() const
  {
    return __values;
  }

  /** 
   * access to the value at the ith degree of freedom
   * 
   * @param i number of the degree of freedom
   * 
   * @return __values[i]
   */
  inline real_t& operator[](const size_t& i)
  {
    return __values[i];
  }

  /** 
   * Read only Access to the value at the ith degree of freedom
   * 
   * @param i number of the degree of freedom
   * 
   * @return __values[i]
   */
  inline const real_t& operator[](const size_t& i) const
  {
    return __values[i];
  }

  /** 
   * real-only access to the spectral mesh
   * 
   * @return __mesh
   */
  ConstReferenceCounting<SpectralMesh> mesh() const
  {
    return __mesh;
  }

  /** 
   * Checks if the function can be simplified
   * 
   * @return true
   */
  bool canBeSimplified() const
  {
    return false;//true;
  }

  /** 
   * The type of the spectral function
   * 
   * @return __discretizationType
   */
  const DiscretizationType::Type& discretizationType() const
  {
    return __discretizationType;
  }

  /** 
   * Evaluates the SpectralFunction at point @a x.
   * 
   * @param x the position of evaluation
   * 
   * @return @f$ f(x) @f$
   */
  real_t operator()(const TinyVector<3,real_t>& x) const
  {    
    Vector<real_t> baseValuesX(__basisX.dimension());
    Vector<real_t> baseValuesY(__basisY.dimension());
    Vector<real_t> baseValuesZ(__basisZ.dimension());

    __basisX.getValues(__transformX.inverse(x[0]),baseValuesX);
    __basisY.getValues(__transformY.inverse(x[1]),baseValuesY);
    __basisZ.getValues(__transformZ.inverse(x[2]),baseValuesZ);

    size_t l = 0;
    real_t sum = 0;
    for (size_t i=0; i<__basisX.dimension(); ++i) {
      const real_t vi = baseValuesX[i];
      for (size_t j=0;j<__basisY.dimension(); ++j) { 
	const real_t vj = baseValuesY[j];
	const real_t vi_vj = vi*vj;
    	for (size_t k=0; k<__basisZ.dimension(); ++k) {
	  const real_t vk = baseValuesZ[k];
	  const real_t vi_vj_vk = vi_vj * vk;
 	  sum += vi_vj_vk*__values[l];
	  l++;
	}
      }
    }
    
    return sum;
  }
  
  /** 
   * Affects a function to a SpectralFunction
   * 
   * @param f original function
   */
  void operator=(const ScalarFunctionBase& f)
  {
    __values = 0;

    Vector<Vector<real_t> > quadratureBaseValuesX(__gaussLobattoX.numberOfPoints());
    for (size_t i=0; i < __gaussLobattoX.numberOfPoints(); ++i) {
      real_t xi= __gaussLobattoX(i);
      quadratureBaseValuesX[i].resize(__basisX.dimension());
      __basisX.getValues(xi,quadratureBaseValuesX[i]);
    }
    
    Vector<Vector<real_t> > quadratureBaseValuesY(__gaussLobattoY.numberOfPoints());
    for (size_t j=0; j < __gaussLobattoY.numberOfPoints(); ++j) {
      real_t yj= __gaussLobattoY(j);
      quadratureBaseValuesY[j].resize(__basisY.dimension());
      __basisY.getValues(yj,quadratureBaseValuesY[j]);
    }
    
    Vector<Vector<real_t> > quadratureBaseValuesZ(__gaussLobattoZ.numberOfPoints());
    for (size_t k=0; k < __gaussLobattoZ.numberOfPoints(); ++k) {
      real_t zk= __gaussLobattoZ(k);
      quadratureBaseValuesZ[k].resize(__basisZ.dimension());
      __basisZ.getValues(zk,quadratureBaseValuesZ[k]);
    }


    Vector<real_t> nodesX(__gaussLobattoX.numberOfPoints());
    for (size_t i=0; i<__gaussLobattoX.numberOfPoints(); ++i) {
      nodesX[i] = __transformX(__gaussLobattoX(i));
    }
    Vector<real_t> nodesY(__gaussLobattoY.numberOfPoints());
    for (size_t i=0; i<__gaussLobattoY.numberOfPoints(); ++i) {
      nodesY[i] = __transformY(__gaussLobattoY(i));
    }
    Vector<real_t> nodesZ(__gaussLobattoZ.numberOfPoints());
    for (size_t i=0; i<__gaussLobattoZ.numberOfPoints(); ++i) {
      nodesZ[i] = __transformZ(__gaussLobattoZ(i));
    }

    for (size_t i=0; i < __gaussLobattoX.numberOfPoints(); ++i) {
      const real_t& x = nodesX[i];
      const real_t& wi = __gaussLobattoX.weight(i);
      
      for (size_t j=0; j < __gaussLobattoY.numberOfPoints(); ++j) {
	const real_t& y = nodesY[j];
	const real_t& wj = __gaussLobattoY.weight(j);

	for (size_t k=0; k < __gaussLobattoZ.numberOfPoints(); ++k) {	  
	  const real_t& z = nodesZ[k];
	  const real_t& wk = __gaussLobattoZ.weight(k);

	  const real_t weightedF = f(x,y,z) *wi * wj * wk;
	  size_t l=0;

	  for (size_t m=0; m<__basisX.dimension(); ++m) {
	    for (size_t n=0; n<__basisY.dimension(); ++n) {
	      for (size_t r=0; r<__basisZ.dimension(); ++r) {
		__values[l]
		  +=   quadratureBaseValuesX[i][m] * quadratureBaseValuesY[j][n] * quadratureBaseValuesZ[k][r]
		  * weightedF;
		l++;		
	      }
	    }
	  }
	}
      }
    }
    
    size_t l = 0;
    for (size_t i=0; i<__basisX.dimension(); ++i) {
      const real_t wi_8 = 1./8. * (2*i+1);
      for (size_t j=0;j<__basisY.dimension(); ++j) { 
	const real_t wi_wj_8 = wi_8 * (2*j+1);
	for (size_t k=0; k<__basisZ.dimension(); ++k) {
	  const real_t wi_wj_wk_8 = wi_wj_8 * (2*k+1);
	  __values[l] *= wi_wj_wk_8;
	  l++;
	}
      }
    }
  }
  
  /** 
   * Evaluates first derivative of the function
   * 
   * @param x position of evaluation
   * 
   * @return @f$ \partial_x f at position x @f$
   */
  real_t dx(const TinyVector<3>& x) const
  {
    Vector<real_t> baseDerivativeValuesX(__basisX.dimension());
    Vector<real_t> baseValuesY(__basisY.dimension());
    Vector<real_t> baseValuesZ(__basisZ.dimension());

    __basisX.getDerivativeValues(__transformX.inverse(x[0]),baseDerivativeValuesX);
    __basisY.getValues(__transformY.inverse(x[1]),baseValuesY);
    __basisZ.getValues(__transformZ.inverse(x[2]),baseValuesZ);

    size_t l = 0;
    real_t sum = 0;
    for (size_t i=0; i<__basisX.dimension(); ++i) {
      const real_t vi = baseDerivativeValuesX[i];
      for (size_t j=0;j<__basisY.dimension(); ++j) { 
	const real_t vj = baseValuesY[j];
	const real_t vi_vj = vi*vj;
    	for (size_t k=0; k<__basisZ.dimension(); ++k) {
	  const real_t vk = baseValuesZ[k];
	  const real_t vi_vj_vk = vi_vj * vk;
 	  sum += vi_vj_vk*__values[l];
	  l++;
	}
      }
    }
    
    return sum * __transformX.inverseDeterminant();
  }
  
  /** 
   * Evaluates second derivative of the function
   * 
   * @param x position of evaluation
   * 
   * @return @f$ \partial_y f at position x @f$
   */
  real_t dy(const TinyVector<3>& x) const
  {
    Vector<real_t> baseValuesX(__basisX.dimension());
    Vector<real_t> baseDerivativeValuesY(__basisY.dimension());
    Vector<real_t> baseValuesZ(__basisZ.dimension());

    __basisX.getValues(__transformX.inverse(x[0]),baseValuesX);
    __basisY.getDerivativeValues(__transformY.inverse(x[1]),baseDerivativeValuesY);
    __basisZ.getValues(__transformZ.inverse(x[2]),baseValuesZ);

    size_t l = 0;
    real_t sum = 0;
    for (size_t i=0; i<__basisX.dimension(); ++i) {
      const real_t vi = baseValuesX[i];
      for (size_t j=0;j<__basisY.dimension(); ++j) { 
	const real_t vj = baseDerivativeValuesY[j];
	const real_t vi_vj = vi*vj;
    	for (size_t k=0; k<__basisZ.dimension(); ++k) {
	  const real_t vk = baseValuesZ[k];
	  const real_t vi_vj_vk = vi_vj * vk;
 	  sum += vi_vj_vk*__values[l];
	  l++;
	}
      }
    }
    
    return sum * __transformY.inverseDeterminant();
  }
  
  /** 
   * Evaluates third derivative of the function
   * 
   * @param x position of evaluation
   * 
   * @return @f$ \partial_z f at position x @f$
   */
  real_t dz(const TinyVector<3>& x) const
  {
    Vector<real_t> baseValuesX(__basisX.dimension());
    Vector<real_t> baseValuesY(__basisY.dimension());
    Vector<real_t> baseDerivativeValuesZ(__basisZ.dimension());
   
    __basisX.getValues(__transformX.inverse(x[0]),baseValuesX);
    __basisY.getValues(__transformY.inverse(x[1]),baseValuesY);
    __basisZ.getDerivativeValues(__transformZ.inverse(x[2]),baseDerivativeValuesZ);
  
    size_t l = 0;
    real_t sum = 0;
    for (size_t i=0; i<__basisX.dimension(); ++i) {
      const real_t vi = baseValuesX[i];
      for (size_t j=0;j<__basisY.dimension(); ++j) { 
	const real_t vj = baseValuesY[j];
	const real_t vi_vj = vi*vj;
    	for (size_t k=0; k<__basisZ.dimension(); ++k) {
	  const real_t vk = baseDerivativeValuesZ[k];
	  const real_t vi_vj_vk = vi_vj * vk;
 	  sum += vi_vj_vk*__values[l];
	  l++;
	}
      }
    }
    
    return sum * __transformZ.inverseDeterminant();
  }
  
  /** 
   * Constructor
   * 
   * @param mesh mesh supporting the function
   */
  SpectralFunction(ConstReferenceCounting<SpectralMesh> mesh) 
    
    : ScalarFunctionBase(ScalarFunctionBase::spectral),
      __values(mesh->numberOfVertices()),
      __mesh(mesh),
      __discretizationType(DiscretizationType::spectralLegendre),
      __intervalX(__mesh->shape().a()[0],__mesh->shape().b()[0]),
      __intervalY(__mesh->shape().a()[1],__mesh->shape().b()[1]),
      __intervalZ(__mesh->shape().a()[2],__mesh->shape().b()[2]),
      __transformX(__intervalX),
      __transformY(__intervalY),
      __transformZ(__intervalZ),
      __gaussLobattoX(__mesh->degree(0)+1),
      __gaussLobattoY(__mesh->degree(1)+1),
      __gaussLobattoZ(__mesh->degree(2)+1),   
      __basisX(__mesh->degree(0)),
      __basisY(__mesh->degree(1)),
      __basisZ(__mesh->degree(2))
  {
    ;
  }
  
  /** 
   * Constructor
   * 
   * @param mesh mesh supporting the function
   * @param f function of initialization
   */
  SpectralFunction(ConstReferenceCounting<SpectralMesh> mesh, 
		   const ScalarFunctionBase& f)
    : ScalarFunctionBase(ScalarFunctionBase::spectral),
      __values((mesh->degree(0)+1)*(mesh->degree(1)+1)*(mesh->degree(2)+1)),
      __mesh(mesh),
      __discretizationType(DiscretizationType::spectralLegendre),
      __intervalX(__mesh->shape().a()[0],__mesh->shape().b()[0]),
      __intervalY(__mesh->shape().a()[1],__mesh->shape().b()[1]),
      __intervalZ(__mesh->shape().a()[2],__mesh->shape().b()[2]),
      __transformX(__intervalX),
      __transformY(__intervalY),
      __transformZ(__intervalZ),
      __gaussLobattoX(__mesh->degree(0)+1),
      __gaussLobattoY(__mesh->degree(1)+1),
      __gaussLobattoZ(__mesh->degree(2)+1),   
      __basisX(__mesh->degree(0)),
      __basisY(__mesh->degree(1)),
      __basisZ(__mesh->degree(2)) /*((__mesh->degree(2))*/
    
  {
    (*this) = f;
  }
  
  /** 
   * Constructor
   * 
   * @param mesh mesh supporting the function
   * @param d value of initialization
   */
  SpectralFunction(ConstReferenceCounting<SpectralMesh> mesh,
		   const real_t& d)
    : ScalarFunctionBase(ScalarFunctionBase::spectral),
      __values((mesh->degree(0)+1)*(mesh->degree(1)+1)*(mesh->degree(2)+1)),
      __mesh(mesh),
      __discretizationType(DiscretizationType::spectralLegendre),
      __intervalX(__mesh->shape().a()[0],__mesh->shape().b()[0]),
      __intervalY(__mesh->shape().a()[1],__mesh->shape().b()[1]),
      __intervalZ(__mesh->shape().a()[2],__mesh->shape().b()[2]),
      __transformX(__intervalX),
      __transformY(__intervalY),
      __transformZ(__intervalZ),
      __gaussLobattoX(__mesh->degree(0)+1),
      __gaussLobattoY(__mesh->degree(1)+1),
      __gaussLobattoZ(__mesh->degree(2)+1),         
      __basisX(__mesh->degree(0)),
      __basisY(__mesh->degree(1)),
      __basisZ(__mesh->degree(2))/*((__mesh->degree(2))*/
  {
    (*this) = ScalarFunctionConstant(d);
  }
  
  /** 
   * Constructor
   * 
   * @param mesh given mesh
   * @param values given values
   */
  SpectralFunction(ConstReferenceCounting<SpectralMesh> mesh,
		   const Vector<real_t>& values)
    
    : ScalarFunctionBase(ScalarFunctionBase::spectral),
      __values((mesh->degree(0)+1)*(mesh->degree(1)+1)*(mesh->degree(2)+1)),
      __mesh(mesh),
      __discretizationType(DiscretizationType::spectralLegendre),
      __intervalX(__mesh->shape().a()[0],__mesh->shape().b()[0]),
      __intervalY(__mesh->shape().a()[1],__mesh->shape().b()[1]),
      __intervalZ(__mesh->shape().a()[2],__mesh->shape().b()[2]),
      __transformX(__intervalX),
      __transformY(__intervalY),
      __transformZ(__intervalZ),
      __gaussLobattoX(__mesh->degree(0)+1),
      __gaussLobattoY(__mesh->degree(1)+1),
      __gaussLobattoZ(__mesh->degree(2)+1),        
      __basisX(__mesh->degree(0)),
      __basisY(__mesh->degree(1)),
      __basisZ(__mesh->degree(2))/*((__mesh->degree(2))*/
  {
    ASSERT(__values.size() == values.size());
    __values = values;
  }
  
  /** 
   * Destructor
   * 
   */
  ~SpectralFunction()
  {
    ;
  }
};
#endif // SPECTRAL_FUNCTION_HPP

