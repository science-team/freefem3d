//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SolverDriverOptions.hpp,v 1.4 2007/06/09 10:37:06 delpinux Exp $

#ifndef _SOLVER_DRIVER_OPTIONS_HPP_
#define _SOLVER_DRIVER_OPTIONS_HPP_

#include <ParametrizableObject.hpp>

class SolverDriverOptions
  : public ParametrizableObject
{
private:
  std::ostream& put(std::ostream& os) const
  {
    os << this->identifier();
    return os;
  }

public:
  enum MethodType {
    penalty,
    eliminate,
    fatBoundary,
    undefined
  };

  MethodType type()
  {
    MethodType t = undefined;
    get("type", t);
    return t;
  }

  static const char* identifier()
  {
    // autodoc: "use to tune the discretization method"
    return "method";
  }

  SolverDriverOptions(const SolverDriverOptions& S)
  {
    ;
  }

  explicit SolverDriverOptions()
  {
    // autodoc: "selects the discretization method"
    EnumParameter<MethodType>* E
      = new EnumParameter<MethodType>(SolverDriverOptions::eliminate,"type");

    // autodoc: "sets Dirichlet boundary conditions to be computed by penalty"
    (*E).addSwitch("penalty",    SolverDriverOptions::penalty);
    // autodoc: "sets Dirichlet boundary conditions using elimination"
    (*E).addSwitch("eliminate",   SolverDriverOptions::eliminate);
    // autodoc: "sets boundary conditions using FBM (\bf{\Red{not implemented}})"
    (*E).addSwitch("fatBoundary",SolverDriverOptions::fatBoundary);

    add (E);
  }

  ~SolverDriverOptions()
  {
    ;
  }
};

#endif // _SOLVER_DRIVER_OPTIONS_HPP_


