//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Discretization.hpp,v 1.5 2007/05/20 23:25:14 delpinux Exp $


#ifndef DISCRETIZATION_HPP
#define DISCRETIZATION_HPP

#include <ReferenceCounting.hpp>

#include <BaseVector.hpp>
#include <Vector.hpp>

#include <BaseMatrix.hpp>

#include <PDESystem.hpp>

/**
 * @file   Discretization.hpp
 * @author Stephane Del Pino
 * @date   Wed Jul 19 15:48:26 2006
 * 
 * @brief this class is used to define discretizations of PDEProblems.
 * 
 * 
 */
class Discretization
{
public:
  //! The set of discretization types
  enum Type {
    FEM,
    spectral
  };

protected:
  //! The discretization type
  const Discretization::Type __type;

  //! The PDEProblem to discretize.
  const Problem& __problem;

  //! The matrix which will contain the discretization.
  BaseMatrix& __A;

  //! The second member
  BaseVector& __b;

  //! elimination dirichlet informations
  const Vector<bool>* __dirichletList;

public:
  /** 
   * Sets dirichlet vertices list
   * 
   * @param dirichletList list of dirichlet vertices
   */
  void setDirichletList(const Vector<bool>& dirichletList)
  {
    ASSERT(__dirichletList == 0);
    __dirichletList = &dirichletList;
  }

  /** 
   * Computes the matrix using the problem
   * 
   */
  virtual void assembleMatrix() = 0;

  /** 
   * Computes the second memeber using the problem
   * 
   */
  virtual void assembleSecondMember() = 0;

  /** 
   * Provides interface for to allow computation without matrix
   * 
   * @param u the vector representing the diagonal
   */
  virtual void getDiagonal(BaseVector& u) const = 0;

  /** 
   *  Provides interface for to allow computation without matrix
   * 
   * @param x the vector @f$ x @f$
   * @param v the result @f$ A^T x @f$
   */
  virtual void transposedTimesX(const BaseVector& x, BaseVector& v) const = 0;

  /** 
   *  Provides interface for to allow computation without matrix
   * 
   * @param x the vector @f$ x @f$
   * @param v the result @f$ A x @f$
   */
  virtual void timesX(const BaseVector& x, BaseVector& v) const = 0;

  /** 
   * Read only access to the problem
   * 
   * @return __problem
   */
  const Problem& problem() const
  {
    return __problem;
  }

  /** 
   * Read only access to the discretization type
   * 
   * @return __type
   */
  const Discretization::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Access to the matrix
   * 
   * @return __A
   */
  BaseMatrix& A()
  {
    return __A;
  }

  /** 
   * Access to the second member
   * 
   * @return __b
   */
  BaseVector& b()
  {
    return __b;
  }

  /*!
    The constructor.
    The matrix A and the second member b should be computed here.
   */
  /** 
   * Constructor
   * 
   * @param t discretization type
   * @param p given problem
   * @param A matrix to build
   * @param b second member
   */
  Discretization(const Discretization::Type& t,
		 const Problem& p,
		 BaseMatrix& A,
		 BaseVector& b)
    : __type(t),
      __problem(p),
      __A(A),
      __b(b),
      __dirichletList(0)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param d discretization
   */
  Discretization(const Discretization& d)
    : __type(d.__type),
      __problem(d.__problem),
      __A(d.__A),
      __b(d.__b),
      __dirichletList(0)
  {
    ;
  }

  /** 
   * Destructor
   * 
   * 
   */
  virtual ~Discretization()
  {
    ;
  }
};

#endif // DISCRETIZATION_HPP
