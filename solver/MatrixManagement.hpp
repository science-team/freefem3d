//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MatrixManagement.hpp,v 1.3 2007/06/09 10:37:07 delpinux Exp $

#ifndef MATRIX_MANAGEMENT_HPP
#define MATRIX_MANAGEMENT_HPP

#include <cctype>

#include <ReferenceCounting.hpp>
#include <ParametrizableObject.hpp>

#include <SurfaceMesh.hpp>

#include <ErrorHandler.hpp>

class BaseMatrix;
class BaseVector;

class MemoryRepository
{
private:
  std::map<int, ReferenceCounting<BaseMatrix> > __matrices;
public:

  void store(int i,ReferenceCounting<BaseMatrix>& matrix);

  ReferenceCounting<BaseMatrix>& getMatrix(int i);

  bool stored(int i) const;

  ~MemoryRepository()
  {
    ;
  }
};

#warning REARANGE AND RENAME THIS FILE.

class Structured3DMesh;
class PDESystem;

extern  MemoryRepository *memoryRepository;

#include <GetParameter.hpp>
#include <MemoryManagerOptions.hpp>

class MemoryManager
{
private:
  MemoryRepository& __mr;

  GetParameter<MemoryManagerOptions> __options;
public:
  bool ReserveMatrix(ReferenceCounting<BaseMatrix>& A,
		     const size_t dimension,
		     const size_t nbDegreeOfFreefom);

  void ReserveVector(ReferenceCounting<BaseVector>& b,
		     const size_t dimension,
		     const size_t nbDegreeOfFreefom);

  MemoryManager()
    : __mr(*memoryRepository)
  {
    ;
  } 
};

#include <Vector.hpp>
#include <DoubleHashedMatrix.hpp>

#include <Index.hpp>
#include <Dirichlet.hpp>
#include <PDEProblem.hpp>

#include <Structured3DMesh.hpp>
class BoundaryConditionSurfaceMeshAssociation;

template<class VectType>
void computesDirichletMatrix(DoubleHashedMatrix& DM,
			     const DoubleHashedMatrix& A,
			     const PDEProblem& pdePb,
			     BoundaryConditionSurfaceMeshAssociation& bcMeshAssociation,
			     const Structured3DMesh& SMesh)
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "not implemented",
		     ErrorHandler::unexpected);
//   // Dirichlet on the mesh border ...
//   for (size_t i=0; i<pdePb.Dimension(); ++i) {
//     for (bcMeshAssociation::iterator ibcMesh = bcMesh[i].begin();
// 	 ibcMesh !=bcMesh[i].end();
// 	 ++ibcMesh) {
//       const BoundaryCondition& BC = *(*ibcMesh).first;
//       const SurfaceMesh& surfMesh = *(*ibcMesh).second;

//       if (BC.condition().Type()==PDECondition::dirichlet) {
// 	bool proceed = false;
// 	for (size_t n=0; n<6; ++n)
// 	  if(BC.boundary().StructMesh(i))
// 	    proceed=true;
// 	if (proceed) {
// 	  const Dirichlet& D = dynamic_cast<const Dirichlet&>(BC.condition());

// 	  for (size_t vert=0; vert<surfMesh.SurfElements().size(); vert++)
// 	    for (size_t k=0; k<surfMesh.SurfElements(vert).NbQuadVertices(); k++) {
// 	      const QuadratureVertex& q = surfMesh.SurfElements(vert).QuadVertices(k);

// 	      const Index& iv = SMesh.vertexIndex(q); // Index of the vertex of
// 	      // the mesh the closer to q.         
// 	      const Vertex& V = SMesh.vertex(iv);

// 	      const real_t GValue = D.f(V);

// 	      const size_t I = SMesh.number(V);

// // 		switch (pdePb.Dimension()) {
// // 		case 1: {
// // 		  dirichletEliminate(I, i, GValue,
// // 				     static_cast<DoubleHashedMatrix<real_t>&>(*A),
// // 				     static_cast<Vector<real_t>&>(*b));
// // 		  break;
// // 		}
// // 		case 2: {
// // 		  dirichletEliminate(I, i, GValue,
// // 				     static_cast<DoubleHashedMatrix<TinyMatrix<2,2,real_t> >&>(*A),
// // 				     static_cast<Vector<TinyVector<2,real_t> >&>(*b));
// // 		  break;
// // 		}
// // 		case 3: {
// // 		  break;
// // 		}
// // 		case 4: {
// // 		  break;
// // 		}
// // 		default: {
// // 		}
// // 		}
// 	    }
// 	}
//       }
//     }
//   }
}

#endif // MATRIX_MANAGEMENT_HPP
