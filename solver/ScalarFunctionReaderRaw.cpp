//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionReaderRaw.cpp,v 1.3 2007/06/10 15:03:07 delpinux Exp $

#include <ScalarFunctionReaderRaw.hpp>
#include <ScalarFunctionBase.hpp>

#include <ErrorHandler.hpp>
#include <Mesh.hpp>

#include <DiscretizationType.hpp>
#include <Vector.hpp>
#include <FEMFunctionBase.hpp>

#include <FEMFunctionBuilder.hpp>

#include <fstream>

ConstReferenceCounting<ScalarFunctionBase>
ScalarFunctionReaderRaw::
getFunction() const
{
  if (__functionName != "") {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot name function in raw format",
		       ErrorHandler::normal);
  }

  std::ifstream fin(__filename.c_str());
  if (not(fin)) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "error: cannot open file '"+__filename+"'",
		       ErrorHandler::normal);
  }

  const Mesh& mesh = (*__mesh);

  Vector<real_t> v(mesh.numberOfVertices());

  for (size_t j=0; j<=__componentNumber; ++j) {
    for (size_t i=0; i<v.size(); ++i) {
      fin >> v[i];
    }

    if (fin.bad()) {
      throw ErrorHandler(__FILE__,__LINE__,
			 "cannot read all datas in file '"+__filename+"'",
			 ErrorHandler::normal);
    }
  }

  FEMFunctionBuilder builder;
  DiscretizationType d(DiscretizationType::lagrangianFEM1);
  builder.build(d, __mesh, v);
  return builder.getBuiltScalarFunction();
}

ScalarFunctionReaderRaw::
ScalarFunctionReaderRaw(const std::string& filename,
			ConstReferenceCounting<Mesh> mesh,
			const std::string& functionName,
			const int& componentNumber)
  : ScalarFunctionReaderBase(filename, mesh, functionName, componentNumber)
{
  ;
}

ScalarFunctionReaderRaw::
ScalarFunctionReaderRaw(const ScalarFunctionReaderRaw& f)
  : ScalarFunctionReaderBase(f)
{
  ;
}

ScalarFunctionReaderRaw::
~ScalarFunctionReaderRaw()
{
  ;
}
