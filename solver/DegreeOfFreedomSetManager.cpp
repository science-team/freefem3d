//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2005 Stephane Del Pino
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//  $Id: DegreeOfFreedomSetManager.cpp,v 1.2 2006/03/04 18:36:50 delpinux Exp $

#include <DegreeOfFreedomSetManager.hpp>
#include <DegreeOfFreedomPositionsSet.hpp>
#include <StreamCenter.hpp>

#include <Structured3DMesh.hpp>
#include <MeshOfTetrahedra.hpp>

#include <Mesh.hpp>


class DegreeOfFreedomSetManager::Internal
{
private:
  typedef
  std::map<std::pair<const Mesh*, DiscretizationType::Type>,
	   std::pair<size_t, DegreeOfFreedomPositionsSet*> >
  MeshesDOFPositionsCorrespondance;

  MeshesDOFPositionsCorrespondance __meshesDOFPositions;

public:
  const DegreeOfFreedomPositionsSet&
  getDOFPositionsSet(const Mesh& mesh,
		     const DiscretizationType& discretizationType)
  {
    const DiscretizationType::Type type = discretizationType.type();

    MeshesDOFPositionsCorrespondance::iterator
      i = __meshesDOFPositions.find(std::make_pair(&mesh,type));

    if (i != __meshesDOFPositions.end()) {
      (*i).second.first++;
      return (*(*i).second.second);
    } else {
      DegreeOfFreedomPositionsSet* dof
	= new DegreeOfFreedomPositionsSet(discretizationType,mesh);

      __meshesDOFPositions[std::make_pair(&mesh, discretizationType.type())]
	= std::make_pair(1,dof);
      return *dof;
    }
  }

  void unsubscribe(const DegreeOfFreedomPositionsSet& dofSet)
  {
    for (MeshesDOFPositionsCorrespondance::iterator i
	   = __meshesDOFPositions.begin();
	 i != __meshesDOFPositions.end(); ++i) {
      if ((*i).second.second == &dofSet) {
	(*i).second.first--;
	if ((*i).second.first == 0) {
	  delete (*i).second.second;
	  __meshesDOFPositions.erase(i);
	}
	return;
      }
    }
    throw ErrorHandler(__FILE__,__LINE__,
		       "DOF Set not found!",
		       ErrorHandler::unexpected);

  }

  Internal()
  {
    ;
  }

  ~Internal()
  {
    if (__meshesDOFPositions.size() > 0) {
      fferr(1) << __FILE__ << ':' << __LINE__
	       << ":warning: degrees of freedom set manager is not empty!\n";
    }
  }
};


const DegreeOfFreedomPositionsSet&
DegreeOfFreedomSetManager::
getDOFPositionsSet(const Mesh& mesh,
		   const DiscretizationType& discretizationType)
{
  return __internal->getDOFPositionsSet(mesh, discretizationType);
}

void DegreeOfFreedomSetManager::
unsubscribe(const DegreeOfFreedomPositionsSet& dofSet)
{
  __internal->unsubscribe(dofSet);
}


DegreeOfFreedomSetManager::
DegreeOfFreedomSetManager()
{
  __internal = new Internal();
}

DegreeOfFreedomSetManager::
~DegreeOfFreedomSetManager()
{
  delete __internal;
}
