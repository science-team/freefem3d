//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: PDESolution.hpp,v 1.8 2007/05/20 23:26:37 delpinux Exp $

#ifndef PDE_SOLUTION_HPP
#define PDE_SOLUTION_HPP

#include <Solution.hpp>
#include <BaseVector.hpp>

#include <ReferenceCounting.hpp>

#include <DegreeOfFreedomSet.hpp>

#include <vector>

#include <ScalarFunctionBase.hpp>

class PDESolution
  : public Solution
{
protected:
  const DegreeOfFreedomSet& __degreeOfFreedomSet;

  ReferenceCounting<Vector<real_t> > __values;;

private:
  //! No copy constructor
  PDESolution(const PDESolution&);

public:
  ReferenceCounting<Vector<real_t> > values()
  {
    return __values;
  }

  ConstReferenceCounting<Vector<real_t> > values() const
  {
    return __values;
  }

  size_t size() const
  {
    return __degreeOfFreedomSet.size();
  }

  PDESolution(const DegreeOfFreedomSet& d);

  virtual ~PDESolution()
  {
    ;
  }
};

#endif // PDE_SOLUTION_HPP
