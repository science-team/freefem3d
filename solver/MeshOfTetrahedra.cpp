//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MeshOfTetrahedra.cpp,v 1.13 2007/05/20 14:53:53 delpinux Exp $

#include <set>
#include <stack>

#include <Types.hpp>
#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <ConnectivityBuilder.hpp>
#include <ConformTransformation.hpp>

#include <MeshOfTetrahedra.hpp>
#include <P1TetrahedronFiniteElement.hpp>

#include <EdgesBuilder.hpp>
#include <FacesBuilder.hpp>

MeshOfTetrahedra::
MeshOfTetrahedra(ReferenceCounting<VerticesSet> vertices,
		 ReferenceCounting<VerticesCorrespondance> correspondances,
		 ReferenceCounting<Vector<Tetrahedron> > tetrahedra,
		 ReferenceCounting<BorderMeshType> triangles)
    : Mesh(Mesh::tetrahedraMesh,
	   Mesh::volume,
	   vertices,
	   correspondances),
      __cells(tetrahedra),
      __octree(0),
      __surfaceMesh(triangles),
      __facesSet(0),
      __connectivity(*this)
{
  // Connects the surface mesh to the volume mesh
  if (__surfaceMesh != 0) {
    ConnectivityBuilder<MeshOfTetrahedra> builder(*this);
    builder.surfaceMesh(__surfaceMesh);
    (*__surfaceMesh).setBackgroundMesh(this);
  }
}

void MeshOfTetrahedra::buildEdges()
{
  EdgesBuilder<MeshOfTetrahedra> edgesBuilder(*this);
  __edgesSet = edgesBuilder.edgesSet();
}

void MeshOfTetrahedra::buildFaces()
{
  FacesBuilder<MeshOfTetrahedra> facesBuilder(*this);
  __facesSet = facesBuilder.facesSet();
}


void MeshOfTetrahedra::buildLocalizationTools()
{
  ffout(3) << "Generating localization in tetrahedra mesh...\n";
  if (this->numberOfVertices()==0) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "the mesh contains no vertices\n",
		       ErrorHandler::normal);
  }

  {
    // Localization inside a tetrahedra mesh requires the cell to cell
    // connectivity
    ConnectivityBuilder<MeshOfTetrahedra> builder(*this);
    builder.generates(Connectivity<MeshOfTetrahedra>::CellToCells);
  }

  // getting bounding box size
  __a = (this->vertex(0));
  __b = __a;
  for (size_t i=1; i<this->numberOfVertices(); ++i) {
    Vertex& x = this->vertex(i);
    for (size_t k=0; k<3; ++k) {
      __a[k] = (__a[k]<x[k])?__a[k]:x[k];
      __b[k] = (__b[k]>x[k])?__b[k]:x[k];
    }
  }
  ffout(3) << "- Bounding box is " << __a << ',' << __b << '\n';

  __a -= TinyVector<3,real_t>(1,1,1);
  __b += TinyVector<3,real_t>(1,1,1);

  ffout(3) << "- Building the octree\n";
  __octree = new Octree<size_t, 3>(__a,__b);

  for (size_t i = 0; i < this->numberOfCells(); ++i) {
    ConformTransformationP1Tetrahedron T(this->cell(i));

    for (size_t k=0; k<QuadratureFormulaP1Tetrahedron::numberOfQuadraturePoints; ++k) {
      TinyVector<3,real_t> X;
      T.value(QuadratureFormulaP1Tetrahedron::instance()[k], X);
      (*__octree).add(i,X);
    }
  }

  Vector<bool> usedVertices(this->numberOfVertices());
  usedVertices = false;
  for (size_t i = 0; i < this->numberOfCells(); ++i) {
    for (size_t k=0; k<CellType::NumberOfVertices; ++k) {
      const Vertex& x = this->cell(i)(k);
      const size_t vertexNumber = this->vertexNumber(x);
      if (not usedVertices[vertexNumber]) {
	usedVertices[vertexNumber] = true;
	(*__octree).add(i,x);
      }
    }
  }

  ffout(3) << "Generating localization in tetrahedra mesh: done\n";
}

MeshOfTetrahedra::const_iterator
MeshOfTetrahedra::find(const double& x,
		       const double& y,
		       const double& z) const
{
  if (__octree == 0) { // Builds the octree "on-demand"
    const_cast<MeshOfTetrahedra*>(this)->buildLocalizationTools();
  }

  // starts the search
  TinyVector<3,real_t> X(x,y,z);
  Octree<size_t, 3>::iterator i = (*__octree).fuzzySearch(X);

  size_t guessedCellNumber = (*i).value();
  const_iterator t0(*this, guessedCellNumber);

  bool found=false;

  std::set<MeshOfTetrahedra::const_iterator> visited;
  std::set<MeshOfTetrahedra::const_iterator> toVisitSet;
  std::stack<MeshOfTetrahedra::const_iterator> toVisitStack;

  toVisitSet.insert(t0);
  toVisitStack.push(t0);
  const_iterator c (*this);

  do {
    // treating next cell
    c = toVisitStack.top();

    toVisitSet.erase(c);
    toVisitStack.pop();
    visited.insert(c);

    const Tetrahedron& t = *c;

    TinyVector<4, real_t> lambda;

    t.getBarycentricCoordinates(X, lambda);

    found = true;
    for (size_t i=0; i<4; ++i) {
#warning Should use a relevent epsilon here ...
      if (lambda[i] < -1E-6) {
	found = false;
	const Tetrahedron* newT = __connectivity.cells(t)[i];
	if (newT != 0) {
	  const_iterator t1(*this, this->cellNumber(*newT));
  
	  if (visited.find(t1) == visited.end()) {
	    if (toVisitSet.find(t1) == toVisitSet.end()) {
	      toVisitSet.insert(t1);
	      toVisitStack.push(t1);
	    }
	  }
	}
      }
    }
  } while (not(found) and not(toVisitStack.empty()));

  if (not(found)) {
    c = const_iterator(*this,const_iterator::End);
  }
  return  c;
}

