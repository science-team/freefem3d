//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalBilinearBorderOperator.hpp,v 1.5 2007/06/09 10:37:06 delpinux Exp $

#ifndef VARIATIONAL_BILINEAR_BORDER_OPERATOR_HPP
#define VARIATIONAL_BILINEAR_BORDER_OPERATOR_HPP

#include <VariationalBorderOperator.hpp>

/**
 * @file   VariationalBilinearBorderOperator.hpp
 * @author Stephane Del Pino
 * @date   Thu May 30 18:25:38 2002
 * 
 * @brief  describe bilinear operators living on boundaries
 * 
 * Bilinear operators, ie: left hand side in the equation
 */
class VariationalBilinearBorderOperator
  : public VariationalBorderOperator
{
public:
  enum Type {
    alphaUV
  };

private:
  const VariationalBilinearBorderOperator::Type
  __type;			/**< bilinear operator type */

  const size_t __unknownNumber;	/**< number of the considered unknown */

public:

  /** 
   * Returns the type of the operator
   * 
   * @return __type
   */
  const VariationalBilinearBorderOperator::Type&
  type() const
  {
    return __type;
  }

  /** 
   * Returns the number of the considered unknown
   * 
   * @return __unknownNumber
   */
  const size_t& unknownNumber() const
  {
    return __unknownNumber;
  }

  /** 
   * Constructor
   * 
   * @param t the type of the variational bilinear border operator
   * @param unknownNumber the unknown number
   * @param testFunctionNumber the test function number
   * @param border the boundary where the integral is computed
   */
  VariationalBilinearBorderOperator(VariationalBilinearBorderOperator::Type t,
				    const size_t unknownNumber,
				    const size_t testFunctionNumber,
				    ConstReferenceCounting<Boundary> border)
    : VariationalBorderOperator(testFunctionNumber, border),
      __type(t),
      __unknownNumber(unknownNumber)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V a VariationalBilinearOperatorBorder
   */
  VariationalBilinearBorderOperator(const VariationalBilinearBorderOperator& V)
    : VariationalBorderOperator(V),
      __type(V.__type),
      __unknownNumber(V.__unknownNumber)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  virtual ~VariationalBilinearBorderOperator()
  {
    ;
  }
};

#endif // VARIATIONAL_BILINEAR_BORDER_OPERATOR_HPP
