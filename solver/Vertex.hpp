//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Vertex.hpp,v 1.6 2007/06/09 10:37:06 delpinux Exp $

// This class provides tools to manipulate vertices living in R^3.

#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <TinyVector.hpp>
#include <StreamCenter.hpp>

/**
 * @file   Vertex.hpp
 * @author Stephane Del Pino
 * @date   Wed Jun 19 17:38:10 2002
 * 
 * @brief  Vertices management
 * 
 */

class Vertex
  : public TinyVector<3, real_t>
{
 private:
  size_t __reference;		/**< The reference of the vertex */

 public:
  /** 
   * Compares two vertices. Result is false if vertices are not the
   * same variable in memory.
   * 
   * @param V the vertex to compare with
   * 
   * @return true if the two vertices are the \b same
   */
  inline bool operator == (const Vertex& V) const
  {
    return (&V == this);
  }

  /** 
   * Sets the vertex equal to a given one
   * 
   * @param V the model vertex
   * 
   * @return the modified \f$ U := V \f$ vertex
   */
  inline const Vertex& operator = (const Vertex& V)
  {
    TinyVector<3, real_t>::operator = (V);
    __reference = V.__reference;
    return (*this);
  }

  /** 
   * Access to the first coordinate of the vertex
   * 
   * 
   * @return \$f U_1 \$
   */
  inline real_t& x()
  {
    return (*this)[0];
  }

  /** 
   * Access to the second coordinate of the vertex
   * 
   * 
   * @return \$f U_2 \$
   */
  inline real_t& y()
  {
    return (*this)[1];
  }


  /** 
   * Access to the third coordinate of the vertex
   * 
   * 
   * @return \$f U_3 \$
   */
  inline real_t& z()
  {
    return (*this)[2];
  }

  /** 
   * Read-only access to the first coordinate of the vertex
   * 
   * 
   * @return \$f U_1 \$
   */
  inline const real_t& x() const
  {
    return (*this)[0];
  }

  /** 
   * Read-only access to the second coordinate of the vertex
   * 
   * 
   * @return \$f U_2 \$
   */
  inline const real_t& y() const
  {
    return (*this)[1];
  }

  /** 
   * Read-only access to the third coordinate of the vertex
   * 
   * 
   * @return \$f U_3 \$
   */
  inline const real_t& z() const
  {
    return (*this)[2];
  }

  /** 
   * Access to the reference of the vertex
   * 
   * 
   * @return the reference
   */
  inline size_t& reference()
  {
    return __reference;
  }

  /** 
   * Read-only access to the reference of the vertex
   * 
   * 
   * @return the reference
   */
  inline const size_t& reference() const
  {
    return __reference;
  }

  /** 
   * Default constructor. Coordinates and references are set to 0
   * 
   */
  Vertex()
    : TinyVector<3, real_t>(0,0,0),
      __reference(0)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param x first coordinate
   * @param y second coordinate
   * @param z third coordinate
   * @param reference the reference
   * 
   * @note reference is optionnal, if omitted it is 0
   */
  Vertex(const real_t& x,
	 const real_t& y,
	 const real_t& z,
	 const size_t& reference = 0)
    : TinyVector<3, real_t>(x,y,z),
      __reference(reference)
  {
    ;
  }

  /** 
   * Constructor
   * 
   * @param v coordinates
   * @param reference the reference
   * 
   * @note reference is optionnal, if omitted it is 0
   */
  Vertex(const TinyVector<3, real_t>& v,
	 const size_t& reference=0)
    : TinyVector<3, real_t>(v),
      __reference(reference)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param v a given vertex
   * 
   */
  Vertex(const Vertex& v)
    : TinyVector<3, real_t>(v),
      __reference(v.__reference)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~Vertex()
  {
    ;
  }

  /** 
   * Computes the of the distance to another point
   * 
   * @param V the point to compute the distance with
   * 
   * @return \f$ ||U-V||^2 \f$
   */
  inline real_t distance(const TinyVector<3, real_t>& V) const
  {
    return std::sqrt((V[0]-x())*(V[0]-x())
		     +(V[1]-y())*(V[1]-y())
		     +(V[2]-z())*(V[2]-z()));
  }

  /** 
   * Computes the \b square of the distance to another point
   * 
   * @param V the vertex to compute the distance with
   * 
   * @return \f$ ||U-V||^2 \f$
   */
  inline real_t distance2(const TinyVector<3, real_t>& V) const
  {
    return ((V[0]-x())*(V[0]-x())
	    +(V[1]-y())*(V[1]-y())
	    +(V[2]-z())*(V[2]-z()));
  }


  /** 
   * Prints a given vertex to a stream
   * 
   * @param os the given stream
   * @param V the vertex 
   * 
   * @return the modified stream
   */
  friend std::ostream& operator<< (std::ostream& os,
				   const Vertex& V);
};


#endif // VERTEX_HPP





