//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionMaxComputer.hpp,v 1.1 2006/07/20 19:02:23 delpinux Exp $

#ifndef SCALAR_FUNCTION_MAX_COMPUTER_HPP
#define SCALAR_FUNCTION_MAX_COMPUTER_HPP

#include <Types.hpp>
#include <ReferenceCounting.hpp>

class Mesh;
class ScalarFunctionBase;

/**
 * @file   ScalarFunctionMaxComputer.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 01:53:55 2006
 * 
 * @brief Computes the max of a function on a mesh
 * 
 */
class ScalarFunctionMaxComputer
{
private:
  real_t __maxValue;		/**< computed value */

  ConstReferenceCounting<Mesh>
  __mesh;			/**< computational mesh */
  ConstReferenceCounting<ScalarFunctionBase>
  __function;			/**< function to evaluate */

public:
  /** 
   * get the computed max value
   * 
   * @return __maxValue
   */
  const real_t& getValue() const
  {
    return __maxValue;
  }

  /** 
   * Constructor
   * 
   * @param mesh given mesh
   * @param function function
   */
  ScalarFunctionMaxComputer(ConstReferenceCounting<Mesh> mesh,
			    ConstReferenceCounting<ScalarFunctionBase> function);

  /** 
   * Copy constructor
   * 
   * @param f given max computer
   */
  ScalarFunctionMaxComputer(const ScalarFunctionMaxComputer& f);

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionMaxComputer();
};

#endif // SCALAR_FUNCTION_MAX_COMPUTER_HPP
