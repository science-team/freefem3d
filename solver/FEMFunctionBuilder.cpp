//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: FEMFunctionBuilder.cpp,v 1.7 2007/05/20 23:22:26 delpinux Exp $

#include <FEMFunctionBase.hpp>

#include <FEMFunctionBuilder.hpp>
#include <FiniteElementTraits.hpp>

#include <Structured3DMesh.hpp>
#include <SpectralMesh.hpp>
#include <MeshOfTetrahedra.hpp>
#include <MeshOfHexahedra.hpp>

#include <MeshOfTriangles.hpp>

#include <FEMFunction.hpp>

template <typename MeshType>
void
FEMFunctionBuilder::
__build(const DiscretizationType& d,
	const MeshType* mesh)
{
  typedef typename MeshType::CellType CellType;

  switch(d.type()) {
  case DiscretizationType::lagrangianFEM0: {
    typedef
      FiniteElementTraits<CellType,DiscretizationType::lagrangianFEM0>
      FETraits;

    __builtFunction
      = new FEMFunction<MeshType, FETraits>(mesh);
    break;
  }
  case DiscretizationType::lagrangianFEM1: {
    typedef
      FiniteElementTraits<CellType,DiscretizationType::lagrangianFEM1>
      FETraits;

    __builtFunction
      = new FEMFunction<MeshType, FETraits>(mesh);
    break;
  }
  case DiscretizationType::lagrangianFEM2: {
    typedef
      FiniteElementTraits<CellType,DiscretizationType::lagrangianFEM2>
      FETraits;

    __builtFunction
      = new FEMFunction<MeshType, FETraits>(mesh);
    break;

  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown discretization type",
		       ErrorHandler::unexpected);
  }
  }
}


void
FEMFunctionBuilder::
build(const DiscretizationType& d,
      const Mesh* mesh)
{
  switch (mesh->type()) {
  case Mesh::cartesianHexahedraMesh: {
    const Structured3DMesh* m
      = dynamic_cast<const Structured3DMesh*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::hexahedraMesh: {
    const MeshOfHexahedra* m
      = dynamic_cast<const MeshOfHexahedra*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::spectralMesh: {
    const SpectralMesh* m
      = dynamic_cast<const SpectralMesh*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::tetrahedraMesh: {
    const MeshOfTetrahedra* m
      = dynamic_cast<const MeshOfTetrahedra*>(mesh);
    this->__build(d, m);
    break;
  }
  case Mesh::trianglesMesh: {
    const MeshOfTriangles* m
      = dynamic_cast<const MeshOfTriangles*>(mesh);
    this->__build(d, m);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "not supported mesh type",
		       ErrorHandler::unexpected);
  }
  }
}

void
FEMFunctionBuilder::
build(const DiscretizationType& d,
      const Mesh* mesh,
      const ScalarFunctionBase& u)
{
  this->build(d,mesh);
  FEMFunctionBase& femFunctionView = dynamic_cast<FEMFunctionBase&>(*__builtFunction);
  femFunctionView = u;
}


void
FEMFunctionBuilder::
build(const DiscretizationType& d,
      const Mesh* mesh,
      const Vector<real_t>& values,
      const real_t& outsideValue)
{
  this->build(d,mesh);
  FEMFunctionBase& femFunctionView = dynamic_cast<FEMFunctionBase&>(*__builtFunction);
  femFunctionView = values;
  femFunctionView.setOutsideValue(outsideValue);
}

ReferenceCounting<FEMFunctionBase>
FEMFunctionBuilder::
getBuiltFEMFunction()
{
  return __builtFunction;
}

ConstReferenceCounting<ScalarFunctionBase>
FEMFunctionBuilder::
getBuiltScalarFunction() const
{
  return static_cast<const FEMFunctionBase*>(__builtFunction);
}

FEMFunctionBuilder::
FEMFunctionBuilder()
{
  ;
}

FEMFunctionBuilder::
~FEMFunctionBuilder()
{
  ;
}
