//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Triangle.hpp,v 1.17 2007/05/20 23:02:46 delpinux Exp $


#ifndef  TRIANGLE_HPP
#define  TRIANGLE_HPP

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <SurfElem.hpp>

#include <Edge.hpp>

/**
 * @file   Triangle.hpp
 * @author St�phane Del Pino
 * @date   Sat Jul 27 17:32:51 2002
 * 
 * @brief  Describes triangles in 3D
 * @todo Split this class for the 2D case
 */
class Triangle
  : public SurfElem
{
private:

  void __computeVolume()
  {
    const TinyVector<3>& X0 = *(__vertices[0]);
    const TinyVector<3>& X1 = *(__vertices[1]);
    const TinyVector<3>& X2 = *(__vertices[2]);

    const TinyVector<3> A(X1-X0);
    const TinyVector<3> B(X2-X0);

    __volume = 0.5*Norm(A^B);
  }

public:
  enum {
    NumberOfVertices = 3,	/**< number of vertices */
    NumberOfFaces    = 3,	/**< number of faces    */
    NumberOfEdges    = 3	/**< number of edges    */
  };

  typedef Edge FaceType;

  static const size_t faces[NumberOfEdges][FaceType::NumberOfVertices];
  static const size_t edges[NumberOfEdges][Edge::NumberOfVertices];

  /** 
   * computes barycentric coordinates in the current triangle
   * related to a given \a X position.
   * 
   * @param X Cartesian coordinates
   * @param lambda barycentric coordinates
   *
   * @todo This could be optimized
   * @warning this function can only be caught in the planar case
   */
  inline void getBarycentricCoordinates(const TinyVector<2, real_t>& X,
					TinyVector<3, real_t>& lambda) const
  {
    TinyVector<3, real_t> X2;
    X2[0] = X[0];
    X2[1] = X[1];
    X2[2] = 1;

    TinyMatrix<3, 3, real_t> M = 0;
    for (size_t i=0; i<3; ++i) {
      const Vertex& V = (*this)(i);
      for (size_t j=0; j<2; ++j) {
	M(j, i) = V[j];
      }
    }

    for (size_t i=0; i<3; ++i) {
      M(2,i) = 1;
    }

    lambda = X2/M;
  }

  /** 
   * The number of vertices of a triangle
   * 
   * @return the number of vertices
   */
  size_t numberOfVertices() const
  {
    return NumberOfVertices;
  }

  /** 
   * Access to the type of this cell
   * 
   * @return Cell::triangle3d
   */
  Cell::Type type() const
  {
    return Cell::triangle3d;
  }
  
  /** 
   *  Returns the normal to the face.
   * 
   * 
   * @return normal
   */
  const TinyVector<3,real_t> normal() const
  {
    const TinyVector<3>& v0 = *(__vertices[0]);
    const TinyVector<3>& v1 = *(__vertices[1]);
    const TinyVector<3>& v2 = *(__vertices[2]);

    const TinyVector<3> A(v1-v0);
    const TinyVector<3> B(v2-v0);
    TinyVector<3> N(A^B);
    const real_t n = Norm(N);
    N/=n;
    return N;
  }

  /** 
   * Access to the ith edge of the triangle
   * 
   * @param i the number of the Edge
   * 
   * @return the ith edge of the triangle
   */
  Edge edge(size_t i) const
  {
    ASSERT(i<NumberOfVertices);
    return Edge(*__vertices[(i+1)%3], *__vertices[(i+2)%3]);
  }

  /** 
   * Prepares the structures for the Cells
   * 
   * 
   */
  Triangle()
    : SurfElem(NumberOfVertices)
  {
    ;
  }

  /** 
   * Constructs a triangle using 3 vertices.
   * 
   * @param vertices the vertices of the Triangle
   * @param reference the reference of the triangle (default is 0)
   *
   */
  Triangle(const TinyVector<NumberOfVertices, Vertex*>& vertices,
	   const size_t& reference=0)
    : SurfElem(NumberOfVertices, reference)
  {
    for (size_t i=0; i<NumberOfVertices; ++i) {
      __vertices[i] = vertices[i];
    }

    /// Now computes the volume (area) of the triangle
    this->__computeVolume();
  }

  /** 
   * Constructs a triangle using 3 vertices.
   * 
   * @param v0 first vertex
   * @param v1 second vertex
   * @param v2 third vertex
   * @param reference the reference of the triangle (default is 0)
   *
   */
  Triangle(const Vertex& v0,
	   const Vertex& v1,
	   const Vertex& v2,
	   const size_t& reference=0)
    : SurfElem(NumberOfVertices, reference)
  {
    __vertices[0] = (Vertex*)&v0;
    __vertices[1] = (Vertex*)&v1;
    __vertices[2] = (Vertex*)&v2;

    /// Now computes the volume (area) of the triangle
    this->__computeVolume();
  }

  /** 
   * Copy constructor
   * 
   * @param st 
   * 
   */
  Triangle(const Triangle& st)
    : SurfElem(st)
  {
    ;
  }

  /** 
   * Destructor
   * 
   * 
   */
  ~Triangle()
  {
    ;
  }

};

#endif // TRIANGLE_HPP

