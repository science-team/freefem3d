//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VariationalBorderOperatorFV.hpp,v 1.3 2006/07/20 19:08:54 delpinux Exp $

#ifndef VARIATIONAL_BORDER_OPERATOR_FV_HPP
#define VARIATIONAL_BORDER_OPERATOR_FV_HPP

/**
 * @file   VariationalBorderOperatorFV.hpp
 * @author St�phane Del Pino
 * @date   Tue Jul 23 15:15:31 2002
 * 
 * @brief  Defines @f$ \int_\Gamma fv @f$
 * 
 * Defines @f$ \int_\Gamma fv @f$, where @f$\Gamma@f$ is a border of a
 * domain
 */
class VariationalBorderOperatorFV
  : public VariationalLinearBorderOperator
{
private:
  ConstReferenceCounting<ScalarFunctionBase>
  __f;				/**< function @f$ f @f$ */

public:
  /** 
   * Access to the function f
   * 
   * @return *__f
   */
  const ScalarFunctionBase& f() const
  {
    return *__f;
  }

  /** 
   * Constructor of the term @f$ \int f v @f$
   * 
   * @param testFunctionNumber test function number
   * @param f the function @f$ f @f$
   * @param border the border where to compute the integral
   */
  VariationalBorderOperatorFV(const size_t& testFunctionNumber,
			      ConstReferenceCounting<ScalarFunctionBase> f,
			      ConstReferenceCounting<Boundary> border)
    : VariationalLinearBorderOperator(VariationalLinearBorderOperator::FV,
				      testFunctionNumber, border),
      __f(f)
  {
    ;
  }

  /** 
   * Copy constructor
   * 
   * @param V a VariationalBorderOperatorFV
   */
  VariationalBorderOperatorFV(const VariationalBorderOperatorFV& V)
    : VariationalLinearBorderOperator(V),
      __f(V.__f)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~VariationalBorderOperatorFV()
  {
    ;
  }
};

#endif // VARIATIONAL_BORDER_OPERATOR_FV_HPP

