//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ScalarFunctionBuilder.hpp,v 1.2 2006/10/28 12:26:54 delpinux Exp $

#ifndef SCALAR_FUNCTION_BUILDER_HPP
#define SCALAR_FUNCTION_BUILDER_HPP

#include <ReferenceCounting.hpp>
#include <ErrorHandler.hpp>

#include <ScalarFunctionBase.hpp>
#include <BinaryOperation.hpp>

/**
 * @file   ScalarFunctionBuilder.hpp
 * @author Stephane Del Pino
 * @date   Thu Jul 20 00:38:25 2006
 * 
 * @brief factory to build several types of functions
 * 
 */
class ScalarFunctionBuilder
{
private:
  /** 
   * @class Simplifier is used to simplify functions at built
   * time. For instance, if f=1 and g=2, f+g is replaced by 3...
   */
  class Simplifier;

  ConstReferenceCounting<ScalarFunctionBase>
  __builtFunction;		/**< built function */

public:
  /** 
   * set a function
   * 
   * @param f function to set
   */
  void setFunction(ConstReferenceCounting<ScalarFunctionBase> f);

  /** 
   * set binary operation
   * 
   * @param type the type of the operation
   * @param g the second operand
   */
  void setBinaryOperation(const BinaryOperation type,
			  ConstReferenceCounting<ScalarFunctionBase> g);

  /** 
   * set unary minus
   * 
   */
  void setUnaryMinus();

  /** 
   * set not function
   * 
   */
  void setNot();

  /** 
   * Set the C-function labeled by the string @a cfunction
   * 
   * @param cfunction the name of a C-function
   */
  void setCFunction(const std::string& cfunction);

  /** 
   * Read-only access to the function
   * 
   * @return __builtFunction
   */
  ConstReferenceCounting<ScalarFunctionBase>
  getBuiltFunction()
  {
    return __builtFunction;
  }

  /** 
   * Constructor
   * 
   */
  ScalarFunctionBuilder()
    : __builtFunction(0)
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ScalarFunctionBuilder()
  {
    ;
  }
};

#endif // SCALAR_FUNCTION_BUILDER_HPP
