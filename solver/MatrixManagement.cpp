//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MatrixManagement.cpp,v 1.3 2007/06/09 10:37:07 delpinux Exp $

#define _DECLARE_MRC_
#include <MatrixManagement.hpp>
static MemoryRepository MR;
MemoryRepository * memoryRepository = &MR;

#include <TinyVector.hpp>
#include <TinyMatrix.hpp>

#include <Vector.hpp>
#include <SparseMatrix.hpp>

#include <DoubleHashedMatrix.hpp>
#include <UnAssembledMatrix.hpp>

void MemoryRepository::store(int i,ReferenceCounting<BaseMatrix>& matrix)
{
  __matrices[i] = matrix;
}

ReferenceCounting<BaseMatrix>& MemoryRepository::getMatrix(int i)
{
  if (__matrices.count(i) != 0)
    return __matrices[i];
  else {
    throw ErrorHandler(__FILE__,__LINE__,
		       "tying to access to a none stored matrix ("
		       +stringify(i)+")",
		       ErrorHandler::unexpected);
  }
  return __matrices[0];
}

bool MemoryRepository::stored(int i) const
{
  return (__matrices.count(i) != 0);
}

/*!
  returns true if the matrix is new. So if the assembling is to perform.
*/
bool MemoryManager::ReserveMatrix(ReferenceCounting<BaseMatrix>& A,
				  const size_t dimension,
				  const size_t nbDegreeOfFreefom)
{
  int keepMatrix = -1;
  //! keepMatrix != -1 means that the matrix is stored or to be stored.
  if ((keepMatrix == -1) || (!__mr.stored(keepMatrix))) {
    switch (__options.value().matrixType()) {
    case (MemoryManagerOptions::none): {
      A = new UnAssembledMatrix(nbDegreeOfFreefom);
      break;
    }
    case (MemoryManagerOptions::sparse): {
      A = new DoubleHashedMatrix(nbDegreeOfFreefom, nbDegreeOfFreefom);
      break;
    }
    default: {
      A = new DoubleHashedMatrix(nbDegreeOfFreefom, nbDegreeOfFreefom);
      break;
    }
    }
    return true;
  } else {
    A = __mr.getMatrix(keepMatrix);
    return false;
  }
}


void MemoryManager::ReserveVector(ReferenceCounting<BaseVector>& b,
				  size_t dimension,
				  size_t nbDegreeOfFreefom)
{
  b = new Vector<real_t>(nbDegreeOfFreefom);
  static_cast<Vector<real_t>&>(*b) = 0;
}
