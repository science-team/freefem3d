/****************************************************************************
** Meta object code from reading C++ file 'EditorWindow.hpp'
**
** Created: Thu May 24 22:12:42 2007
**      by: The Qt Meta Object Compiler version 59 (Qt 4.2.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/ff3d/graphics/EditorWindow.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'EditorWindow.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.2.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_EditorWindow[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_EditorWindow[] = {
    "EditorWindow\0\0fontDialogue()\0"
};

const QMetaObject EditorWindow::staticMetaObject = {
    { &QTextEdit::staticMetaObject, qt_meta_stringdata_EditorWindow,
      qt_meta_data_EditorWindow, 0 }
};

const QMetaObject *EditorWindow::metaObject() const
{
    return &staticMetaObject;
}

void *EditorWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_EditorWindow))
	return static_cast<void*>(const_cast< EditorWindow*>(this));
    return QTextEdit::qt_metacast(_clname);
}

int EditorWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: fontDialogue(); break;
        }
        _id -= 1;
    }
    return _id;
}
