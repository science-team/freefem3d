//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SyntaxHighLighter.hpp,v 1.1 2005/08/08 09:44:50 delpinux Exp $

#ifndef SYNTAX_HIGHLIGHTER_HPP
#define SYNTAX_HIGHLIGHTER_HPP

#include <QtCore/QHash>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtGui/QTextBlock>
#include <QtGui/QTextCharFormat>

class QTextDocument;

/**
 * @file   SyntaxHighLighter.hpp
 * @author Stephane Del Pino
 * @date   Sun Aug  7 16:01:41 2005
 * 
 * @brief  Syntax highlighting class.
 * @note   This is taken from Qt4's documentation
 * 
 */

class SyntaxHighLighter
  : public QObject
{
  Q_OBJECT
public:
  SyntaxHighLighter(QObject* parent = 0);

  ~SyntaxHighLighter();

  void addToDocument(QTextDocument* doc);
  void addMapping(const QString& pattern,
		  const QTextCharFormat &format);

private slots:
void highlight(int from,
	       int removed,
	       int added);

private:
  void highlightBlock(QTextBlock block);

  QHash<QString,QTextCharFormat> __mappings;
};


#endif // SYNTAX_HIGHLIGHTER_HPP
