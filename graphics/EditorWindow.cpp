//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: EditorWindow.cpp,v 1.11 2005/08/08 09:53:16 delpinux Exp $

#include <EditorWindow.hpp>
#include <FFSyntaxHighlighter.hpp>

#include <QtGui/QTextCharFormat>
#include <QtGui/QFontDialog>

void EditorWindow::fontDialogue()
{
  bool ok;
  QFont font
    = QFontDialog::getFont(&ok,this->currentFont(), this);
  if (ok) {
    this->setFont(font);
    this->document()->setDefaultFont(font);
    this->setCurrentFont(font);
  }
}

EditorWindow::EditorWindow(QWidget* widget)
    : QTextEdit(widget)
{
  FFSyntaxHighlighter* syntaxHighlighter
    = new FFSyntaxHighlighter(this);

  QTextCharFormat defaultFormat;
  defaultFormat.setFontFamily("Courier New");
  defaultFormat.setFontPointSize(10);

  this->setFont(defaultFormat.font());
//   this->setFont(QFont("Courier New", 10));
  this->document()->setDefaultFont(defaultFormat.font());
  this->setCurrentFont(defaultFormat.font());
}


EditorWindow::~EditorWindow()
{
  ;
}
