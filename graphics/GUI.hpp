//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: GUI.hpp,v 1.4 2005/07/31 22:30:05 delpinux Exp $


#ifndef GUI_HPP
#define GUI_HPP

#include <QtGui/QApplication>

/**
 * @file   GUI.hpp
 * @author Stephane Del Pino
 * @date   Wed Nov 10 19:07:33 2004
 * 
 * @brief  ff3d's GUI
 * 
 * This class is makes ff3d a Qt application
 */

class GUI
  : public QApplication
{
public:
  /** 
   * Constructor
   * 
   * @param argc number of arguments
   * @param argv the arguments
   * 
   */
  GUI(int argc, char* argv[]);

  /** 
   * Destructor
   * 
   * 
   */
  ~GUI()
  {
    ;
  }
};

#endif // GUI_HPP
