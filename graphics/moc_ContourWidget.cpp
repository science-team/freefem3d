/****************************************************************************
** Meta object code from reading C++ file 'ContourWidget.hpp'
**
** Created: Thu May 24 22:12:42 2007
**      by: The Qt Meta Object Compiler version 59 (Qt 4.2.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/ff3d/graphics/ContourWidget.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ContourWidget.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.2.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_ContourWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      17,   15,   14,   14, 0x0a,
      38,   36,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ContourWidget[] = {
    "ContourWidget\0\0i\0setPercentage(int)\0"
    "v\0setValue(real_t)\0"
};

const QMetaObject ContourWidget::staticMetaObject = {
    { &QVBoxLayout::staticMetaObject, qt_meta_stringdata_ContourWidget,
      qt_meta_data_ContourWidget, 0 }
};

const QMetaObject *ContourWidget::metaObject() const
{
    return &staticMetaObject;
}

void *ContourWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ContourWidget))
	return static_cast<void*>(const_cast< ContourWidget*>(this));
    return QVBoxLayout::qt_metacast(_clname);
}

int ContourWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QVBoxLayout::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: setPercentage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: setValue((*reinterpret_cast< real_t(*)>(_a[1]))); break;
        }
        _id -= 2;
    }
    return _id;
}
