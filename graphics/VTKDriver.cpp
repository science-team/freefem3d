//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VTKDriver.cpp,v 1.19 2006/08/27 21:31:32 delpinux Exp $

#include <config.h>

#ifdef HAVE_GUI_LIBS

#include <MeshOfHexahedra.hpp>
#include <MeshOfTetrahedra.hpp>
#include <Structured3DMesh.hpp>

#include <SurfaceMeshOfTriangles.hpp>
#include <SurfaceMeshOfQuadrangles.hpp>

#include <RunningOptions.hpp>

// #include <qpushbutton.h>
// #include <qapplication.h>
// #include <qslider.h>
// #include <qgrid.h>
// #include <qlabel.h>
// #include <qvgroupbox.h>
// #include <qlayout.h>
// #include <qmenubar.h>
// #include <qpopupmenu.h>

// #include <QGLvtkWindow.hpp>

#include <VTKDriver.hpp>

#include <vtkProperty.h>
#include <vtkPointData.h>
#include <vtkContourFilter.h>
#include <vtkCellData.h>
#include <vtkLODActor.h>
#include <vtkRenderer.h>
#include <vtkStructuredGridOutlineFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkXOpenGLRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkTriangle.h>
#include <vtkQuad.h>
#include <vtkHexahedron.h>
#include <vtkTetra.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkDataSetToPolyDataFilter.h>
#include <vtkDoubleArray.h>


// #include <ContourWidget.hpp>

// #include <GUI.hpp>

// extern GUI* gui;

vtkDoubleArray* VTKDriver::__getValues(const Mesh& m,
				       ConstReferenceCounting<ScalarFunctionBase> pU)
{
  vtkDoubleArray* values = vtkDoubleArray::New();
  values->Resize(m.numberOfVertices());

  if (pU == 0) { // reference values are plotted
    for (size_t i=0; i<m.numberOfVertices(); ++i) {
      values->InsertNextValue(m.vertex(i).reference());
    }
  } else { // we plot the function values
#warning COULD BE OPTIMIZED A LOT
    // If plotting a fem function vertices values are recomputed even
    // if values are know (ie: we plot the function on its definition
    // mesh)...
    // This is slower but always works...
    const ScalarFunctionBase& u = *pU;
    for (size_t i=0; i<m.numberOfVertices(); ++i) {
      values->InsertNextValue(u(m.vertex(i)));
    }
  }
  return values;
}

template <>
struct VTKDriver::Traits<MeshOfTetrahedra>
{
  typedef vtkUnstructuredGrid VTKMeshType;
  typedef vtkTetra VTKCellType;
};

template <>
struct VTKDriver::Traits<MeshOfHexahedra>
{
  typedef vtkUnstructuredGrid VTKMeshType;
  typedef vtkHexahedron VTKCellType;
};

template <>
struct VTKDriver::Traits<Structured3DMesh>
{
  typedef vtkUnstructuredGrid VTKMeshType;
  typedef vtkHexahedron VTKCellType;
};

template <>
struct VTKDriver::Traits<SurfaceMeshOfTriangles>
{
  typedef vtkUnstructuredGrid VTKMeshType;
  typedef vtkTriangle VTKCellType;
};

template <>
struct VTKDriver::Traits<SurfaceMeshOfQuadrangles>
{
  typedef vtkUnstructuredGrid VTKMeshType;
  typedef vtkQuad VTKCellType;
};

template <typename MeshType>
void VTKDriver::__plot(const MeshType& m,
		       ConstReferenceCounting<ScalarFunctionBase> pU)
{
  // Defines needed types
  typedef
    typename VTKDriver::Traits<MeshType>::VTKMeshType
    VTKMeshType;
  typedef
    typename VTKDriver::Traits<MeshType>::VTKCellType
    VTKCellType;
  typedef typename MeshType::CellType CellType;

  vtkPoints* vertices = vtkPoints::New();
  vertices->SetNumberOfPoints(m.numberOfVertices());

  // Creates vertices list
  for (size_t i=0; i<m.numberOfVertices(); ++i) {
    const Vertex& X = m.vertex(i);
    vertices->InsertPoint(i,X[0],X[1],X[2]);
  }

  VTKCellType* cell = VTKCellType::New();
  VTKMeshType* grid = VTKMeshType::New();
  grid->Allocate(m.numberOfCells(),1);

  // Converts the mesh
  for (size_t i=0; i<m.numberOfCells(); ++i) {
    const CellType& K = m.cell(i);
    for (size_t j=0; j<CellType::NumberOfVertices; ++j) {
      cell->GetPointIds()->SetId(j,m.vertexNumber(K(j)));
    }
    grid->InsertNextCell(cell->GetCellType(),
			 cell->GetPointIds());
  }

  grid->SetPoints(vertices);

  vtkDoubleArray* values = this->__getValues(m, pU);

  grid->GetPointData()->SetScalars(values);

  // We create an instance of vtkPolyDataMapper to map the polygonal data 
  // into graphics primitives. We connect the output of the cone souece 
  // to the input of this mapper 

//   vtkStructuredGridOutlineFilter* outlineFilter = vtkStructuredGridOutlineFilter::New();
//   outlineFilter->SetInput(grid);

  vtkDataSetMapper* mapper = vtkDataSetMapper::New();
  mapper->SetInput(grid);
  mapper->SetScalarRange(values->GetRange());

  // contour
  vtkContourFilter* contour = vtkContourFilter::New();
  contour->SetInput(grid);

  contour->SetNumberOfContours(1);
//   contour->GenerateValues(10,
// 			  0.95*values->GetRange()[0]+0.05*values->GetRange()[1],
// 			  0.05*values->GetRange()[0]+0.95*values->GetRange()[1]);
  contour->SetValue(0,
		    0.95*values->GetRange()[0]+0.05*values->GetRange()[1]);
  contour->UseScalarTreeOn();

  vtkPolyDataMapper* mapper2 = vtkPolyDataMapper::New();
  mapper2->SetInput(contour->GetOutput());
  mapper2->SetScalarRange(values->GetRange());

  // create an actor to represent the cone. The actor coordinates rendering of
  // the graphics primitives for a mapper. We set this actor's mapper to be
  // coneMapper which we created above.
  //

  vtkLODActor *actor1 = vtkLODActor::New();
  actor1->SetMapper( mapper );
  actor1->GetProperty()->SetEdgeColor (1,1,1);
  actor1->GetProperty()->EdgeVisibilityOn();
  actor1->GetProperty()->SetRepresentationToWireframe();
  actor1->GetProperty()->SetOpacity(0.2);
  actor1->GetProperty()->SetColor(1,1,1);
  actor1->SetNumberOfCloudPoints(1000);

  vtkLODActor *actor2 = vtkLODActor::New();
  actor2->SetMapper( mapper2 );
  actor2->SetNumberOfCloudPoints(1000);

  // Create the Renderer and assign actors to it. A renderer is like a
  // viewport. It is part or all of a window on the screen and it is
  // responsible for drawing the actors it has.  We also set the background
  // color here
  //
  vtkRenderer *ren1= vtkRenderer::New();
  ren1->AddActor( actor1 );
  ren1->AddActor( actor2 );
  ren1->SetBackground( 0.1, 0.2, 0.4 );

//   QWidget* mainWidget = new QWidget(0,0);

//   QBoxLayout * mainWindow = new QVBoxLayout(mainWidget,5,2,"mainWindow");
//   QMenuBar* menuBar = new QMenuBar(mainWidget,"menubar");
//   mainWindow->addWidget(menuBar);
//   menuBar->setSeparator( QMenuBar::InWindowsStyle );
//   QPopupMenu* popup;
//   popup = new QPopupMenu(mainWidget);
//   popup->insertItem( "&Continue", gui, SLOT(quit()) );
//   menuBar->insertItem( "&File", popup );

//   QGrid* box = new QGrid(2,mainWidget,"grid",0);
//   box->layout()->setSpacing( 6 );
//   QVGroupBox*optionsFrame = new QVGroupBox(QString("options"),box,"options");
//   optionsFrame->setMaximumWidth(250);
//   QVBoxLayout* optionsContent = new QVBoxLayout( optionsFrame->layout() );
//   optionsContent->setAlignment( Qt::AlignTop );

//   ContourWidget* contourWidget;
//   contourWidget = new ContourWidget(optionsFrame, "contour");
//   contourWidget->show();
//   optionsContent->addWidget(contourWidget);

//   QVGroupBox*graphicFrame = new QVGroupBox(QString("vtk"),box,"graphics");
//   QVBoxLayout* graphicContent = new QVBoxLayout( graphicFrame->layout() );
//   graphicContent->setAlignment( Qt::AlignTop );

//   mainWidget->setGeometry( 0, 0, 640, 480 );
//   box->show();
//   box->setMinimumSize(320,200);
//   QSizePolicy qSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
//   box->setSizePolicy(qSizePolicy);
//   mainWindow->addWidget(box);

//   QGLvtkWindow* window = new QGLvtkWindow(graphicFrame,"OpenGL Window");
//   graphicContent->insertWidget(1,window);

//   window->addRenderer(ren1);
//   window->setSizePolicy(qSizePolicy);

//   contourWidget->set(window, contour, values);

//   mainWidget->show();
//   gui->setMainWidget( mainWidget );
//   gui->exec();

  values->Delete();
  grid->Delete();
  vertices->Delete();
}

void VTKDriver::plot(const Mesh& m,
		     ConstReferenceCounting<ScalarFunctionBase> u)
{
  if (not(RunningOptions::instance().useGUI())) {
    if (not(RunningOptions::instance().haveDisplay())) {
      fferr(1) << "warning: could not open DISPLAY, not processing plot()\n";
    } else {
      fferr(1) << "warning: not processing plot() when using '-nw' option\n";
    }
    return;
  }

  switch (m.type()) {
  case Mesh::hexahedraMesh: {
    this->__plot(dynamic_cast<const MeshOfHexahedra&>(m), u);
    break;
  }
  case Mesh::cartesianHexahedraMesh: {
    this->__plot(dynamic_cast<const Structured3DMesh&>(m), u);
    break;
  }
  case Mesh::tetrahedraMesh: {
    this->__plot(dynamic_cast<const MeshOfTetrahedra&>(m), u);
    break;
  }
  case Mesh::surfaceMeshTriangles: {
    this->__plot(dynamic_cast<const SurfaceMeshOfTriangles&>(m), u);
    break;
  }
  case Mesh::surfaceMeshQuadrangles: {
    this->__plot(dynamic_cast<const SurfaceMeshOfQuadrangles&>(m), u);
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "unknown mesh type",
		       ErrorHandler::unexpected);
  }
  }
}

#endif // HAVE_GUI_LIBS
