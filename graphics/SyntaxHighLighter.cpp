//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: SyntaxHighLighter.cpp,v 1.2 2005/11/27 21:44:12 delpinux Exp $

#include <SyntaxHighLighter.hpp>
#include <QtGui/QTextDocument>
#include <QtGui/QTextLayout>

SyntaxHighLighter::
SyntaxHighLighter(QObject *parent)
  : QObject(parent)
{
  ;
}

SyntaxHighLighter::
~SyntaxHighLighter()
{
  ;
}

void SyntaxHighLighter::
addToDocument(QTextDocument *doc)
{
  connect(doc, SIGNAL(contentsChange(int, int, int)),
	  this, SLOT(highlight(int, int, int)));
}

void SyntaxHighLighter::
addMapping(const QString &pattern,
	   const QTextCharFormat &format)
{
  __mappings[pattern] = format;
}

void SyntaxHighLighter::
highlight(int position, int removed, int added)
{
  QTextDocument *doc = qobject_cast<QTextDocument *>(sender());

  QTextBlock block = doc->findBlock(position);
  if (!block.isValid())
    return;

  QTextBlock endBlock;
  if (added > removed)
    endBlock = doc->findBlock(position + added);
  else
    endBlock = block;

  while (block.isValid() && !(endBlock < block)) {
    highlightBlock(block);
    block = block.next();
  }
}

void SyntaxHighLighter::
highlightBlock(QTextBlock block)
{
  QTextLayout *layout = block.layout();
  const QString text = block.text();

  QList<QTextLayout::FormatRange> overrides;

  foreach (QString pattern, __mappings.keys()) {
    QRegExp expression(pattern);
    int i = text.indexOf(expression);
    while (i >= 0) {
      QTextLayout::FormatRange range;
      range.start = i;
      range.length = expression.matchedLength();
      range.format = __mappings[pattern];
      range.format.setFontFamily("Courier New");
      range.format.setFontPointSize(10);

      overrides << range;

      i = text.indexOf(expression, i + expression.matchedLength());
    }
  }

  layout->setAdditionalFormats(overrides);
  const_cast<QTextDocument *>(block.document())->markContentsDirty(block.position(), block.length());
}
