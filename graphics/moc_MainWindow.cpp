/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.hpp'
**
** Created: Thu May 24 22:12:42 2007
**      by: The Qt Meta Object Compiler version 59 (Qt 4.2.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/ff3d/graphics/MainWindow.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.2.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_MainWindow[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      22,   11,   11,   11, 0x08,
      40,   31,   11,   11, 0x08,
      54,   11,   11,   11, 0x08,
      61,   11,   11,   11, 0x08,
      70,   11,   11,   11, 0x08,
      78,   11,   11,   11, 0x08,
      84,   11,   11,   11, 0x08,
      92,   11,   11,   11, 0x08,
     101,   11,   11,   11, 0x08,
     108,   11,   11,   11, 0x08,
     120,   11,   11,   11, 0x08,
     130,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0newFile()\0choose()\0fileName\0"
    "load(QString)\0save()\0saveAs()\0print()\0"
    "run()\0pause()\0resume()\0stop()\0aboutff3d()\0"
    "aboutQt()\0aboutVtk()\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

const QMetaObject *MainWindow::metaObject() const
{
    return &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
	return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: newFile(); break;
        case 1: choose(); break;
        case 2: load((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: save(); break;
        case 4: saveAs(); break;
        case 5: print(); break;
        case 6: run(); break;
        case 7: pause(); break;
        case 8: resume(); break;
        case 9: stop(); break;
        case 10: aboutff3d(); break;
        case 11: aboutQt(); break;
        case 12: aboutVtk(); break;
        }
        _id -= 13;
    }
    return _id;
}
