//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 Stéphane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ContourWidget.hpp,v 1.3 2005/07/31 23:58:41 delpinux Exp $

#ifndef CONTOUR_WIDGET_HPP
#define CONTOUR_WIDGET_HPP

#include <QtGui/QWidget>
#include <QtGui/QSlider>
#include <QtGui/QVBoxLayout>

#include <QVTKWidget.h>
#include <vtkContourFilter.h>
#include <vtkDoubleArray.h>

#include <QRealSpinBox.hpp>

#include <iostream>

class ContourWidget
  : public QVBoxLayout
{
  Q_OBJECT

private:
  QVTKWidget* __window;
  vtkContourFilter* __contour;
  vtkDoubleArray* __values;

  QRealSpinBox* __spinBox;
  QSlider* __slider;
public slots:

  void setPercentage(int i)
  {
    if (__spinBox->percentage() != __slider->value()) {
      __spinBox->setPercentage(__slider->value());
    }
    __contour->SetValue(0,__spinBox->value());
//     __window->updateGL();
  }

  void setValue(real_t v)
  {
    if (__spinBox->percentage() != __slider->value()) {
      __slider->setValue(__spinBox->percentage());
    }
    __contour->SetValue(0,v);
//     __window->updateGL();
  }

public:
  void set(QVTKWidget* window,
	   vtkContourFilter* contour,
	   vtkDoubleArray* value)
  {
    __window  = window;
    __contour = contour;
    __values  = value;
    __spinBox->setRange(__values->GetRange()[0], __values->GetRange()[1]);
  }

  ContourWidget(QWidget* parent = 0)
    : QVBoxLayout(parent),
      __spinBox(new QRealSpinBox(0., 1., 3, 0.01, parent)),
      __slider(new QSlider(Qt::Horizontal, parent))
  {
//     setMaximumWidth(__spinBox->width());
    
    connect(__slider,SIGNAL(valueChanged(int)), this, SLOT(setPercentage(int)) );
    connect(__spinBox,SIGNAL(valueChanged(real_t)), this, SLOT(setValue(real_t)));
  }

  ~ContourWidget()
  {
    ;
  }
};


#endif // CONTOUR_WIDGET_HPP
