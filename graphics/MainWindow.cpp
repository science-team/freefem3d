//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: MainWindow.cpp,v 1.12 2007/02/19 15:42:47 delpinux Exp $

#include <MainWindow.hpp>

#include <QtGui/QCloseEvent>
#include <QtGui/QImage>
#include <QtGui/QPixmap>
#include <QtGui/QToolBar>
#include <QtGui/QToolButton>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>

#include <QtGui/QPushButton>
#include <QtGui/QApplication>
#include <QtGui/QSlider>
#include <QtGui/QGridLayout>

#include <QtGui/QLabel>
#include <QtGui/QGroupBox>
#include <QtGui/QLayout>

#include <QtGui/QTabWidget>
#include <QtGui/QFileDialog>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtGui/QMainWindow>
#include <QtGui/QStatusBar>

#include <QtGui/QMessageBox>
#include <QtGui/QPrinter>
#include <QtGui/QApplication>

#include <QtGui/QPainter>
#include <QtGui/QWhatsThis>
#include <QtGui/QAction>

#include <QtGui/QDockWidget>

#include <filesave.xpm>
#include <fileopen.xpm>
#include <fileprint.xpm>

#include <QtCore/QTimer>
#include <QVTKWidget.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

#include <vtkAxes.h>
#include <vtkVectorText.h>
#include <vtkTextSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkLODActor.h>

#include <EditorWindow.hpp>

#include <FFThread.hpp>
#include <StreamCenter.hpp>

#include <sstream>

#include <QtGui/QTextCharFormat>

void MainWindow::__buildFileTools()
{
    QAction * fileNewAction;
    QAction * fileOpenAction;
    QAction * fileSaveAction, * fileSaveAsAction, * filePrintAction;
    QAction * fileCloseAction, * fileQuitAction;

    fileNewAction = new QAction("&New", this);
    connect(fileNewAction, SIGNAL(activated()), this,
             SLOT(newFile()));
    fileNewAction->setShortcut(tr("Ctrl+N"));

    fileOpenAction = new QAction(QPixmap(fileopen), "&Open...", this);
    connect(fileOpenAction, SIGNAL(activated()), this, SLOT(choose()));

    fileOpenAction->setShortcut(tr("Ctrl+O"));
    const char * fileOpenText =
      "<p><img source=\"fileopen\"> "
      "Click this button to open a <it>new file</it>. <br>"
      "You can also select the <b>Open</b> command "
      "from the <b>File</b> menu.</p>";
//     QMimeSourceFactory::defaultFactory()->setPixmap("fileopen",
//                           fileOpenAction->iconSet().pixmap());
    fileOpenAction->setWhatsThis(fileOpenText);

    fileSaveAction = new QAction(QPixmap(filesave), "&Save", this);
    connect(fileSaveAction, SIGNAL(activated()), this, SLOT(save()));

    fileSaveAction->setShortcut(tr("Ctrl+S"));
    const char * fileSaveText =
      "<p>Click this button to save the file you "
      "are editing. You will be prompted for a file name.\n"
      "You can also select the <b>Save</b> command "
      "from the <b>File</b> menu.</p>";
    fileSaveAction->setWhatsThis(fileSaveText);

    fileSaveAsAction = new QAction(tr("Save &As..."),  this);
    connect(fileSaveAsAction, SIGNAL(activated()), this,
             SLOT(saveAs()));
    fileSaveAsAction->setWhatsThis(fileSaveText);

    filePrintAction = new QAction(QPixmap(fileprint), tr("&Print..."), this);
    connect(filePrintAction, SIGNAL(activated()), this,
             SLOT(print()));
    filePrintAction->setShortcut(tr("Ctrl+P"));

    const char * filePrintText = "Click this button to print the file you "
                     "are editing.\n You can also select the Print "
                     "command from the File menu.";
    filePrintAction->setWhatsThis(filePrintText);

    fileCloseAction = new QAction("&Close", this);
    connect(fileCloseAction, SIGNAL(activated()), this,
             SLOT(close()));
    fileCloseAction->setShortcut(tr("Ctrl+W"));

    fileQuitAction = new QAction(tr("&Quit"), this);
    connect(fileQuitAction, SIGNAL(activated()), qApp,
             SLOT(closeAllWindows()));
    fileQuitAction->setShortcut(tr("Ctrl+Q"));

    // populate a tool bar with some actions

    QToolBar * fileTools = addToolBar(tr("File operations"));
    fileTools->addAction(fileOpenAction);
    fileTools->addAction(fileSaveAction);
    fileTools->addAction(filePrintAction);

//     (void)QWhatsThis::whatsThisButton(fileTools);


    // populate a menu with all actions

    QMenu * file = new QMenu(tr("&File"), this);
    menuBar()->addMenu(file);
    file->addAction(fileNewAction);
    file->addAction(fileOpenAction);
    file->addAction(fileSaveAction);
    file->addAction(fileSaveAsAction);
    file->addSeparator();
    file->addAction(filePrintAction);
    file->addSeparator();
    file->addAction(fileCloseAction);
    file->addAction(fileQuitAction);
}

void MainWindow::__buildEditorTools()
{
    QMenu * editorMenu = new QMenu(tr("&Editor"), this);
    menuBar()->addMenu(editorMenu);
    editorMenu->addAction(tr("&Undo"), __editor, SLOT(undo()), tr("Ctrl+Z"));
    editorMenu->addAction(tr("&Redo"), __editor, SLOT(redo()), tr("Ctrl+Y"));
    editorMenu->addSeparator();
    editorMenu->addAction(tr("Cu&t"),  __editor, SLOT(cut()),  tr("Ctrl+X"));
    editorMenu->addAction(tr("&Copy"), __editor, SLOT(copy()), tr("Ctrl+C"));
    editorMenu->addAction(tr("&Paste"),__editor, SLOT(paste()),tr("Ctrl+V"));
    editorMenu->addSeparator();
    editorMenu->addAction(tr("Select All"), __editor, SLOT(selectAll()));
    editorMenu->addSeparator();
    editorMenu->addAction(tr("Change &Font"),__editor, SLOT(fontDialogue()));
    
//     editorMenu->addAction(fileNewAction);
}


void MainWindow::__buildActionTools()
{
  // adds the action menu
  QMenu * action = new QMenu("&Action", this);
  menuBar()->addMenu(action);
  action->addAction(tr("&Run"), this, SLOT(run()),
		      tr("Ctrl+R"));
  action->addAction(tr("&Pause"), this, SLOT(pause()));
  action->addAction(tr("&Resume"), this, SLOT(resume()));
  action->addAction(tr("&Stop"), this, SLOT(stop()));
}

void MainWindow::__buildHelpTools()
{
  QMenu * help = new QMenu(tr("&Help"), this);
  menuBar()->addMenu(help);
  help->addAction(tr("&About"), this, SLOT(aboutff3d()),
		  tr("F1"));
  help->addAction(tr("About &Qt"), this, SLOT(aboutQt()));
  help->addAction(tr("About &Vtk"), this, SLOT(aboutVtk()));
  help->addSeparator();
  help->addAction(tr("What's &This"), this, SLOT(whatsThis()),
		  tr("Shift+F1"));
}

MainWindow::MainWindow()
  : QMainWindow(0),
    __ffThread(0)
{
    printer = new QPrinter(QPrinter::HighResolution);

    // create and define the central widget
    QTabWidget* tabWidget = new QTabWidget(this);


    __editor = new EditorWindow;
    __editor->setFocus();

    setCentralWidget(tabWidget);
  
    tabWidget->addTab(__editor,tr("Editor"));

    QVTKWidget* graphicWindow = new QVTKWidget;
    tabWidget->addTab(graphicWindow, tr("Graphics"));

    vtkRenderer *ren1= vtkRenderer::New();
    ren1->SetBackground( 0.1, 0.2, 0.4 );
    vtkVectorText* text = vtkVectorText::New();
    text->SetText("ff3d");

    vtkPolyDataMapper* textMapper = vtkPolyDataMapper::New();
    textMapper->SetInput(text->GetOutput());
    vtkLODActor *actor = vtkLODActor::New();
    actor->SetMapper(textMapper);
    ren1->AddActor( actor );

    graphicWindow->GetRenderWindow()->AddRenderer(ren1);
    graphicWindow->GetRenderWindow()->Render();

    // console
    QDockWidget* consoleDock = new QDockWidget("console", this);
    __console = new QTextEdit;
    __console->setReadOnly(true);
    __console->setUndoRedoEnabled(false);
    consoleDock->setWidget(__console);
    this->addDockWidget(Qt::BottomDockWidgetArea, consoleDock);

    // Build menus
    this->__buildFileTools();
    this->__buildEditorTools();
    this->__buildActionTools();

    menuBar()->addSeparator();
    
    this->__buildHelpTools();

    statusBar()->showMessage(tr("Wellcome to FreeFEM3D"), 2000);
}


MainWindow::~MainWindow()
{
  if (__ffThread !=0) {
    delete __ffThread;
  } 
  delete printer;
}



void MainWindow::newFile()
{
    MainWindow *ed = new MainWindow;
    ed->show();
}

void MainWindow::choose()
{
    filename = QFileDialog::getOpenFileName(this);
    if (!filename.isEmpty())
	load(filename);
    else
	statusBar()->showMessage("Loading aborted", 2000);
}


void MainWindow::load(const QString &fileName)
{
    QFile f(fileName);
    if (!f.open( QIODevice::ReadOnly))
	return;

    QTextStream ts(&f);
    __editor->setPlainText(ts.readAll());
    __editor->setWindowModified(FALSE);
    setWindowTitle(fileName);
    statusBar()->showMessage("Loaded document " + fileName, 2000);
}


void MainWindow::save()
{
    if (filename.isEmpty()) {
	saveAs();
	return;
    }

    const QString& text = __editor->toPlainText();
    QFile f(filename);
    if (!f.open(QIODevice::WriteOnly)) {
	statusBar()->showMessage(QString("Could not write to %1").arg(filename),
				 2000);
	return;
    }

    QTextStream t(&f);
    t << text;
    f.close();

    __editor->setWindowModified(FALSE);

    setWindowTitle(filename);

    statusBar()->showMessage(QString("File %1 saved").arg(filename), 2000);
}


void MainWindow::saveAs()
{
    QString fn = QFileDialog::getSaveFileName(this);
    if (!fn.isEmpty()) {
	filename = fn;
	save();
    } else {
	statusBar()->showMessage("Saving aborted", 2000);
    }
}


void MainWindow::print()
{
  printer->setFullPage(TRUE);
  statusBar()->showMessage("Printing...");
  QPainter p;
  if(!p.begin(printer)) {               // paint on printer
    statusBar()->showMessage("Printing aborted", 2000);
    return;
  }
  fferr(1) << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
// 	QPaintDeviceMetrics metrics(p.device());
// 	int dpiy = metrics.logicalDpiY();
// 	int margin = (int) ((2/2.54)*dpiy); // 2 cm margins
// 	QRect body(margin, margin, metrics.width() - 2*margin, metrics.height() - 2*margin);
// 	QSimpleRichText richText(QStyleSheet::convertFromPlainText(editor->text()),
// 				  QFont(),
// 				  editor->context(),
// 				  editor->styleSheet(),
// 				  editor->mimeSourceFactory(),
// 				  body.height());
// 	richText.setWidth(&p, body.width());
//   	QRect view(body);
// 	int page = 1;
// 	do {
// 	    richText.draw(&p, body.left(), body.top(), view, colorGroup());
// 	    view.moveBy(0, body.height());
// 	    p.translate(0, -body.height());
// 	    p.drawText(view.right() - p.fontMetrics().width(QString::number(page)),
// 			view.bottom() + p.fontMetrics().ascent() + 5, QString::number(page));
// 	    if (view.top()  >= richText.height())
// 		break;
// 	    printer->newPage();
// 	    page++;
// 	} while (TRUE);

// 	statusBar()->showMessage("Printing completed", 2000);
//     } else {
// 	statusBar()->showMessage("Printing aborted", 2000);
//     }
}

void MainWindow::closeEvent(QCloseEvent* ce)
{
  if (!__editor->isWindowModified()) {
    ce->accept();
    return;
  }

  switch(QMessageBox::information(this, "FreeFEM3D",
				  "The document has been changed since "
				  "the last save.",
				  "Save Now", "Cancel", "Leave Anyway",
				  0, 1)) {
  case 0:
    save();
    ce->accept();
    break;
  case 1:
  default: // just for sanity
    ce->ignore();
    break;
  case 2:
    ce->accept();
    break;
  }
}


void MainWindow::run()
{
  std::stringstream text;
  text << __editor->toPlainText().toAscii().constData() << std::ends;

  QTextCharFormat defaultFormat;
  defaultFormat.setFontFamily("Courier New");
  defaultFormat.setFontPointSize(8);

  __console->mergeCurrentCharFormat(defaultFormat);
  __console->append("Running");
  
  __ffThread =new FFThread;
  __ffThread->setInput(text);
  __ffThread->setConsole(__console);
  __ffThread->start();

  statusBar()->showMessage("Running...", 2000);
  usleep(500000);
}

void MainWindow::pause()
{
  fferr(1) << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
}
void MainWindow::resume()
{
  fferr(1) << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
}

void MainWindow::stop()
{
  if (__ffThread != 0) {
    __ffThread->terminate();
    delete __ffThread;
  }
}


void MainWindow::aboutff3d()
{
    QMessageBox::about(this, "FreeFEM3D",
			"FreeFEM3D, blablabla");
}


void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this, "Qt blabla");
}

void MainWindow::aboutVtk()
{
    QMessageBox::about(this, "Vtk", "Vtk blabla");
}
