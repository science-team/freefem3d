/****************************************************************************
** Meta object code from reading C++ file 'QRealSpinBox.hpp'
**
** Created: Thu May 24 22:12:42 2007
**      by: The Qt Meta Object Compiler version 59 (Qt 4.2.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/ff3d/graphics/QRealSpinBox.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QRealSpinBox.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.2.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_QRealSpinBox[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      20,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      41,   14,   13,   13, 0x0a,
      58,   14,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_QRealSpinBox[] = {
    "QRealSpinBox\0\0value\0valueChanged(real_t)\0"
    "setValue(real_t)\0setPercentage(int)\0"
};

const QMetaObject QRealSpinBox::staticMetaObject = {
    { &QSpinBox::staticMetaObject, qt_meta_stringdata_QRealSpinBox,
      qt_meta_data_QRealSpinBox, 0 }
};

const QMetaObject *QRealSpinBox::metaObject() const
{
    return &staticMetaObject;
}

void *QRealSpinBox::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QRealSpinBox))
	return static_cast<void*>(const_cast< QRealSpinBox*>(this));
    return QSpinBox::qt_metacast(_clname);
}

int QRealSpinBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSpinBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: valueChanged((*reinterpret_cast< real_t(*)>(_a[1]))); break;
        case 1: setValue((*reinterpret_cast< real_t(*)>(_a[1]))); break;
        case 2: setPercentage((*reinterpret_cast< int(*)>(_a[1]))); break;
        }
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void QRealSpinBox::valueChanged(real_t _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
