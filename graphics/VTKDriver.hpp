//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: VTKDriver.hpp,v 1.7 2006/08/27 21:31:32 delpinux Exp $

#ifndef VTKDRIVER_HPP
#define VTKDRIVER_HPP

#include <Mesh.hpp>
#include <ScalarFunctionBase.hpp>

#include <ReferenceCounting.hpp>

#include <config.h>

class vtkDoubleArray;

/**
 * @file   VTKDriver.hpp
 * @author Stephane Del Pino
 * @date   Sat Nov 16 14:33:13 2002
 * 
 * @brief  A first attempt to VTK management inside ff3d
 * 
 */
class VTKDriver
{
private:
  /** 
   * Gets value of the function (*u) or sets values to vertices
   * references if u == 0 (not provided).
   * 
   * @param m the mesh
   * @param u the reference to the function
   * 
   * @return a vtkDoubleArray that contains the values
   */
  vtkDoubleArray*
  __getValues(const Mesh& m,
	      ConstReferenceCounting<ScalarFunctionBase> u);

  template <typename MeshType>
  void __plot(const MeshType& m,
	      ConstReferenceCounting<ScalarFunctionBase> u);

  /** 
   * Automatic ff3d to VTK type conversions
   */
  template <typename MeshType>
  struct Traits {};
public:
  
#ifdef HAVE_GUI_LIBS
  void plot(const Mesh& mesh,
	    ConstReferenceCounting<ScalarFunctionBase> pU = 0);
#else // HAVE_GUI_LIBS
  void plot(const Mesh& mesh,
	    ConstReferenceCounting<ScalarFunctionBase> pU = 0)
  {
    fferr(0) << "Warning: cannot call 'plot' function, GUI was not compiled!\n";
  }
#endif // HAVE_GUI_LIBS

  VTKDriver()
  {
    ;
  }

  ~VTKDriver()
  {
    ;
  }
};

#endif // VTKDRIVER_HPP
