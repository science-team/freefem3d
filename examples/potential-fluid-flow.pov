//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2003  Laboratoire J.-L. Lions UPMC Paris

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: potential-fluid-flow.pov,v 1.2 2004/09/01 21:22:58 delpinux Exp $

// inflow
sphere {
  <0,1,1>,0.5
  pigment {color rgb <1,0,0>}
}

// outflow
sphere {
  <5,9,1>,0.5
  pigment {color rgb <0,0,1>}
}

// Obstacles
union {

    // Wall #1
	box {
	    <0,3,0>,<3,3.5,2>
	}

    // Wall #2
	box {
	    <1,7,0>,<5,7.5,2>
	    }
    pigment {color rgb <0,1,0>}
}
