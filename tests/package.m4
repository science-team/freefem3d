# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [freefem3d])
m4_define([AT_PACKAGE_TARNAME],   [freefem3d])
m4_define([AT_PACKAGE_VERSION],   [1.0pre10])
m4_define([AT_PACKAGE_STRING],    [freefem3d 1.0pre10])
m4_define([AT_PACKAGE_BUGREPORT], [stephane.delpino@math.jussieu.fr])
m4_define([EXEEXT], [])
