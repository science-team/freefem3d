# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005, 2006 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id: io-unstructured-p1-field-medit.at,v 1.3 2007/06/10 15:04:44 delpinux Exp $

AT_SETUP([Medit unstructured P1-FEM field IO])

AT_KEYWORDS (io)

###################################
# CHECKS UNSTRUCTURED P1 FIELD IO #
###################################

AT_DATA([test.ff],
[[
vector a = (0,0,0);
vector b = (2,3,1);
vector n = (3,2,5);

mesh m = tetrahedrize(structured(n,a,b));

femfunction f(m) = x+y+z;
double intf = int[m](f);

save(medit,"f",[f,x,0],m);

function xx = read(medit,"f.bb":1,m);
double error = int[m](abs(xx-x));

if (error < 1E-8) {
  cout << "read: ok\n";
} else {
  cout << "read: failed\n";
}

cat("f.bb");
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 test.ff],0,
[[read: ok
3 3 30 2
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
1
0
0
1
0
0
1
0
0
1
0
0
1
0
0
1
0
0
1
0
0
1
0
0
1
0
0
1
0
0
2
0
0
2
0
0
2
0
0
2
0
0
2
0
0
2
0
0
2
0
0
2
0
0
2
0
0
2
0
]],
[])

AT_CLEANUP
