# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id: laplacian-order1-2-p1.at,v 1.3 2006/04/09 13:25:46 delpinux Exp $

AT_SETUP([Laplacian/order 1 [ver.2] (P1-FEM)])

AT_DATA([test.ff],
[[
vertex a = (1,0,0);
vertex b = (3,2,3);
vertex n = (5,7,6);

mesh M = tetrahedrize(structured(n,a,b));

double a1 = 01;
double b1 = 02;
double c1 = 03;
double d1 = 04;

double a2 = 05;
double b2 = 06;
double c2 = 07;
double d2 = 08;

double a3 = 09;
double b3 = 10;
double c3 = 11;
double d3 = 12;

double ap = 13;
double bp = 14;
double cp = 15;
double dp = 16;

function u1exact = a1*x+b1*y+c1*z+d1;
function u2exact = a2*x+b2*y+c2*z+d2;
function u3exact = a3*x+b3*y+c3*z+d3;

function pexact = ap*x+bp*y+cp*z+dp;

solve(u1,u2,u3,p) in M
  krylov(type=bicg),
  bicg(epsilon=1E-20)
{
  test(v1,v2,v3,q)
    int(grad(u1)*grad(v1) -p*dx(v1))
   +int(grad(u2)*grad(v2) -p*dy(v2))
   +int(grad(u3)*grad(v3) -p*dz(v3))
   +int(grad(p)*grad(q) +dx(u1)*q+dy(u2)*q+dz(u3)*q)
   =
    int(ap*v1)
   +int(bp*v2)
   +int(cp*v3)
   +int(q*a1+q*b2+q*c3);

   u1 = u1exact on M;
   u2 = u2exact on M;
   u3 = u3exact on M;
   p  = pexact  on M;
}


double l2u1error = sqrt(int[M]((u1-u1exact)^2));
if (l2u1error < 1E-8)
  cout << "u1: ok\n";
else
  cout << "oops: dx(u) u1-u1exact " << l2u1error << "\n";

double l2u2error = sqrt(int[M]((u2-u2exact)^2));
if (l2u2error < 1E-8)
  cout << "u2: ok\n";
else
  cout << "oops: dx(u) u2-u2exact " << l2u2error << "\n";

double l2u3error = sqrt(int[M]((u3-u3exact)^2));
if (l2u3error < 1E-8)
  cout << "u3: ok\n";
else
  cout << "oops: dx(u) u3-u3exact " << l2u3error << "\n";

double l2perror = sqrt(int[M]((p-pexact)^2));
if (l2perror < 1E-8)
  cout << "p: ok\n";
else
  cout << "oops: dx(u) p-pexact " << l2perror << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d -V 0 -nw test.ff],0,
[[u1: ok
u2: ok
u3: ok
p: ok
]])

AT_CLEANUP
