# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id: laplace-dirichlet-p1.at,v 1.4 2006/04/09 13:25:46 delpinux Exp $

AT_SETUP([Laplacian full Dirichlet  (P1-FEM)])

AT_DATA([test.ff],
[[
vertex a = (1,0,0);
vertex b = (3,2,3);
vertex n = (5,7,6);

mesh M = tetrahedrize(structured(n,a,b));

function uexact = x+2*y-6*z;

solve(u) in M
  cg(epsilon=1E-20)
{
  test(v)
   int(grad(u)*grad(v)) = 0;
  u = uexact on M;
}

double l2error = sqrt(int[M]((u-uexact)^2));
if (l2error < 1E-8)
  cout << "ok\n";
else
  cout << "oops: l2 error is " << l2error << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d -V 0 -nw test.ff],0,
[[ok
]])


AT_CLEANUP
