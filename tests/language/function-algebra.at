# This file is part of ff3d - http://www.freefem.org/ff3d
# Copyright (C) 2005 Stephane Del Pino

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

# $Id: function-algebra.at,v 1.4 2007/03/30 23:40:35 delpinux Exp $

AT_SETUP([Function algebra])

################################
# CHECKS FUNCTION CONSTRUCTION #
################################

AT_DATA([construction.ff],
[[
cout << sin(x)*(2*(x+y)-3*z)^2 << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 construction.ff],0,
[[(sin(x)*(((2*(x+y))-(3*z))^2))
]],
[])

###############################
# CHECKS FUNCTION COMPOSITION #
###############################

AT_DATA([composition.ff],
[[
function f = sin(x)*cos(y)*z;
function g = f(y,z,x);

double pi = acos(-1);

cout << g(1,pi/2,pi) << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 composition.ff],0,
[[-1
]],
[])

###############################
# CHECKS FUNCTION EVALUATIONS #
###############################

AT_DATA([function-evaluation.ff],
[[
vector n = (4,3,5);
vector a = (0,0,0);
vector b = (1,2,1);
mesh m = structured(n,a,b);

function f = x+y*z;
cout << "max value = " << max[m](f) << "\n";
cout << "min value = " << min[m](f) << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 function-evaluation.ff],0,
[[max value = 3
min value = 0
]],
[])

################################
# CHECKS C-FUNCTIONS EMBEDDING #
################################

AT_DATA([cfunctions.ff],
[[function ABS= abs(x);
function SIN  = sin(x);
function ASIN = asin(x);
function COS  = cos(x);
function ACOS = acos(x);
function TAN  = tan(x);
function ATAN = atan(x);
function SQRT = sqrt(x);
function EXP  = exp(x);
function LOG  = log(x);
function POW  = x^y;

double pi = acos(-1);
double eps = 1E-10;

cout << "abs ";
if (ABS(-1,0,0) == 1) {
 cout << "ok\n";
} else {
 cout << "failed! " << ABS(-1,0,0) << "\n";
}

cout << "sin ";
if (abs(SIN(pi/2,0,0) - 1) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << SIN(pi/2,0,0) << "\n";
}

cout << "asin ";
if (abs(ASIN(1,0,0) - pi/2) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << ASIN(1,0,0) << "\n";
}

cout << "cos ";
if (abs(COS(pi,0,0) + 1) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << COS(pi,0,0) << "\n";
}

cout << "acos ";
if (abs(ACOS(-1,0,0) -pi) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << ACOS(-1,0,0) << "\n";
}

cout << "tan ";
if (abs(TAN(pi/4,0,0) - 1) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << TAN(pi/4,0,0) << "\n";
}

cout << "atan ";
if (abs(ATAN(1,0,0) - pi/4) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << ATAN(1,0,0) << "\n";
}

cout << "sqrt ";
if (abs(SQRT(4,0,0) - 2)<eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << SQRT(4,0,0) << "\n";
}

cout << "exp ";
if (abs(EXP(1,0,0) - 2.71828182845905) < eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << EXP(1,0,0) << "\n";
}

cout << "log ";
if (abs(LOG(1,0,0))<eps) {
  cout << "ok\n";
} else {
  cout << "failed! " << LOG(1,0,0) << "\n";
}

cout << "pow ";
if (abs(POW(3,3,0) - 27) < eps)  {
  cout << "ok\n";
} else {
  cout << "failed! " << POW(3,3,0) << "\n";
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 cfunctions.ff],0,
[[abs ok
sin ok
asin ok
cos ok
acos ok
tan ok
atan ok
sqrt ok
exp ok
log ok
pow ok
]],
[])

########################################
# CHECKS SPECIAL-FUNCTIONS EXPRESSIONS #
########################################

AT_DATA([special-functions.ff],
[[function MOD= x%3;

cout << "modulo ";
if (MOD(5,0,0) == 2) {
 cout << "ok\n";
} else {
 cout << "failed! " << MOD(5,0,0) << "\n";
}
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 special-functions.ff],0,
[[modulo ok
]],
[])

#############################
# CHECKS BOOLEAN OPERATIONS #
#############################

AT_DATA([boolean.ff],
[[function gt = one(x>y);
cout << "one(x>y)(1,0,0) = " << gt(1,0,0) << "\n";
cout << "one(x>y)(1,1,0) = " << gt(1,1,0) << "\n";

function ge = one(x>=y);
cout << "one(x>=y)(1,1,0) = " << ge(1,1,0) << "\n";

function le = one(y<=z);
cout << "one(y<=z)(0,1,0) = " << le(0,1,0) << "\n";
cout << "one(y<=z)(0,1,1) = " << le(0,1,1) << "\n";

function lt = one(x<z);
cout << "one(x<z)(1,0,0) = " << lt(1,0,0) << "\n";
cout << "one(x<z)(0,0,1) = " << lt(0,0,1) << "\n";

function eq = one(x==y);
cout << "one(x==y)(1,1,0) = " << eq(1,1,0) << "\n";
cout << "one(x==y)(1,0,0) = " << eq(1,0,0) << "\n";

function ne = one(x!=y);
cout << "one(x!=y)(1,0,0) = " << ne(1,0,0) << "\n";
cout << "one(x!=y)(1,1,0) = " << ne(1,1,0) << "\n";


function OR = one((x!=y)or(x==y));
cout << "one((x!=y)or(x==y))(0,0,0) = " << OR(0,0,0) << "\n";

function XOR = one((x!=y)xor(x==y));
cout << "one((x!=y)xor(x==y))(0,0,0) = " << XOR(0,0,0) << "\n";

function AND = one((x!=y)and(x==y));
cout << "one((x!=y)and(x==y))(0,0,0) = " << AND(0,0,0) << "\n";

function TRUE = one(true);
cout << "one(true)(1,0,0) = " << TRUE(1,0,0) << "\n";

function FALSE = one(false);
cout << "one(false)(1,0,0) = " << FALSE(1,0,0) << "\n";

cout << "one(not(x!=y))(1,0,0) = " << one(not(x!=y))(1,0,0) << "\n";
cout << "one(not(x!=y))(0,0,0) = " << one(not(x!=y))(0,0,0) << "\n";
]])

AT_CHECK([$abs_top_builddir/ff3d]EXEEXT [-nw -V 0 boolean.ff],0,
[[one(x>y)(1,0,0) = 1
one(x>y)(1,1,0) = 0
one(x>=y)(1,1,0) = 1
one(y<=z)(0,1,0) = 0
one(y<=z)(0,1,1) = 1
one(x<z)(1,0,0) = 0
one(x<z)(0,0,1) = 1
one(x==y)(1,1,0) = 1
one(x==y)(1,0,0) = 0
one(x!=y)(1,0,0) = 1
one(x!=y)(1,1,0) = 0
one((x!=y)or(x==y))(0,0,0) = 1
one((x!=y)xor(x==y))(0,0,0) = 1
one((x!=y)and(x==y))(0,0,0) = 0
one(true)(1,0,0) = 1
one(false)(1,0,0) = 0
one(not(x!=y))(1,0,0) = 0
one(not(x!=y))(0,0,0) = 1
]],
[])

AT_CLEANUP
