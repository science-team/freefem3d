Source: freefem3d
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Section: math
Priority: optional
Build-Depends: debhelper (>= 11~),
               doc-base,
               bison,
               texlive,
               texlive-latex-extra,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/science-team/freefem3d.git
Vcs-Git: https://salsa.debian.org/science-team/freefem3d.git

Package: freefem3d
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: doc-central
Description: Language and solver for partial differential equations in 3D
 FreeFEM3D (aka ff3d) is a 3D solver of partial differential
 equations (PDE).  It is a member of the familly of the freefem
 programs (see http://www.freefem.org).
 .
 ff3d, as well as its cousins, is a PDE solver driven by a
 user-friendly language. It solves many kind of problems such as
 elasticity, fluids (Stokes and Navier-Stokes) and a lot more. The
 user has to enter the equation associated with the problem, giving
 either the PDE in strong formulation or weak (variational)
 formulation.
 .
 ff3d can use either the Finite Elements method (the mesh of the
 geometry being provided by the user) or a Fictitious Domain like
 approach where the geometry is described using Constructive Solid
 Geometry (CSG). This description is done using the POV-Ray language
 but others such as VRML could be added.
 .
 The processing of the results is left to the user.  One can use
 various graphic tools: output in the MEdit mesh format or VTK are
 supported. The implementation of a VTK base visualization module is
 underway.
 .
 The goal of ff3d is to provide a good teaching tool and a research
 toolbox (the code is written in C++ and its design is such that new
 methods can be easily implemented).
