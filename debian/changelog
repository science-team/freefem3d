freefem3d (1.0pre10-4) unstable; urgency=medium

  * Team upload.
  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * debhelper 11
  * Convert from cdbs to dh
  * hardening=+all
  * Standards-Version: 4.1.3

 -- Andreas Tille <tille@debian.org>  Wed, 24 Jan 2018 22:24:40 +0100

freefem3d (1.0pre10-3.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch to debhelper compat level 10 (Closes: #817460).
  * Add B-D: dh-autoreconf (Closes: #796431).
  * Drop Christophe Prud'homme from Uploaders (Closes: #835013).
  * Honor CFLAGS and CXXFLAGS envvars.

 -- Andrey Rahmatullin <wrar@debian.org>  Sat, 26 Nov 2016 00:55:28 +0500

freefem3d (1.0pre10-3.3) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/rules: added dh-autoreconf and workaround to undef __vector to avoid
    conflict with the gcc macro (Fernando Seiti Furusato). Closes: #768044.
  * Fix build failure with GCC 5. Closes: #777855.
  * Build with -std=gnu++98.

 -- Matthias Klose <doko@debian.org>  Wed, 08 Jul 2015 13:46:44 +0200

freefem3d (1.0pre10-3.2) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control, debian/rules: Switch to automake1.11. (Closes:
    #724369)

 -- Eric Dorland <eric@debian.org>  Sun, 16 Feb 2014 03:03:12 -0500

freefem3d (1.0pre10-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * 30_gcc47: Fix build failure with GCC 4.7.  Closes: #667170.
    - algebra/Vector.hpp: Include <cmath> for sqrt.
    - solver/FEMDiscretization.hpp: Add "this->" qualifier.

 -- Matej Vela <vela@debian.org>  Thu, 17 May 2012 14:07:03 +0100

freefem3d (1.0pre10-3) unstable; urgency=low

  [ Sylvestre Ledru ]
  * Team upload
  * Ack previous NMU
  * Package migrated to the Debian Science team
  * Standards-Version updated to version 3.9.2
  * Switch to dpkg-source 3.0 (quilt) format
  * Fix debhelper-but-no-misc-depends lintian warning
  * Remove the suffix in the description

  [ Daniel Leidert (dale) ]
  * debian/control: Vcs fields transition. Added Homepage field.
    (Vcs-Svn): Fixed.
  * debian/copyright: Added missing information.
  * debian/freefem3d.doc-base: Fixed encoding.
    (Section): Fixed accordingly to doc-base (>= 0.8.10).
  * debian/watch: Added.

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 19 Sep 2011 14:43:03 +0200

freefem3d (1.0pre10-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Introduce CDBS' simple-patchsys to include the patch below
    + use CDBS facilities to run autotools
  * Fix "FTBFS with GCC 4.4: missing #include" with patches
    debian/patches/10_gcc44_enum.patch similar to upstream and
    debian/patches/20_gcc44_include.patch thanks Martin Michlmayr
    (Closes: #504845)

 -- Filippo Giunchedi <filippo@debian.org>  Sat, 13 Feb 2010 11:57:36 +0100

freefem3d (1.0pre10-2) unstable; urgency=low

  [Stephane Del Pino]
  * Fix software description according to upstream
  * Bugs fixed:
     o Bug#433176: "FTBFS: make: aclocal-1.9: Command not found"
       (Closes: #433176)
     o Fixed doc construction dependencies

 -- Christophe Prud'homme <prudhomm@debian.org>  Sat, 07 Jul 2007 14:58:00 +0100

freefem3d (1.0pre10-1) unstable; urgency=low

  [Stephane Del Pino]
  * New upstream release
    Bugs fixed in 1.0pre10:
     o Bug #11530: "cannot install..." [bison checking in configure]
     o Bug #7709 : "Mesh Transformation code not finished" [code finished]
     o Fixed Debian Bug#420922: FTBFS with GCC 4.3: missing #includes,
       thanks to Martin Michlmayr (Closes: #420922).
     o The code is built even when cannot build documentation
     o P2 and Q2 elements are now fully working (thanks to Edouard Oudet's reports)

   * New in 1.0pre10:
     o Added function building simplifier [optimization]
     o Removed deprecated functions from language syntax
     o Save multiple functions and fields into one VTK file
     o Many ff3d feature are now tests in the test suite
     o Output improvements: save/read in vtk format for vectors, fields,...

 -- Christophe Prud'homme <prudhomm@debian.org>  Sat, 07 Jul 2007 14:58:00 +0100

freefem3d (1.0pre9-1) unstable; urgency=low

  [Stephane Del Pino]
  * New upstream release
    Bugs fixed in 1.0pre9:
     o Fixed advection operator on cartesian meshes
     o Language: fixed "on M" for non-cartesian meshes boundary conditions
     o Fixed Debian Bug#358053: FTBFS with G++ 4.1: extra qualification
     o Fixed P1: first order and second order derivative right hand side
       for variational formulae
     o Fixed #16041: related to bad error message
    New in 1.0pre9:
     o Completely rewrote the function algebra
     o Added (at last!) a set of non-regression tests
     o Added reader for 2D 'am_fmt' mesh format
     o Added domain by function definition (e.g. domain(f<g))
  [Christophe Prud'homme]
  * update Standards-Version to 3.7.2 (no change)

 -- Christophe Prud'homme <prudhomm@debian.org>  Fri, 22 Dec 2006 13:28:13 +0100

freefem3d (1.0pre8-2) unstable; urgency=low

  * Bug fix: "FTBFS with G++ 4.1: extra qualification", thanks to Martin
    Michlmayr (Closes: #358053).
  * Bug fix: "freefem3d: Spelling in package description", thanks to Igor
    Khavkine (Closes: #326760).

 -- Christophe Prud'homme <prudhomm@debian.org>  Fri, 14 Apr 2006 20:10:41 +0200

freefem3d (1.0pre8-1) unstable; urgency=low

  * New upstream release
  * Changed Maintainer to Debian Scientific Computing Team and added
    myself as uploader

 -- Christophe Prud'homme <prudhomm@debian.org>  Fri, 14 Apr 2006 18:19:28 +0200

freefem3d (1.0pre7-3) unstable; urgency=low

  * Removed VTK and Qt dependencies (GUI is just experimental code),
    thanks to S. Del Pino the upstream author
  * Ported to cdbs
  * freefem3d: /usr/bin/ff3d has undefined non-weak symbols (Closes: #287577)
  * Changed my email address to the one at d.o

 -- Christophe Prud'homme <prudhomm@debian.org>  Sun,  4 Sep 2005 20:39:41 +0200

freefem3d (1.0pre7-2) unstable; urgency=low

  * Rebuilt with new g++ default (g++-4.0)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Wed, 13 Jul 2005 07:44:02 +0200

freefem3d (1.0pre7-1) unstable; urgency=low

  * New upstream release
  * Backport that fixes non symetrical systems defined using variationnal
    formula.
  * Bug fix: "freefem3d: typos in package description", thanks to Matt
    Kraai (Closes: #308173)
  * Various fixes for the build system to remove lintian warnings
  * Update Standards-Version to 3.6.2
  * Bug fix: "freefem3d: ..was kickedout on a libopts9 dep conflict with "a"
    libopts... ", thanks to Arnt Karlsen (Closes: #308417).

 -- Christophe Prud'homme <prudhomm@mit.edu>  Mon, 27 Jun 2005 08:01:57 +0200

freefem3d (1.0pre5-2) unstable; urgency=low

  * Bug fix: "freefem3d: /usr/bin/ff3d has undefined non-weak symbols",
    thanks to Laurent Bonnaud (Closes: #287577).
  * Bug fix: "freefem3d: FTBFS (amd64/gcc-4.0): explicit specialization of
    'void ConnectivityBuilder&lt;MeshOfHexahedra&gt;::setFaces()' must be
    introduced by 'template &lt;&gt;'", thanks to Andreas Jochens (Closes:
    #293018).

 -- Christophe Prud'homme <prudhomm@debian.org>  Sat, 12 Mar 2005 23:41:51 +0100

freefem3d (1.0pre5-1) unstable; urgency=low

  * New upstream release

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sun, 18 Jul 2004 18:13:19 +0200

freefem3d (1.0pre4-3) unstable; urgency=low

  * fixed compilation failure on alpha architectures

 -- Christophe Prud'homme <prudhomm@mit.edu>  Mon, 10 May 2004 15:02:44 +0200

freefem3d (1.0pre4-2) unstable; urgency=low

  * Fixed wrong shell usage in ff3d

 -- Christophe Prud'homme <prudhomm@mit.edu>  Thu,  6 May 2004 16:36:05 +0200

freefem3d (1.0pre4-1) unstable; urgency=low

  * New upstream release
  * pre3 orig tarball was broken severely. Fixed compilation process (closes: #245814)
  * Depends on g++ 3.3 now at least

 -- Christophe Prud'homme <prudhomm@mit.edu>  Thu,  6 May 2004 13:30:43 +0200

freefem3d (1.0pre3-1) unstable; urgency=low

  * New upstream release
  * Now documentation should be properly generated on all plateforms (closes: #242724)
  * Added full proper examples
  * Added more documentation formats: pdf, ps, dvi

 -- Christophe Prud'homme <prudhomm@mit.edu>  Fri, 16 Apr 2004 14:17:14 +0200

freefem3d (1.0pre2-3) unstable; urgency=low

  * do not compress PDF files
  * added some examples(added by debian maintainer but not in original distribution)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Wed,  7 Apr 2004 10:18:15 +0200

freefem3d (1.0pre2-2) unstable; urgency=low

  * Get rid of ff3d.info. Useless file anyway

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sun,  4 Apr 2004 22:46:28 +0200

freefem3d (1.0pre2-1) unstable; urgency=low

  * New upstream release
  * Fix compilation problem (closes: #240378)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sun,  4 Apr 2004 21:59:28 +0200

freefem3d (1.0pre1-5) unstable; urgency=low

  * Build-Depends on tetex-extra

 -- Christophe Prud'homme <prudhomm@mit.edu>  Fri, 30 Jan 2004 17:32:05 +0100

freefem3d (1.0pre1-4) unstable; urgency=low

  * Build-Depends on tetex-bin (closes: #229406)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Mon, 26 Jan 2004 13:54:03 +0100

freefem3d (1.0pre1-3) unstable; urgency=low

  * Fix the (too long) short description in control file.
  * Fix the word of the long description in the control file.
  * Fix Build-Depends, forgot bison in the list

 -- Christophe Prud'homme <prudhomm@mit.edu>  Fri, 23 Jan 2004 14:29:20 +0100

freefem3d (1.0pre1-2) unstable; urgency=low

  * use sh to execute configure, hopefully fix buildd
  * close ITP (closes: #227632)

 -- Christophe Prud'homme <prudhomm@mit.edu>  Fri, 23 Jan 2004 11:56:28 +0100

freefem3d (1.0pre1-1) unstable; urgency=low

  * Initial release.

 -- Christophe Prud'homme <prudhomm@mit.edu>  Sun, 11 Jan 2004 15:28:10 +0100
