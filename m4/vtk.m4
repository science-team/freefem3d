AC_DEFUN([AC_CHECK_VTK],
[
  REQUIRED_VTK_MAJOR=5
  REQUIRED_VTK_MINOR=0

  REQUIRED_VTK=`echo "$REQUIRED_VTK_MAJOR.$REQUIRED_VTK_MINOR"`

  AC_MSG_CHECKING([for VTK library (>= $REQUIRED_VTK)])

  vtkinclude=""
  vtklibpath=""
dnl look for vtk in standard pathes
  for i in /usr /usr/local
  do
    j=vtk-5.0
    if test -e "$i/include/$j/vtkConfigure.h"
    then
      VTK_VERSION=`grep VTK_VERSION $i/include/$j/vtkConfigure.h`
      VTK_VERSION=`expr "$VTK_VERSION" : '.*"\(.*\)"' '|' "$VTK_VERSION"`
      vtkinclude=$i/include/$j
      vtklibpath=$i/lib/$j

      VTK_MAJOR=`expr "$VTK_VERSION" : '\(.*\)\..*\..*'`
      VTK_MINOR=`expr "$VTK_VERSION" : '.*\.\(.*\)\..*'`
      if test $VTK_MAJOR -lt $REQUIRED_VTK_MAJOR
      then
        FOUND_VTK=false
      else
        if test $VTK_MINOR -lt $REQUIRED_VTK_MINOR
        then
          FOUND_VTK=false
        else 
          FOUND_VTK=true
        fi
      fi
    fi
  done

  if test x$FOUND_VTK != xtrue
  then
     AC_MSG_RESULT(no)
  else
     AC_MSG_RESULT(yes)
     VTK_CXXFLAGS="-I$vtkinclude"
     VTK_LDADD="-L$vtklibpath -lvtkRendering -lvtkGraphics -lvtkHybrid -lvtkImaging -lvtkCommon -lQVTK"
  fi
  AC_SUBST(VTK_CXXFLAGS)
  AC_SUBST(VTK_LDADD)
])
