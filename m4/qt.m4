dnl Checking for Qt libraries (original script picked at http://autoqt.sf.net)
dnl From Jim Meyering

# Check for Qt compiler flags, linker flags, and binary packages
AC_DEFUN([AC_CHECK_QT],
[
AC_REQUIRE([AC_PROG_CXX])
AC_REQUIRE([AC_PATH_X])

# FOUND_QT will be set to false if important componants are missing
FOUND_QT=true

AC_MSG_CHECKING([QTINCLUDE_DIR])
AC_ARG_WITH([qtdir], [  --with-qtinlude=DIR     Qt include directory [default=$QTINCLUDE_DIR]], QTINCLUDE_DIR=$withval)
# Check that QTINCLUDE_DIR is defined or that --with-qtinclude given
if test x"$QTINCLUDE_DIR" = x ; then
    QT_SEARCH="/usr/lib/qt4"
    for i in $QT_SEARCH; do
        if test -f $i/include/Qt/qglobal.h -a x$QTINCLUDE_DIR = x;
	then
	  QTDIR="$i"  # Qt includes found at a non standard location
	  QTINCLUDE_DIR="$i/include"
	fi
    done
fi

# second pass. Now looking for Debian-like 
if test x"$QTINCLUDE_DIR" = x ; then
    if test -f /usr/include/qt4/Qt/qglobal.h -a x$QTINCLUDE_DIR = x;
    then
	QTINCLUDE_DIR="/usr/include/qt4"
    fi
fi

if test x"$QTINCLUDE_DIR" = x ; then
    FOUND_QT=false
    AC_MSG_WARN([*** Could not find Qt headers.])
fi
AC_MSG_RESULT([$QTINCLUDE_DIR])

# If some Qt library were found:
if test x"$QTINCLUDE_DIR" != x ; then

  # Change backslashes in QTDIR to forward slashes to prevent escaping
  # problems later on in the build process, mainly for Cygwin build
  # environment using MSVC as the compiler
  # TODO: Use sed instead of perl
  QTINCLUDE_DIR=`echo $QTINCLUDE_DIR | perl -p -e 's/\\\\/\\//g'`

  # Figure out which version of Qt we are using
  AC_MSG_CHECKING([Qt version])
  QT_VER=`grep 'define.*QT_VERSION_STR\W' $QTINCLUDE_DIR/Qt/qglobal.h | perl -p -e 's/\D//g'`
  case "${QT_VER}" in
    4*)
        QT_MAJOR="4"
    ;;
    *)
	FOUND_QT=false
        AC_MSG_WARN([*** Don't know how to handle this Qt major version])
    ;;
  esac
  AC_MSG_RESULT([$QT_VER ($QT_MAJOR)])

  # Check that moc is in path
  AC_CHECK_PROG(MOC, moc, moc)
  if test x"$MOC" = x ; then
        FOUND_QT=false
        AC_MSG_WARN([*** moc must be in path])
  fi

  # uic is the Qt user interface compiler
  AC_CHECK_PROG(UIC, uic, uic)
  if test x"$UIC" = x ; then
          FOUND_QT=false
          AC_MSG_WARN([*** uic must be in path])
  fi


  # Calculate Qt include path
  QT_CXXFLAGS="-I$QTINCLUDE_DIR -I$QTINCLUDE_DIR/QtGui -I$QTINCLUDE_DIR/QtCore"

  QT_IS_EMBEDDED="no"

  QT_SEARCH="/usr/lib /usr/lib/qt4"

  # On unix, figure out if we're doing a static or dynamic link
  case "${host}" in
    *-cygwin)
	AC_MSG_WARN([ff3d's compilation with qt4 is not implenented yet! Please Report.])
        ;;

    *)
        for i in $QT_SEARCH; do
	    if test x"$QT_LIB" = x; then
	        if test x"`ls $i/libQt* 2> /dev/null`" != x ; then
                    QT_LIB="-lQtGui -lQtCore"
		    QTLIB_DIR="$i"
                fi
            fi
        done
        if test x"$QT_LIB" = x ; then
	  FOUND_QT=false
        fi
        ;;
  esac

  QT_LDADD="-L$QTLIB_DIR $QT_LIB"

  AC_MSG_CHECKING([QT_CXXFLAGS])
  AC_MSG_RESULT([$QT_CXXFLAGS])
  AC_MSG_CHECKING([QT_LDADD])
  AC_MSG_RESULT([$QT_LDADD])

  AC_SUBST(QT_CXXFLAGS)
  AC_SUBST(QT_LDADD)
fi
])
