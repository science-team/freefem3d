AC_DEFUN([AC_CHECK_MPI],
[
  AC_MSG_CHECKING([for MPI library (Required by PETSc)])

  mpiinclude=""
  mpilibpath=""
dnl look for mpi in standard pathes
  for i in /usr /usr/local
  do
    if test -e "$i/include/mpi/mpi.h"
    then
      mpiinclude="$i/include/mpi"
    fi
    if test "x`ls $i/lib/libmpi.* 2> /dev/null`" != "x"
    then
      mpilibpath="$i/lib"
    fi
  done

  if test "x$mpiinclude" == "x"
  then
    AC_MSG_ERROR(no (Could not find "mpi.h"))
  fi

  if test "x$mpilibpath" == "x"
  then
    AC_MSG_ERROR(no (Could not find MPI library))
  fi

  AC_MSG_RESULT(yes)
dnl if this point is readched, mpi have been found
  MPI_CXXFLAGS="-I$mpiinclude"
  MPI_LDADD="-L$mpilibpath -lmpi"

  AC_SUBST(MPI_CXXFLAGS)
  AC_SUBST(MPI_LDADD)
])
