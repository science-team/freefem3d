AC_DEFUN([AC_CHECK_COMPILE_OPTS],
[

dnl Checking which definition to use for real_t.
  AC_ARG_ENABLE(
  real_t,
  AC_HELP_STRING([--enable-real_t],[sets real_t to float or double (default=double)]),
  [ ac_real_t=$enableval ],
  [ ac_real_t="double"])

  AH_TEMPLATE(HAVE_REAL_TYPE,
   [Defined real_t (1:double, 2:float)])

  case $ac_real_t in
  "double")
    AC_DEFINE(HAVE_REAL_TYPE,1)
    ;;
  "float")
    AC_DEFINE(HAVE_REAL_TYPE,2)
    ;;
  *)
    AC_MSG_ERROR(real_t can only be defined as "double" or "float")
    ;;
  esac

dnl Enalbe or not system "exec" command
  AH_TEMPLATE(ALLOW_EXEC,
   [allow use of exec command])

  AC_ARG_ENABLE(
  exec,
  AC_HELP_STRING([--enable-exec],[allows use of exec command (default=yes)]),
  [
   if test $enableval = "no";
     then ac_use_exec_code="no"
     else ac_use_exec_code="yes"
   fi
  ], [ac_use_exec_code="yes"])

  if test $ac_use_exec_code = "yes"
  then
	AC_DEFINE(ALLOW_EXEC)
  fi

dnl Check debugging mode for compilation.
  AC_ARG_ENABLE(
  debug,
  AC_HELP_STRING([--enable-debug],[creates debugging code (default=no)]),
  [
   if test $enableval = "no";
     then ac_use_debug_code="no"
     else ac_use_debug_code="yes"
   fi
  ], [ac_use_debug_code="no"])

  AC_ARG_ENABLE(
  optimize,
  AC_HELP_STRING([--enable-optimize],[creates optimized code (default=no)]),
  [
   if test $enableval = "no";
     then ac_use_opt_code="no"
     else ac_use_opt_code="yes"
   fi
  ], [ac_use_opt_code="no"])

  if test $ac_use_debug_code = "yes" -a $ac_use_opt_code = "yes"
  then
    AC_MSG_ERROR(Cannot use --enable-debug and --enable-optimize together!)
  fi

  if test $ac_use_debug_code = "yes"
  then
    CXXFLAGS="-g -Wall"
    CFLAGS="-g -Wall"
  else
    if test $ac_use_opt_code = "yes"
    then
      CXXFLAGS="-Wall -DNDEBUG -O2 -funroll-all-loops -fargument-noalias-global -fno-gcse"
      CFLAGS="-Wall -DNDEBUG -O2 -funroll-all-loops -fargument-noalias-global -fno-gcse"
    else
      CXXFLAGS="$CXXFLAGS -Wall -DNDEBUG"
      CFLAGS="$CFLAGS -Wall -DNDEBUG"
    fi
  fi
])
