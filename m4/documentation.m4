AC_DEFUN([AC_CHECK_DOCUMENTATION],[
  AH_TEMPLATE(HAVE_GUI_LIBS,
   [Defined to 1 if GUI libraries are present on the system])

  AC_CHECK_PROG(LATEX,[latex], yes, no)
  AC_CHECK_PROG(PDFLATEX,[pdflatex], yes, no)
  AC_CHECK_PROG(MAKEINDEX,[makeindex], yes, no)

  BUILD_DOC=true
  DOC_MISSING=""
  if test x"$LATEX" = xno ; then
	BUILD_DOC=false
        DOC_MISSING="$DOC_MISSING latex"
  fi
  if test x"$PDFLATEX" = xno ; then
	BUILD_DOC=false
        DOC_MISSING="$DOC_MISSING pdflatex"
  fi
  if test x"$MAKEINDEX" = xno ; then
	BUILD_DOC=false
        DOC_MISSING="$DOC_MISSING makeindex"
  fi

  if test x"$BUILD_DOC" != x"true"
  then
	AC_MSG_WARN([cannot build documentation install:$DOC_MISSING]);
  fi

  AM_CONDITIONAL(GENERATE_DOCUMENTATION,test x"$BUILD_DOC" = xtrue)
])
