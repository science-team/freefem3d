//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Console.cpp,v 1.2 2006/11/02 15:41:25 delpinux Exp $

#include <Console.hpp>

#include <ErrorHandler.hpp>

#ifdef HAVE_GUI_LIBS
#include <QtGui/QTextEdit>
#include <iostream>

void Console::
setConsole(QTextEdit* console)
{
  __console = console;
}

void Console::
_writeError(std::string output)
{
  size_t cr_position = output.find('\n');
  while (cr_position < output.size()) {
    std::cout << "[errput] "<< __error_buffer->str()
	      << output.substr(0,cr_position+1);
    output = output.substr(cr_position+1);
    cr_position = output.find('\n');
    delete __error_buffer;
    __error_buffer = new std::stringstream();
  }
  (*__error_buffer) << output;

}

void Console::
_writeStd(std::string output)
{
  size_t cr_position = output.find('\n');
  while (cr_position < output.size()) {
    (*__std_buffer) << output.substr(0,cr_position);
    output = output.substr(cr_position+1);
    cr_position = output.find('\n');

    QString line(__std_buffer->str().c_str());
    __console->append(line);
    __console->update();

    delete __std_buffer;
    __std_buffer = new std::stringstream();
  }
  (*__std_buffer) << output;
}

#else

void Console::
_writeError(std::string output)
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "cannot use console output",
		     ErrorHandler::unexpected);
}

void Console::
_writeStd(std::string output)
{
  throw ErrorHandler(__FILE__,__LINE__,
		     "cannot use console output",
		     ErrorHandler::unexpected);
}

#endif // HAVE_GUI_LIBS

Console::Console()
{
  __std_buffer = new std::stringstream();
  __error_buffer = new std::stringstream();
}

Console::~Console()
{
  delete __std_buffer;
  delete __error_buffer;
}
