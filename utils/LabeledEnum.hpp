//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: LabeledEnum.hpp,v 1.3 2004/12/31 14:00:47 delpinux Exp $

#ifndef LABELED_ENUM_HPP
#define LABELED_ENUM_HPP

#include <StringEquality.hpp>
#include <IdentifierSet.hpp>

#include <ErrorHandler.hpp>

#include <sstream>

template <typename GivenEnumType>
class LabeledEnum
{
public:
  typedef GivenEnumType EnumType;

private:

  typedef std::map<EnumType,
		   const char*,
		   std::less<EnumType> > EnumSet;

  typedef std::map<const char*,
		   EnumType,
		   StringEquality> LabelSet;

  EnumSet  __enumSet;
  LabelSet __labelSet;

public:
  void get(IdentifierSet& I)
  {
    for (typename LabelSet::iterator i = __labelSet.begin();
	 i != __labelSet.end(); ++i) {
      I.insert((*i).first);
    }
  }

  void add(const char* c, const EnumType t)
  {
    typename LabelSet::iterator name = __labelSet.find(c);
    typename EnumSet::iterator value = __enumSet.find(t);

    if (name != __labelSet.end()) {
      std::stringstream errorMsg;
      errorMsg << "the label '" << c << "' has already been associated to '"
	       << t << "'\n";
      errorMsg << "found: " <<(*name).first << '\n';
      errorMsg << "list:\n";
      for (typename LabelSet::iterator i = __labelSet.begin();
	   i != __labelSet.end(); ++i) {
	errorMsg << '\t' <<(*i).first << '\n';
      }
      errorMsg << std::ends;

      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::unexpected);

    } else if (value != __enumSet.end()) {
      std::stringstream errorMsg;
      errorMsg << "the enum '" << t << "' has already been associated to '"
	       << (*value).second << "'" << std::ends;

      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::unexpected);      
    } else {
      __labelSet[c] = t;
      __enumSet [t] = c;
    }
  }

  const EnumType operator()(const char* c) const
  {
    typename LabelSet::const_iterator i = __labelSet.find(c);
    if (i != __labelSet.end()) {
      return (*i).second;
    } else {
      std::stringstream errorMsg;
      errorMsg << "'" << c << "' is not associated to any enum\n";
      errorMsg << "Possible cases are:\n";
      errorMsg << *this << std::ends;
      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::unexpected);      
      return (*i).second;
    } 
  }

  friend std::ostream& operator << (std::ostream& os,
				    const LabeledEnum<EnumType>& l)
  {
    for (typename LabeledEnum<EnumType>::LabelSet::const_iterator i
	   = l.__labelSet.begin();
 	 i != l.__labelSet.end(); ++i) {
      os << (*i).second << '\t' << (*i).first << '\n';
    }
    return os;
  }

  LabeledEnum()
  {
    ;
  }

  ~LabeledEnum()
  {
    ;
  }
};

#endif // LABELED_ENUM_HPP
