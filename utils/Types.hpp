//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Types.hpp,v 1.1.1.1 2003/02/17 16:32:50 delpinux Exp $

#ifndef TYPES_HPP
#define TYPES_HPP

#include <config.h>

/**
 * @file   Types.hpp
 * @author St�phane Del Pino
 * @date   Sun Nov  3 15:45:15 2002
 * 
 * @brief  Basic types definition are set here in ff3d
 * 
 * 
 */

#if HAVE_REAL_TYPE==1 // double
typedef double real_t;
#endif // HAVE_REAL_TYPE==1

#if HAVE_REAL_TYPE==2 // float
typedef float real_t;
#endif // HAVE_REAL_TYPE=2


#endif // TYPES_HPP

