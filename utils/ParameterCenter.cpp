//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ParameterCenter.cpp,v 1.3 2004/12/31 16:38:58 delpinux Exp $

#include <ParameterCenter.hpp>

#include <list>

ReferenceCounting<Parameter> ParameterCenter::getParameter(const char* address)
{
  std::string a = address;
  typedef std::list<std::string> StringList;
  StringList name;

  size_t begin = 0;
  size_t end = begin;
  do {
    end = a.find("::", begin);
    name.push_back(a.substr(begin,end-begin));
    if (end <= a.size()) {
      begin = end + 2;
    } else {
      break;
    }
  } while (true);

  if(name.size()>2) {
    fferr(2) << '\n' << __FILE__ << ':' << __LINE__ << ": not implemented\n";
  }
  StringList::iterator iName = name.begin();
  ParameterSet::iterator object = __parametersSet.find((*iName).c_str());
  if(object != __parametersSet.end()) {
    ++iName;
    ReferenceCounting<Parameter> p = (*(*object).second).get((*iName).c_str());
    return p;
  } else {
    std::stringstream errorMsg;
    errorMsg << "no option named " << *iName << '\n'
	     << "availables options are:";
    for(object = __parametersSet.begin();
	object !=  __parametersSet.end(); ++object) {
      errorMsg << (*object).first << std::ends;
    }

    throw ErrorHandler(__FILE__,__LINE__,
		       errorMsg.str(),
		       ErrorHandler::normal);
    return 0;
  }
}

void ParameterCenter::get(const char * address, real_t& d)
{
  ReferenceCounting<Parameter> p = getParameter(address);
  switch ((*p).type()) {
  case Parameter::Double: {
    d = static_cast<real_t&>(dynamic_cast<DoubleParameter&>(*p));
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "wrong parameter type reading "+stringify(address),
		       ErrorHandler::normal);
  }
  }
}

void ParameterCenter::get(const char * address, int& integer)
{
  ReferenceCounting<Parameter> p = getParameter(address);
  switch ((*p).type()) {
  case Parameter::Double: {
    integer = static_cast<int&>(dynamic_cast<IntegerParameter&>(*p));
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "wrong parameter type reading "+stringify(address),
		       ErrorHandler::normal);
  }
  }
}

void ParameterCenter::get(const char * address, std::string& aString)
{
  ReferenceCounting<Parameter> p = getParameter(address);
  switch ((*p).type()) {
  case Parameter::String: {
    aString = static_cast<std::string&>(dynamic_cast<StringParameter&>(*p));
    break;
  }
  default: {
    throw ErrorHandler(__FILE__,__LINE__,
		       "wrong parameter type reading "+stringify(address),
		       ErrorHandler::normal);
  }
  }
}

void ParameterCenter::reset()
{
  for(ParameterSet::iterator i = __parametersSet.begin();
      i != __parametersSet.end(); ++i) {
    (*(*i).second).reset();
  }
}

void ParameterCenter::get(IdentifierSet& I)
{
  for (ParameterCenter::ParameterSet::iterator i = __parametersSet.begin();
       i != __parametersSet.end(); ++i) {
    I.insert((*i).first);
    (*(*i).second).get(I);
  }
}

