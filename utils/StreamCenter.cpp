//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: StreamCenter.cpp,v 1.7 2006/05/20 15:23:48 delpinux Exp $

#include <StreamCenter.hpp>

#ifdef HAVE_GUI_LIBS
#include <QtGui/QTextEdit>

void StreamCenter::
setConsole(QTextEdit* console)
{
  Console::instance().setConsole(console);
  __out.setType(OStream::qt_standard);
  __err.setType(OStream::qt_error);
}

#endif // HAVE_GUI_LIBS



StreamCenter::StreamCenter()
  : __debugLevel(1),
    __nullStream(OStream::null),
    __log(OStream::null),
    __out(OStream::standard),
    __err(OStream::error)
{
  ;
}
