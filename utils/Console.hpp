//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2006 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Console.hpp,v 1.2 2006/11/02 15:41:24 delpinux Exp $

#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <Stringify.hpp>
#include <ThreadStaticBase.hpp>

#include <string>

#include <config.h>

#ifdef HAVE_GUI_LIBS
class QTextEdit;
#endif // HAVE_GUI_LIBS

class Console
  : public ThreadStaticBase<Console>
{
private:
#ifdef HAVE_GUI_LIBS
  QTextEdit* __console;
#endif // HAVE_GUI_LIBS

  std::stringstream* __std_buffer;
  std::stringstream* __error_buffer;

  void _writeStd(std::string output);
  void _writeError(std::string output);

public:
#ifdef HAVE_GUI_LIBS
  void setConsole(QTextEdit* console);
#endif // HAVE_GUI_LIBS

  template <typename T>
  void writeStd(const T& t)
  {
    this->_writeStd(stringify(t));
  }

  template <typename T>
  void writeError(const T& t)
  {
    this->_writeError(stringify(t));
  }

  Console();
  ~Console();
};

#endif // CONSOLE_HPP
