//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ParameterCenter.hpp,v 1.3 2004/12/31 14:00:47 delpinux Exp $

#ifndef PARAMETER_CENTER_HPP
#define PARAMETER_CENTER_HPP

#include <StringParameter.hpp>
#include <IntegerParameter.hpp>
#include <DoubleParameter.hpp>
#include <EnumParameter.hpp>

#include <IdentifierSet.hpp>

#include <ParametrizableObject.hpp>
#include <ReferenceCounting.hpp>

#include <ErrorHandler.hpp>

#include <ParameterCenter.hpp>

#include <sstream>

class ParameterCenter
  : public StaticBase<ParameterCenter>
{
private:
  typedef std::map <const char*,
		    ReferenceCounting<ParametrizableObject>,
		    StringEquality> ParameterSet;
  ParameterSet __parametersSet;

  ReferenceCounting<Parameter> getParameter(const char* address);

public:

  //! gets the identifiers associated to parameters.
  void get(IdentifierSet& I);

  template <typename __GivenParameter>
  void subscribe(__GivenParameter* gp)
  {
    __parametersSet[__GivenParameter::identifier()]
      = dynamic_cast<ParametrizableObject*>(gp);
  }

  ReferenceCounting<ParametrizableObject> get(const char* parameterName)
  {
    ParameterSet::iterator i = __parametersSet.find(parameterName);
    if (i != __parametersSet.end()) {
      return (*i).second;
    } else {
      std::stringstream errorMsg;
      errorMsg << "'" << parameterName << "' not found!\n";
      errorMsg << "List of parameters:\n";
      for (i = __parametersSet.begin(); i !=  __parametersSet.end();
	   ++i) {
	errorMsg << '\t' << (*i).first << '\n';
      }
      errorMsg << std::ends;
      throw ErrorHandler(__FILE__,__LINE__,
			 errorMsg.str(),
			 ErrorHandler::normal);
    }
    return 0;
  }

  void reset();

  template <typename t>
  void set(const char * address, const t& d)
  {
    ReferenceCounting<Parameter> p = getParameter(address);
    (*p).set(d);
  }

  void get(const char* address, real_t& aDouble);
  void get(const char* address, int& anInteger);
  void get(const char* address, std::string& aString);

  explicit ParameterCenter()
  {
    ;
  }

  ~ParameterCenter()
  {
    ;
  }
};

#endif // PARAMETER_CENTER_HPP
