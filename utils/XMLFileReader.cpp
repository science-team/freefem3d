//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: XMLFileReader.cpp,v 1.1 2007/06/10 15:28:38 delpinux Exp $

#include <XMLFileReader.hpp>
#include <XMLTree.hpp>

#include <XMLLexer.hpp>

// The lexer pointer.
XMLLexer* xmllexer;
// The parser function.
int xmlparse();

#include <fstream>


ReferenceCounting<XMLTree>
XMLFileReader::
xmlTree()
{
  return __xmlTree;
}

void
XMLFileReader::
clear()
{
  __xmlTree = 0;
}

void
XMLFileReader::
read(const std::string& filename)
{
  std::ifstream fin(filename.c_str());

  if (not fin) {
    throw ErrorHandler(__FILE__,__LINE__,
		       "error: cannot open file '"+filename+"'",
		       ErrorHandler::normal);
  }

  __xmlTree = new XMLTree();

  {
    xmllexer = new XMLLexer(fin);
    xmlparse();
    delete xmllexer;
  }

  __xmlTree->check();
}


XMLFileReader::
XMLFileReader()
{
  ;
}

XMLFileReader::
~XMLFileReader()
{
  ;
}
