//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ZoneCopy.cpp,v 1.1.1.1 2003/02/17 16:32:50 delpinux Exp $

#include <cstring>
#include <ZoneCopy.hpp>


#define CONST_ZONE_COPY(T)			\
inline void ZoneCopy(T* const destination,	\
		     const T* const source,	\
		     size_t size)		\
{						\
  std::memcpy(static_cast<void*>(destination),	\
	      static_cast<const void*>(source),	\
	      size*sizeof(T));			\
}

#define ZONE_COPY(T)				\
inline void ZoneCopy(T* const destination,	\
		     T* const source,		\
		     size_t size)		\
{						\
  std::memcpy(static_cast<void*>(destination),	\
	      static_cast<void*>(source),	\
	      size*sizeof(T));			\
}

CONST_ZONE_COPY(real_t); ZONE_COPY(real_t);
CONST_ZONE_COPY(int);    ZONE_COPY(int);

