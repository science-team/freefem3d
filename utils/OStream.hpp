//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: OStream.hpp,v 1.2 2006/05/20 15:27:41 delpinux Exp $

#ifndef OSTREAM_HPP
#define OSTREAM_HPP

#include <iostream>
#include <Console.hpp>

class OStream
{
public:
  enum Type {
    standard,
    error,
    null,
    qt_standard,
    qt_error
  };

private:
  Type __type;

  template <typename T>
  void _writes(const T& t)
  {
    switch(__type) {
    case standard: {
      std::cout << t;
      break;
    }
    case error: {
      std::cerr << t;
      break;
    }
    case qt_standard: {
      Console::instance().writeStd(t);
      break;
    }
    case qt_error: {
      Console::instance().writeError(t);
      break;
    }
    case null: {
      break;
    }
    }
  }

public:
  void setType(const OStream::Type& type)
  {
    __type = type;
  }

  template <typename T>
  OStream& operator<<(const T& t)
  {
    this->_writes(t);
    return *this;
  }

  OStream(const OStream::Type& type)
    : __type(type)
  {
    ;
  }

  virtual ~OStream()
  {
    ;
  }
};

#endif // OSTREAM_HPP
