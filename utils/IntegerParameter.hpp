//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: IntegerParameter.hpp,v 1.3 2007/06/09 10:37:06 delpinux Exp $


#ifndef INTEGER_PARAMETER_HPP
#define INTEGER_PARAMETER_HPP

#include <Parameter.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/*!
  class IntegerParameter
  This is the base describes Integer Parameters.

  \author St�phane Del Pino.
*/
class IntegerParameter
  : public Parameter
{
private:
  const int __defaultIntValue;
  int __intValue;

  std::ostream& put (std::ostream& os) const
  {
    os << __intValue;
    return os;
  }

public:
  //! Does not add other identifiers.
  void get(IdentifierSet& I)
  {
    ;
  }

  void set(const real_t d)
  {
    __intValue = int(d);
  }

  void set(const int i)
  {
    __intValue = i;
  }

  void set(const char* c)
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot assignate the string '"+stringify(c)
		       +"' to an integer parameter",
		       ErrorHandler::normal);
  }

  void reset()
  {
    __intValue = __defaultIntValue;
  }

  const std::string typeName() const
  {
    return "int";
  }

  operator int&()
  {
    return __intValue;
  }

  operator int() const
  {
    return __intValue;
  }

  IntegerParameter(const int& i, const char* label)
    : Parameter(Parameter::Integer, label),
      __defaultIntValue(i),
      __intValue(i)
  {
    ;
  }

  IntegerParameter(const IntegerParameter& ip)
    : Parameter(ip),
      __defaultIntValue(ip.__defaultIntValue),
      __intValue(ip.__intValue)
  {
    ;
  }

  ~IntegerParameter()
  {
    ;
  }
};

#endif // INTEGER_PARAMETER_HPP
