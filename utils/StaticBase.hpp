//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: StaticBase.hpp,v 1.3 2004/12/06 01:11:22 delpinux Exp $

#ifndef STATIC_BASE_HPP
#define STATIC_BASE_HPP

/**
 * @file   StaticBase.hpp
 * @author St�phane Del Pino
 * @date   Wed Nov  6 00:50:42 2002
 * 
 * @brief This class is the template for classes embedding a static
 * member
 * 
 */

template <typename EmbeddingClass>
class StaticBase
{
protected:
  static EmbeddingClass* __pInstance; /**< The static variable */

public:
  /** 
   * Access to auto instanciated static;
   * 
   * 
   * @return *__pInstance
   */
  static EmbeddingClass& instance()
  {
    return *__pInstance;
  }

  /** 
   * Creates __pInstance in Embedding class.
   * 
   */
  static void create()
  {
    __pInstance = new EmbeddingClass();
  }

  /** 
   * Destroyes __autoInstanciated in Embedding class.
   * 
   */
  static void destroy()
  {
    delete __pInstance;
  }

  /** 
   * Constructor
   * 
   */
  StaticBase()
  {
    ;
  }

  /**
   * Destructor
   * 
   */
  virtual ~StaticBase()
  {
    ;
  }
};

template <typename EmbeddingClass> EmbeddingClass* StaticBase<EmbeddingClass>::__pInstance;

#endif // STATIC_BASE_HPP
