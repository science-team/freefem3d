//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: CommandLineOption.hpp,v 1.2 2004/12/31 14:00:47 delpinux Exp $

#ifndef COMMAND_LINE_OPTION_HPP
#define COMMAND_LINE_OPTION_HPP

#include <StreamCenter.hpp>

#include <ErrorHandler.hpp>

#include <string>
#include <vector>

/**
 * @file   CommandLineOption.hpp
 * @author Stephane Del Pino
 * @date   Mon Dec 27 12:24:56 2004
 * 
 * @brief  Describes command line options
 * 
 */
class CommandLineOption
{
public:
  enum ArgumentsType
  {
    integer,
    real,
    string,
    end
  };

private:
  const int __identifier;	/**< Option id */

  const std::string __longName;	/**< long name (--help for instance) */
  const std::string __shortName; /**< short name (-h for instance)    */
  const std::string __description; /**< Description of the option */
  std::vector<ArgumentsType> __arguments; /**< Arguments type */

public:

  /**
   * Returns the short name of an option
   * 
   * @return __shortName
   */
  const std::string& shortName() const
  {
    return __shortName;
  }

  /** 
   * Returns the long name of an option
   * 
   * @return __longName
   */
  const std::string& longName() const
  {
    return __longName;
  }

  /** 
   * Returns the description of an option
   * 
   * @return __description
   */
  const std::string& description() const
  {
    return __description;
  }

  const std::string arguments() const
  {
    std::string argument;
    for (size_t i=0; i<__arguments.size(); ++i) {
      if (i!=0) argument += ',';
      switch (__arguments[i]) {
      case CommandLineOption::integer: {
	argument += "int";
	break;
      }
      case CommandLineOption::real: {
	argument += "real";
	break;
      }
      case CommandLineOption::string: {
	argument += "str";
	break;
      }
      case CommandLineOption::end:
      default: {
	throw ErrorHandler(__FILE__,__LINE__,
			   "unexpected command line option",
			   ErrorHandler::unexpected);
      }
      }
    }
    return argument;
  }

  /** 
   * Returns option idetifier
   * 
   * @return __identifier
   */
  const int& identifier() const
  {
    return __identifier;
  }

  /** 
   * Constructor
   * 
   */
  CommandLineOption(const int& id,
		    const std::string& longName,
		    const std::string& shortName,
		    const std::string& description,
		    ArgumentsType arguments[] = 0)
    : __identifier(id),
      __longName(longName),
      __shortName(shortName),
      __description(description)
  {
    if (arguments != 0) {
      for (int i=0; arguments[i]!=CommandLineOption::end; ++i) {
	__arguments.push_back(arguments[i]);
      }
    }
  }
  /** 
   * Destructor
   * 
   */
  ~CommandLineOption()
  {
    ;
  }
};

#endif // COMMAND_LINE_OPTION_HPP
