//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: CommandLineParser.cpp,v 1.15 2007/05/20 23:02:46 delpinux Exp $

#include <CommandLineParser.hpp>
#include <CommandLineOption.hpp>

#include <RunningOptions.hpp>

#include <BaseName.hpp>

#include <config.h>

#include <iomanip>
#include <cstdlib>
#include <cstdio>

void CommandLineParser::__treatOption(const int& optionNumber,
				      int& argumentNumber)
{
  CommandLineOption& currentOption = * __options[optionNumber];
  switch (currentOption.identifier()) {
  case CommandLineParser::help: {
    this->showHelp();
    std::exit(0);
    break;
  }
  case CommandLineParser::moreHelp: {
    this->showMoreHelp();
    std::exit(0);
    break;
  }
  case CommandLineParser::version: {
    this->showVersion();
    std::exit(0);
    break;
  }
  case CommandLineParser::noWindow: {
    RunningOptions::instance().setGUI(false);
    break;
  }
  case CommandLineParser::verbosity: {
    if (argumentNumber+1>=__numberOfArguments) {
      this->showHelp();
      std::cerr << "\nerror: missing verbosity argument\n";
      std::exit(1);  
    }
    argumentNumber++;
    if (sscanf(__arguments[argumentNumber], "%d", &__verbosity) <= 0) {
      this->showHelp();
      std::cerr << "\nerror: expecting an integer as verbosity argument\n";
      std::exit(1);  
    }
    break;
  }
  default: {
    std::cerr << __FILE__ << ':' << __LINE__ << ": Not implemented\n";
    std::exit(1);
  }
  }
}

void CommandLineParser::showHelp() const
{
  this->showVersion();
  this->showUsage();
}

void CommandLineParser::showBugReport() const
{
  std::cout
    << "\nBUG REPORT: Please send bug reports to:\n"
    "  ff3d-dev@nongnu.org or freefem@ann.jussieu.fr\n"
    "or better, use the Bug Tracking System:\n"
    "  http://savannah.nongnu.org/bugs/?group=ff3d\n";
}

void CommandLineParser::showLicence() const
{
  std::cout <<
    "\nLICENCE:\n"
    "This program is free software; you can redistribute it and/or modify it\n"
    "under the terms of the GNU General Public License as published by the\n"
    "Free Software Foundation; either version 2, or (at your option) any later\n"
    "version."
    "\n"
    "This program is distributed in the hope that it will be useful, but\n"
    "WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n"
    "General Public License for more details.\n"
    "\n"
    "You should have received a copy of the GNU General Public License\n"
    "along with this program; if not, write to the Free Software\n"
    "Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA\n"
    "02111-1307, USA.\n"
    "\n"
    "See the COPYING file in ff3d's root directory, the exact\n"
    "terms of this licence can also be consult online at GNU project\n"
    "official web site: http://www.gnu.org/copyleft/gpl.html\n";
}

void CommandLineParser::showDescription() const
{
  std::cout <<
    "\nDESCRIPTION:\n"
    "FreeFEM3D (aka ff3d) is a 3D solver of partial differential\n"
    "equations (PDE).  It is a member of the familly of the freefem\n"
    "programs (see http://www.freefem.org).\n"
    "\n"
    "ff3d, as well as its cousins, is a PDE solver driven by a\n"
    "user-friendly language. It solves many kind of problems such as\n"
    "elasticity, fluids (Stokes and Navier-Stokes) and a lot more. The\n"
    "user has to enter the equation associated with the problem, giving\n"
    "either the PDE in strong formulation or weak (variational)\n"
    "formulation.\n"
    "\n"
    "ff3d does not use standard Finite Elements method but a Fictitious\n"
    "Domain approach. The geometry is described using Constructive Solid\n"
    "Geometry (CSG). This description is done using the POV-Ray language\n"
    "but others such as VRML could be added.\n"
    "\n"
    "The processing of the results is left to the user.  One can use\n"
    "various graphic tools: OpenDX being the most powerful free software\n"
    "on the market nowadays. The implementation of a VTK base\n"
    "visualization module is underway.\n"
    "\n"
    "The goal of ff3d is to provide a good teaching tool and a research\n"
    "toolbox (the code is written in C++ and its design is such that new\n"
    "methods can be easily implemented).\n";
}

void CommandLineParser::showCredits() const
{
  std::cout <<
    "\nCREDITS:\n"
    "Project leader\n"
    "  Olivier Pironneau     <Olivier.Pironneau@math.jussieu.fr>\n"
    "Developers\n"
    "  St�phane Del Pino     <Stephane.DelPino@math.jussieu.fr>\n"
    "  C�cile Dobrzynski     <dobrzyns@ann.jussieu.fr>\n"
    "  Pascal Hav�           <Pascal.Have@math.jussieu.fr>\n"
    "Debian Packager\n"
    "  Christophe Prud'homme <prudhomm@debian.org>\n";
}

void CommandLineParser::showMoreHelp() const
{
  this->showHelp();
  this->showDescription();
  this->showLicence();
  this->showCredits();
  this->showBugReport();
}

void CommandLineParser::showUsage() const
{
  std::cout << "USAGE:  " << baseName(this->__arguments[0])
	    << " [ -<flag> [<val>] | --<name>[ <val>] ]... [filename]\n";
  size_t maxLongLenght = 0;
  size_t maxShortLenght = 0;
  size_t maxArgLenght = 0;
  for (size_t i=0; i<__options.size(); ++i) {
    maxLongLenght = std::max(maxLongLenght, __options[i]->longName().size());
    maxShortLenght = std::max(maxShortLenght, __options[i]->shortName().size());
    maxArgLenght = std::max(maxArgLenght, __options[i]->arguments().size());
  }

  std::cout << std::setw(maxShortLenght+2)<< "Flag" << ','
	    << std::setw(maxLongLenght) << "Name"  <<' '
	    << std::setw(maxArgLenght+1) << "Args"
	    << " Description\n";

  for (size_t i=0; i<__options.size(); ++i) {
    std::cout << std::setw(maxShortLenght+2)
	      << __options[i]->shortName() << ','
	      << std::setw(maxLongLenght)
	      << __options[i]->longName()  <<' '
	      << std::setw(maxArgLenght+1)  << __options[i]->arguments()
	      << " " << __options[i]->description() << '\n';
  }
  std::cout << "Options are specified by doubled hyphens and their name\n"
	    << "or by a single hyphen and the flag character.\n";
}

void CommandLineParser::showVersion() const
{
  std::cout << "ff3d - a General PDE solver - Ver. "
	    << VERSION << '\n';
}

void CommandLineParser::__parse()
{
  for (int i=1; i<__numberOfArguments; ++i) {
    std::string opt = __arguments[i];
    if (opt[0] == '-') {
      if (opt.size()>1) {
	if (opt[1] == '-') {
	  OptionsLexerType::const_iterator iOption = __longOptionsLexer.find(opt);
	  if (iOption == __longOptionsLexer.end()) {
	    this->showHelp();
	    std::cerr << "\nerror: unknown long argument '" << opt << "'\n";
	    std::exit(1);
	    break;
	  } else {
	    this->__treatOption(iOption->second, i);
	  }
	} else {
	  OptionsLexerType::const_iterator iOption = __shortOptionsLexer.find(opt);
	  if (iOption == __shortOptionsLexer.end()) {
	    this->showHelp();
	    std::cerr << "\nerror: unknown short argument '" << opt << "'\n";
	    std::exit(1);
	    break;
	  } else {
	    this->__treatOption(iOption->second, i);
	  }
	}
      } else {
	this->showHelp();
	std::cerr << "\nerror: '-' is not a valid option\n";
	std::exit(1);
	break;
      }
    } else { // reading filename
      if (__filename.size() != 0) {
	this->showHelp();
	std::cerr << "\nerror: reading '" << opt
		  << "' filename '" <<__filename
		  << "' was already specified\n";
	std::exit(1);
      }
      __filename = opt;
    }
  }
}

CommandLineParser::
CommandLineParser(int argc,
		  char** argv)
  : __numberOfArguments(argc),
    __arguments(argv),
    __verbosity(3)
{
  // options initialization.
  __options.push_back(new CommandLineOption(CommandLineParser::version,
					    "--version", "-v",
					    "Display version information and exit"));
  
  __options.push_back(new CommandLineOption (CommandLineParser::help,
					     "--help", "-h",
					     "Display usage information and exit"));

  __options.push_back(new CommandLineOption (CommandLineParser::moreHelp,
					     "--more-help", "-H",
					     "Display more usage information and exit"));

  __options.push_back(new CommandLineOption (CommandLineParser::noWindow,
					     "--no-window", "-nw",
					     "Specifies that no GUI is to be used"));

  CommandLineOption::ArgumentsType verbosityArguments[]
    = { CommandLineOption::integer,
	CommandLineOption::end };
  __options.push_back(new CommandLineOption(CommandLineParser::verbosity,
					    "--verbosity", "-V",
					    "Set level of verbosity",
					    verbosityArguments));

  for (size_t i=0; i < __options.size(); ++i) {
    const CommandLineOption& c = *(__options[i]);
    const std::string& shortName = c.shortName();
    const std::string& longName  = c.longName();

    ASSERT(__shortOptionsLexer.find(shortName) == __shortOptionsLexer.end());
    ASSERT(__longOptionsLexer.find(longName) == __longOptionsLexer.end());

    __shortOptionsLexer[shortName] = i;
    __longOptionsLexer[longName] = i;
  }

  // Proceeding now to parsing
  this->__parse();

  if (not(RunningOptions::instance().useGUI())) {
    if (not(this->hasFilename())) {
      this->showHelp();
      std::cerr << "\nerror: when no GUI is used, filename is mandatory\n";
      std::cerr << "    Check your DISPLAY variable or remove '-nw' option\n";
      std::exit(1);
    }
  }
}

CommandLineParser::
~CommandLineParser()
{
  for (OptionsList::iterator i = __options.begin();
       i != __options.end(); ++i) {
    delete *i;
  }
}
