//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: ThreadStaticBase.hpp,v 1.3 2007/05/20 23:02:46 delpinux Exp $

#ifndef THREAD_STATIC_BASE_HPP
#define THREAD_STATIC_BASE_HPP

#include <config.h>
#include <Assert.hpp>

#ifdef HAVE_PTHREAD
#include <pthread.h>

#include <map>

/**
 * @file   ThreadStaticBase.hpp
 * @author Stephane Del Pino
 * @date   Sun Dec  5 20:07:32 2004
 * 
 * @brief By thread static objects base class. This is the equivalent
 * of StaticBase in the multi-threaded context
 * 
 */
template <typename EmbeddingClass>
class ThreadStaticBase
{
protected:
  static std::map<pthread_t, EmbeddingClass*> __pInstances;

public:
  /** 
   * Access to auto instanciated static;
   * 
   * @return *(__pInstances[])
   */
  static EmbeddingClass& instance()
  {
    const pthread_t threadId = pthread_self();
    ASSERT(__pInstances.find(threadId) != __pInstances.end());
    return *(__pInstances[threadId]);
  }

  /** 
   * Creates __pInstance in Embedding class.
   * 
   */
  static void create()
  {
    const pthread_t threadId = pthread_self();
    ASSERT(__pInstances.find(threadId) == __pInstances.end());
    __pInstances[threadId] = new EmbeddingClass();
  }

  /** 
   * Destroyes __autoInstanciated in Embedding class.
   * 
   */
  static void destroy()
  {
    const pthread_t threadId = pthread_self();
    ASSERT(__pInstances.find(threadId) != __pInstances.end());
    delete __pInstances[threadId];
    __pInstances.erase(threadId);
  }

  /** 
   * Constructor
   * 
   */
  ThreadStaticBase()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ThreadStaticBase()
  {
    ;
  }
};

template <typename EmbeddingClass> std::map<pthread_t, EmbeddingClass*>
ThreadStaticBase<EmbeddingClass>::__pInstances;

#else  // HAVE_PTHREAD

#include <StaticBase.hpp>

// ThreadStaticBase is the same that StaticBase when no thread version is built

template <typename T> 
struct ThreadStaticBase
  : public StaticBase<T>
{
  /** 
   * Constructor
   * 
   */
  ThreadStaticBase()
  {
    ;
  }

  /** 
   * Destructor
   * 
   */
  ~ThreadStaticBase()
  {
    ;
  }  
};

#endif // HAVE_PTHREAD

#endif // THREAD_STATIC_BASE_HPP
