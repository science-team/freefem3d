//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2007 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: XMLFileReader.hpp,v 1.1 2007/06/10 15:28:38 delpinux Exp $

#ifndef XML_FILE_READER_HPP
#define XML_FILE_READER_HPP

class XMLTree;

#include <ReferenceCounting.hpp>
#include <string>

/**
 * @file   XMLFileReader.hpp
 * @author Stephane Del Pino
 * @date   Sun Jun 10 17:08:33 2007
 * 
 * @brief  general XML file reader
 * 
 */
class XMLFileReader
  : public ThreadStaticBase<XMLFileReader>
{
private:
  ReferenceCounting<XMLTree>
  __xmlTree;			/**< built xml tree */

public:
  /** 
   * Access to the xml tree
   * 
   * @return __xmlTree
   */
  ReferenceCounting<XMLTree>
  xmlTree();

  /** 
   * Empties the structures by setting __xmlTree to 0
   * 
   */
  void clear();

  /** 
   * Reads the xml tree from a file
   * 
   * @param filename the input filename
   */
  void read(const std::string& filename);

  /** 
   * Constructor
   *
   */
  XMLFileReader();

  /** 
   * Destructor
   * 
   */
  ~XMLFileReader();
};

#endif // XML_FILE_READER_HPP
