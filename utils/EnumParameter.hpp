//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: EnumParameter.hpp,v 1.2 2004/12/31 14:00:47 delpinux Exp $


#ifndef ENUM_PARAMETER_HPP
#define ENUM_PARAMETER_HPP

#include <Parameter.hpp>
#include <LabeledEnum.hpp>

#include <IdentifierSet.hpp>

#include <Stringify.hpp>
#include <ErrorHandler.hpp>

/*!
  \class EnumParameter
  This is the base describes Enum Parameters.

  \author St�phane Del Pino.
*/
template <typename EnumType>
class EnumParameter
  : public Parameter
{
private:
  LabeledEnum<EnumType> __switch;

  void reset()
  {
    __enumValue = __defaultEnumValue;
  }

  const EnumType __defaultEnumValue;
  EnumType __enumValue;

  std::ostream& put (std::ostream& os) const
  {
    os << __enumValue;
    return os;
  }

public:
  //! Possible enums are added to the language.
  void get(IdentifierSet& I)
  {
    __switch.get(I);
  }

  void addSwitch(const char* c, const EnumType t)
  {
    __switch.add(c,t);
  }

  void set(const real_t d)
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot assignate the real value '"+stringify(d)
		       +"' to an enum parameter",
		       ErrorHandler::normal);
  }

  void set(const int i)
  {
    throw ErrorHandler(__FILE__,__LINE__,
		       "cannot assignate the integer value '"+stringify(i)
		       +"' to an enum parameter",
		       ErrorHandler::normal);
  }

  void set(const char* c)
  {
    __enumValue = __switch(c);
  }

  operator EnumType&()
  {
    return __enumValue;
  }

  operator const EnumType&() const
  {
    return __enumValue;
  }

  const std::string typeName() const
  {
    return "enum";
  }

  EnumParameter(const EnumParameter& sp)
    : Parameter(sp),
      __switch(sp.__switch),
      __defaultEnumValue(sp.__defaultEnumValue),
      __enumValue(sp.__enumValue)
  {
    ;
  }

  EnumParameter(const EnumType defaultEnum , const char* label)
    : Parameter(Parameter::Enum, label),
      __defaultEnumValue(defaultEnum),
      __enumValue(defaultEnum)
  {
    ;
  }

  ~EnumParameter()
  {
    ;
  }
};

#endif // ENUM_PARAMETER_HPP
