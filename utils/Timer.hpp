//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Timer.hpp,v 1.8 2007/06/09 10:37:06 delpinux Exp $


#ifndef TIMER_HPP
#define TIMER_HPP

#ifndef __MINGW32__

#include <ctime>
#include <unistd.h>    // std::sysconf()
#include <sys/times.h> // std::times()
#endif // __MINGW32__

#include <iostream>

#include <Types.hpp>


/*!
  \class timer

  The role of objects of this class is to measure the execution time between
  instructions.

  \todo check that the time is computable (ie start() and end() have been
  called

  \author St�phane Del Pino
 */

class Timer
{
private:
#ifndef __MINGW32__
  real_t __startTimeCPU;
  real_t __stopTimeCPU;

  time_t __startTime;
  time_t __stopTime;

  bool __started;
  bool __stopped;

  struct tms tp;

  real_t __cpuTime()
  {
    static const long _CLK_TCK = sysconf(_SC_CLK_TCK);
    times(&tp);
    return static_cast<real_t>(tp.tms_utime)/_CLK_TCK;
  }
#endif // __MINGW32__

public:
  void start()
  {
#ifndef __MINGW32__
    ASSERT(!__started);
    __started=true;
    __stopped=false;
    __startTime = std::time(NULL);
    __startTimeCPU = __cpuTime();
#endif // __MINGW32__
  }

  void stop()
  {
#ifndef __MINGW32__
    ASSERT(__started);
    __started=false;
    __stopped=true;

    __stopTime = std::time(NULL);
    __stopTimeCPU = __cpuTime();
#endif // __MINGW32__
  }

  friend std::ostream& operator << (std::ostream& os, const Timer& T)
  {
#ifndef __MINGW32__
    if (!T.__stopped) {
      os << "Timer not stopped\n";
      return os;
    }
    os << "time: " << std::difftime(T.__stopTime, T.__startTime) << 's'
       << " - CPU time: " << T.__stopTimeCPU - T.__startTimeCPU << 's';
#else // __MINGW32__
    os << "time: not implemented under windows";
#endif // __MINGW32__
    return os;
  }

  Timer()
#ifndef __MINGW32__
    : __startTimeCPU(0),
      __stopTimeCPU(0),
      __startTime(0),
      __stopTime(0),
      __started(false),
      __stopped(false)
#endif // __MINGW32__
  {
    ;
  }
};

#endif // TIMER_HPP
