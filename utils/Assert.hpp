//  This file is part of ff3d - http://www.freefem.org/ff3d
//  Copyright (C) 2001, 2002, 2003 St�phane Del Pino

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2, or (at your option)
//  any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software Foundation,
//  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

//  $Id: Assert.hpp,v 1.2 2007/06/17 20:44:59 delpinux Exp $

#ifndef ASSERT_HPP
#define ASSERT_HPP

#ifndef NDEBUG

#include <ErrorHandler.hpp>
#include <Stringify.hpp>

#include <unistd.h>
#include <iostream>

#define ASSERT(expression)						\
  if (not (expression)) {						\
    std::cerr << "##############################################\n";	\
    std::cerr << "assertion failed: '"+stringify(#expression)+"'\n";	\
    std::cerr << "to attach gdb to this process do\n";			\
    std::cerr << "\tgdb -pid " << getpid() << '\n';			\
    std::cerr << "else press control-c\n";				\
    std::cerr << "##############################################\n";	\
    pause();								\
  }


#else // NDEBUG

#define ASSERT(expression)

#endif // NDEBUG

#endif // ASSERT_HPP
